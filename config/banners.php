<?php

return array (
  0 => 
  array (
    'img' => '/myuploads/slider_main/DSC_4575_2.jpg',
    'link' => 'http://florescence.com.ua/catalog/bukety',
    'text' => 
    array (
      'en' => 'Author\'s bouquets',
      'ru' => 'Авторские букеты',
      'ua' => 'Авторські букети',
    ),
  ),
  1 => 
  array (
    'img' => '/myuploads/slider_main/DSC_7118_2.jpg',
    'link' => 'http://florescence.com.ua/vesilna-florystyka',
    'text' => 
    array (
      'en' => 'Wedding decoration',
      'ru' => 'Свадебное оформление',
      'ua' => 'Весільне оформлення',
    ),
  ),
  2 => 
  array (
    'img' => '/myuploads/slider_main/DSC_4475_61.jpg',
    'link' => 'http://florescence.com.ua/catalog/kvity-v-korobkakh',
    'text' => 
    array (
      'en' => 'Flowers in boxes',
      'ru' => 'Цветы в коробках',
      'ua' => 'Квіти в коробках',
    ),
  ),
);