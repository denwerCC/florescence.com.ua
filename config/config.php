<?php

return array (
  'ua' => 
  array (
    'adminemail' => 'khris01.01.12@gmail.com,Sashka_zagrebina@mail.ru,olexandrazagrebina@gmail.com',
    'sitename' => 'Доставка букетів та подарунків у Львові ',
    'footertext' => '© Всі права захищено 2017 р',
    'address' => 'м. Львів',
    'email' => 'khris01.01.12@gmail.com',
    'phone' => '<span>+38 (099) 082 51 52</span> <span>+38 (097) 994 94 81</span>',
    'google_analytic_code' => 'UA-99126475-1',
  ),
  'ru' => 
  array (
    'adminemail' => 'khris01.01.12@gmail.com,Sashka_zagrebina@mail.ru,olexandrazagrebina@gmail.com',
    'sitename' => 'Доставка букетов от флориста во Львове',
    'footertext' => '© Все права защищены 2017 р',
    'address' => 'Львов, ул. Героев УПА, 15',
    'email' => 'khris01.01.12@gmail.com',
    'phone' => '<span>+38 (099) 082 51 52</span> <span>+38 (097) 994 94 81</span>',
  ),
  'en' => 
  array (
    'adminemail' => 'khris01.01.12@gmail.com,Sashka_zagrebina@mail.ru,olexandrazagrebina@gmail.com',
    'sitename' => 'Fresh bouquets delivery in Lviv',
    'footertext' => 'All rights reserved 2017',
    'address' => 'Heroiv UPA street, 15',
    'email' => 'khris01.01.12@gmail.com',
    'phone' => '<span>+38 (099) 082 51 52</span> <span>+38 (097) 994 94 81</span>',
  ),
);