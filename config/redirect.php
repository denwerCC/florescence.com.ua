<?php

return array (
  'our-works' => 'nashi-roboty',
  'subscribe/size L/size-l' => 'pidpyska-na-kvity/size-l',
  'subscribe/size M/m' => 'pidpyska-na-kvity/size-m',
  'subscribe/size S/s' => 'pidpyska-na-kvity/size-s',
  'subscribe/size S/size-s' => 'pidpyska-na-kvity/size-s',
  'wedding-design' => 'vesilna-florystyka',
);