<?php

return array (
  'header' => 
  array (
    'en' => 'Terms and Cost',
    'ru' => 'Условия и стоимость',
    'ua' => 'Умови та вартість',
  ),
  'text' => 
  array (
    'en' => '<p>Delivery in the city during the day is free of charge. Delivery in the city at night - 300 UAH.</p><p>Delivery for the city - 6 UAH / km in one direction.</p><p>Contact the manager to discuss shipping conditions.</p>',
    'ru' => '<p>Доставка по городу днем - бесплатно. Доставка по городу ночью - 300 грн.</p><p>Доставка за город - 6 грн / км в одну сторону.</p><p>Для обсуждения условий доставки обратитесь к менеджеру.</p>',
    'ua' => '<p>Доставка по місту вдень - безкоштовна. Доставка по місту вночі - 300 грн.</p><p>Доставка за місто - 6 грн/км в одну сторону.</p><p>Для обговорення умов доставки зверніться до менеджера.</p>',
  ),
);