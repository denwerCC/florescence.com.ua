<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
	'vkontakte' => [
		'client_id' => '6035611',
		'client_secret' => '8XLG9LeL6g8dn3mFEaHY',
		'redirect' => 'http://florescence.com.ua/auth/callback/vkontakte',
	],

	'facebook' => [
		'client_id' => '319436111894492',
		'client_secret' => '47285b303803b29789ab924b802535ae',
		'redirect' => 'https://florescence.com.ua/auth/callback/facebook',
	],
	'instagram' => [
		'client_id' => '5b56991b10864237921d9551109b18c0',
		'client_secret' => '55c48ee06b004deb80e9dc0299e8a742',
		'redirect' => 'https://florescence.com.ua/auth/callback/instagram'
	],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
