<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
*/
// faker generator
Route::get('faker', 'ItemsController@faker')->name('admin.faker');
// home
Route::get('/', 'PagesController@index')->name('admin.home');
// backup
Route::get('backup', 'BackupController@index')->name('admin.backup');
Route::get('backup/create', 'BackupController@create')->name('admin.backup.create');
Route::get('backup/restore/{file}', 'BackupController@restore')->name('admin.backup.restore');
Route::get('backup/delete/{file}', 'BackupController@delete')->name('admin.backup.delete');
// gallery
Route::resource('gallery', 'GalleryController', ['as'=> 'admin']);
Route::post('gallery/upload', 'GalleryController@upload')->name('admin.gallery.upload');
Route::get('gallery/delimage/{id}', 'GalleryController@delimage')->name('admin.gallery.delimage');
Route::post('gallery/adddata/{id}', 'GalleryController@addData')->name('admin.gallery.adddata');
Route::post('gallery/sort', 'GalleryController@sort')->name('admin.gallery.sort');
// pages
Route::resource('pages', 'PagesController', ['as'=> 'admin']);
Route::get('pages/category/{id}', 'PagesController@index')->name('admin.pages.category');
Route::get('pages/{id}/delete', 'PagesController@destroy')->name('admin.pages.delete');
Route::get('pages/{id}/public', 'PagesController@publicate')->name('admin.pages.publicate');
Route::get('pages/{id}/viewedit', 'PagesController@editView')->name('admin.view.edit');
Route::get('pages/{view}/viewclear/{lang}', 'PagesController@clearView')->name('admin.view.clear');
Route::post('pages/{view}/save', 'PagesController@saveView')->name('admin.view.save');
Route::post('pages/uploadimg', 'PagesController@uploadImage')->name('admin.pages.uploadimg');
Route::get('pages/{id}/deleteimg/{image}', 'PagesController@deleteImg')->name('admin.pages.deleteimg');
Route::post('pages/sort', 'PagesController@sort')->name('admin.pages.sort');
// menus
Route::resource('menus', 'MenusController', ['as'=> 'admin']);
Route::get('menu/delete/{id}', 'MenusController@destroy')->name('admin.menus.delete');
Route::post('menus/sort', 'MenusController@sort')->name('admin.menus.sort');
// categories
Route::resource('categories', 'CategoriesController', ['as'=> 'admin']);
Route::get('category/delete/{id}', 'CategoriesController@destroy')->name('admin.categories.delete');
Route::post('categories/sort', 'CategoriesController@sort')->name('admin.categories.sort');
// rubrics
Route::resource('rubrics', 'RubricsController', ['as'=> 'admin']);
Route::get('rubrics/delete/{id}', 'RubricsController@destroy')->name('admin.rubrics.delete');
Route::post('rubrics/sort', 'RubricsController@sort')->name('admin.rubrics.sort');
// items
Route::post('items/changeprice/{category_id?}', 'ItemsController@changePrice')->name('admin.items.changeprice');
Route::resource('items', 'ItemsController', ['as'=> 'admin']);
Route::get('items/category/{id}', 'ItemsController@index')->name('admin.items.category');
Route::get('items/delete/{id}', 'ItemsController@destroy')->name('admin.items.delete');
Route::get('items/clone/{id}', 'ItemsController@cloneItem')->name('admin.items.clone');
Route::post('items/sort', 'ItemsController@sort')->name('admin.items.sort');
Route::post('items/uploadimg', 'ItemsController@uploadImage')->name('admin.items.uploadimg');
Route::get('items/{id}/deleteimg/{image}', 'ItemsController@deleteImg')->name('admin.items.deleteimg');
Route::post('items/editprice', 'ItemsController@editPrice')->name('admin.items.price');
// filters
Route::resource('filters', 'FiltersController', ['as'=> 'admin']);
Route::get('filters/delete/{id}', 'FiltersController@destroy')->name('admin.filters.delete');
Route::post('filters/sort', 'FiltersController@sort')->name('admin.filters.sort');
// config
Route::resource('config', 'ConfigController', ['as'=> 'admin']);
Route::get('banners', 'BannersController@index')->name('admin.banners');
Route::post('banners/update', 'BannersController@update')->name('admin.banners.update');
Route::get('banners_subscribe', 'BannersSubscribeController@index')->name('admin.banners_subscribe');
Route::post('banners_subscribe/update', 'BannersSubscribeController@update')->name('admin.banners_subscribe.update');
Route::get('order_form', 'OrderFormController@index')->name('admin.order_form');
Route::post('order_form/update', 'OrderFormController@update')->name('admin.order_form.update');
Route::get('calendar_settings', 'CalendarSettimgsController@index')->name('admin.calendar_settings');
Route::post('calendar_settings/update', 'CalendarSettimgsController@update')->name('admin.calendar_settings.update');
// orders
Route::resource('orders', 'OrdersController', ['as'=> 'admin']);
Route::get('orders/status/{status}', 'OrdersController@index')->name('admin.orders.status');
Route::get('orders/delete/{id}', 'OrdersController@destroy')->name('admin.orders.delete');
Route::get('orders/search/{command}/{query}', 'OrdersController@search')->name('admin.orders.search');
// comments
Route::resource('comments', 'CommentsController', ['as'=> 'admin']);
Route::get('comments/delete/{id}', 'CommentsController@destroy')->name('admin.comments.delete');
// coupons
Route::resource('coupons', 'CouponsController', ['as'=> 'admin']);
Route::get('coupons/delete/{id}', 'CouponsController@destroy')->name('admin.coupons.delete');
// subscribers
Route::resource('subscribers', 'SubscribeController', ['as'=> 'admin']);
Route::get('subscribers/delete/{id}', 'SubscribeController@destroy')->name('admin.subscribers.delete');
// FLOWERS SUBSCRIPTION
Route::resource('fSubscription', 'FSubscribersController', ['as'=> 'admin']);
Route::get('fSubscription/delete/{id}', 'FSubscribersController@destroy', ['as' => 'admin'])->name('f.delete');
//MASTER SHOW
Route::resource('master-show', 'MasterShowController', ['as' => 'admin']);
Route::get('master-show/delete/{id}', 'MasterShowController@destroy', ['as' => 'admin'])->name('ms.delete');
//certificates
Route::resource('certificates', 'CertificatesController', ['as' => 'admin']);
Route::get('certificates/delete/{id}', 'CertificatesController@destroy', ['as' => 'admin'])->name('certificates.destroy');
//uploaded photos
Route::resource('photos', 'PhotosController', ['as'=> 'admin']);
Route::get('photos/delete/{id}', 'PhotosController@destroy', ['as' => 'admin'])->name('photos.destroy');
// translation
Route::resource('translation', 'TranslationController', ['as'=> 'admin']);
// users
Route::resource('users', 'UsersController', ['as'=> 'admin']);
Route::get('users/delete/{id}', 'UsersController@delete');
Route::get('users/activate/{id}', 'UsersController@activate');


Route::any('contacts', 'ContactsController@index', ['as'=> 'contacts']);
Route::any('contacts/data', 'ContactsController@anyData', ['as'=> 'contacts.data'])->name('contacts.data');
Route::get('contacts/delete/{id}', 'ContactsController@delete', ['as'=> 'contacts.delete'])->name('contacts.delete');

Route::any('calendar', 'CalendarController@index', ['as'=> 'calendar']);
Route::any('calendar/data', 'CalendarController@anyData', ['as'=> 'calendar.data'])->name('calendar.data');


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
//    $exitCode = Artisan::call('vendor:publish');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('config:clear');
//    echo $exitCode;
    // return what you want
});