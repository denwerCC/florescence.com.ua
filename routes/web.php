<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
*/

Route::get('test', function (Request $request){
	dd(\App\Http\Controllers\CartController::recalCartIfChangeCurrency());

//	$json = file_get_contents('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');
	//$obj = json_decode($json);
//    dump(currency());

//	dd($obj);
//	dump(\App\Http\Controllers\CurrencyController::changeCurrency(3220,'uah', 'eur'));
//	dump(\App\Http\Controllers\CurrencyController::getCurrencyIndex());
    //return App::environment();
    //dd(App::getLocale());
//    dd($request);
//    dd(config('app.locale'));
//    dd(App::getLocale());
//    dump(getCurrencyMenu());

});

Route::get('/currency/{currency}', 'CurrencyController@update')->name('currency.change');
//locales
Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
// login/register/password reminder
    Route::auth();
//Auth::routes(); - alias
    // redirects

    foreach (config('redirect') as $link => $redirect) {
        // Laravel >5.5
        // Route::redirect(parse_url($link)['path'], $redirect, 301);
        Route::get(parse_url($link)['path'], function () use ($redirect) {
           return redirect($redirect);
        });
    }

// admin login
    Route::get('admincab/login', 'Admin\Auth\LoginController@showLoginForm');
    Route::get('admincab/logout', 'Admin\Auth\LoginController@logout');
// login
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
// account
    Route::group(['prefix' => 'account', 'middleware' => 'auth'], function () {
        Route::get('/', 'AccountController@index')->name('account');
        Route::get('profile', 'AccountController@profile')->name('profile');
        Route::get('wishlist', 'AccountController@wishlist')->name('wishlist');
        Route::get('order/{id}', 'AccountController@order')->name('account.order');
    });
// user
    Route::resource('user', 'UserController');
// payment
    Route::group(['prefix' => 'payment'], function () {
// payment LiqPay
        Route::get('pay/liqpay', 'LiqPayController@index')->name('pay.liqpay');
        Route::get('liqpay/{id}', 'LiqPayController@userResponse')->name('liqpay.user');
        Route::post('liqpay/{id}', 'LiqPayController@serverResponse')->name('liqpay.server');
// payment PayPal
        Route::get('paypal', 'PayPalController@index')->name('pay.paypal');
        Route::get('paypal/{id}', 'PayPalController@userResponse')->name('paypal.user');
        Route::post('paypal/{id}', 'PayPalController@serverResponse')->name('paypal.server');
        Route::get('paypal/cancel/{id}', 'PayPalController@error')->name('paypal.error');
    });
//upload-rom
    Route::get('/upload-form', 'PagesController@uploadForm')->name('upload.form');
//callback
    Route::get('/callback', 'PagesController@callbackForm')->name('callback');
    Route::get('/callback-master-show', 'PagesController@callbackMSForm')->name('callback.ms');
    Route::post('/master-show/store', 'MasterShowController@store')->name('msubs.store');
    Route::get('/certificate-callback/{price_id}', 'PagesController@cetrificate')->name('sertificate.callback');
    Route::post('/certificate-callback/store/{price_id}', 'CertificateController@store')->name('certificate.store');
// home
    Route::get('/', 'PagesController@index')->name('home');
//master show page view
//Route::get('/master-show', 'PagesController@masterShow')->name('master-show');
//flowers-subscription
    Route::get('subscribe', 'PagesController@subsc')->name('flowers-sub');
    Route::post('subscription/store', 'FlowersController@store')->name('subs.store');
    Route::get('subscribe/{slug}', 'Admin\FSubscribersController@subscribePage')->name('flower.subscribe');
// comments
    Route::post('comments/add/{type}/{id}', 'CommentsController@store')->name('comments.store');
// blog
    Route::get('blog', 'PagesController@blog')->name('blog');
    Route::get('category/{id}', 'PagesController@blogcategory')->name('blog.category');
// items
    Route::post('sort', 'ItemsController@sortUpdate')->name('sort');
    Route::get('item/search', 'ItemsController@search')->name('item.search');
    Route::get('sale', 'ItemsController@pageByFilter')->name('sale');
    Route::get('catalog', 'ItemsController@index')->name('catalog');
    Route::get('catalog', 'ItemsController@index')->name('catalog');
    Route::get('catalog/{slug}', 'ItemsController@category')->name('catalog.category');
    Route::get('view/-{id}', 'ItemsController@show')->name('item');
    Route::get('view/{slug}-{id}', 'ItemsController@show')->name('item')->where('slug', '.*');

//photo flowers uploads
    Route::post('upload-photo', 'PhotosController@upload')->name('photo.upload');

    Route::post('buy-one-click', 'BuyController@buyOneClick')->name('buy.one_click');

// invoice
    //Route::get('bank-invoice/{id}', 'CommerceController@invoice')->name('invoice');
// cart
//    Route::get('cart/add/{id}', 'CommerceController@add')->name('cart.add');
//    Route::post('cart/add/{id}', 'CommerceController@add')->name('cart.add.post');
//    Route::get('cart/delete/{key}', 'CommerceController@delete')->name('cart.delete');
//    Route::post('cart', 'CommerceController@update')->name('cart.update');
//    Route::get('cart', 'CommerceController@show')->name('cart');
//    Route::get('checkout', 'CommerceController@checkout')->name('checkout');
//    Route::get('order/{id}', 'CommerceController@orderStatus')->name('order');
//    Route::post('order/complete', 'CommerceController@createOrder')->name('order.complete');
//    Route::get('buy', 'CommerceController@buy')->name('cart.buy');
//Route::post('login', 'Auth\AuthController@login');

	//CART
    Route::get('cart', 'CartController@show')->name('cart');
	Route::get('cart/add/{id}', 'CartController@add')->name('cart.add');
	Route::post('cart/add/{id}', 'CartController@add')->name('cart.add.post');
	Route::post('cart', 'CartController@update')->name('cart.update');
    Route::get('cart/delete/{key}', 'CartController@delete')->name('cart.delete');
    //ORDER
	Route::get('order', 'OrderController@show')->name('order');
	Route::post('order/save', 'OrderController@save')->name('order.save');
	Route::post('order/calendar_save', 'OrderController@calendarSave')->name('order.calendar_save');
	Route::get('order/calendar_save', function(){
		return redirect('/calendar');
	});
	Route::get('bank-invoice/{id}', 'OrderController@invoice')->name('invoice');
	Route::get('order/{id}', 'OrderController@orderStatus')->name('order.view');
    Route::post('order/complete', 'OrderController@complete')->name('order.complete');



    Route::post('send', 'ContactsController@send')->name('send');
    Route::post('send-contact', 'ContactsController@sendContact')->name('contact.send');
    //Route::post('send_shortcode_form', 'ContactsController@send_shortcode_form')->name('send_shortcode_form');
// pages
    Route::get('{slug}', 'PagesController@show')->name('page');
//social autorization
    Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider')->name('social-login');
    Route::get('auth/callback/{provider}', 'Auth\LoginController@handleProviderCallback');
});

