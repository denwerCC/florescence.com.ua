var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    browserSync = require('browser-sync');

gulp.task('sass', function() {
  return gulp.src('css/style.scss')
  .pipe(sass())
  .pipe(gulp.dest('css/'))
  .pipe(browserSync.reload({stream: true}))
});
// gulp.task('browser-sync', function() {
//   browserSync({
//     server: {
//       baseDir: 'Scada',
//       open: 'external',
//       host: 'Scada',
//       proxy: 'Scada',
//     },
//     notify: false
//   });
// });
gulp.task('browser-sync', function() {
   browserSync({
      notify: false,
      open: 'external',
      host: 'kvitochka',
      proxy: 'kvitochka',
   });
});
gulp.task('watch', ['browser-sync', 'sass'], function() {
  gulp.watch('css/style.scss', ['sass']);
  gulp.watch('**/*.html', browserSync.reload);
  gulp.watch('**/*.php', browserSync.reload);
  gulp.watch('**/*.tpl', browserSync.reload);
  gulp.watch('/js/**/*.js', browserSync.reload);
});
