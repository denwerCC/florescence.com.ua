<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetUserPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','provider', 'facebook_id', 'avatar', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function sendPasswordResetNotification($token)
	{
		$this->notify(new ResetUserPassword($token));
	}

	public static function createBySocialProvider($providerUser)
	{
		return self::create([
			'email' => $providerUser->getEmail(),
			'username' => $providerUser->getNickname(),
			'name' => $providerUser->getName(),
		]);
	}

}
