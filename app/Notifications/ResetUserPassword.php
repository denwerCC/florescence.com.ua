<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetUserPassword extends Notification
{
    use Queueable;
	/**
	 * The password reset token.
	 *
	 * @var string
	 */
	public $token;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
	public function __construct($token)
	{
		$this->token = $token;
	}
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->from('robot@' . request()->getHost(), Config::get("config.". App::getLocale() .".sitename"))
			->subject('Сброс пароля на сайте ' . request()->getHost())
			->line('Вы получили это письмо, так как был запрошен сброс пароля для Вашего аккаунта.')
			->action('Сбросить пароль', url('password/reset', $this->token))
			->line('Если Вы не запрашивали сброс пароля - не предпринимайте никаких действий в этом письме.');
	}

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
