<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Validator;
use SimpleImage as Image;
use View;
use File;

class CommentsController extends Controller
{

	public function store(Request $request,$type,$id)
	{

		$data = $request->input();
		$data['ip'] = $request->ip();
		$data['commentable_type'] = $type;
		$data['commentable_id'] = $id;

		if($user = \Auth::user()){
			$data['user_id'] = $user->id;
			$data['name'] = $user->name;
			$data['phone'] = $user->phone;
			$data['email'] = $user->email;
		}

		$comment = Comment::create($data);

		if ($request->hasFile('img')) {
			$this->addImage($request->file('img'), $comment->id);
		}

		return back()->with('message', l('Запись добавлена'));
	}

	public function addImage($img, $id)
	{
		// getting all of the post data
		$image = array('img' => $img);
		// $f_name = $img->getClientOriginalName();
		// setting up rules
		$rules = array('img' => 'mimes:jpeg,bmp,png',); //mimes:jpeg,bmp,png and for max size max:10000
		// doing the validation, passing post data, rules and the messages
		$validator = Validator::make($image, $rules);
		if ($validator->fails()) {
			// send back to the page with the input data and errors
			return $validator->errors()->all();
		} else {
			$image = $id . ".jpg";
			$img = Image::make($img);
			$img->crop(297, 347)->save("upload/rewievs/" .  $image);
			return $image;
		}

	}

}
