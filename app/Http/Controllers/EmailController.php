<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use Mail;
use Config;

class EmailController extends Controller
{

	public $domain_email;
	public $admin_emails;
	public $name_email;

	/**
	 * @return mixed
	 */
	public function __construct()
	{
		$lang = App::getLocale();
		$this->domain_email = 'robot@' . request()->getHost();
		$this->admin_emails = explode(",",Config::get("config.$lang.adminemail"));
		$this->name_email = Config::get("config.$lang.sitename");
	}


	public function newOrderToAdmin($data)
	{

		if(empty($this->admin_emails)) return;

		Mail::send('emails.order-to-admin',['order' => $data], function ($message) use ($data) {
			$message->subject("Нова покупка на сайті " . request()->getHost() . " на суму " . $data->amount . ' грн'  );
			$message->from($this->domain_email, $this->name_email);
			foreach ($this->admin_emails as $email){
                $message->to($email);
            }
		});

		return $this;
	}

	public function confirmRegistration($user,$password)
	{

		if(!filter_var($user->email, FILTER_VALIDATE_EMAIL)){
			return back()->with('error', 'E-mail указан неверно');
		}

		$data = $user->toArray();
		$data['password'] = $password;
		$subject = Config('email_subject.registration.' . App::getLocale());

		Mail::send('emails.' . App::getLocale() . '.registration', $data, function ($message) use ($subject,$user) {
			$message->subject($subject);
			$message->from($this->domain_email, $this->name_email);
			$message->to($user->email);
		});
		return $this;
	}

	public function confirmOrder($order)
	{

		if(!filter_var($order->email, FILTER_VALIDATE_EMAIL)){
			return back()->with('error', 'E-mail указан неверно');
		}

		$data = $order->toArray();
		$data['link'] = $order->link;
		$subject = Config('email_subject.order.' . App::getLocale());

		Mail::send('emails.' . App::getLocale() . '.order', $data, function ($message) use ($subject,$order) {
			$message->subject($subject);
			$message->from($this->domain_email, $this->name_email);
			$message->to($order->email);
		});
		return $this;
	}

	public function confirmDelivery($order)
	{

		if(!filter_var($order->email, FILTER_VALIDATE_EMAIL)){
			return back()->with('error', 'E-mail указан неверно');
		}

		$data = $order->toArray();
		$data['link'] = $order->link;
		$data['text'] = request()->get('text');

		$subject = Config('email_subject.delivery.' . App::getLocale());

		Mail::send('emails.' . App::getLocale() . '.delivery', $data, function ($message) use ($subject,$order) {
			$message->subject($subject);
			$message->from($this->domain_email, $this->name_email);
			$message->to($order->email);
		});
		return $this;
	}

    public function sendToAdmin($subject, $data)
    {
        $text = '';
        foreach ($data as $key => $value) {
            $text .= "$key: $value\n";
        }

        Mail::raw($text, function ($message) use ($subject) {
            $message->subject($subject);
            $message->from($this->domain_email, $this->name_email);
            foreach ($this->admin_emails as $email){
                $message->to($email);
            }
        });

        return $this;
    }

}
