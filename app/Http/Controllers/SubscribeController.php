<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{

	public function store(Request $request)
	{

		self::subscribe($request->input('email'));

		return response('<h2>'.l('Дякуємо!').'</h2><p>'.l('Ви підписались на новини').'</p>');

	}

	static public function subscribe($user)
	{
		if(isset($user->id)){
			$user = $user->toArray();
			$user['user_id'] = $user['id'];
		}
		$user['ip'] = request()->ip();
		// emulate fillable method
		Subscriber::firstOrCreate(array_only($user, ['ip','user_id', 'name', 'email', 'status']));
	}

}
