<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;
use App\Models\Order;

class AccountController extends Controller
{

	public function __construct()
	{
		// we can use middleware in secured controllers
		$this->middleware('auth');
	}

	public function index(){

		$data = [
			'title' => 'Личный кабинет',
			'orders'=> Order::whereUserId(Auth::user()->id)->orderBy('id','desc')->get()
		];

		return view('account.orders',$data);
	}

	public function profile(){

		$data = [
			'title' => 'Мой профиль',
		];

		return view('account.profile',$data);
	}

	public function wishlist(){

		$items = \App\Models\Favorite::whereUserId(Auth::user()->id)->with('item')->get()->mapWithKeys(function ($item) {
			return [$item->id => $item->item];
		})->sortBy('created_at');

		$data = [
			'title' => 'Избранные товары',
			'items'=> $items
		];

		return view('account.items',$data);
	}

	public function order($id){

		$order = Order::where(['id'=>$id,'user_id'=>Auth::user()->id])->firstOrFail();

		$data = [
			'title' => 'Заказ № ' . $order->id . " - " . $order->created_at,
			'order' => $order
		];

		return view('account.order',$data);
	}
}
