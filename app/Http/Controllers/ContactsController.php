<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App;
use Mail;
use Request;
use Validator;
use Config;
use App\Repositories\Interfaces\IContactRepository;
use App\Http\Requests\SendContactRequest;

class ContactsController extends Controller
{
    protected $contactRepository;

	public function __construct(IContactRepository $contactRepository)
	{
	    $this->contactRepository = $contactRepository;

		$lang = App::getLocale();
		$this->domainName = preg_replace("/\.*/","",request()->server("SERVER_NAME"));
		//$this->admin_email = 'jdfdf@df.df'; // Config::get("config.$lang.adminemail");
		$this->admin_email = Config::get("config.$lang.adminemail");
		if(substr_count($this->admin_email, ',')){
            $this->admin_email = explode(',',$this->admin_email);
        }
        // Config::get("config.$lang.adminemail");
//        var_dump(Config::get("config.$lang.adminemail"));
//        exit;
	}

	public function send(\Illuminate\Http\Request $request)
	{

		$data = $request->input();
		array_forget($data, '_token');

		$messages = array(
			'required' => 'Поле :attribute должно быть заполнено.',
			'min' => 'Укажите минимум :min символов для поля :attribute',
			'message.min' => 'Слишком короткое сообщение',
			'email' => 'Некорректно введен E-mail',
		);

		if($request->type == 'CallbackMe' or $request->type == 'Oneclick'){
			$rules = array(
				'phone' => 'required'
			);
		}elseif($request->type == 'master-show'){
			$rules = array(
				'name' => 'required',
				'phone' => 'required',
				'date' => 'required'
			);
		}
		elseif($request->type == 'shortcodeForm'){
			$rules = array(
				'name' => 'required|max:255',
				'phone' => 'required',
				'message' => 'required|min:5',
			);
		}
		else{
			$rules = array(
				'name' => 'required|max:255',
				'email' => 'email|required',
				'message' => 'required|min:5',
			);
		}




		$validator = Validator::make($data, $rules, $messages);
		$this->validateWith($validator);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}

		$text = '';
		foreach ($data as $key => $field) {
			$text .= $key . ": " . $field . "\n";
		}
		$text .= "IP: " . $request->ip() . "\n";
		$text .= "Страница: " . $request->headers->get('referer') . "\n";
		$text .= "Браузер и ОС: " . $request->server("HTTP_USER_AGENT") . "\n";


		App\Models\Contact::create([
			'name' => $request->get('name', ''),
			'phone' => $request->get('phone', ''),
			'email' => $request->get('email', ''),
			'page' => $request->headers->get('referer'),
			'comment' => $request->get('message', ''),
		]);

		Mail::raw($text, function ($message) use ($request) {
			$message->subject("Сообщение от пользователя с сайта " . $request->getHost());
			$message->from('robot@' . $request->getHost(), $this->domainName);
			$message->to($this->admin_email);
		});

		if (Request::ajax()) {
			return response()->json(['status' => 'success', 'message' => l('Дякуємо! Ми звяжемося з Вами найближчим часом.')]);
		}
		return redirect()->back()->withMessage(trans('new_app.Повідомлення успішно відправлено'));
	}

    /**
     * @param SendContactRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendContact(SendContactRequest $request)
    {
        $this->contactRepository->store($request);

        return redirect()->back();
    }
}
