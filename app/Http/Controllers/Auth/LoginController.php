<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use Authenticatable;
    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    
    public function handleProviderCallback($provider)
    {
        $user_social = Socialite::driver($provider)->user();
//		$tokenSecret = $user->tokenSecret;

		$user = User::where('user_id', $user_social->getId())->where('provider',$provider)->first();
        if ($user) {
           Auth::login($user, true);
        } else {
            $user = User::create([
                'user_id' => $user_social->getId(),
                'name' => $user_social->getName(),
                'email' => $user_social->getEmail(),
                'provider' => $provider
            ]);
            Auth::login($user);
        }
        if(\Request::query() == 'access_denied'){
        	return redirect('/');
        }
        return redirect('/');
    }
    
}
