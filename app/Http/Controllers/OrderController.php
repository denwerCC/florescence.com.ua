<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 07.04.2018
 * Time: 15:09
 */

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItems;
use Cookie;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class OrderController extends Controller {

	public function show( Request $request ) {

		return View( 'order.order', array(
			'count_items' => Cart::count(),
		) );
	}

	public function save( Request $request ) {
		if ( empty( Cart::count() ) ) {
			return redirect( route( 'cart' ) );
		}

		$this->validate( $request, [
			'name'  => 'required',
			'phone' => 'required',
			/*            'order.email' => 'email|required',
						'order.address' => 'required_unless:delivery,Самовывоз'*/
		], [], [
			'name'  => 'Имя',
			'phone' => 'Телефон',
		] );
		$order = new Order();

		$user      = \Auth::user();
		$order->ip = $request->ip();
		//Подсчет ведем в гривне
		$amount = 0;
		foreach ( Cart::content() as $item ) {
			$amount += $item->model->price;//
		}
		$order->amount         = $amount;
		$order->user_id        = isset($user->id)? $user->id : 0;
		$order->lang           = \App::getLocale();
		$order->code           = uniqid();
		$order->name           = $request->get( 'name' );
		$order->phone          = $request->get( 'phone' );
		$order->email          = $request->get( 'email' );
		$order->delivery_name  = $request->get( 'delivery_name' );
		$order->delivery_phone = $request->get( 'delivery_phone' );
		$order->delivery_date  = $request->get( 'delivery_date' );
		$order->time           = $request->get( 'time' );
		$order->delivery_time  = $request->get( 'delivery_time' );
		$order->address        = $request->get( 'address' );
		$order->payment_method = $request->get( 'payment_method' );
		$order->text           = $request->get( 'text', '' ) ? 'Отримувач інша людина' : 'Отримувач я';
		$order->note           = $request->get( 'note' );
		$order->note           .= PHP_EOL;
		$order->note           .= ( $request->get( 'delivery_anonym_label',
				'' ) == 'on' ) ? ' Доставити анонімно' . PHP_EOL : '';
		if ( $request->get( 'postcard', '' ) == 'on' ) {
			$order->note .= '***' . PHP_EOL;
			$order->note .= 'Додати листівку ' . PHP_EOL;
			$order->note .= 'Текст листівки ' . $request->get( 'postcard_text', '' ) . PHP_EOL;
			$order->note .= '***' . PHP_EOL;
		}
		$order->note .= ! empty( $request->get( 'delivery_email' ) ) ? 'Email отримувача ' . $request->get( 'delivery_email' ) . PHP_EOL : '';
		$order->save();

		// coupons
		( new \App\Http\Controllers\CouponsController )->storeCouponOrder( $order );

		foreach ( Cart::content() as $item ) {
			$item_c           = new OrderItems();
			$item_c->order_id = $order->id;
			$item_c->item_id  = $item->model->id;
			$item_c->title    = $item->model->title;
			$item_c->price    = $item->model->price;
			$item_c->count    = $item->qty;
			$item_c->save();
		}

		//Отправляем письмо
		( new \App\Http\Controllers\EmailController )->newOrderToAdmin( $order )
		                                             ->confirmOrder( $order );

		$document = [
			'items'          => $order->items,
			'total'          => $amount,
			'order'          => $order,
			'count'          => Cart::count(),
			'payment'        => '',
			'form'           => $request->all(),
			'order_id'       => $order->id,
			'payment_method' => isset( $order->payment_method ) ? $order->payment_method : '',
			'payment_status' => 'Очікує оплати',
			'gaCommerce'     => '',//$this->gaCommerce($order->id,$cartitems),
			'code'           => $order->code
		];

		Cart::destroy();

		// subscribe
		if ( $request->subscribe ) {
			\App\Http\Controllers\SubscribeController::subscribe( $user );
		}

		switch ( $order->payment_method ) {
			case 'Liqpay':
				return $this->payLiqpay( $order, $document );
				break;
			case 'PayPal':
				return $this->payPayPal( $order, $document );
				break;
			default:
				return $this->complete( $order, $document );
				break;

		}

		return $this->complete( $order, $document );

	}

	public function orderStatus( $id ) {

		$order = Order::whereCode( $id )->firstOrFail();

		if ( ! $order ) {

			$data = [
				'title'       => l( 'Замовлення відсутнє в базі' ),
				'description' => l( 'Замовлення відсутнє в базі' ),
				'content'     => l( 'Замовлення відсутнє в базі' )
			];

			return view( 'page', $data );

		}
		$data = [
			'total'          => $order->amount,
			'count'          => $order->count,
			'form'           => $order,
			'items'          => $order->items,
			'order_id'       => $order->id,
			'order'       => $order,
			'payment_method' => $order->payment_method,
			'payment_status' => $order->payment_status_value,
			'code'           => $order->code
		];

		return View( 'commerce.order-complete', $data );
	}

	public function invoice( $id ) {

		$order = Order::whereCode( $id )->firstOrFail();

		return View( 'commerce.bank-invoice', [
			'order'  => $order,
			'config' => config( 'bank' )
		] );
	}

	public function payLiqpay( Order $order, $document ) {
		return ( new \App\Http\Controllers\LiqPayController )->pay( $order->amount, $order->id,
				$order->code ) . $document['gaCommerce'] . "<script>form = document.getElementsByTagName('form')[0];form.style.display='none',form.submit();</script>";
	}

	public function paypayPal( Order $order, $document ) {
		return ( new \App\Http\Controllers\PayPalController )->pay( $order->amount, $order->id,
				$order->code ) . $document['gaCommerce'] . "<script>form = document.getElementsByTagName('form')[0];form.style.display='none',form.submit();</script>";
	}

	public function paypayPrivat24( Order $order, $document ) {
		$document['payment'] = " <a class='btn' target='_blank' href='" . route( 'invoice',
				$order->id ) . "'>Распечатать квитанцию</a>  ";

		return View( 'commerce.order-complete', $document )->withCookie( Cookie::forget( 'cart' ) );
	}

	public function complete( Order $order, $document ) {
		return View( 'commerce.order-complete', $document )->withCookie( Cookie::forget( 'cart' ) );
	}


	public function calendarSave( Request $request ) {

		if ( $request->session()->has( 'order.id' ) ) {
			$request->session()->forget('order.id');
			return redirect('/calendar');
		}

		$delivery_date = $request->get( 'delivery_date', '' );
		$occasion           = $request->get( 'occasion', '' );
		$occasion_text      = $request->get( 'occasion_text', '-' );
		$delivery_name      = $request->get( 'delivery_name', '' );
		$delivery_phone     = $request->get( 'delivery_phone', '' );
		$delivery_email     = $request->get( 'delivery_email', '' );
		$name               = $request->get( 'name', '' );
		$phone              = $request->get( 'phone', '' );
		$email              = $request->get( 'email', '' );
		$order_bouquet_size = $request->get( 'order_bouquet_size', '' );
		$order_product_id   = $request->get( 'order_product_id', '' );
		$address            = $request->get( 'address', '' );
		$time               = $request->get( 'time', '' );
		$size               = $request->get( 'order_bouquet_size', '' );
		$product_id         = $request->get( 'order_product_id', '' );
		$product            = Item::first( $product_id );
		$delivery_time      = $request->get( 'delivery_time', '' );

		$request->session()->put( 'order.delivery_date', $delivery_date );
		$request->session()->put( 'order.delivery_name', $delivery_name );
		$request->session()->put( 'order.delivery_phone', $delivery_phone );
		$request->session()->put( 'order.delivery_email', $delivery_email );
		$request->session()->put( 'order.name', $name );
		$request->session()->put( 'order.phone', $phone );
		$request->session()->put( 'order.email', $email );
		$request->session()->put( 'order.time', $time );
		$request->session()->put( 'order.size', $size );
		$request->session()->put( 'order.address', $address );
		$request->session()->put( 'order.delivery_time', $delivery_time );
		//$request->session()->regenerate(true);
		if ( empty( $order_product_id ) ) {
			return back()->with( 'error', trans( 'new_app.Оберіть букет' ) );
		}
		if ( empty( $order_bouquet_size ) ) {
			return back()->with( 'error', trans( 'new_app.Оберіть розмір букету' ) );
		}
		if ( ! ( $product instanceof Item ) ) {
			return back()->with( 'error',
				trans( 'new_app.Продукт не знайдено' ) . ' ' . trans( 'new_app.Оберіть букет' ) );
		}

		$calendar_settings = config( 'calendar_settings' );

		//return back()->with('message', 'OK');
		//dd($request->all());

		$order = new Order();

		$user      = \Auth::user();
		$order->ip = $request->ip();
		//Подсчет ведем в гривне
		$amount = array_get( $calendar_settings, 'sizes.' . $size . '.price', 0 );

		$request->session()->flash( 'order.size', $size );
		$request->session()->flash( 'order.amount', $amount );
		$order->amount         = $amount;
		$order->calendar_flag  = 1;
		$order->user_id        = isset( $user->id ) ? $user->id : 0;
		$order->lang           = \App::getLocale();
		$order->code           = uniqid();
		$order->name           = $name;
		$order->phone          = $phone;
		$order->email          = $email;
		$order->delivery_name  = $delivery_name;
		$order->delivery_phone = $delivery_phone;
		$order->delivery_date  = $delivery_date;
		$order->time           = $time;
		$order->delivery_time  = $delivery_time;
		$order->address        = $address;
		$order->payment_method = $request->get( 'payment_method' );
		$order->note           = $request->get( 'note' );
		$order->note           .= PHP_EOL;
		$order->note           .= ( $request->get( 'delivery_anonym',
				'' ) == 'on' ) ? ' Доставити анонімно' . PHP_EOL . '***' . PHP_EOL : '';

		$order->note .= PHP_EOL;
		$order->note .= 'Размір букету: ' . $size . PHP_EOL;
		$order->note .= 'Товар: ' . $product->title . '(' . $product->price . '/№' . $product->id . ')' . PHP_EOL;
		$order->note .= '***' . PHP_EOL;
		if ( $request->get( 'postcard', '' ) == 'on' ) {
			$order->note .= 'Додати листівку ' . PHP_EOL;
			$order->note .= 'Текст листівки: ' . $request->get( 'postcard_text', '' ) . PHP_EOL;
			$order->note .= '***' . PHP_EOL;
		}
		if ( ! empty( $occasion ) ) {
			$order->note .= PHP_EOL;
			$order->note .= 'Привід: ' . $occasion . PHP_EOL;
			$order->note .= 'Свій привід: ' . $occasion_text . PHP_EOL;
			$order->note .= '***' . PHP_EOL;
		}
		$order->note .= ! empty( $request->get( 'delivery_email' ) ) ? 'Email отримувача ' . $request->get( 'delivery_email' ) . PHP_EOL : '';

		$order->text = $order->note;


		$order->save();

		$request->session()->flash( 'order.id', $order->id );

		$item_c           = new OrderItems();
		$item_c->order_id = $order->id;

		$item_c->item_id = $product->id;
		$item_c->title   = $product->title;
		$item_c->price   = $product->price;
		$item_c->count   = 1;
		$item_c->save();

		//Отправляем письмо
		( new \App\Http\Controllers\EmailController )->newOrderToAdmin( $order )->confirmOrder( $order );

		$document = [
			'items'          => $order->items,
			'total'          => $amount,
			'count'          => 1,
			'payment'        => '',
			'form'           => $request->all(),
			'order_id'       => $order->id,
			'payment_method' => isset( $order->payment_method ) ? $order->payment_method : '',
			'payment_status' => 'Очікує оплати',
			'gaCommerce'     => '',//$this->gaCommerce($order->id,$cartitems),
			'code'           => $order->code
		];

		switch ( $order->payment_method ) {
			case 'Liqpay':
				return $this->payLiqpay( $order, $document );
				break;
			case 'PayPal':
				return $this->payPayPal( $order, $document );
				break;
			default:
				return $this->completeCalendar( $order, $document );
				break;

		}


		return $this->completeCalendar( $order, $document );


	}

	public function completeCalendar( Order $order, $document ) {
		return View( 'commerce.order-calendar-complete', $document )->withCookie( Cookie::forget( 'cart' ) );
	}


}