<?php

namespace App\Http\Controllers;

use Cookie;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Cart;//use Gloudemans\Shoppingcart\Facades\Cart;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\Item;

class CartController  extends Controller{

	public function show(){
		self::recalCartIfChangeCurrency();
//		Cart::destroy();
		//Cart::add('192ao12', 'Product 1', 1, 9.99);
//		dump(Cart::content());
//		dump(Cart::content()->groupBy('id'));
//		dd(Cart::search(267));
		//dump(Cart::subtotal());
		$data = array(
			'cart' => Cart::content(),
			'count_items' => Cart::count(),
			'total' => Cart::subtotal(),
		);
		return View('cart.cart',$data);
	}

	public function add($id){
		$count = \request()->get('count', 1);
		if($count < 1){
			$count = 1;
		}

		$product = Item::find((int) $id);
//		dd($product);

		if(!$product)  return back()->with('message', trans('new_app.Продукт не знайдено'));

		Cart::add([
			'id' => $product->id,
			'name' => $product->title,
			'qty' => $count,
			'price' =>$product->priceFormatted
		])->associate('Item');;

		return redirect(route('cart'));

	}

	public function delete($id){

		try{
			$rowId = $this->getCartRowIdByItemId($id);
			if(!empty($rowId)){
				Cart::remove($rowId);
				return redirect(route('cart'))->with('message', trans('new_app.Продукт успішно видалений'));
			}
			return redirect(route('cart'))->with('message', '#Error::delete id='.$id);
		}
		catch (\Error $e){
//			dd($e);
			return redirect(route('cart'))->with('error', '#Error '.$e->getMessage());
		}
	}

	public function getCartRowIdByItemId($id = 0){
		if(Cart::count() < 1) return;
		$cart = Cart::content()->groupBy('id');
		$p = $cart->get($id);
		if(!$p) return;
		$p = $p->first();
		if(!isset($p->rowId)) return;

		return $p->rowId;
	}

	public function update(Request $request){
		$errors = [];
//		dd($request->get('product_id'));
		$array = $request->get('product_id', []);
		if(empty($array)) return back();

		foreach ($array as $product_id => $count){
			$rowId = $this->getCartRowIdByItemId($product_id);
			if(!empty($rowId)){
				Cart::update($rowId, $count);
			}else{
				array_push($errors, 'Not_found ProductID'.$product_id);
			}
		}
		if(!empty($errors)){
			return back()->with('error', implode(',',$errors));
		}
		return back()->with('message', trans('new_app.Кошик перерахований'));
	}

	public static function recalCartIfChangeCurrency(){
		$items = Cart::content();
		if(empty($items)) return;
		//dd($items);
		foreach (Cart::content() as $item){
			//dd($item->model->priceFormatted);
			Cart::update($item->rowId, ['price' => $item->model->priceFormatted]); // Will update the name
		}
		//dd(Cart::content());
//		Cart::update($rowId, ['name' => 'Product 1']); // Will update the name
	}

	public static function getCartCount(){
		return Cart::count();
	}


}