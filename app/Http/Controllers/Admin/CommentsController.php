<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Comment;
use Validator;
use SimpleImage as Image;
use View;
use File;

class CommentsController extends \App\Http\Controllers\CommentsController
{

	public function index()
	{
		$comments = Comment::where("commentable_type",'!=','comment')->orWhereNull('user_id')->get()->sortByDesc('id');

		$document = [
			'title' => 'Все комментарии',
			'comments' => $comments,
			//'allcomments' => Comment::count()
		];

		return view('admin.comments.index', $document);
	}

	public function edit($id)
	{

		$comment = Comment::findOrFail($id);

		$document = [
			'title' => 'Информация о комментарии',
			'comment' => $comment,
		];
		return view('admin.comments.edit', $document);
	}

	public function update(Request $request, $id)
	{
		$data = $request->all();

		if (empty($request->public)) {
			$data['public'] = Null;
		} else {
			$data['public'] = 1;
		}

		Comment::find($id)->update($data);

		return back()->with('message', 'Запись обновлена');
	}

	public function destroy($id)
	{
		Comment::find($id)->delete();
		File::delete("upload/rewievs/$id.jpg");
		return back()->with('message', "Запись удалена");
	}

}
