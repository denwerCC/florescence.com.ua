<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Config;
use File;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class BannersController extends Controller
{
    const TYPE = 'banners';

    public function index(Request $request){

        return view('admin.config.'.self::TYPE, $this->bannersForm());
    }

    public function bannersForm()
    {
        $form = [
            'type' => 'clone',
            'label' => 'Баннер',
            self::TYPE => config(self::TYPE) ?: [$this->getItemBannerArr()]
        ];

        $document = [
            'title' => 'Баннеры на главной',
            'form' => $form,
            'view' => self::TYPE,
            'type' => self::TYPE,
        ];

        return $document;
    }

    public function getItemBannerArr(){
        $banner = [
            'img' => '',
            'link' => '',
            'text' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
        ];
        return $banner;
    }

    public function update(Request $request)
    {
        $banners = array_values($request->get(self::TYPE));
        //dd($banners);


        $this->saveConfig($banners, self::TYPE);

        return back()->with('message', l('Конфигурация обновлена'));
    }

    public function saveConfig($data, $filename)
    {
        $config = "<?php\n\nreturn ";
        //$data = array_filter($data);
        $config .= var_export($data, true);
        $config .= ';';
        File::put(config_path("{$filename}.php"), $config);
    }

}