<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Config;
use File;
use Illuminate\Http\Request;

class ConfigController extends Controller
{

    public function index()
    {
        return $this->edit('config');
    }

    public function edit($type)
    {
        $form = $type . 'Form';

        $formData = $this->$form($type);
        $view = isset($formData['view']) ? 'admin.config.' . $formData['view'] : 'admin.config.index';
        return view($view, $formData);
    }

    public function configForm($type)
    {

        foreach (\Config::get('app.locales') as $lang => $key) {
            $form[$lang] = [
                'adminemail' => [
                    'type' => 'text',
                    'label' => 'E-mail администратора',
                    'value' => config("config.$lang.adminemail"),
                ],
                'sitename' => [
                    'type' => 'text',
                    'label' => 'Название сайта',
                    'value' => config("config.$lang.sitename"),
                ],
                'footertext' => [
                    'type' => 'text',
                    'label' => 'Текст в "подвале" сайта',
                    'value' => config("config.$lang.footertext"),
                ],
                'address' => [
                    'type' => 'text',
                    'label' => 'Адрес',
                    'value' => config("config.$lang.address"),
                ],
                'email' => [
                    'type' => 'text',
                    'label' => 'E-mail',
                    'value' => config("config.$lang.email"),
                ],
                'phone' => [
                    'type' => 'textarea',
                    'label' => 'Телефон',
                    'value' => config("config.$lang.phone"),
                ],
                'google_analytic_code' => [
                    'type' => 'text',
                    'label' => 'Google Analytics Code',
                    'value' => config("config.ua.google_analytic_code"),
                ]
            ];
        }

        $document = [
            'title' => 'Настройки Сайта',
            'dataform' => $form,
            'type' => $type
        ];

        return $document;
    }

    public function emailForm()
    {

        $subject = Config('email_subject');

        foreach (Config::get('app.locales') as $lang => $key) {

            $data[$lang] = [
                'registration' => [
                    'subject' => Config("email_subject.registration." . $lang),
                    'text' => File::get(resource_path("views/emails/$lang/registration.blade.php"))
                ],
                'order' => [
                    'subject' => Config("email_subject.order." . $lang),
                    'text' => File::get(resource_path("views/emails/$lang/order.blade.php"))
                ],
                'delivery' => [
                    'subject' => Config("email_subject.delivery." . $lang),
                    'text' => File::get(resource_path("views/emails/$lang/delivery.blade.php"))
                ],
            ];
        }

        return ['view'=>'email','emails' => $data];
    }

    public function socialForm($type)
    {

        $form = [
            'facebook' => [
                'type' => 'text',
                'label' => 'Facebook',
                'value' => config($type . '.facebook'),
            ],
            /*			'twitter' => [
                            'type' => 'text',
                            'label' => 'Twitter',
                            'value' => config($type . '.twitter'),
                        ],*/
            'instagram' => [
                'type' => 'text',
                'label' => 'Instagram',
                'value' => config($type . '.instagram'),
            ],
            'vk' => [
                'type' => 'text',
                'label' => 'Vkontakte',
                'value' => config($type . '.vk'),
            ],
            'google-plus' => [
                'type' => 'text',
                'label' => 'Google+',
                'value' => config($type . '.google-plus'),
            ],
        ];

        $document = [
            'title' => 'Ссылки социальных сетей',
            'form' => $form,
            'type' => $type
        ];

        return $document;
    }

    public function redirectForm($type)
    {
        $form = [
            'redirect' => [
                'type' => 'clone',
                'label' => 'Перенаправлення',
                'value' => config($type) ?: ['redirect' => [0 => '']]
            ]
        ];

        $document = [
            'title' => '301 Redirects',
            'view' => 'clone',
            'form' => $form,
            'type' => $type
        ];

        return $document;

    }

    public function liqpayForm($type)
    {

        $form = [
            'public_key' => [
                'type' => 'text',
                'label' => 'Merchant ID (Public Key)',
                'value' => config($type . '.public_key'),
            ],
            'private_key' => [
                'type' => 'text',
                'label' => 'Private Key (Secret code)',
                'value' => config($type . '.private_key'),
            ],
            'sandbox' => [
                'type' => 'checkbox',
                'label' => 'Тестовый режим',
                'value' => config($type . '.sandbox')
            ],
        ];

        $document = [
            'title' => 'Настройки LiqPay',
            'form' => $form,
            'type' => $type
        ];

        return $document;
    }

    public function widgetForm($type)
    {
        $form = [
            'widget' => [
                'type' => 'textarea',
                'class' => 'noeditor',
                'label' => 'Код виджета',
                'value' => config($type . '.widget')
            ]
        ];

        $document = [
            'title' => 'Вставка дополнительных кодов на сайт',
            'form' => $form,
            'type' => $type
        ];

        return $document;

    }

    public function deliveryForm($type)
    {
        $form = [
            'delivery' => [
                'type' => 'clone',
                'label' => 'Доставка',
                'value' => config($type) ?: ['delivery' => [0 => '']]
            ]
        ];

        $document = [
            'title' => 'Настройки типов доставки',
            'form' => $form,
            'type' => $type
        ];

        return $document;

    }

    public function bankForm($type)
    {

        $form = [
            'bank' => [
                'type' => 'textarea',
                'label' => 'Реквізити ПриватБанку',
                'value' => config($type . '.bank'),
            ],
            'westernunion' => [
                'type' => 'text',
                'label' => 'Імя отримувача Western Union',
                'value' => config($type . '.westernunion'),
            ],
            'moneygram' => [
                'type' => 'text',
                'label' => 'Імя отримувача Moneygram',
                'value' => config($type . '.moneygram'),
            ],
        ];

        $document = [
            'title' => 'Налаштування оплати',
            'form' => $form,
            'type' => $type
        ];

        return $document;
    }

    public function update(Request $request, $configname)
    {
        if ($configname == 'email') {
            return $this->updateEmail($request);
        }
        $data = $request->except(['_token', '_method']);

        if ($configname == 'redirect') {
            $data = array_combine($data['key'], $data['value']);
        }

        $this->saveConfig($data, $configname);

        return back()->with('message', l('Конфигурация обновлена'));
    }

    public function saveConfig($data, $configname)
    {
        $config = "<?php\n\nreturn ";
        $data = array_filter($data);
        $config .= var_export($data, true);
        $config .= ';';
        File::put(config_path("$configname.php"), $config);
    }

    public function updateEmail($request)
    {
        $data = $request->get('email');

        $subject = Config('email_subject');

        foreach ($data as $lang => $type) {
            $key = key($type);
            $data = $type[$key];

            $this->saveConfig([$lang => $data['subject']], 'email_subject.' . $key);
            File::put(resource_path("views/emails/$lang/$key.blade.php"), $data['text']);
        }

        return back()->with('message', 'Текст письма сохранен');

    }

}
