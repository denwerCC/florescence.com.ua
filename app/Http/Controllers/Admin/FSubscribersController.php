<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\fSubscribers;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Rubric;

class FSubscribersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscribePage($slug){
	    $page = Page::where('category_id',4)->where('slug', $slug)->first();
        if(!$page){
            abort(404);
        }
	    $cat = Rubric::find(['category_id' => 4])->first();
    	return view('subscription-order', ['page'=>$page, 'rubric' => $cat]);
    }

    public function index()
    {
        return view('admin.fSubscription.index', ['subscribers'=>fSubscribers::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $subscriber = fSubscribers::where('id', $id)->first();
	    if(!is_null($subscriber)){
		    $subscriber->delete();
	    }
	    return back()->with('message', l('Успішно видалено'));
    }
}
