<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Config;
use File;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Models\Item;


class CalendarSettimgsController extends Controller
{
    const TYPE = 'calendar_settings';

    public function index(Request $request){

        return view('admin.config.'.self::TYPE, $this->form());
    }

    public function form()
    {
    	$default_config = $this->getItemArr();
    	$config = config(self::TYPE) ?: $default_config;
    	if(!isset($config['sizes']) && isset($default_config['sizes'])){
		    $config['sizes'] = $default_config['sizes'];
	    }
    	if(!isset($config['products'])){
			    $config['products'] = Item::all();
	    }
        $form = [
            'label' => 'Текст',
            self::TYPE => $config
        ];

        $document = [
            'title' => 'Календарь событий',
            'form' => $form,
            'view' => self::TYPE,
            'type' => self::TYPE,
        ];

        return $document;
    }

    public function getItemArr(){
        $item = [
            'e_title' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_header_1' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_text_1' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_header_2' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_text_2' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_header_3' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_text_3' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_header_4' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'e_text_4' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
	        'sizes' => [
	        	's' => [
	        		'price' => 3000,
	        		'items' => [],
		        ],
	        	'm' => [
			        'price' => 5000,
			        'items' => [],
		        ],
	        	'l' => [
			        'price' => 7000,
			        'items' => [],
		        ],
	        ],
        ];
        return $item;
    }

    public function update(Request $request)
    {
//    	dd($request->get(self::TYPE));
	    $items = $request->get(self::TYPE);
	    if(isset($items['products'])){
	        unset($items['products']);
	    }


        $this->saveConfig($items, self::TYPE);

        return back()->with('message', l('Конфигурация обновлена'));
    }

    public function saveConfig($data, $filename)
    {
        $config = "<?php\n\nreturn ";
        //$data = array_filter($data);
        $config .= var_export($data, true);
        $config .= ';';
        File::put(config_path("{$filename}.php"), $config);
    }

}