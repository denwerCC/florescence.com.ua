<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemFilter;
use Config;
use DB;
use File;
use Illuminate\Http\Request;
use Validator;
use View;


class ItemsController extends Controller
{
    
    use \App\Traits\Image;
    
    public $datacount;
    public $allitems;
    
    public function __construct()
    {
        
        $this->allitems = Item::select(['id', 'category_id'])->get();
        $this->datacount = $this->allitems->count();
        
        $data = [
            'document' => [
                'datacount' => $this->datacount
            ],
            'categories' => Category::all()->load('childrens', 'parent'),
            'allitems' => $this->allitems
        ];
        
        View::share($data);
        
    }
    
    public function index(Request $request, $category = false)
    {
        $data = $request->all();
        
        if (!$data) {
            $data['draw'] = 1;
            $data['length'] = 1;
            $data['start'] = 0;
        }
        
        if ($search = $request->search['value']) {
            
            $items = Item::where('title', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%')
                ->orWhere('content', 'like', '%' . $search . '%');
            
            $datacount = $items->count();
            
        } else {
            if ($category !== false) {
                
                $datacount = $this->allitems->where('category_id', $category)->count();
                $items = Item::where('category_id', $category);
            } else {
                $datacount = $this->datacount;
                $items = Item::with('category');
                
            }
        }
        
        // sort
        $sortcolumns = [
            'id',
            'img',
            'title',
            'category_id',
            'created_at',
            'sku',
            'price'
        ];
        if (isset($data['order'][0]['column'])) {
            $sortcolumn = $sortcolumns[$data['order'][0]['column']];
            $dir = $data['order'][0]['dir'];
        } else {
            $sortcolumn = 'level';
            $dir = 'asc';
        }
        
        $items = $items->skip($data['start'])
            ->take($data['length'])
            ->orderBy($sortcolumn, $dir)
            ->get();
        
        $document = [
            'title' => $category ? $items->first()->category->title : 'Все позиции',
            'category_id' => $category ? $items->first()->category_id : false,
        ];
        
        $mod = array();
        //$items->values()->all() // reset keys
        foreach ($items as $n => $item) {
            $items[$n]->thumb = $item->thumb;
            $items[$n]["DT_RowId"] = $item->id;
            $items[$n]["item_id"] = $item->id;
            $items[$n]["categoryTitle"] = $item->category ? $item->category->title : '';
            $mod[] = $items[$n];
        };
        
        // Request::ajax()
        if ($request->ajax()) {
            
            $data = [
                "draw" => $data['draw'],
                "recordsTotal" => $datacount,
                "recordsFiltered" => $datacount,
                "data" => $mod
            ];
            
            return json_encode($data);
        }
        
        return view('admin.items.items', $document);
    }
    
    public function edit($id)
    {
        $item = Item::findOrFail($id);
        
        foreach (Config::get('app.locales') as $lang => $key) {
            $items[$lang] = $item->translate($lang);
            if (!$items[$lang]) {
                $items[$lang] = new Item;
            }
        }
        
        $document = [
            'title' => 'Редактирование позиции',
            'items' => $items,
            'Item' => $item,
            'filters+active' => '',
            'route' => ['admin.items.update', $id],
            'method' => 'put'
        ];
        
        return view('admin.items.edit', $document);
    }
    
    
    public function create()
    {

        $item = new Item();
        $item->public = 1;
//		$item->stock = 1;

        foreach (Config::get('app.locales') as $lang => $key) {
            $items[$lang] = $item;
	        if (!$items[$lang]) {
		        $items[$lang] = new Item;
	        }
        }

        $document = [
            'title' => 'Добавление позиции',
            'Item' => $item,
            'items' => $items,
            'route' => 'admin.items.store',
            'method' => 'post'
        ];

        return view('admin.items.edit', $document);
    }
    
    public function store(Request $request)
    {
        $data = $request->all();
        
        $data['level'] = Item::max('level') + 1;
        
        if (empty($request->date)) {
            $data['date'] = \Carbon\Carbon::now();
        }
        
        $item = Item::create(array_filter($data));
        
        // filters
        if (!empty($data['filter'])) {
            $this->addFilters($data['filter'], $item);
        }
        
        // translate
        foreach (Config::get('app.locales') as $lang => $key) {
            if (empty($data[$lang]['slug'])) {
                $data[$lang]['slug'] = $data[$lang]['title'];
            }
            $data[$lang]['slug'] = tolink($data[$lang]['slug']);
            $item->saveTranslation($lang, $data[$lang]);
        }
        
        if (!empty($data['imgs'])) {
            $this->addImages($data['imgs'], $item->id);
            $item->img = json_encode($data['imgs']);
            $item->save();
        }
        
        return redirect(route('admin.items.edit', $item->id))->with('message', 'Позиция добавлена');
    }
    
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $item = Item::find($id);
        // images
        if (!empty($data['imgs'])) {
            $this->addImages($data['imgs'], $id);
            $data['img'] = json_encode($data['imgs']);
        }
        
        // filters
        if (!empty($data['filter'])) {
            $this->addFilters($data['filter'], $item);
        }
        if (empty($request->price)) {
            $data['price'] = '0';
        }
        if (empty($request->public)) {
            $data['public'] = '0';
        } else {
            $data['public'] = 1;
        }
//		if (empty($request->stock)) {
//			$data['stock'] = '';
//		} else {
//			$data['stock'] = 1;
//		}
        if (empty($request->date)) {
            $data['date'] = \Carbon\Carbon::now();
        }
        
        $item->update($data);
        // translate
        foreach (Config::get('app.locales') as $lang => $key) {
            if (empty($data[$lang]['slug'])) {
                $data[$lang]['slug'] = $data[$lang]['title'];
            }
            $data[$lang]['slug'] = tolink($data[$lang]['slug']);
            
            $item->saveTranslation($lang, $data[$lang]);
        }
        
        return back()->with('message', l('Позиция обновлена'));
    }
    
    public function addFilters($locale_filters, $item)
    {
        
        ItemFilter::where('item_id', $item->id)->delete();
        
        // delete empty data
        array_clean($locale_filters);
        
        foreach ($locale_filters as $filter_id => $langfilter) {
            
            $filter = ItemFilter::create([
                    'item_id' => $item->id,
                    'filter_id' => $filter_id,
                ]
            );
            
            foreach ($langfilter as $lang => $value) {
                
                if (is_object($file = $value['value']) and $file->isFile()) {
                    $fileName = $filter_id . "-" . tolink($file->getClientOriginalName(), "\.");
                    $file->move($item->fileDir, $fileName);
                    $value['value'] = $fileName;
                }
                
                $filter->saveTranslation($lang, $value);
            }
            
        }
        
    }
    
    public function editPrice()
    {
        $data = request();
        Item::find($data->id)->update($data->input());
        return result('success');
    }
    
    public function addImages($imgs, $id)
    {
        
        $temp_path = public_path("upload/tmp/");
        $watermark = false;
        $dest = "upload/items/$id/big/";
        
        foreach (File::files($temp_path) as $img) {
            $img = File::basename($img);
            
            if (!array_key_exists($img, $imgs)) {
                continue;
            }
            
            File::makeDirectory($dest, 493, true, true);
            File::copy($temp_path . $img, $dest . $img);
            
        }
        
        File::cleanDirectory($temp_path);
    }
    
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->delete();
        return result("Товар " . $item->title . " удален");
    }
    
    public function deleteImg($id, $image)
    {
        
        $Model = Item::find($id);
        // del from db
        $images = json_decode($Model->img, 1);
        
        unset($images[$image]);
        
        $Model->img = json_encode($images);
        
        $Model->update();
        
        foreach (scandir($Model->dir) as $dir) {
            if ($dir == '.' or $dir == '..') {
                continue;
            }
            if (is_file($Model->dir . $dir . "/" . $image)) {
                unlink($Model->dir . $dir . "/" . $image);
            }
        }
        
        return back()->withMessage('Image successfully deleted');
    }
    
    public function sort(Request $request)
    {
        $data = $request->all()['sort'];
        foreach ($data as $level=>$row){
        	Item::where('id',$row['id'])->update(['level'=>$row['level']]);
        }
    }
    
    public function changePrice(Request $request, $category_id = false)
    {
        
        $this->validate($request, [
            'formula' => 'required|regex:/^([\*-\/\+][\d]+)+$/'
        ]);
        
        $query = DB::table('items');
        
        if ($category_id) {
            $query->where('category_id', $category_id);
        }
        
        if ($request->price_old) {
            $query->update(array(
                'price_old' => DB::raw('price'),
            ));
        }
        
        $query->update(array(
            'price' => DB::raw('price' . $request->formula),
        ));
        
        return back()->with('message', l('Цена обновлена'));
        
    }
    
    
    public function cloneItem($id)
    {
        
        $itemDonor = Item::findOrFail($id)->load('filters'); // load relation for saving on next
        $item = $itemDonor->replicate();
        
        // push - include save() method and save all relations
        $item->push();
        
        File::copyDirectory($itemDonor->dir, $item->dir);
        
        foreach ($item->getRelations() as $relation => $models) {
            foreach ($models as $model) {
                $model->item_id = $item->id;
                $model->create($model->toArray());
            }
        }
        
        return back()->with('message', l('Продукт успешно клонирован'));
        
    }
    
    public function faker()
    {
        $faker = \Faker\Factory::create('ru_RU');
        
        $categories = Category::all();
        
        foreach ($categories as $category) {
            //if ($category->parent_id) {
            
            foreach (range(0, 12) as $n) {
                
                $item = new Item();
                $item->title = $faker->realText(30, 3);
                $item->category_id = $category->id;
                $item->description = $faker->realText;
                $item->public = 1;
//				$item->stock = 100;
                $item->content = $faker->realText;
                $item->price = $faker->numberBetween(10, 500);
                $item->save();
                File::makeDirectory($item->dir . "/big", 0775, true);
                
                $image = $faker->image($item->dir . "/big", 329, 497, false, false);
                $item->update(['img' => json_encode([$image => ''])]);
                
                /*					ItemFilter::create([
                                        'item_id' => $item->id,
                                        'filter_id' => 4,
                                        'value' => $faker->name
                                    ]);*/
            }
            
            //}
            
            
        }
        
        return 'ok';
    }
    
}
