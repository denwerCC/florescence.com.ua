<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Category;
use App\Models\Item;
use Config;
use Illuminate\Http\Request;
use SimpleImage as Image;
use View;

class CategoriesController extends Controller
{

	public function __construct()
	{

		$this->allitems = Item::select(['id', 'category_id'])->get();
		$this->datacount = $this->allitems->count();

		$data = [
			'document' => [
				'datacount' => $this->datacount
			],
			'categories' => Category::all()->sortBy('title')->load('childrens','parent'),
			'allitems' => $this->allitems
		];

		View::share($data);

	}

	public function index()
	{
		$document = [
			'cats' => Category::whereNULL('parent_id')->with('childrens')->orderBy('level')->get()
		];

		return view('admin.categories.categories', $document);
	}

	public function create()
	{
		return view('admin.categories.create');
	}

	public function store(Request $request)
	{
		$data = array_filter($request->input());
		if (empty($data['slug'])) $data['slug'] = $data['title'];
		$data['slug'] = tolink($data['slug']);
		$category = Category::create($data);

		if ($request->hasFile('img')) {
			$category->img = $this->addImage($request->file('img'), $category);
			$category->save();
		}

		return back()->with('message', 'Категория добавлена');
	}

	public function edit($id)
	{

		$category = Category::find($id);

		foreach (Config::get('app.locales') as $lang => $key) {
			$Categories[$lang] = $category->translate($lang) ? $category->translate($lang) : new Category;
		}

		$document = [
			'title' => 'Редактирование категории',
			'Category' => $category,
			'Categories' => $Categories,
			'route' => ['admin.categories.update', $id],
			'method' => 'put',
		];

		return view('admin.categories.edit', $document);
	}

	public function update(Request $request, $id)
	{
		$data = $request->all();

		$category = Category::find($id);
		$data['parent_id'] = $data['parent_id'] ? $data['parent_id'] : NULL;

		if ($request->hasFile('img')) {
			$data['img'] = $this->addImage($request->file('img'), $category);
		}

		$category->update($data);

        //var_export($data['ua']);
//        var_export($category->getAttributes());
//        exit;
		foreach (Config::get('app.locales') as $lang => $key) {
			if (empty($data[$lang]['slug'])) $data[$lang]['slug'] = $data[$lang]['title'];
			$data[$lang]['slug'] = tolink($data[$lang]['slug']);

			$category->saveTranslation($lang, $data[$lang]);
		}

		return back()->with('message', 'Категория обновлена');
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		$category = Category::find($id);

		// items delete with all subcategories
		$ids = array_pluck($this->treeList($category),'id');
		// deleting in foreach to force model ::deleting method
		foreach (Item::whereIn('category_id',$ids)->get() as $item){
			$item->delete();
		}

		$category->delete();
		return back()->with('message', "Категория " . $category->title . " удалена");
	}

	public function sort(Request $request)
	{

		$this->createLevels($request->input('data'));

	}

	private function createLevels($data, $parent_id = false)
	{
		foreach ($data as $level => $it) {
			$item['level'] = $level;

			$item['parent_id'] = $parent_id ? $parent_id : NULL;

			if (!empty($it['children'])) {
				$this->createLevels($it['children'], $it['id']);
			}

			Category::find($it['id'])->update($item);
		}
	}

	public function treeList($category)
	{
		$categories = array();

		if(!$category->childrens->isEmpty()){

			foreach ($category->childrens as $category){
				$categories = array_merge($categories, $this->treeList($category));
			}

		}
		$categories[] = $category;

		return $categories;

	}

	public function addImage($img, $category)
	{

		// getting all of the post data
		$image = array('img' => $img);
		// $f_name = $img->getClientOriginalName();
		// setting up rules
		$rules = array('img' => 'mimes:jpeg,bmp,png,max:' . get_max_file_size()); //mimes:jpeg,bmp,png and for max size max:10000
		// doing the validation, passing post data, rules and the messages
		$validator = \Validator::make($image, $rules);
		if ($validator->fails()) {
			// send back to the page with the input data and errors
			return $validator->errors()->all();
		} else {

			$image = $category->id . ".jpg";

			//clear if there are previous images
			$category->deleteImg($image);

			$img = Image::make($img);
			$img->best_fit(1000, 1600)->save($category->dir . "big/" . $image)->crop(120, 120)->save($category->dir . "small/" . $image);
			return $image;
		}

	}
	
}
