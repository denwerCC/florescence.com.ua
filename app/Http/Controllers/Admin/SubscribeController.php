<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Subscriber;
use View;

class SubscribeController extends Controller
{

	public function index()
	{
		$subscribers = Subscriber::all()->sortByDesc('id');

		$document = [
			'title' => 'Подписчики',
			'subscribers' => $subscribers
		];

		return view('admin.subscribers.index', $document);
	}

	public function update(Request $request, $id)
	{

		Subscriber::find($id)->update($request->all());

		return back()->with('message', 'Запись обновлена');
	}

	public function destroy($id)
	{
		Subscriber::find($id)->delete();
		return back()->with('message', "Запись удалена");
	}

}
