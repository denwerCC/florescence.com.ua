<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Filter;
use App\Models\Item;
use App\Models\Category;
use App\Models\Page;
use App\Models\ItemFilter;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Config;
use Validator;
use SimpleImage as Image;

class FiltersController extends Controller
{

	public $datacount;
	public $allitems;

	public function __construct()
	{

		$this->allitems = Item::select(['id', 'category_id'])->get();
		$this->datacount = $this->allitems->count();

		$data = [
			'document' => [
				'datacount' => $this->datacount
			],
			'categories' => Category::all()->load('childrens','parent'),
			'allitems' => $this->allitems
		];

		View::share($data);

	}

	public function index()
	{

		$document = [
			'filters' => Filter::whereNull('parent_id')->with('childs')->orderBy('level')->get()
		];
		return view('admin.filters.index', $document);
	}

	public function create()
	{
		return view('admin.filters.create');
	}

	public function store(Request $request)
	{

		$data = array_filter($request->input());
		$data['slug'] = isset($data['slug']) ? tolink($data['slug'],"\.") : tolink($data['title']);
		Filter::create($data);
		return back()->with('message', 'Фильтр добавлен');
	}

	public function edit($id)
	{

		$filter = Filter::find($id);

		foreach (Config::get('app.locales') as $lang => $key) {
			$filters[$lang] = Filter::whereId($id)->translateInto($lang)->first();
		}

		$document = [
			'title' => 'Редактирование параметров',
			'filters' => $filters,
			'Filter' => $filter,
			'allFilters' => Filter::where('id','<>',$filter->id)->pluck('title', 'id')->toArray(),
			'route' => ['admin.filters.update', $id],
			'method' => 'put',
		];

		return view('admin.filters.edit', $document);
	}

	public function update(Request $request, $id)
	{
		$data = $request->all();

		$filter = Filter::find($id);

		$filter->update($data);

		foreach (Config::get('app.locales') as $lang => $key) {
			if (empty($data[$lang]['slug'])) $data[$lang]['slug'] = $data[$lang]['title'];
			$data[$lang]['slug'] = tolink($data[$lang]['slug'],"\.");
			$filter->saveTranslation($lang,$data[$lang]);
		}

		return back()->with('message', 'Параметр обновлен');
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		Filter::find($id)->delete();
		return back()->with('message', "Параметр $id удален");
	}

	public function sort(Request $request)
	{
		$data = $request->all()['data'];

		$this->createLevels($data);
		//$affectedRows = User::where('votes', '>', 100)->update(['status' => 2]);
	}

	private function createLevels($data, $parent_id = false)
	{
		foreach ($data as $level => $it) {
			$item['level'] = $level;

			$item['parent_id'] = $parent_id ? $parent_id : NULL;

			if (!empty($it['children'])) {
				$this->createLevels($it['children'], $it['id']);
			}
			echo "<pre>" . $it['id'];
			print_r($item);
			echo "</pre>";
			Filter::find($it['id'])->update($item);
		}
	}

	public function uploadImage(Request $request, $id)
	{

		$img = $request->file('img');

		$rules = array('photo', 'mimes:jpeg,bmp,png,max:2M'); //mimes:jpeg,bmp,png and for max size max:10000
		// doing the validation, passing post data, rules and the messages
		$validator = Validator::make(['img' => $img], $rules);
		if ($validator->fails()) {
			$error = $validator->errors()->all();
			$response = ['status' => 'error', 'message' => $error];
			return response()->json($response);
		} else {

			//$image = $img->getClientOriginalName();
			$image = $id . '.jpg';

			try {
				Image::make($img)->fit_to_width(150)->save("upload/filters/$image");
			} catch (\Exception $e) {
				$error = $e->getMessage() . ". File: " . $image;
				$response = ['status' => 'error', 'message' => $error];
				return response()->json($response);
			}
			return response()->json(['status' => 'success', 'file' => $image]);

		}

	}

}
