<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class ExelController extends Controller
{
	public function index($entity)
	{
		$entity = ucfirst($entity);
		if ($entity == 'item') {
			$model = $this->exportItems();
		} else if ($entity == 'Order') {
			return $this->exportOrders();
		} else {
			$model = call_user_func(["\App\Models\$entity", 'all']);
		}
		return $this->export($entity, $model);
	}

	public function exportOrders()
	{

		$filename = 'Orders';
		$orders = \App\Models\Order::orderBy('id', 'desc');

		if (request('from')) {
			$orders = $orders->where('created_at', '>=', request('from'));
		}
		if (request('to')) {
			$orders = $orders->where('created_at', '<=', request('to'));
		}

		$orders = $orders->get();

		$items[] = ['№ заказа', 'Доставщик', 'ФИО', 'ID покупателя', 'Телефон', 'e-mail', 'Адрес', 'Статус заказа', 'Комментарий', 'Способ оплаты', 'Дата заказа', 'Название', 'Артикул', 'Цена', 'Подписка', 'Количество'];
		foreach ($orders as $order) {

			$item = $order->toArray();
			$item['created_at'] = substr($item['created_at'], 0, 10);

			if (!$order->items->isEmpty()) {

				foreach ($order->items as $item) {

					$orderdata = [
						$order->id,
						$order->delivery,
						$order->name,
						$order->user_id,
						$order->phone,
						$order->email,
						$order->address,
						$order->statusname,
						$order->note,
						$order->payment_method,
						$order->created_at
					];

					$item = array_only($item->toArray(), ['title', 'sku', 'parameters', 'price', 'count']);
					if ($item['parameters']) {
						$item['parameters'] = implode(", ", $item['parameters']);
					} else {
						$item['parameters'] = '';
					}

					$items[] = $orderdata + $item;
				}
			}

		}

		return \Excel::create($filename, function ($excel) use ($filename, $items) {

			$excel->sheet($filename, function ($sheet) use ($items) {

				$sheet->fromArray($items);

			});

		})->export('xlsx');

	}

	public function exportItems()
	{
		$items = \App\Models\Item::select(['id', 'title', 'category_id', 'price', 'price_old', 'offer', 'slug'])
			->get();

		return $items->map(function ($item, $key) {
			$item->category_id = isset($item->category->title) ? $item->category->title : 'Без категории';
			$item->slug = $item->link;
			return $item;
		});
	}

	public function export($filename, $model)
	{

		return \Excel::create($filename, function ($excel) use ($filename, $model) {

			// Set the title
			//$excel->setTitle('Our new awesome title');

			// Chain the setters
			/*			$excel->setCreator('Maatwebsite')
							->setCompany('Maatwebsite');*/

			// Call them separately
			//$excel->setDescription('A demonstration to change the file properties');

			$excel->sheet($filename, function ($sheet) use ($model) {

				//dd($items);
				$sheet->fromModel($model);
				//$sheet->fromArray($items);

				// style
				// Set black background
				$sheet->row(1, function ($row) {

					// call cell manipulation methods
					$row->setBackground('#EAF6EE')
						//->setFontWeight('bold');

						->setFont(array(
							'family' => 'Calibri',
							'size' => '12',
							'bold' => true
						));

				});

			});

		})
			//->store('xlsx', false, true);
			->export('xlsx');
		//->download('xlsx');
		//return 111;
	}
}
