<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Order;
use View;

class OrdersController extends Controller
{

	public function __construct()
	{

		$this->datacount = Order::count();

		$data = [
			'datacount' => $this->datacount
		];

		View::share('document', $data);

	}

	public function index(Request $request, $status = false)
	{
		$data = $request->all();

		if (!$data) {
			$data['draw'] = 1;
			$data['length'] = 10;
			$data['start'] = 1;
		}

		if (!empty($status)) {
			$items = Order::where('status',$status);
			$datacount = $items->count();
		} else {
			$datacount = $this->datacount;
			$items = Order::whereNotNull('id');
		}
		$items->where('calendar_flag', 0);

		// sort
		$sortcolumns = [
			'id', 'name', 'email', 'created_at', 'amount', 'status', 'payment_status'
		];
		if (isset($data['order'][0]['column'])) {
			$sortcolumn = $sortcolumns[$data['order'][0]['column']];
			$dir = $data['order'][0]['dir'];
		}else{
			$sortcolumn = 'level';
			$dir = 'asc';
		}

		$items = $items->skip($data['start'])
			->take($data['length'])
			->orderBy($sortcolumn,'desc')
			->get();

		$document = [
			'title' => 'Все покупки',
			'Item' => $items
		];

		// Request::ajax()
		if ($request->ajax()) {

			foreach ($items as $n=>$item){
				$items[$n]['statusname'] = $item->statusname;
				$items[$n]['payment_status_value'] = $item->payment_status_value;
			}

			$data = [
				"draw" => $data['draw'],
				"recordsTotal" => $datacount,
				"recordsFiltered" => $datacount,
				"data" => array_values($items->toArray())
			];

			return json_encode($data);
		}

		// todo ad deleting to Cron
		// delete cart rows older 1 year
		$date = strtotime(date('Y-m-d H:i:s') . ' -1 year');
		\App\Models\Cart::where('updated_at','<',date('Y-m-d H:i:s', $date))->delete();

		return view('admin.orders.index', $document);
	}

	public function search(Request $request, $command, $query)
	{
		$data = $request->all();

		switch ($command) {
			case 'in':
				$items = Order::whereIn('id',explode(',',$query))->get();
		}

		if (!$data) {
			$data['draw'] = 1;
			$data['length'] = 10;
			$data['start'] = 1;
		}

		$datacount = $this->datacount;

		$document = [
			'title' => 'Все покупки',
			'Item' => $items
		];

		// Request::ajax()
		if ($request->ajax()) {

			foreach ($items as $n=>$item){
				$items[$n]['statusname'] = $item->statusname;
				$items[$n]['payment_status_value'] = $item->payment_status_value;
			}

			$data = [
				"draw" => $data['draw'],
				"recordsTotal" => $datacount,
				"recordsFiltered" => $datacount,
				"data" => array_values($items->toArray())
			];

			return json_encode($data);
		}

		return view('admin.orders.index', $document);
	}

	public function edit($id)
	{

		$order = Order::findOrfail($id);

		$document = [
			'title' => 'Информация о покупке',
			'order' => $order,
		];
		return view('admin.orders.edit', $document);
	}

	public function update(Request $request, $id)
	{

		$data = $request->input();
	
		$order = Order::find($id);
	
		// change stock
//		if($data['status']==3 and $order->status !=3){
//			$order->items->each(function ($item) {
//				$item->attr->increment('stock');
//			});

//		}elseif($data['status']!=3 and $order->status ==3){
//			$order->items->each(function ($item) {
//				$item->attr->decrement('stock');
//			});
//		}
	
	
		$order->update($data);

		return back()->with('message', 'Запись обновлена');
	}
	
	public function destroy($id)
	{
		Order::findOrFail($id)->delete();
		return back()->with('message', "Запись удалена");
	}

}
