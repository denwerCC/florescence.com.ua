<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Validator;

class CouponsController extends Controller
{

	public function index()
	{
		$coupons = Coupon::all()->sortByDesc('id');

		$document = [
			'title' => 'Купоны',
			'coupons' => $coupons
		];

		return view('admin.coupons.index', $document);
	}

	public function store(Request $request)
	{

		$data = $request->input();

		$messages = array(
			'unique' => 'Поле :attribute должно быть уникальным.',
			'required' => 'Поле :attribute должно быть заполнено.'
		);

		$rules = array(
			'code' => 'required|unique:coupons'
		);

		$validator = Validator::make($data, $rules, $messages);
		$this->validateWith($validator);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}

		if(!$data['expired']){
			unset($data['expired']);
		}

		Coupon::create($data);
		return back()->with('message', 'Запись добавлена');
	}


	public function edit($id)
	{

		$coupon = Coupon::find($id);

		$document = [
			'title' => 'Информация о купоне',
			'Coupon' => $coupon,
		];
		return view('admin.coupons.edit', $document);
	}

	public function update(Request $request, $id)
	{

		Coupon::find($id)->update($request->all());

		return back()->with('message', 'Запись обновлена');
	}

	public function destroy($id)
	{
		Coupon::find($id)->delete();
		return back()->with('message', "Запись удалена");
	}

}
