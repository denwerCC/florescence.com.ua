<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Datatables;
use View;

class CalendarController  extends Controller{

	public function index() {
		return view( 'admin.calendar.calendar', [
			'title' => 'Календар'
		] );
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function anyData() {
		return Datatables::of( Order::query()->orderBy('created_at', 'desc')->where('calendar_flag', '=', 1) )->make( true );
	}
}