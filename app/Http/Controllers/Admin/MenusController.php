<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Menu;
use Config;
use Illuminate\Http\Request;

class MenusController extends Controller
{

	public function index()
	{
		$document = [
			'menus' => Menu::whereNULL('parent_id')->with('childrens')->orderBy('level')->get()
		];

		return view('admin.menus.menus', $document);
	}

	public function create()
	{
		return view('admin.menus.create');
	}

	public function store(Request $request)
	{
		$data = array_filter($request->input());
		if (empty($data['slug'])) $data['slug'] = $data['title'];
		$data['slug'] = tolink($data['slug']);
		Menu::create($data);
		return back()->with('message', 'Категория добавлена');
	}

	public function edit($id)
	{

		$Menu = Menu::find($id);

		foreach (Config::get('app.locales') as $lang => $key) {
			$menus[$lang] = $Menu->translate($lang) ? $Menu->translate($lang) : new Menu;
		}

		$document = [
			'title' => 'Редактирование меню',
			'Menu' => $Menu,
			'menus' => $menus,
			'categories' =>[0=>'Без категории']+  Menu::where('id','<>',$Menu->id)->get()->pluck('title', 'id')->toArray(),
			'route' => ['admin.menus.update', $id],
			'method' => 'put',
		];

		return view('admin.menus.edit', $document);
	}

	public function update(Request $request, $id)
	{
		$data = $request->all();

		$Menu = Menu::find($id);
		$Menu->update($data);

		foreach (Config::get('app.locales') as $lang => $key) {
			if (empty($data[$lang]['slug'])) $data[$lang]['slug'] = $data[$lang]['title'];
			$data[$lang]['slug'] = tolink($data[$lang]['slug']);
			$Menu->saveTranslation($lang, $data[$lang]);
		}

		return back()->with('message', 'Меню обновлено');
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		$Menu = Menu::find($id);
		$Menu->delete();
		return back()->with('message', "Раздел меню " . $Menu->title . " удален");
	}

	public function sort(Request $request)
	{

		$this->createLevels($request->input('data'));

	}

	private function createLevels($data, $parent_id = false)
	{
		foreach ($data as $level => $it) {
			$item['level'] = $level;

			$item['parent_id'] = $parent_id ? $parent_id : NULL;

			if (!empty($it['children'])) {
				$this->createLevels($it['children'], $it['id']);
			}

			Menu::find($it['id'])->update($item);
		}
	}

}
