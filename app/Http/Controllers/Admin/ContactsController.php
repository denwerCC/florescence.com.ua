<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Datatables;
use View;

class ContactsController extends Controller {
	public function index() {
		return view( 'admin.contacts.contacts', [
			'title' => 'Контакти'
		] );
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function anyData() {
		return Datatables::of( Contact::query()->orderBy('created_at', 'desc') )->make( true );
	}

	public function delete( $id ) {
		Contact::findOrFail( $id )->delete();

		return back()->with( 'message', "Запись удалена" );
	}

}
