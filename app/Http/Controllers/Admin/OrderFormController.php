<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class OrderFormController extends Controller
{
    const TYPE = 'order_form';

    public function index(Request $request){

        return view('admin.config.'.self::TYPE, $this->form());
    }

    public function form()
    {
        $form = [
            'label' => 'Текст',
            self::TYPE => config(self::TYPE) ?: $this->getItemArr()
        ];

        $document = [
            'title' => 'Оформление заказа',
            'form' => $form,
            'view' => self::TYPE,
            'type' => self::TYPE,
        ];

        return $document;
    }

    public function getItemArr(){
        $item = [
            'text' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
            'header' => array_fill_keys(array_keys(LaravelLocalization::getSupportedLocales()), ''),
        ];
        return $item;
    }

    public function update(Request $request)
    {
	    $items = $request->get(self::TYPE);
        $this->saveConfig($items, self::TYPE);
        Artisan::call('config:clear');
        Artisan::call('cache:clear');
        return back()->with('message', l('Конфигурация обновлена'));
    }

    public function saveConfig($data, $filename)
    {
        $config = "<?php\n\nreturn ";
        //$data = array_filter($data);
        $config .= var_export($data, true);
        $config .= ';';
        File::put(config_path("{$filename}.php"), $config);
    }

}