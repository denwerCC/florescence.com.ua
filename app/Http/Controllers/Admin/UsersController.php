<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use View;

class UsersController extends Controller
{
	public function __construct()
	{

		// we can use middleware in secured controllers
		//$this->middleware('auth');

		$data = [
			'user' => Auth::user(),
			'pagescount' => ''
		];

		View::share('document', $data);

	}

	public function index(Request $request)
	{

		$data = $request->all();

		if(!$data){
			$data['draw'] = 1;
			$data['length'] = 1;
			$data['start'] = 1;
		}

		// sort
		$sortcolumns = [
			'id', 'name', 'email', 'created_at'
		];
		if (isset($data['order'][0]['column'])) {
			$sortcolumn = $sortcolumns[$data['order'][0]['column']];
			$dir = $data['order'][0]['dir'];
		}else{
			$sortcolumn = 'id';
			$dir = 'desc';
		}

		$models = User::where('id','>','1')
			->skip($data['start'])
			->take($data['length'])
			->orderBy($sortcolumn,$dir)
			->get();

		$datacount = User::count() - 1;

		$document = [
			'title' => 'Все пользователи'
		];

		$mod = array();

		foreach ($models as $n => $Models) {
			$models[$n]->thumb = $Models->thumb;
			$mod[] = $models[$n];
		};

		// Request::ajax()
		if ($request->ajax()) {

			$data = [
				"draw" => $data['draw'],
				"recordsTotal" => $datacount,
				"recordsFiltered" => $datacount,
				"data" => $mod
			];

			return json_encode($data);
		}

		return view('admin.users.users', $document);
	}

	public function show($id){

		$model = User::findOrFail($id);

		$document = [
			'title' => $model->name,
			'model' => $model,
		];
		return view('admin.users.show', $document);
	}

	public function activate($id)
	{

		$user = User::find($id);

		if($user->active==1){
			$user->active = 0;
			$state = 'деактивирован';
		}else{
			$user->active = 1;
			$state = 'активирован';
		}

		$user->save();

		return back()->with('message', "Пользователь " . $user->name . " ". $user->last_name . " $state");

	}

	/**
	 * @param $id
	 */
	public function delete($id)
	{
		return $this->destroy($id);
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		$User = User::find($id);

		$User->delete();
		return back()->with('message', "Пользователь " . $User->name . " ". $User->last_name . " удален");
	}
}
