<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Subscriber;
use App\User;

class MailingController extends Controller
{
	//
	public function index()
	{

		$document = [
			'title' => 'Рассылка E-mail',
			'targets' => [
				'subscribers' => 'Подписчики (' . count($this->getTargets('subscribers')) . ')',
				'buyers' => 'Покупатели ' . count($this->getTargets('buyers')) . ')',
				'user' => 'Зарег. пользователи (' . count($this->getTargets('user')) . ')',
				'mailbook' => '<a href="' . route('admin.mailbook') . '">Адресная книга (' . count($this->getTargets('mailbook')) . ')</a> ',
			]
		];

		return view('admin.mailing.index', $document);
	}

	public function getTargets($type)
	{

		if ($type == 'subscribers') return Subscriber::select('email')->get();
		if ($type == 'buyers') return Order::select('email')->get();
		if ($type == 'user') return User::select('email')->get();
		if ($type == 'mailbook') return config('mailbook');

		return [];
	}

	public function update()
	{

		if(empty(request()->email['target'])) return back()->withErrors('Необходимо выбрать получателей')->withInput();

		$emails = [];

		foreach ($this->getTargets(request()->email['target']) as $email) {

			if(isset($email->email)){
				$curMail = $email->email;
			}else{
				$curMail =  $email;
			}

			if (filter_var($curMail, FILTER_VALIDATE_EMAIL)) {
				$emails[] = $curMail;
			}

		}

		if(!$emails) return back()->withErrors('Не обнаружено адресов в правильном формате. Письмо не отправлено')->withInput();

		\Mail::send([], [], function ($message) use ($emails) {
			$message->setBody(request()->email['text'], 'text/html');
			$message->subject(request()->email['subject']);
			$message->from('mail@' . request()->getHost(), 'Интернет-магазин ' . request()->getHost());
			$message->bcc($emails);
		});

		//var_dump( Mail:: failures());
		return back()->with('message', 'Письма отправлены: ' . count($emails) . " получателей.")->withInput();
	}

	public function mailbook()
	{

		$document = [
			'title' => 'Адресная книга',
			'emails' => implode("\r\n", config('mailbook'))
		];
		return view('admin.mailing.mailbook', $document);
	}

	public
	function mailbookUpdate()
	{
		$data = explode("\r\n", request()->emails);
		(new ConfigController())->saveConfig($data, 'mailbook');
		return back()->with('message', 'Записи сохранены');
	}

}
