<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Rubric;
use Config;
use File;
use Illuminate\Http\Request;
use SimpleImage as Image;
use Validator;
use View;

class PagesController extends Controller
{
    
    use \App\Traits\Image;
    
    public $pages;
    public $allpages;
    public $pagescount;
    public $categories;
    
    public function __construct()
    {
        
        // we can use middleware in secured controllers
        //$this->middleware('auth');
        $this->allpages = Page::select(['id', 'category_id'])->get();
        $this->pagescount = $this->allpages->count();
        $this->categories = Rubric::whereNull('parent_id')->get();
        
        $data = [
            'document' => [
                'pagescount' => $this->pagescount,
            ],
            'categories' => $this->categories,
            'allpages' => $this->allpages
        ];
        
        View::share($data);
        
    }
    
    public function index(Request $request, $rubric_id = false)
    {
        $data = $request->all();
        
        if (!$data) {
            $data['draw'] = 1;
            $data['length'] = 1;
            $data['start'] = 0;
        }
        
        if ($search = $request->search['value']) {
            
            $pages = Page::where('title', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%')
                ->orWhere('slug', 'like', '%' . $search . '%')
                ->orWhere('content', 'like', '%' . $search . '%');
            
            $this->pagescount = $pages->count();
            
        } else {
            if (is_numeric($rubric_id)) {
                $this->pagescount = $this->allpages->where('category_id', $rubric_id)->count();
                $pages = Page::where('category_id', $rubric_id);
                
            } else {
                $pages = Page::whereNotNull('id');
            }
        }
        
        // sort
        $sortcolumns = [
            'id',
            'img',
            'title',
            'slug',
            'created_at'
        ];
        if (isset($data['order'][0]['column'])) {
            $sortcolumn = $sortcolumns[$data['order'][0]['column']];
            $dir = $data['order'][0]['dir'];
        } else {
            $sortcolumn = 'level';
            $dir = 'asc';
        }
        
        $pages = $pages
            ->with('rubric')
            ->skip($data['start'])
            ->take($data['length'])
            ->orderBy($sortcolumn, $dir)->get();
        
        $document = [
            'title' => $rubric_id ? Rubric::findOrFail($rubric_id)->title : 'Все страницы',
            'pages' => $pages
        ];
        
        if ($request->ajax()) {
            $page = [];
            foreach ($pages as $n => $cpage) {
                $cpage->thumb = $cpage->thumb;
                $cpage["rubricTitle"] = $cpage->rubric ? $cpage->rubric->title : '';
                $cpage["DT_RowId"] = $cpage->id;
                $page[] = $cpage;
            };
            
            $data = [
                "draw" => $data['draw'],
                "recordsTotal" => $this->pagescount,
                "recordsFiltered" => $this->pagescount,
                "data" => $page
            ];
            
            return json_encode($data);
        }
        
        return view('admin.pages.pages', $document);
    }
    
    public function editView($id)
    {
        
        $locale = \App::getLocale();
        $page = Page::findOrFail($id);
        
        $view = $page->slug;
        $file = false;
        
        $data = [
            'page' => $page,
            //'otherdata': ...,
        ];
        
        //if (!$file) return back()->withErrors(["Шаблон $view не знайдено"]);
        
        $data['content'] = (new App\Http\Controllers\PagesController)->show($view)->render();
        return view('admin.pages.landing', $data)->with(['view' => $page->slug]);
    }
    
    public function clearView($id, $lang)
    {
        $page = Page::findOrFail($id);
        
        $view = $page->slug;
        
        if (is_file(resource_path("views") . "/landing/$view-$lang.blade.php")) {
            File::delete(resource_path("views") . "/landing/$view-$lang.blade.php");
            $message = "Шаблон $view-$lang видалено<br>";
        } else {
            $message = l('Шаблонів не знайдено');
        }
        return back()->with('message', $message);
    }
    
    public function saveView(Request $request, $view)
    {
        $data = $request->get('content');
        $locale = \App::getLocale();
        
        if (is_file(resource_path("views") . "/landing/$view-$locale.blade.php")) {
            $file = resource_path("views") . "/landing/$view-$locale.blade.php";
        } else {
            $file = resource_path("views") . "/$view.blade.php";
        }
        
        $content = File::get($file);
        
        preg_match_all("|.fldata.*>(.*)<[^>]*><!-- end -->|sU", $content, $mtch);
        
        $data = array_map(function ($text) {
            return preg_replace("|</?p>|", "", $text);
        }, $data);
        
        if (count($mtch[1]) != count($data)) {
            $content = "Дані не збережено! Шаблон містить помилки. Перезавантажте сторінку і спробуйте ще раз.";
            return response($content);
        }
        
        foreach ($mtch[1] as $number => $value) {
            $content = str_replace($value, $data[$number], $content);
        }
        
        File::put(resource_path("views") . "/landing/$view-$locale.blade.php", $content);
        
        echo "Saved";
        
    }
    
    public function edit($id)
    {
        
        $page = Page::findOrFail($id);
        
        $pages = array();
        foreach (Config::get('app.locales') as $lang => $key) {
            $pages[$lang] = $page->translate($lang);
            if (!$pages[$lang]) {
                $pages[$lang] = new Page();
            }
        }
        
        $document = [
            'title' => 'Редактирование страницы',
            'pages' => $pages,
            'route' => ['admin.pages.update', $id],
            'method' => 'put',
            'Page' => $page,
            'RubricTree' => $this->getRubricTree()
        ];
        
        return view('admin.pages.edit', $document);
    }
    
    public function getRubricTree()
    {
        
        $RubricTree[] = 'Без категории';
        
        foreach ($this->categories as $Rubric) {
            
            if (!$Rubric->childrens->isEmpty()) {
                
                $subTree = '';
                foreach ($Rubric->childrens as $item) {
                    $subTree[$item->id] = $item->title;
                }
                
                $RubricTree[$Rubric->title] = $subTree;
                
            } else {
                $RubricTree[$Rubric->id] = $Rubric->title;
            }
            
        }
        
        return $RubricTree;
    }
    
    public function create()
    {
        
        $page = new Page();
        foreach (Config::get('app.locales') as $lang => $key) {
            //$pages[$lang] = $page->translate($lang);
            $pages[$lang] = $page;
        }
        
        $page->public = 1;
        $page->date = \Carbon\Carbon::now();
        
        $document = [
            'title' => 'Создание страницы',
            'Page' => $page,
            'pages' => $pages,
            'route' => 'admin.pages.store',
            'method' => 'post',
            'RubricTree' => $this->getRubricTree()
        ];
        return view('admin.pages.edit', $document);
    }
    
    public function store(Request $request)
    {
        $data = $request->all();
        
        if ($data['public'] == 'on') {
            $data['public'] = 1;
        }
        $data['level'] = Page::max('level') + 1;
        $page = Page::create(array_except($data, 'imgs'));
        
        if ($request->hasFile('img')) {
            $page->img = $this->addImage($request->file('img'), $page);
            $page->save();
        }
        
        if (!empty($data['imgs'])) {
            $this->addImages($data['imgs'], $page->id);
            $page->imgs = json_encode($data['imgs']);
            $page->save();
        }
        
        // translate
        foreach (Config::get('app.locales') as $lang => $key) {
            if (empty($data[$lang]['slug'])) {
                $data[$lang]['slug'] = $data[$lang]['title'];
            }
            if (empty($data[$lang]['slug'])) {
                $data[$lang]['slug'] = $page->id;
            }
            $data[$lang]['slug'] = tolink($data[$lang]['slug']);
            
            $page->saveTranslation($lang, $data[$lang]);
        }
        
        return redirect(route('admin.pages.edit', $page->id))->with('message', 'Страница добавлена');
    }
    
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $page = Page::find($id);
        if ($request->hasFile('img')) {
            $data['img'] = $this->addImage($request->file('img'), $page);
        }

        // dd($data);
        
        // images
        if (empty($data['imgs'])) {
            $data['imgs'] = array();
        }
        $this->addImages($data['imgs'], $id);
        $data['imgs'] = json_encode($data['imgs']);

        if (empty($request->public)) {
            $data['public'] = 0;
        } else {
            $data['public'] = 1;
        }
        $page->update($data);
        
        // translate
        foreach (Config::get('app.locales') as $lang => $key) {
            
            if (empty($data[$lang]['slug'])) {
                $data[$lang]['slug'] = $data[$lang]['title'];
            }
            $data[$lang]['slug'] = tolink($data[$lang]['slug']);
            /*			\DB::table('pages_translations')
                            ->where('page_id', $page->id)
                            ->where('locale', $lang)
                            ->update($data[$lang]);*/
            //dd(111);
            $page->saveTranslation($lang, $data[$lang]);
        }
        
        return back()->with('message', l('Страница обновлена'));
    }
    
    public function publicate($id)
    {
        $page = Page::find($id);
        if ($page->public == 1) {
            $page->public = 0;
            $statustext = 'скрыта';
        } else {
            $page->public = 1;
            $statustext = 'опубликована';
        }
        $page->update();
        
        return back()->with('message', "Страница " . $page->title . " $statustext");
        
    }
    
    
    public function addImage($img, $page)
    {
        $image = array('img' => $img);
        $rules = array('img' => 'image|max:' . get_max_file_size());
        $validator = Validator::make($image, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return $validator->errors()->all();
        } else {
            
            $image = $page->id . ".jpg";
            
            //clear if there are previous images
            $page->deleteImg($image);
            
            $img = Image::make($img);
            $img->best_fit(1000, 1600)->save($page->dir . "big/" . $image);
            return $image;
        }
    }
    
    public function addImages($imgs, $id)
    {
        
        $temp_path = public_path("upload/tmp/");
        $watermark = false;
        $dest = "upload/pages/$id/big/";
        
        foreach (File::files($temp_path) as $img) {
            $img = File::basename($img);
            if (!array_key_exists($img, $imgs)) {
                continue;
            }
            File::makeDirectory($dest, 493, true, true);
            File::copy($temp_path . $img, $dest . $img);
            
        }
        
        File::cleanDirectory($temp_path);
    }
    
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $page = Page::find($id);
        File::deleteDirectory($page->dir);
        $page->delete();
        return result("Страница " . $page->title . " удалена");
    }
    
    public function deleteImg($id, $image)
    {
        
        $Model = Page::find($id);
        // del from db
        $images = json_decode($Model->imgs, 1);
        
        unset($images[$image]);
        
        $Model->imgs = json_encode($images);
        
        $Model->update();
        
        foreach (scandir($Model->dir) as $dir) {
            if ($dir == '.' or $dir == '..') {
                continue;
            }
            if (is_file($Model->dir . $dir . "/" . $image)) {
                unlink($Model->dir . $dir . "/" . $image);
            }
        }
        
        return back()->withMessage('Image successfully deleted');
    }
    
    public function reorder()
    {
        foreach (Page::all()->groupBy('category_id') as $category) {
            foreach ($category as $level => $page) {
                $page->update(['level' => $level]);
            }
        }
    }
    
    public function sort(Request $request)
    {
        
        foreach ($request->get('sort') as $row) {
            Page::whereId($row['id'])->update($row);
        }
        
        return 'ok';
    }
    
}
