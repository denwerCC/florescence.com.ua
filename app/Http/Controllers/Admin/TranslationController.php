<?php

namespace App\Http\Controllers\Admin;

use Config;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Lang;

class TranslationController extends Controller
{
	public function index()
	{
		Lang::setFallback(false);
		$locale_default = Config::get('app.locale_default');

		$tranlationdata = $this->getTranslationData();
		$translation = array();
		foreach ($tranlationdata as $langstring) {
			$langstring = trim($langstring);
			$translation[$langstring] = $langstring;
		}
		
		$translationApp = Lang::get('app', [], $locale_default);
		
		if(!is_array($translationApp)){
            $translationApp = [];
        }

		// default translation
		$translArr = array_merge($translation, $translationApp);
		asort($translArr);
		$progress = [];
		foreach (Config::get('app.locales') as $lang => $title) {
			$transl = Lang::get('app', [], $lang);
			if ($transl == 'app') {
				$transl = [];
			}
			$progressdata[$lang] = count(array_filter($transl));

			foreach ($translArr as $key=>$item) {
				if ($lang == $locale_default) {
					$translated[$key][$lang] = isset($transl[$key]) ? $transl[$key] : $key;
				} else {
					$translated[$key][$lang] = isset($transl[$key]) ? $transl[$key] : '';
				}

			}

		}

		// count untranslated words
		$progressdata[$locale_default] = count(array_filter($translArr));
		$progress['data'] = $progressdata;
		$allcount = 0;
		foreach ($progressdata as $lang => $count) {
			$progress['pers'][$lang] = ceil($count/$progressdata[$locale_default]*100);
			$allcount += $count;
		}

		$progress['all'] = floor($allcount*100/$progressdata[$locale_default]/count(Config::get('app.locales')));
		$progress['allcount'] = $allcount;

		$document = [
			'translation' => $translated,
			'progress' => $progress
		];
		return view('admin.translation.index', $document);
	}

	public function getTranslationData()
	{
		$files = \File::allFiles(resource_path("views"));
		$data = [];
		foreach ($files as $file){
			preg_match_all("/{{l\('(.*)'\)}}|{{l\(\"(.*)\"\)}}|l\('(.*)'\)/sU",file_get_contents($file->getPathname()),$mtch);
			$data = array_merge($data,$mtch[1]);
		}

		return array_filter(array_unique($data));
	}

	public function store(Request $request)
	{

		foreach ($request->get('translation') as $lang => $data) {

			$data = array_map(function ($text) {
				$text = str_replace('"', "“", $text);
				return str_replace("'", "’", $text);
			}, $data);
			$this->saveLang($data, $lang);

		}

		return back()->with('message', 'Конфигурация обновлена');
	}

	public function saveLang($data, $lang)
	{
		$langdata = "<?php\n\nreturn [\n";
		foreach ($data as $key => $value) {
			$langdata .= "    '$key' => '" . htmlspecialchars(trim($value), ENT_QUOTES) . "',\n";
		}

		$langdata .= '];';
		File::put(resource_path("lang/" . $lang . "/app.php"), $langdata);
	}
}
