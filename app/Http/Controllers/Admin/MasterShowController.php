<?php

namespace App\Http\Controllers\Admin;



use App\Models\MSSubscribers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MasterShowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_show_subs = MSSubscribers::all();

	    if (request()->ajax()) {

		    $data = [
			    //"draw" => 1,
			    "recordsTotal" => $master_show_subs->count(),
			    "recordsFiltered" => $master_show_subs->count(),
			    "data" => $master_show_subs
		    ];

		    return response()->json($data);
	    }

        return view('admin.master-show.master-show', ['subs' => $master_show_subs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscriber = MSSubscribers::where('id', $id);
        if(!is_null($subscriber)){
        	$subscriber->delete();
        }
        return back()->with('message', 'Успішно видалено');
    }
}
