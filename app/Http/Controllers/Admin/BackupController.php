<?php

namespace App\Http\Controllers\Admin;

use Barryvdh\Reflection\DocBlock\Type\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackupController extends Controller
{

	public function index(){

		$files = \File::allfiles(storage_path('dumps'));
		$items = [];
		foreach ($files as $file){

			$item['name'] = $file->getFilename();
			$item['size'] = round($file->getSize()/1024/1000, 3);
			$item['name'] = $file->getFilename();
			$item['date'] = $this->dateFormat($item['name']);

			$items[] = $item;
		}

		$files = collect($items)->sortByDesc('name');
		return view('admin.backups.index',['files'=>$files]);
	}

	private function dateFormat($filename){
		return preg_replace("|(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(.*)|","$1.$2.$3 $4:$5:$6",$filename);
	}

	public function create(){
		\Artisan::call('db:backup');
		return back()->with('message',l('Резервная копия успешно создана'));
	}

	public function restore($file){
		\Artisan::call('db:restore',['dump'=>$file]);
		return back()->with('message',l('Резервная копия успешно восстановлена'));
	}

	public function delete($file)
	{
		\File::delete(storage_path('dumps'). "/" .$file);
		return result('Резервная копия успешно удалена');
	}
}
