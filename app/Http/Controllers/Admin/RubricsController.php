<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Rubric;
use SimpleImage as Image;
use Config;
use Illuminate\Http\Request;

class RubricsController extends Controller
{

	public function index()
	{

		$document = [
			'rubrics' => Rubric::whereNull('parent_id')->get()
		];

		return view('admin.rubrics.index', $document);
	}

	public function create()
	{
		return view('admin.rubrics.create');
	}

	public function store(Request $request)
	{

		$data = $request->input();

		$data['slug'] = isset($data['slug']) ? tolink($data['slug']) : tolink($data['title']);
		$data['parent_id'] = !empty($data['parent_id']) ? $data['parent_id'] : null;

		$rubric = Rubric::create($data);

		if ($request->hasFile('img')) {
			$rubric->img = $this->addImage($request->file('img'), $rubric);
			$rubric->save();
		}

		return back()->with('message', 'Категория добавлена');
	}

	public function edit($id)
	{

		$rubric = Rubric::find($id);

//		foreach (Config::get('app.locales') as $lang => $key) {
//			$rubrics[$lang] = $rubric->translate($lang);
//		}
		$rubrics = array();
		foreach (Config::get('app.locales') as $lang => $key) {
			$rubrics[$lang] = $rubric->translate($lang);
			if(!$rubrics[$lang]) $rubrics[$lang] = new Rubric();
		}

		$document = [
			'title' => 'Редактирование категории',
			'rubrics' => $rubrics,
			'Rubric' => $rubric,
			'Rubrics' => [null => 'Без категории'] + array_except(Rubric::all()->sortBy('title')->pluck('title', 'id')->toArray(), $rubric->id),
			'route' => ['admin.rubrics.update', $id],
			'method' => 'put',
		];

		return view('admin.rubrics.edit', $document);
	}

	public function update(Request $request, $id)
	{
		$data = $request->all();

		$rubric = Rubric::find($id);
		$data['parent_id'] = $data['parent_id'] ? $data['parent_id'] : NULL;

		if ($request->hasFile('img')) {
			$data['img'] = $this->addImage($request->file('img'), $rubric);
		}
		if(isset($data['video_bg'])){
			$data['video_bg'] = trim($data['video_bg']);
			$data['video_bg'] = trim($data['video_bg'],'/');
		}

		$rubric->update($data);

		foreach (Config::get('app.locales') as $lang => $key) {
			if (empty($data[$lang]['slug'])) $data[$lang]['slug'] = $data[$lang]['title'];
			$data[$lang]['slug'] = tolink($data[$lang]['slug']);

			$rubric->saveTranslation($lang, $data[$lang]);

//            echo '<pre>';
//            var_dump($lang);
//            var_dump($rubric);
//		    echo '</pre>';
//            exit;
		}

		return back()->with('message', 'Категория обновлена');
	}


	public function destroy($id)
	{
		$rubric = Rubric::find($id);
		File::deleteDirectory($rubric->dir);
		$rubric->delete();
		return back()->with('message', "Категория " . $rubric->title . " удалена");
	}

	public function sort(Request $request)
	{

		$this->createLevels($request->input('data'));

	}

	private function createLevels($data, $parent_id = false)
	{
		foreach ($data as $level => $it) {
			$item['level'] = $level;

			$item['parent_id'] = $parent_id ? $parent_id : NULL;

			if (!empty($it['children'])) {
				$this->createLevels($it['children'], $it['id']);
			}

			rubric::find($it['id'])->update($item);
		}
	}

	public function addImage($img, $rubric)
	{

		// getting all of the post data
		$image = array('img' => $img);
		// $f_name = $img->getClientOriginalName();
		// setting up rules
		$rules = array('img' => 'mimes:jpeg,bmp,png,max:' . get_max_file_size()); //mimes:jpeg,bmp,png and for max size max:10000
		// doing the validation, passing post data, rules and the messages
		$validator = \Validator::make($image, $rules);
		if ($validator->fails()) {
			// send back to the page with the input data and errors
			return $validator->errors()->all();
		} else {

			$image = $rubric->id . ".jpg";

			//clear if there are previous images
			$rubric->deleteImg($image);

			$img = Image::make($img);
			$img->best_fit(1000, 1600)->save($rubric->dir . "big/" . $image)->crop(120, 120)->save($rubric->dir . "small/" . $image);
			return $image;
		}

	}

}
