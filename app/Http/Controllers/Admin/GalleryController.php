<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Gallery;
use DB;
use Illuminate\Http\Request;
use SimpleImage as Image;
use Validator;

class galleryController extends Controller
{

	public function index()
	{

		$document = [
			'images' => Gallery::all()->sortByDesc('level')
		];

		return view('admin.gallery.gallery', $document);
	}

	public function delimage($id)
	{
		$image = Gallery::find($id);
		$image->delete();
		\File::deleteDirectory('upload/gallery/' . $id);
		if (request()->ajax()) {
			return 'ok';
		}

		return back()->with('message', 'Изображение ' . $image->img . ' удалено');
	}

	public function addData(Request $request, $id)
	{

		Gallery::find($id)->update($request->input());

		if (request()->ajax()) {

		}
		return 'Данные сохранены';
	}

	public function sort(Request $request)
	{
		$data = array_reverse($request->data);
		foreach ($data as $level => $id) {
			Gallery::find($id)->update(array("level" => $level));
		}
		return 'Позиции успешно обновлены!';
	}

	public function upload(Request $request)
	{

		$img = $request->file('img');

		$rules = array('photo', 'mimes:jpeg,bmp,png,max:' . get_max_file_size()); //mimes:jpeg,bmp,png and for max size max:10000
		// doing the validation, passing post data, rules and the messages
		$validator = Validator::make(['img' => $img], $rules);
		if ($validator->fails()) {
			$error = $validator->errors()->all();
			$response = ['status' => 'error', 'message' => $error];
			return response()->json($response);
		} else {

			$image = tolink($img->getClientOriginalName(), "\.");

			try {

				DB::beginTransaction();
				$gallery = Gallery::create(['img' => $image, 'title' => ' ']); // empty title -> force to write data in translation table. If no - we cant delete the row in future
				Image::make($img)
					//->fit_to_width(1000)
					->crop(1034, 478)
					->save("upload/gallery/" . $gallery->id . "/big-$image")
					->thumbnail(180)
					->save("upload/gallery/" . $gallery->id . "/thumb-$image");

			} catch (\Exception $e) {

				DB::rollBack();
				$error = $e->getMessage() . ". File: " . $image;
				$response = ['status' => 'error', 'message' => $error];
				return response()->json($response);
			} finally {
				DB::commit();
			}

			return response()->json(['status' => 'success', 'file' => $image]);

		}

	}

}
