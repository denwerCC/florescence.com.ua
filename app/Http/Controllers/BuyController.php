<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Filter;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItems;
use Cookie;
use Illuminate\Http\Request;
use Validator;

class BuyController extends Controller
{

	public function buyOneClick(Request $request){
		$this->validate($request,[
			'name' => 'required',
			'phone' => 'required',
			'product_id' => 'required|integer|min:1',
			'product_count' => 'required|integer|min:1',
		]);

		$data = array();
		$product = Item::find($request->get('product_id'));
		$count = $request->get('product_count');
		$data['name'] = $request->get('name');
		$data['phone'] = $request->get('phone');
		$data['ip'] = $request->ip();
		$data['code'] = uniqid();
		$data['amount'] = $product->price * $count;
		$data['lang'] = \App::getLocale();
		$data['level'] = 'BuyOneClick';
		$data['note'] = 'Покупка в один клик';
		$order = Order::create($data);
		OrderItems::create(array(
			'order_id' => $order->id,
			'item_id' => $product->id,
			'title' => $product->title,
			'price' => $product->price,
			'count' => $count,
		));

		(new \App\Http\Controllers\EmailController)->newOrderToAdmin($order);

		return back()->with('message', trans('new_app.Замовлення в 1 клік') . ' №' . $order->id);
		//dd($request);
	}
}