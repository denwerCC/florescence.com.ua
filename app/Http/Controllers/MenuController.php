<?php

namespace App\Http\Controllers;

use Menu;

class MenuController extends Controller
{

    public function show($name, $attr = false)
    {

        if (!Menu::get($name)) {

            Menu::make($name, function ($menu) use ($name,$attr) {

                if (is_numeric($name)) {
                    $data = $this->getMenu($name);
                } else {
                    $data = $this->{$name}();
                }

                $listAttr = [];
                if(isset($attr['li'])){
                    $listAttr = $attr['li'];
                }

                foreach ($data['menu'] as $list) {

                    $menu->add($list->title, route($data['route'], $list->slug))->nickname($list->id)->attr($listAttr);

                    if (!$list->childrens->load('childrens')->isEmpty()) {
                        $parent_id = $list->id;
                        foreach ($list->childrens->sortBy('level') as $list) {
                            $menu->item($parent_id)->add($list->title,
                                route($data['route'], $list->slug))->nickname($list->id);
                            if (!$list->childrens->load('childrens')->isEmpty()) {
                                $parent_id2 = $list->id;
                                foreach ($list->childrens->sortBy('level') as $list) {
                                    $menu->item($parent_id2)->add($list->title, route($data['route'], $list->slug));
                                }
                            }
                        }
                    }

                }

            });
        }

        if ($attr and empty($attr['level'])) {
            unset($attr['li']);
            return Menu::get($name)->asUl($attr);
        }

        return $this->render(Menu::get($name)->roots(), $attr);

    }

    public function getMenu($id)
    {

        $menu = \App\Models\Menu::where('parent_id', $id)->with('childrens')->orderBy('level')->get();
        return ['menu' => $menu, 'route' => 'page'];

    }

    public function catalog()
    {

        $menu = \App\Models\Category::whereNull('parent_id')->with('childrens')->orderBy('level')->get();

        return ['menu' => $menu, 'route' => 'catalog.category'];

    }

    public function render($items, $attr)
    {

        $menu = '';

        foreach ($items as $item) {

            if ($item->isActive) {
                $menu .= "<li class='active'>";
            } else {
                $menu .= "<li>";
            }

            $menu .= "<a href=\"" . $item->url() . "\">" . $item->title . "</a>";
            if ($attr['level'] != 1 and $item->hasChildren()) {
                $menu .= "<ul class='sub_menu'>";
                $menu .= $this->render($item->children(), $attr);
                $menu .= "</ul>";
            }

            $menu .= "</li>";
        }

        return $menu;
    }
}
