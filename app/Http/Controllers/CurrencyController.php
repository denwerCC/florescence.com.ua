<?php
/*
/*Usage in model:
public function getCurrencyAttribute()
{
    return CurrencyController::getCurrencyIndex();
}
	public function getPriceFormattedAttribute()
{
    return round($this->price/CurrencyController::getCurrency());
}
*/

namespace App\Http\Controllers;

use App;
use Cookie;

class CurrencyController extends Controller
{
    
    static $currency;
    static $rate;
    static $exchange;
    static $index = array('uah' => "UAH", 'usd' => "USD", 'eur' => "EUR");
    static $default_index = "uah";
    static $base_index = "uah"; // main currency form bank
    static $current_index;
    
    static function getCurrencyIndex()
    {
        $currency = request()->cookie('currency');
        if (App::getLocale() == 'en') {  // automaticly changes to usd for en
            if (!$currency) {
                $currency = "usd";
            }
        }
    
        self::$current_index = $currency ? $currency : self::$default_index;
        self::$currency = isset(self::$index[self::$current_index]) ? self::$index[self::$current_index] : self::$index[self::$default_index];
        return self::$currency;
    }
    
    static function setCurrency()
    {
        if (self::$rate) {
            return self::$rate;
        }
        
        if (!isset(self::$exchange[self::$default_index])) {
            return false;
        }

        self::getCurrencyIndex();
        
        if (self::$default_index != self::$base_index) {
            $current = self::getCurrency();
            self::$rate = 1 * self::$exchange[self::$default_index] / $current;
        } else {
            if (self::$current_index == self::$default_index) {
                self::$rate = 1;
            } else {
                if (!self::$rate = self::getCurrency()) {
                    self::$rate = 1;
                }
            }
        }
        
        return self::$rate;
    }
    
    static public function changeCurrency($amount, $from, $to)
    {
        
        $from = strtolower($from);
        $to = strtolower($to);
        
        if ($from == $to) {
            return $amount;
        }
        
        // from default currency to other
        if ($from == self::$default_index) {
            return $amount / self::getCurrency($to);
        }
        
        // from other to default currency
        if ($to == self::$default_index) {
            return $amount * self::getCurrency($from);
        }
        
        // from other to other throght default currency
        return $amount * self::getCurrency($from) / self::getCurrency($to);
        
    }
    
    static public function getCurrency($index = false)
    {
        self::getCurrencyIndex();
        if (!$index) {
            $index = self::$current_index;
        }

        if (!config('currency.updated')
//            or config('currency.updated') < time() - (4 * 60 * 60)
        ) { // update every 4 hours
            $json = file_get_contents('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');
            $obj = json_decode($json);


            //dd($obj);
            // https://www.liqpay.com/exchanges/exchanges.cgi
	        $exchange = [];
            foreach ($obj as $obj_arr){
                if(isset($obj_arr->ccy) && (strtolower($obj_arr->ccy) == 'usd') && isset($obj_arr->sale)){
	                $exchange['usd'] = $obj_arr->sale;
                }
                if(isset($obj_arr->ccy) && (strtolower($obj_arr->ccy) == 'eur') && isset($obj_arr->sale)){
	                $exchange['eur'] = $obj_arr->sale;
                }
            }
//            $exchange['usd'] = $obj[2]->sale;
//            $exchange['eur'] = $obj[0]->sale;

            self::saveConfig(['updated' => time(), 'sale' => $exchange], 'currency');
        }
        
        $exchange = config('currency.sale');
        $exchange['uah'] = 1;
        return $exchange[$index];
    }
    
    public function update($currency)
    {
        //Обязательно пересчитываем корзину после смены валюты, для совпадения значений сумм
        CartController::recalCartIfChangeCurrency();
        return back()->withCookie(cookie()->forever('currency', $currency));
    }
    
    static public function change()
    {
        //Для определения валюты по умолчанию в приложении
        self::getCurrencyIndex();
        self::setCurrency();
        $html = '';
        foreach (self::$index as $in => $name) {

            $selected = "";
            if (self::$current_index == $in) {
                $selected = " class='selected' ";
            }
            $html .= "<a rel='nofollow' href='" . route('currency.change', $in) . "' $selected>$name</a> ";
        }
        
        return $html;
        ?>
        <!--        <form name="currency" action="/shop/change_currency.php" method='post'>
            <select class="form-control" name='new_curr'>
				<?php/*
				foreach ($config_money_ind_arr as $in => $name) {

					$selected = "";
					if ($curr_money_ind == $in) {
						$selected = " selected";
					}
					echo "<option $selected value='$in'>$name</option>";
				}
				*/
        ?>
            </select>
        </form>-->
        <?php
    }
    
    static public function saveConfig($data, $configname)
    {
        $config = "<?php\n\nreturn ";
        $data = array_filter($data);
        $config .= var_export($data, true);
        $config .= ';';
        \File::put(config_path("$configname.php"), $config);
    }
    
}
