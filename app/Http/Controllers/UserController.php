<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{

	public function create($user)
	{

		if (empty($user['password'])) {
			$password = str_random(6);
		}

		$user['password'] = bcrypt($password);

		try {
			$user = User::create($user);
		} catch (\Exception $e) {
			if ($e->getCode() == 23000) {
				flashError('Пользователь с таким E-mail уже присутствует');
				return false;
			}

			flashError($e->getMessage());
			return false;

		}

		(new \App\Http\Controllers\EmailController)->confirmRegistration($user, $password);

		return $user;
	}

	public function update(Request $request)
	{

		$user = Auth::user();

		$data = $request->input();

		if (isset($data['password'])) {
			$data['password'] = \Hash::make($request->get('password'));
		}

		$user->update($data);

		return back()->with('message', 'Пользовательские данные изменены');
	}

}
