<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Http\Request;

class PhotosController extends Controller
{
    public function upload(Request $request){
    	$this->validate($request,[
    		'file' => 'required|image|mimes:jpg,jpeg,png|max:' . get_max_file_size(),
		    'email' => 'required|email',
		    'phone' => 'required',
		    'name' => 'required|max:255'
	    ]);
    	$request->file('file')->move(public_path('upload/photos'),$request->file('file')->getClientOriginalName());
	    $uploadfile = [
		    'img' => $request->file('file')->getClientOriginalName()
	    ];

	    Photo::create($uploadfile + $request->input());
	    return back()->with('message', '');
    }
}
