<?php
// v 1.0.0
namespace App\Http\Controllers;

use App;
use App\Models\Page;
use App\Models\Rubric;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Menu;
use Mockery\Exception;

//use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function weddingDecor()
	{
		return view('wedding-design', ['rubric' => Rubric::first()]);
	}

	public function subsc()
	{
		$pages = Page::where('category_id', 4)->get();
		$page = Rubric::find(4);
		return view('subscription', [
			'page'=> $page,
			'pages' => $pages,
			'banners' => config('banners_subscribe'),
		]);
	}

	public function cetrificate($key)
	{
		return view('certificate-callback', ['price_id' => $key])->render();
	}

	public function subscribePage($slug)
	{
		$page = Page::where('parent_id', 4)->where('slug', $slug)->first();
		if(!$page){
			abort(404);
		}
		return view('subscription-order', ['page' => $page]);
	}

	public function blog()
	{
		$rubric = Rubric::whereSlug('blog')->first();
		if(!$rubric) abort(404);
		if(!isset($rubric->pages) || !($rubric->pages instanceof Collection)) abort(404);

		$articles = $rubric->pages->sortByDesc('date');
		$paginate = $articles->paginate(5);
		$paginate->setPath(preg_replace('|([&?]*page=[0-9]+)+|', '', $_SERVER['REQUEST_URI']));

		return view('blog.pages', [
			'rubric' => $rubric,
			'paginate' => $paginate,
			'articles' => $paginate->items(),
		]);
	}

	public function index()
	{
	    $articles = Rubric::whereSlug('blog')->first()->pages->sortByDesc('date')->take(3);

		$document = [
			'page'=> Page::find(34),
			'categories' => Category::all()->sortBy('level'),
			'banners' => config('banners'),
			'articles' => $articles,
		];

		return view('home', $document);
	}

	public function rubric($slug)
	{
		$rubric = Rubric::where('slug', $slug)->orderBy('level')->firstOrFail();
		$document = [
			'rubric' => $rubric
		];
		if (($rubric->id == 2) || ($rubric->id == 6)) {
			return view('wedding-design', ['rubric' => $rubric]);
		}
		//Проекты
		if (($rubric->id == 7)) {
			return view('wedding-projects', ['rubric' => $rubric]);
		}
		return view('blog.pages', $document);
	}

	public function masterShow()
	{
		$page = Page::where('slug', 'master-show')->where('public', '=', 1)->firstOrFail();
		$rubric = Rubric::where('slug', 'mastershow')->orderBy('level')->firstOrFail();
		$videos = [
			['MVI_0109_x264.mp4', 'MVI_0109_x264.mp4'],
			['MVI_0121_x264.mp4', 'MVI_0121_x264.mp4'],
		];
		$video = $videos[rand(0, count($videos) - 1)];
		return $this->showLanding(['rubric' => $rubric, 'video' => $video, 'page' => $page]);
	}

	public function show($slug)
	{
		if($slug=='master-show') return $this->masterShow();
		$rubric = Rubric::where('slug', $slug)->first();

		if ($rubric) {
			return $this->Rubric($slug);
		}

		$page = Page::where('slug', $slug)->where('public', '=', 1)->firstOrFail();

		$videos = [];
//		if ($page->category_id == 2) {
//			return view('wedding-design-one', $page)->with([
//			    'images' => $page->images,
//                'page' => $page,
//                'rubric' => $page->rubric,
//            ]);
//		}
		//Проект
		if (in_array($page->category_id, array(7,2,6)) ) {
			return view('wedding-projects-one', $page)->with([
			    'images' => $page->images,
                'page' => $page,
                'rubric' => $page->rubric,
            ]);
		}

		if ($page->isStatic) {
			return $this->showLanding(['page' => $page]);
		}
//		$page->content = $this->insertModule($page->content);

		if($page->slug=='calendar'){
			return $this->calendar($page);
		}

		return view('page', ['page'=>$page]);
	}

	public function calendar($page){
		if(!($page instanceof Page)) abort(404);
		$calendar_settings = config('calendar_settings');
		if(!is_array($calendar_settings)) abort(404);
		$products = [];
		if(isset($calendar_settings['sizes']) && is_array($calendar_settings['sizes'])){
			foreach ($calendar_settings['sizes'] as $size =>  $size_arr){
				if(isset($size_arr['items']) && is_array($size_arr['items'])){
					$calendar_settings['sizes'][$size]['products'] = Item::all()->whereIn('id', array_keys($size_arr['items']));
				}
			}
		}
		//dump($calendar_settings);


		return view('calendar', [
							'page'=>$page,
                            'title' => $page->title,
                            'calendar_settings' => $calendar_settings,
						]);


	}

	public function insertModule($content)
	{
		preg_match_all("/{module:([A-Za-z0-9_\-]+)(?:,(.+))?(?:,(.+))?}/", $content, $matches);
		$module = [];
		foreach ($matches[1] as $k => $mod) {
			//$moddir = $matches[2][$k];
			$module[] = view('modules.' . $mod)->render();
		}

		return str_replace($matches[0], $module, $content);
	}

	public function showLanding($data)
	{

		$locale = \App::getLocale();
		$view = $data['page']->slug;

		if (is_file(resource_path("views") . "/landing/$view-$locale.blade.php")) {
			$view = "landing.$view-$locale";
		}

		return View($view, $data);

	}

	public function callbackForm()
	{
		return view('callback-form')->render();
	}

	public function callbackMSForm()
	{
		return view('master-show-callback')->render();
	}

	public function uploadForm()
	{
		return view('upload-form')->render();
	}

}
