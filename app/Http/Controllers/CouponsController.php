<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\CouponOrder;

class CouponsController extends Controller
{

	public function setCoupon($code, $total)
	{

		$coupon = Coupon::whereCode($code)
			->orWhere(function ($query) {
				$query->where('expired', '>', date('Y-m-d H:i:s'))
					->where('expired', '0000-00-00 00:00:00');
			})->first();

		if (!$coupon) return ['discount_text' => 'Неверный купон'];

		if ($coupon->active != 1) return ['discount_text' => 'Купон неактивен'];

		session(['coupon' => $coupon]);

		if ($coupon->type == '*') {
			$discount = $total - ($coupon->discount * $total);
		} elseif ($coupon->type == '-') {
			$discount = $total - ($total - $coupon->discount);
		}

		$discount_data = [
			'total' => $total,
			'total_with_discount' => $total - $discount,
			'discount' => $discount,
			'discount_text' => 'Ваша скидка: ' . $discount . ' грн'
		];

		return $discount_data;
	}

	public function storeCouponOrder($order)
	{
		if ($coupon = session('coupon')) {
			CouponOrder::create([
				'coupon_id' => $coupon->id,
				'order_id' => $order->id,
				'code' => $coupon->code,
				'discount' => $coupon->DiscountValue
			]);

			if ($coupon->oneoff == 1) {
				$coupon->active = 0;
				$coupon->save();
			}

			session()->forget('coupon');
		}
	}

	public function generateCoupon($order)
	{
		$config = config('coupons');

		if ($config['active'] != 1 or empty($config['minorder']) or $config['minorder'] > $order['amount']) return false;

		$data = [
			'code' => str_random(6),
			'type' => '*',
			'oneoff' => 1,
			'discount' => (100 - $config['discount']) * 0.01
		];

		if (isset($config['expired'])) {
			$data['expired'] = \Carbon\Carbon::now()->addDays($config['expired']);
		}

		$coupon = Coupon::create($data);
		$coupon['discount'] = $config['discount'];

		if (isset($config['expired'])) {
			$coupon['days'] = $config['expired'];
		} else {
			$coupon['days'] = false;
		}

		(new \App\Http\Controllers\EmailController)->sendCoupon($order, $coupon);

	}

}
