<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class PayPalController extends Controller
{
    
    public function index()
    {
        $uniqid = uniqid();
        return $this->pay(100, 1, $uniqid);
        
    }
    
    public function error($orderid)
    {
        return View('commerce.payment.payment-error', ['orderid' => $orderid]);
    }
    
    public function pay($amount, $orderid, $uniqid)
    {
        
        //$this->_public_key = config();
        //$this->_private_key = config();
        // to EUR
        
        
        return View('commerce.payment.paypal-form', ['orderid' => $orderid, 'amount' => $amount, 'uniqid' => $uniqid]);
        
    }
    
    public function serverResponse(Request $request, $id)
    {
        
        $order = Order::whereCode($id)->firstOrFail();
        $order->update(['payment_status' => $request->get('status')]);
        
    }
    
    public function userResponse($id)
    {
        
        $order = Order::whereCode($id)->first();
        return $order->payment_status_value;
        
    }
    
}