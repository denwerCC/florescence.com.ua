<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Page;
use App\Models\Category;
use App\Models\Filter;
use App\Models\Item;
use Swap;

//use Illuminate\Http\Request;

class ItemsController extends Controller
{

	public static $items_limit = 12;//

	// items for mainPgae
	public function main()
	{

		return Item::where('public', 1)->where('category_id', 8)->orderBy('id', 'desc')->get();

	}

	public function index()
	{
		$order = in_array(request()->get('order'), ['asc', 'desc']) ? request()->get('order') : 'asc';
		$items = Item::where('public', 1);
		switch (request()->get('sort')){
			case 'name':
				$items = $items->orderBy('title', $order)->get();
				break;
			case 'price':
				$items = $items->orderBy('price', $order)->get();
				break;
			default:
				$items = $items->orderBy('level', 'asc')->latest();
				break;
		}

		$categories = Category::all();

		$paginate = $items->paginate(self::$items_limit);
		$paginate->setPath(preg_replace('|([&?]*page=[0-9]+)+|', '', $_SERVER['REQUEST_URI']));

		$document = [
			'page' => Page::find(35),
			'items' => $paginate->items(),
			'paginate' => $paginate,
			'categories' => $categories
		];
		return view('commerce.catalog', $document);
	}

	/**
	 * @return array()
	 */
	public function findByFilter($id)
	{
		$cats = Category::select('id')->orderBy('level')->get()->pluck('id');

		$items = Item
			::leftJoin('item_filters', 'items.id', '=', 'item_filters.item_id')
			->with('category')
			->where('item_filters.filter_id', $id)
			->orderByRaw('FIELD(category_id, ":values")', ['values' => $cats])
			->orderBy('category_id', 'desc')
		;

		$items = $this->sort($items);

		return $items;
	}

	public function pageByFilter($id = 21)
	{

		$filter = Filter::findOrFail(21);

		$document = [
			'title' => $filter->title,
			'description' => $filter->description,
			'items' => $this->findByFilter($id)
		];

		if ($document['items']->isEmpty()) abort(404);

		return view('commerce.catalog', $document);
	}

	public function category($slug)
	{

		$category = Category::where('slug', $slug)->firstOrFail();

		if (!$category->childrens->isEmpty()) {
			return view('commerce.category', ['category' => $category]);
		}



		$order = in_array(request()->get('order'), ['asc', 'desc']) ? request()->get('order') : 'asc';
		switch (request()->get('sort')){
			case 'name':
				$items = Item::where('public', 1)
				    ->whereCategoryId($category->id)->orderBy('title', $order)->get();
				break;
			case 'price':
				$items = Item::where('public', 1)
				    ->whereCategoryId($category->id)->orderBy('price', $order)->get();
				break;
			default:
				$items = Item::where('public', 1)
				             ->whereCategoryId($category->id)->orderBy('level', 'asc')->latest();

				$items = $this->sort($items);
				break;
		}

		if ($items->isEmpty()) abort(404);

		// if its only 1 item
		if ($items->count() == 1) {
			return redirect($items->first()->link);
		}


		$paginate = $items->paginate(self::$items_limit);
		$paginate->setPath(preg_replace('|([&?]*page=[0-9]+)+|', '', $_SERVER['REQUEST_URI']));
//		dd($paginate->items());

		$document = [
			'category' => $category,
			'items' => $paginate->items(),
			'paginate' => $paginate,
			'categories' => Category::all(),
		];

		return view('commerce.catalog', $document);
	}

	static public function rand($category, $limit)
	{

		$item = Item::inRandomOrder()
			->where('public', 1)
			->where('category_id', $category)
			->take($limit)->get();
		return $item;
	}

	public function show($slug, $id=false){
	    if($id){
            $item = Item::where('slug', $slug)->where('id', $id)->firstOrFail();
        }else{
            $item = Item::findOrFail($slug);
        }

		$document = [
			'title' => $item->title,
			'item' => $item,
			'items' => Item::inRandomOrder()
				->where('public', 1)
				->where('category_id', $item->category_id)
//				->where('stock', '>=' , 1)
				->where('id', '<>', $item->id)
				->take(4)->get(),
			'itemhistory' => $this->history($item->id)
			/*'navigation' => [
				'prev_page' => null !== ($it = Item::where('id', '<', $item->id)->where('category_id', $item->category_id)->orderBy('id', 'desc')->first()) ? $it->link : false,
				'next_page' => null !== ($it = Item::where('id', '>', $item->id)->where('category_id', $item->category_id)->orderBy('id')->first()) ? $it->link : false
			]*/
		];
		$similarItems = Item::where('category_id', '!=', $item->category_id)->take(4)->get();

		return view('commerce.item', $document, ['similarItems' => $similarItems]);
	}

	static function sorter($arr_sort = false)
	{
		if (!$arr_sort) {
			$arr_sort = Array(
				"level-asc" => l('По замовчуванню'),
				"price-asc" => l('По ціні') . " &uarr;",
				"price-desc" => l('По ціні') . " &darr;",
				"title-asc" => l('По назві') . " A-Z",
				"title-desc" => l('По назві') . " Z-A",
				"created_at-asc" => l('По даті') . " &uarr;",
				"created_at-desc" => l('По даті') . " &darr;",
				"sku-asc" => l('По артикулу'),
			);
		}
		return $arr_sort;
	}

	public function sort($items)
	{
		$sort = "level";
		$direction = 'asc';
		if (preg_match("|([a-z_]*)\-([a-z]*)|", request()->cookie('sort'), $fnd)) {
			$sort = $fnd[1];
			$direction = $fnd[2];
		}

		return $items
			->with('filters')
			->with('category')
//			->orderBy('stock', 'desc')
			->orderBy($sort, $direction)
			->get();
//			->paginate(9);
	}


	public function sortUpdate()
	{
		return back()->cookie(cookie()->forever('sort', request()->sort));
	}

	public function search()
	{

		$search = request()->q;

		$items = Item::where('title', 'like', '%' . $search . '%')
			->orWhere('description', 'like', '%' . $search . '%')
			->orWhere('content', 'like', '%' . $search . '%');

		$items = $this->sort($items);

		$description = $items->count() ? 'найдено ' . $items->count() . ' вещей' : ' ничего не найдено';

		$document = [
			'title' => 'Поиск',
			'description' => 'По запросу "' . $search . '" ' . $description,
			'items' => $items
		];

		return view('commerce.catalog', $document);
	}

	static function history($id)
	{

		if (!$id) return false;

		$ids = request()->cookie('itemhistory');

		if (is_array($ids)) {
			$items = Item::whereIn('id', $ids)->get();
		} else {
			$ids = [];
			$items = [];
		}

		if (count($ids) > 6) {

			array_shift($ids);

		}

		$ids[] = $id;

		\Cookie::queue(cookie('itemhistory', $ids));

		return $items;
	}

}
