<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Filter;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItems;
use Cookie;
use Illuminate\Http\Request;
use Validator;

class CommerceController extends Controller
{
    
    public $total;
    public $count;
    public $data;
    public $cart;
    public static $items;
    public static $data_static;
    
    public function informer()
    {
        $this->cartitems();
        return self::$data_static;
    }
    
    public function checkout()
    {
        
        $items = $this->cartitems();
        
        if (!$items) return redirect(route('home'));
        
        $document = [
            'items' => $items,
            'total' => $this->total,
            'count' => $this->count
        ];
        return View('commerce.checkout', $document);
    }
    
    public function cart()
    {
        if($this->cart) return $this->cart;
        if(!$cart_id = request()->cookie('cart')) return false;
        $this->cart = Cart::whereCartId($cart_id)->first();
        return $this->cart;
    }
    
    public function cartitems($cart = false)
    {
        
        if (isset(self::$items)) return self::$items;
        
        if (empty($cart) and $this->cart()) {
            $cart = $this->cart()->items;
        }
        
        if (!is_array($cart)) return [];
        
        $ids = array_pluck($cart, 'id');
        
        $items_data = Item::find($ids);
        $items = [];
        
        foreach ($cart as $key => $item) {
            $itemModel = $items_data->find($item['id']);
            
            if (empty($itemModel)) {
                continue;
            }
            
            $items[$key] = $itemModel->replicate();
            
            if (isset($item['parameters'][10])) {
                $items[$key]['price'] = $itemModel->filter($item['parameters'][10])->value;
            }
            
            $items[$key]['id'] = $item['id'];
            $items[$key]['parameters'] = $item['parameters'];
            $items[$key]['count'] = $item['count'];
            
            $this->count += $item['count'];
            $this->total += $item['count'] * $items[$key]['price'];
        }
        
        $this->data = [
            'total' => $this->total,
            'count' => $this->count
        ];
        
        self::$data_static = $this->data;
        self::$items = collect($items);
        
        $this->getDiscount();
        
        return collect($items);
    }
    
    public function createOrder(Request $request)
    {
        $cartitems = $this->cartitems();
        if (!$cartitems) return redirect(route('cart'));
        
        $this->validate($request, [
            'order.name' => 'required',
            'order.phone' => 'required',
/*            'order.email' => 'email|required',
            'order.address' => 'required_unless:delivery,Самовывоз'*/
        ],[],[
            'order.name' => 'Имя',
            'order.phone' => 'Телефон',
            'order.email' => 'E-mail',
            'order.address' => 'Адрес'
        ]);
        $data = $request->get('order');
        // create new user
/*        if (!\Auth::check()) {
            $user = (new \App\Http\Controllers\UserController)->create($data);
            if ($user) {
                // login and remember
                \Auth::login($user, true);
            } else {
                return back()->withInput();
            }
        } else {
            $user = \Auth::user();
        }*/
        $user = \Auth::user();
        //$data['address'] = $data['country'] . ". " . $data['city'] . ". " . $data['address'] . ". " . $data['postcode'];
        
        $data['ip'] = $request->ip();
        $data['amount'] = $this->total;
        $data['code'] = uniqid();
//        $data['user_id'] = $user->id;
        $data['lang'] = \App::getLocale();
        $order = Order::create($data);
        // coupons
        (new \App\Http\Controllers\CouponsController)->storeCouponOrder($order);
        
        foreach ($cartitems as $item) {
            $item['parameters'] = $this->parametersText($item->parameters, 'json');
            $item['order_id'] = $order->id;
            $item['item_id'] = $item->id;
            
            //$item->decrement('stock');
            $item->stock -= $item['count'];
            $item->save;
            
            OrderItems::create($item->toArray());
        }
        
        $order = Order::find($order->id);
        (new \App\Http\Controllers\EmailController)->newOrderToAdmin($order)
            ->confirmOrder($order);
            
        $document = [
            'items' => $cartitems,
            'total' => $this->total,
            'count' => $this->count,
            'payment' => '',
            'form' => $request->input()['order'],
            'order_id' => $order->id,
            'payment_method' => isset($data['payment_method']) ? $data['payment_method'] : '',
            'payment_status' => 'Очікує оплати',
            'gaCommerce' => '',//$this->gaCommerce($order->id,$cartitems),
            'code' => $data['code']
        ];
        
        Cookie::queue(Cookie::forget('cart'));
        
        // subscribe
        if($request->subscribe){
            \App\Http\Controllers\SubscribeController::subscribe($user);
        }
        
        if ($data['payment_method'] == 'Liqpay') {
            // form on page
            //$document['payment'] = (new \App\Http\Controllers\LiqPayController)->pay($this->total, $order->id, $data['code']);
            // immedietly redirect
            return (new \App\Http\Controllers\LiqPayController)->pay($this->total,$order->id,$data['code']) . $document['gaCommerce']."<script>form = document.getElementsByTagName('form')[0];form.style.display='none',form.submit();</script>";
        }else if($data['payment_method'] == 'PayPal') {
            // immedietly redirect
            return (new \App\Http\Controllers\PayPalController)->pay($this->total,$order->id,$data['code']) . $document['gaCommerce']."<script>form = document.getElementsByTagName('form')[0];form.style.display='none',form.submit();</script>";
        }
/*        else if($data['payment_method'] == 'Bank'){
            $document['payment'] = " <a class='btn' target='_blank' href='" . route('invoice',$data['code']) . "'>Распечатать квитанцию</a>  ";
        }*/

        return View('commerce.order-complete', $document)->withCookie(Cookie::forget('cart'));
        
    }
    
    public function invoice($id)
    {
        
        $order = Order::whereCode($id)->firstOrFail();
        
        return View('commerce.bank-invoice',[
            'order'=>$order,
            'config' => config('bank')
        ]);
    }
    
    public function orderStatus($id)
    {
        
        $order = Order::whereCode($id)->firstOrFail();
        
        if(!$order){
            
            $data = [
                'title' => l('Замовлення відсутнє в базі'),
                'description' => l('Замовлення відсутнє в базі'),
                'content' =>  l('Замовлення відсутнє в базі')
            ];
            
            return view('page',$data);
            
        }
        $data = [
            'total' => $order->amount,
            'count' => $order->count,
            'form' => $order,
            'items' => $order->items,
            'order_id' => $order->id,
            'payment_method' => $order->payment_method,
            'payment_status' => $order->payment_status_value,
            'code' => $order->code
        ];
        
        return View('commerce.order-complete', $data);
    }
    
    public function show()
    {
        
        $document = [
            'items' => $this->cartitems(),
            'total' => $this->total,
            'count' => $this->count,
            'data' => $this->data
        ];
        
        session(['url.intended' => url()->current() . "#step2"]); // for redirect after login
        
        return View('commerce.cart', $document);
        
    }
    
    public function add(Request $request, $id)
    {
        
        $cart = [];
        
        if(!$this->cart()){
            $cart_id = uniqid();
            $this->cart = Cart::create(['cart_id'=>$cart_id,'ip'=>request()->ip()]);
            Cookie::queue(cookie()->forever('cart', $cart_id));
        }else{
            //$cart = $this->cart()->items;
            $cart = [];
        }
        
        Item::findOrFail($id);
        
        $count = $request->count ? $request->count : 1;
        $count = (int) preg_replace("/\-|,/","",$count);
        
        $array['parameters'] = $request->parameters;
        $key = $request->parameters ? implode(';', $request->parameters) : '';
        $ident = sha1($id . $key);
        $array['id'] = $id;
        
        // set count for items
        if ($cart and array_key_exists($ident, $cart)) {
            $array['count'] = $cart[$ident]['count'] + $count;
        } else {
            $array['count'] = $count;
        }
        
        $cart[$ident] = $array;
        
        $this->cart->update(['data'=>json_encode($cart)]);
        
        if ($request->ajax()) {
            $this->cartitems($cart);
            return response()->json($this->data);
        }
        
        return redirect(route('cart'));
        
    }
    
    public function update(Request $request)
    {
        
        $data = $request->input('count');
        $cart = $this->cart()->items;
        
        foreach ($data as $key => $count) {
            $count = (int) preg_replace("/\-|,/","",$count);
            $cart[$key]['count'] = $count = intval($count) ? $count : 1;
        }
        
        $this->cart->update(['data'=>json_encode($cart)]);
        
        if ($request->ajax()) {
            $this->cartitems($cart);
            return response()->json($this->data);
        }
        
        return redirect(route('cart'));
        
    }
    
    public function getDiscount()
    {
        
        if ($code = request()->get('code')) {
            session()->forget('coupon');
        }
        
        if ($code or (session('coupon') and $code = session('coupon')->code)) {
            
            $data = (new CouponsController)->setCoupon($code, $this->total);
            
            if (is_array($data)) {
                if (isset($data['total_with_discount'])) $this->total = $data['total_with_discount'];
                if (isset($data['discount'])) $this->data['discount'] = $data['discount'];
                if (isset($data['discount_text'])) $this->data['discount_text'] = $data['discount_text'];
                $this->data['total'] = $this->total;
            }
            
        }
        
    }
    
    public function parametersText($parameters,$json=false)
    {
        
        if (!$parameters) return null;
        
        foreach ($parameters as $parent_id => $filter_id) {
            $parametersText[Filter::find($parent_id)->title] = Filter::find($filter_id)->title;
        }
        return $json ? json_encode($parametersText) : $parametersText;
    }
    
    public function delete($key)
    {
        
        $cart = $this->cart()->items;
        
        unset($cart[$key]);
        $this->cart->update(['data'=>json_encode($cart)]);
        
        if (request()->ajax()) {
            $items = $this->cartitems($cart);
            unset($items[$key]);
            
            $total = 0;
            $count = 0;
            foreach ($items as $key => $item) {
                $total += $item['count'] * $item['price'];
                $count += $item['count'];
            }
            
            return response()->json(['total' => $total, 'count' => $count]);
        }
        
        return redirect(route('cart'));
        
    }
    
}