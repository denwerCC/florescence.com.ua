<?php
namespace App\Http\Middleware;

use Closure;

class ShortcodesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        //Исключаем админку и левые запросы
        if(!method_exists($response, 'content') || substr_count($_SERVER['REQUEST_URI'], '/admincab')):
            return $response;
        endif;

        $content = str_replace('[[shortcode_form]]', '<!--[[shortcode_form]]START-->'.view('shortcode.contact_form'), $response->content());

        $response->setContent($content);

        return $response;
    }
}