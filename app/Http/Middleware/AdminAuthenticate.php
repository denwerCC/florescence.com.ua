<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuthenticate
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param  string|null $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard)
	{

		if (!auth()->check()) {

			if ($request->ajax() || $request->wantsJson()) {
				return response('Unauthorized.', 401);
			} else {
				return redirect()->guest('admincab/login');
			}
		} else
			if (auth()->user()->id != $guard) {
				return redirect()->guest('/')->withMessage('U`r not admin!');
			}

		return $next($request);
	}

}
