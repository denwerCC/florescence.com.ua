<?php

//Выборка из текста определенного количества слов
function getWordsFromText($text, $word_limit = 5){
    $text = trim(strip_tags($text));
    if(empty($text)) return '';

    $words = explode(' ', $text, ($word_limit+1));//+1 = Что бы последний оставался остаток который мы удялаем
//        var_dump($words);
    if(count($words) > 2){
        array_pop($words);//Удаляем последний элемент массива
    }
    return implode(' ', $words);
}


function menu($name,$attr=false){
	return (new \App\Http\Controllers\MenuController)->show($name,$attr);
}

function flashError($message){
	if(\Session::get('errors') and \Session::get('errors')->hasBag()){
		$bag = \Session::get('errors')->getBag('default');
	}else{
		$bag = new \Illuminate\Support\MessageBag();
	}

	$bag->add(0,$message);

	$error = (new \Illuminate\Support\ViewErrorBag)->put('default', $bag);
	\Session::flash ('errors', $error);
}

function array_clean(&$array)
{
	if (is_array($array)) {
		foreach ($array as $key => &$item) {
			array_clean($item, $array, $key);
		}

		$array = array_filter($array);
	}

}

function currency(){
	return \App\Http\Controllers\CurrencyController::change();
}
function currencyIndex(){
	return \App\Http\Controllers\CurrencyController::$current_index;
}
//Текущая валюта по сайту
function getCurrentCurrency(){
	return \App\Http\Controllers\CurrencyController::getCurrencyIndex();
}
//Выводим меню валют
function getCurrencyMenu(){
    $currentCurrency = \App\Http\Controllers\CurrencyController::getCurrencyIndex();
//    dd(get_class_vars(\App\Http\Controllers\CurrencyController::class));
//    dd($currencyIndex);
    return view('parts.currency_menu',[
        'currentCurrency' => $currentCurrency,
        'currencyItems' => \App\Http\Controllers\CurrencyController::$index,
    ]);
}

//Выводим меню языков
function getLangsMenu(){
    return view('parts.currency_menu');
}

function langswitch($lang=false)
{

    if($langs = config('langs')){
        if($lang) return $langs[$langs];
        return $langs;
    }

    $route = Route::getCurrentRoute();

    if ($route) {
        $routeName = $route->getName();
    } else {
        $routeName = '';
    }

    if ($routeName == 'page') {
        $page = \App\Models\Page::whereSlug(Route::getCurrentRoute()->parameter('slug'))->first();
    } elseif ($routeName == 'catalog.category') {
        $page = \App\Models\Category::whereSlug(Route::getCurrentRoute()->parameter('slug'))->first();
    } elseif ($routeName == 'item') {
        $page = \App\Models\Item::find(Route::getCurrentRoute()->parameter('id'));
    }

    $langs = [];
    $url = false;

    foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties) {

        if (isset($page)) {
            $model = $page->translate($localeCode);
            if($routeName == 'item'){
                $url = route('item', ['slug'=>$model->slug,$page->id]);
            }else{
                $url = route($routeName, $model ? $model->slug : '/' . $localeCode);
            }
        }

        $langs[$localeCode] = [
            'url' => \LaravelLocalization::getLocalizedURL($localeCode, $url),
            'lang' => $properties['native']
        ];

        if (LaravelLocalization::getCurrentLocale() == $localeCode) {
            $langs[$localeCode]['active'] = true;
        } else {
            $langs[$localeCode]['active'] = false;
        }

        if($lang==$localeCode){
            return $langs[$localeCode];
        }

    }

    /*    \App\Http\Middleware\AfterMiddleware::$callback = function ($response){
            return $response;
            // Perform action
            return $response->setContent(preg_replace("/<head>/", 11111,$response->getContent()));
        };*/

    config()->set(['langs'=>$langs]);

    return $langs;
}

function l($word){

	$translated = trans("app.".$word);

	return preg_replace("|^app\.|","",$translated);
}

// v1.0.0
function cyrlink($text){

	//$text = urlencode($text);
	$text = preg_replace("/[^А-ЯІЇа-яіїєЄA-Za-z0-9_\s\-]/u", '', $text);
	return mb_strtolower(trim(preg_replace("/[\s-]+/u","-",$text)),utf8);
}


function dateFormat($value,$type='d.m.Y'){
	return date( $type, strtotime($value));
}

// v1.0.0

function translit($str,$to_url=false,$filter=false)
{

	$tr = array(
		"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Ґ"=>"G",
		"Д"=>"D","Е"=>"E","Yo"=>"E","Є"=>"Ye","Ж"=>"J","З"=>"Z",
		"И"=>"I","І"=>"I","Ї"=>"Yi",
		"Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
		"О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
		"У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
		"Ш"=>"Sh","Щ"=>"Sch","Ъ"=>"","Ы"=>"Y","Ь"=>"",
		"Э"=>"E","Ю"=>"Yu","Я"=>"Ya","а"=>"a","б"=>"b",
		"в"=>"v","г"=>"g","ґ"=>"g","д"=>"d","е"=>"e","ё"=>"yo","є"=>"ie","ж"=>"j",
		"з"=>"z","и"=>"i","і"=>"i","ї"=>"i","й"=>"y","к"=>"k","л"=>"l",
		"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
		"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
		"ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
		"ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
	);

	if(App::getLocale() == 'ua' or App::getLocale()=='uk' or App::getLocale() == 'укр'){

		$tr['И'] = 'Y';
		$tr['и'] = 'y';
		$tr['Х'] = 'Kh';
		$tr['х'] = 'kh';
		$tr['Ц'] = 'Ts';
		$tr['ц'] = 'ts';
		$tr['Щ'] = 'Shch';
		$tr['щ'] = 'shch';
		$tr['ю'] = 'іu';
		$tr['я'] = 'ia';
	}

	$link = strtr($str,$tr);

	if($to_url){

		$link = preg_replace("/[^A-Za-z0-9_\s\-$filter]/", '', $link);
		$link = preg_replace("/[\s-]+/", "-",$link);

		return strtolower(trim($link));
	}else{
		return $link;
	}

}

// v1.0.0
function tolink($link,$filter=false){

	if(defined('CYRLINK') and CYRLINK==true){
		return cyrlink($link,$filter);
	}else{
		return translit($link,"to_url",$filter);
	}
}

function get_config($config){
	return htmlspecialchars_decode(config('config.'. App::getLocale() . "." . $config));
}

function get_max_file_size() {
	return intval(ini_get('upload_max_filesize')) * 1024;
}

// inches to feet
function convertMetrix($value)
{

	$value = intval($value);

	if($value>140){

		$inches = $value * 0.393701;
		$ft = intval($inches / 12);

		$inches_r = $ft*12;

		return $value . " - " . $ft . "' " . round($inches - $inches_r,1) . "\"";
	}

	return $value . " - " . round($value * 0.393701) . "\"";

}

function cart(){
	return (new \App\Http\Controllers\CommerceController())->informer();
}

function getCartCount(){
	return \App\Http\Controllers\CartController::getCartCount();
}

function result($message,$type = 'success'){

	if(request()->ajax()){
		return response()->json(['message'=>$message,'status'=>$type]);
	}

	if($type=='success') return back()->withErrors($message)->withInput();

	return back()->withMessage($message);
}