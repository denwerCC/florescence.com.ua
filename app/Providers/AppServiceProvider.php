<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
		//Пагинация для коллекций
	    if (!Collection::hasMacro('paginate')) {
		    Collection::macro('paginate',
			    function ($perPage = 15, $page = null, $options = []) {
				    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
				    return (new LengthAwarePaginator(
					    $this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))
					    ->withPath('');
			    });
	    }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	    if ($this->app->environment() !== 'production') {
		    $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
	    }

	    $this->app->bind('App\Repositories\Interfaces\IContactRepository', 'App\Repositories\ContactRepository');
    }
}
