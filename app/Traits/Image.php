<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 06.02.2017
 * Time: 23:45
 */

namespace app\Traits;

use File;
use Image as ImageInt;
use Validator;

trait Image
{

    function image($width = 0, $height = false, $image = false)
    {
        if (empty($this->imgDefault)) {
            $this->imgDefault = "/images/noimg.jpg";
        }

        if (!$image) {
            if (empty($this->img)) {
                return $this->imgDefault;
            }
            $image = $this->image;
        } else {
            if ($width === 0 and $height === 0) {
                return $image;
            }
        }

        $image = preg_replace("|^/{1}|", "", $image);

        if ($height === false) {
            $height = $width;
        }
        $dir = $width . "x" . $height;

        if (!$this->dir) {
            throw new \Exception('There are no dir attribute provided for ' . get_class());
        }

        $dest = $this->dir . $dir;

        File::makeDirectory($dest, 0775, true, true);

        $filename = pathinfo($image)['basename'];
        $img = $dest . "/" . $filename;

        if (is_file($img)) {
            return "/" . $img;
        }

        if ($filename == $image) {
            $image = $this->dir . 'big/' . $image;
        }

        if (!is_file($image) or "/" . $image == $this->imgDefault) {
            return $this->imgDefault;
        }

        $img_obj = ImageInt::make($image);

        if ($height === 0) {
            $img_obj->width($width);
        } else {
            if ($width === 0) {
                $img_obj->height($height);
            } else {
                $img_obj->fit($width, $height, function ($constraint) {
                    $constraint->upsize();
                });
            }
        }

        $img_obj->save($img);

        return "/" . $img;

    }

    public function getImagesAttribute()
    {
        if (!$this->imgs) {
            $imgs = array();
        } else {
            $imgs = array_map(function ($item) {
                return "/" . $this->dir . "big/" . pathinfo($item)['basename'];
            }, $this->imgs);
        }
        return collect($imgs);
    }

    public function deleteImg($image)
    {

        if (!is_dir($this->dir)) {
            return false;
        }

        foreach (\File::directories($this->dir) as $dir) {
            if (is_file($dir . "/" . $image)) {
                unlink($dir . "/" . $image);
            }
        }
    }

    public function uploadImage()
    {

        $this->validate(request(), [
            'img' => 'image|mimes:jpeg,jpg,bmp,png|max:' . get_max_file_size()
        ]);

        $img = request()->file('img');

        $image = tolink($img->getClientOriginalName(), "\.");

        try {
            ImageInt::make($img)->resize(1980, 1080, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save("upload/tmp/$image");
        } catch (\Exception $e) {
            $error = $e->getMessage() . ". File: " . $image;
            $response = ['status' => 'error', 'message' => $error];
            return response()->json($response);
        }

        return response()->json(['status' => 'success', 'file' => $image]);
    }
}