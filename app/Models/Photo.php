<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'size', 'title', 'created_at', 'img', 'name', 'email', 'phone'
    ];

	public function getImageAttribute()
	{
		if(is_file($this->dir . $this->id . ".jpg")){
			return "/" . $this->dir . $this->id . ".jpg";
		}else{
			return "/img/noimg.png";
		}
	}
}
