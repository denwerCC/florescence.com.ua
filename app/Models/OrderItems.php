<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
	protected $fillable = [
		'order_id', 'item_id', 'title', 'sku', 'count', 'price', 'parameters'
	];

	public function attr()
	{
		return $this->belongsTo('App\Models\Item','item_id');
	}

	public function getParametersAttribute($value)
	{

		if(isset($value)){
			return json_decode($value,true);
		}

		return [];

	}

}
