<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

	protected $fillable = [
		'code', 'expired', 'type', 'discount'
	];

	public function getDiscountValueAttribute()
	{
		if($this->type=='*'){
			$discount = 100 - $this->discount * 100;
			return $discount . "%";
		}elseif
		($this->type=='-'){
			return "-".$this->discount;
		}

		return $this->discount;
	}

	public function orders()
	{
		return $this->hasMany('App\Models\CouponOrder');
	}

}
