<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cart
 *
 * @property-read mixed $items
 * @property-read \App\User $user
 * @mixin \Eloquent
 */
class Cart extends Model
{
	protected $fillable = [
		'cart_id', 'user_id', 'data', 'ip'
	];
	
	public function user()
	{
		return $this->belongsTo('App\User','user_id');
	}
	
	public function getItemsAttribute()
	{
		return json_decode($this->data, 1);
	}
}
