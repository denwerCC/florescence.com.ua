<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

	protected $fillable = [
		'name', 'email', 'phone', 'address', 'delivery','delivery_name', 'delivery_date', 'delivery_time','time','delivery_phone', 'text', 'items', 'note', 'amount', 'user_id', 'payment_method', 'payment_status', 'status', 'ip', 'code', 'tracking_number', 'lang', 'promoted','calendar_flag'
	];

	public function getStatusnameAttribute()
	{
		return $this->statuses[$this->status];
	}

	public function getStatusesAttribute()
	{
		return ['Новый', 'В процессе', 'Оформлен', 'Отменен', 'Завершен'];
	}

	public function getRefundlinkAttribute()
	{
		return route('refund.show', $this->code);
	}


	public function getLinkAttribute()
	{
		return route('order', $this->code);
	}

	public function items()
	{
		return $this->hasMany('App\Models\OrderItems');
	}

	public function coupon()
	{
		return $this->hasOne('App\Models\CouponOrder');
	}

	public function refund()
	{
		return $this->hasMany('App\Models\Refund');
	}

	public function getPaymentStatusValueAttribute()
	{

		return $this->payment_statuses[$this->payment_status];

	}

	public function getPaymentStatusesAttribute()
	{

		return [
			'' => 'Ожидает оплату',
			'success' => 'успешный платеж',
			'failure' => 'неуспешный платеж',
			'wait_secure' => 'платеж на проверке',
			'wait_accept' => 'Деньги с клиента списаны, но магазин еще не прошел проверку',
			'wait_lc' => 'Аккредитив. Деньги с клиента списаны, ожидается подтверждение доставки товара',
			'processing' => 'Платеж обрабатывается',
			'sandbox' => 'тестовый платеж',
			'subscribed' => 'Подписка успешно оформлена',
			'unsubscribed' => 'Подписка успешно деактивирована',
			'reversed' => 'Возврат клиенту после списания'
		];

	}

	public static function boot()
	{
		self::created(function ($order) {
			// coupons
			//(new \App\Http\Controllers\CouponsController)->storeCouponOrder($order);
		});

		self::saving(function ($filter) {

		});

		self::updating(function ($order) {

			$oldstatus = \App\Models\Order::find($order->id)->status;
			if ($oldstatus != 2 and $order->status == 2) {
				if (request()->get('sendtouser')) {
					(new \App\Http\Controllers\EmailController)->confirmDelivery($order);
				}
			}

		});

		parent::boot();
	}

}
