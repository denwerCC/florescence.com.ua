<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Certificate
 *
 * @mixin \Eloquent
 */
class Certificate extends Model
{
    protected $fillable = [
        'name', 'phone', 'email', 'price'
    ];
	public static $prices = [
		1=>300,
		2=>400,
		3=>500
	];
}
