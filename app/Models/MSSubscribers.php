<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MSSubscribers extends Model
{
    protected $fillable = [
    	'name', 'surname', 'age', 'from'
    ];
}
