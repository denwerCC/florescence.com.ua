<?php

namespace App\Models;

use App\Http\Controllers\CurrencyController;
use App\Traits\Image;
use Illuminate\Database\Eloquent\Model;
use Laraplus\Data\Translatable;

class Item extends Model
{

    use Translatable, Image;

    protected $fillable = [
        'title',
        'category_id',
        'date',
        'price',
        'offer',
        'level',
        'slug',
        'img',
        'description',
        'content',
        'filter_id',
        'public',
        'meta_title',
        'meta_description'
    ];

    public $imgDefault = "/img/noimg.jpg";

    public function filter($id)
    {
        if ($filter = $this->filters->where('filter_id', $id)->first()) {
            return $filter;
        }

        return new ItemFilter;
    }

    // $id - filter group

    public function filters($id = false)
    {
        // if its filter group

        if ($id) {
            return $this->filters->load('attr')->mapWithKeys(function ($item) use ($id) {
                if ($item->attr->parent_id == $id and $item->value) {
                    return [$item->id => $item];
                }
                return false;
            });
        }

        // if it all active filters
        return $this->hasMany('App\Models\ItemFilter');
    }

    /*	public function getPriceAttribute($value)
        {
            return number_format($value, 0, '', ' ');
        }*/

    public function getPriceFormattedAttribute()
    {
        return round($this->price / CurrencyController::getCurrency());
    }

    public function category()
    {
        //Для измежания ошибок при создании новых товаров устанавливаем категорию по умолчанию
    	if(empty($this->category_id)){
		    $this->category_id = 1;
	    }
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function getLinkAttribute()
    {
        return route("item", [$this->slug, $this->id]);
    }

//	public function getIsInStockAttribute()
//	{
//		if ($this->stock > 0) return true;
//
//		if ($this->date > date('Y-m-d H:i:s')) return true;
//
//		return false;
//	}

    public function getIsInCartAttribute()
    {
        return collect((new \App\Http\Controllers\CommerceController)->cartitems())->contains('id', $this->id);
    }

    public function getCurrencyAttribute()
    {
        return CurrencyController::getCurrencyIndex();
    }

    public function getDirAttribute()
    {
        return "upload/items/$this->id/";
    }

    public function getImgsAttribute()
    {
        if (!$this->img) {
            return array();
        }
        return array_keys(json_decode($this->img, 1));
    }

    public function getImageAttribute()
    {
        if (empty($this->imgs[0])) {
            return $this->imgDefault;
        }
        return "/" . $this->dir . "big/" . $this->imgs[0];
    }

    public function getThumbAttribute()
    {
        return $this->image(60);
    }

    public function getTitleAttribute($value)
    {
        // if($this->category_id==2 and \Route::getCurrentRoute()->getName()=='item'){
            // return l('Весільний') . ' ' . mb_strtolower($value);
        // }

        return $value;

    }

    public function getMetaTitleAttribute()
    {

        if($this->category_id==2){
            return trans('app.:title. Замовлення та ціна у м. Львів',['title'=>$this->title]);
//            return trans('app.Ціна на :title у Львові від :price ☎ (099) 082-51-52 ❀ Доставка весільних букетів ➜ Цілодобово ❀ Консультація флориста ❀ Доступні ціни',['title'=>$this->title,'price'=>$this->priceFormatted . ' ' . $this->currency]);
        }

        return trans('app.Замовити :сategory - «:title». Ціна: :price. Цілодобова доставка квітів у Львові',['title'=>$this->title,'сategory'=>mb_strtolower($this->category->title),'price'=>$this->priceFormatted . ' ' . $this->currency]);
    }

    public function getMetaDescriptionAttribute()
    {

        if($this->category_id==2){
            return trans('app.Ціна на :title у Львові від :price ☎ (099) 082-51-52 ❀ Доставка весільних букетів ➜ Цілодобово ❀ Консультація флориста ❀ Доступні ціни.',['title'=>mb_strtolower($this->title),'price'=>$this->priceFormatted . ' ' . $this->currency]);
            //return trans('app.Ціна на :title у Львові від :price ☎ (099) 082-51-52 ❀ Доставка весільних букетів ➜ Цілодобово ❀ Консультація флориста ❀ Доступні ціни',['title'=>mb_strtolower($this->category->title),'price'=>$this->priceFormatted . ' ' . $this->currency]);
        }

        return trans('app.:сategory - «:title» ❀ Ціна: :price ➜ Доставка квітів та подарунків у Львові 24h/7 ☎ (099) 082 51 52 ❀ Оригінальні букети ❀ Приємні ціни',['title'=>$this->title,'сategory'=>$this->category->title,'price'=>$this->priceFormatted . ' ' . $this->currency]);
    }

    public static function boot()
    {

        self::created(function ($item) {
        });

        self::saving(function ($item) {
        });

        self::updating(function ($item) {
        });

        self::deleted(function ($item) {
            \File::deleteDirectory($item->dir);
        });

        parent::boot();
    }

}
