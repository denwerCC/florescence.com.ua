<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Laraplus\Data\Translatable;
use App\Traits\Image;

/**
 * App\Models\Category
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $childrens
 * @property-read mixed $dir
 * @property-read mixed $image
 * @property-read mixed $images
 * @property-read mixed $h1
 * @property-read mixed $meta_description
 * @property-read mixed $meta_title
 * @property-read mixed $thumb
 * @property-read \App\Models\Category $parent
 * @mixin \Eloquent
 */
class Category extends Model
{
	use Translatable, Image;

	protected $fillable = [
		'title', 'h1','category_id', 'parent_id', 'id_m', 'level', 'slug', 'img', 'description', 'content', 'meta_title', 'meta_description'
	];

	public function getDirAttribute()
	{
		return "upload/categories/$this->id/";
	}

    public function getMetaTitleAttribute($value)
    {
        if($value) return $value;
        return $this->title;
    }

    public function getMetaDescriptionAttribute($value)
    {
        if($value) return $value;
        return $this->description;
    }

	public function getImageAttribute($attr)
	{
		if ($this->img) {
			return "/" . $this->dir . "big/$this->img";
		}

		// get first item image from current cutegory
		if (!$this->childrens->isEmpty()) {
			return $this->childrens->first()->image;
		}

		$item = \App\Models\Item::whereCategoryId($this->id)->first();
		if (isset($item)) {
			return $item->image;
		}

		return '/img/noimg.jpg';
	}


	public function getThumbAttribute()
	{
		if (!$this->img) return "/img/noimg.jpg";
		return "/" . $this->dir . "small/$this->img";
	}

	public function childrens()
	{
		return $this->hasMany('App\Models\Category', 'parent_id');
	}

	public function parent()
	{
		return $this->belongsTo('App\Models\Category', 'parent_id');
	}

	public function allChildrensList()
	{
		$allCategories = [];
		$categories = $this->childrens->load('childrens')->sortBy('level');

		while (count($categories) > 0) {
			$nextCategories = [];
			foreach ($categories as $category) {
				if (!$category->childrens->isEmpty()) {
					$allCategories = array_merge($allCategories, $category->childrens->sortBy('level')->toArray());
					//$nextCategories = array_merge($nextCategories, $category->childrens->toArray());
				} else {
					$allCategories[] = $category->toArray();
				}
			}
			//$categories = $nextCategories;
			$categories = [];
		}

		return new Collection($allCategories); //Illuminate\Database\Eloquent\Collection

	}

}
