<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
	protected $fillable = [
		'ip','user_id', 'name', 'email', 'status'
	];

}
