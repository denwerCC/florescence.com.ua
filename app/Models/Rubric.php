<?php

namespace App\Models;
use Laraplus\Data\Translatable;
use Illuminate\Database\Eloquent\Model;

class Rubric extends Model
{
	use Translatable, \App\Traits\Image;

	protected $fillable = [
		'title','h1', 'rubric_id', 'parent_id', 'level', 'slug', 'img', 'description', 'meta_title', 'meta_description','content', 'video_bg'
	];

	public function getDirAttribute()
	{
		return "upload/rubrics/$this->id/";
	}

	public function childrens()
	{
		return $this->hasMany('App\Models\Rubric','parent_id');
	}

	public function pages()
	{
		return $this->hasMany('App\Models\Page','category_id');
	}

	public function getImageAttribute($attr)
	{

		if ($this->img) {
			return "/" . $this->dir . "big/$this->img";
		}

		return $this->imgDefault;
	}

    public function getMetaTitleAttribute($value)
    {
        if($value) return $value;
        return $this->title;
    }

    public function getMetaDescriptionAttribute($value)
    {
        if($value) return $value;
        return $this->description;
    }

}
