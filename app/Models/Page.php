<?php

namespace App\Models;

use Laraplus\Data\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Image;

class Page extends Model
{

	use Translatable, Image;

	protected $fillable = [
		'img', 'imgs', 'category_id', 'level', 'public','meta_title', 'meta_description', 'title', 'slug', 'content', 'description', 'keywords', 'date'
	];

	public function rubric()
	{
		return $this->belongsTo('App\Models\Rubric', 'category_id');
	}
    
    public function getImgsAttribute($imgs)
    {
        if (!$imgs) return array();
        return array_keys(json_decode($imgs, 1));
    }

    public function getMetaTitleAttribute($value)
    {
        if($value) return $value;
        return $this->title;
    }

    public function getMetaDescriptionAttribute($value)
    {
        if($value) return $value;
        return $this->description;
    }

	public function getDateFormattedAttribute($value)
	{
		return date('d.m.Y', strtotime($value));
	}

	public function getIsStaticAttribute($value)
	{

		return $this->category_id==5 ? true : false;
	}

	public function getDirAttribute()
	{
		return "upload/pages/$this->id/";
	}

	public function getLinkAttribute()
	{
		return route('page',$this->slug);
	}

	public function getThumbAttribute()
	{
		return $this->image(270);
	}

	public function thumb($image)
	{
		return $this->image(270,270, $image);
	}

	public function getImageAttribute()
	{
		if (!$this->img)  return $this->imgDefault;
		return "/" . $this->dir . "big/$this->img";
	}

	public static function boot()
	{
		static::created(function ($page) {

		});
		parent::boot();
	}

}