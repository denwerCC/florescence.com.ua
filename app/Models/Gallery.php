<?php

namespace App\Models;

use Laraplus\Data\Translatable;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
	use Translatable;

	protected $fillable = [
		'title', 'description', 'link', 'category_id', 'level', 'img', 'public'
	];

	public function getThumbAttribute(){
		return '/upload/gallery/' . $this->id . "/thumb-" . $this->img;
	}

	public function getImageAttribute(){
		return '/upload/gallery/' . $this->id . "/big-" . $this->img;
	}

}
