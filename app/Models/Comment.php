<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Image;

class Comment extends Model
{

	use Image;

	public $dir = 'upload/rewievs/';

	protected $fillable = [
		'user_id', 'name','email', 'phone','ip','commentable_type','commentable_id', 'text', 'public'
	];

	public function getImageAttribute()
	{
		if(is_file($this->dir . $this->id . ".jpg")){
			return "/" . $this->dir . $this->id . ".jpg";
		}else{
			return "/admin/img/default-user-avatar.png";
		}
	}

	public function commentable()
	{
		return $this->morphTo();
	}

	public function user(){
		return $this->belongsTo('App\User');
	}

	public function answer(){
		return $this->hasOne('App\Models\Comment', 'commentable_id')->whereCommentableType('comment');
	}

	public function answers(){
		return $this->hasMany('App\Models\Comment', 'commentable_id');
	}

	public function item(){
		return $this->belongsTo('App\Models\\'. ucfirst($this->commentable_type), 'commentable_id');
	}

}
