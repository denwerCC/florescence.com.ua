<?php

namespace App\Models;
use Laraplus\Data\Translatable;
use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    use Translatable;

    protected $fillable = [
        'title', 'filter_id', 'parent_id', 'type', 'level', 'slug', 'img', 'description'
    ];

    public function childs()
    {
        return $this->hasMany('App\Models\Filter', 'parent_id');
    }

	public function parent()
	{
		return $this->belongsTo('App\Models\Filter', 'parent_id');
	}

	public function getTypesAttribute()
	{
		$array = [
			"Кнопка","Цвет","Изображение","Текст","Файл"
		];
		return $array;
	}

	public function getImageAttribute()
	{
		//if($this->parent->type!=2) return false;
		if (!$this->slug) return "/img/noimg.jpg";
		return '/upload/filters/' . $this->slug;
	}

	public static function boot()
	{
		self::created(function ($filter) {

		});

		self::saving(function ($filter) {

			if(empty($filter->parent_id)) $filter->parent_id = null;

		});

		parent::boot();
	}

}
