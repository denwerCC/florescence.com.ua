<?php

namespace App\Models;

use Laraplus\Data\Translatable;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	use Translatable;

	protected $fillable = [
		'title', 'parent_id', 'level', 'slug', 'img', 'description'
	];

	public function childrens()
	{
		return $this->hasMany('App\Models\Menu','parent_id');
	}

}
