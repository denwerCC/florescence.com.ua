<?php

namespace App\Models;
use Laraplus\Data\Translatable;
use Illuminate\Database\Eloquent\Model;

class ItemFilter extends Model
{
    use Translatable;

    protected $fillable = [
        'item_id', 'filter_id', 'value'
    ];

	protected $translatable = ['value'];

	public function attr()
	{
		return $this->belongsTo('App\Models\Filter','filter_id');
	}

	public function item()
	{
		return $this->belongsTo('App\Models\Item','item_id');
	}
}
