<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class fSubscribers extends Model
{
    protected $fillable =[
	    'name', 'email', 'address', 'abonement', 'time', 'information', 'phone', 'size'
    ];
}
