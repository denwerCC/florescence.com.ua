<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Http\Requests\SendContactRequest;
use App\Repositories\Interfaces\IContactRepository;

/**
 * Class ContactRepository
 * @package App\Repositories
 */
class ContactRepository implements IContactRepository
{
    /**
     * @param SendContactRequest $request
     * @return bool|mixed
     */
    public function store(SendContactRequest $request)
    {
        $contact = new Contact();
        $contact->fill($request->all());

        return $contact->save();
    }
}