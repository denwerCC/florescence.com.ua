<?php

namespace App\Repositories\Interfaces;


use App\Http\Requests\SendContactRequest;

/**
 * Interface IContactRepository
 * @package App\Repositories\Interfaces
 */
interface IContactRepository
{
    /**
     * @param SendContactRequest $request
     * @return mixed
     */
    public function store(SendContactRequest $request);
}