-- MySQL dump 10.13  Distrib 5.5.50, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: kvitochka
-- ------------------------------------------------------
-- Server version	5.5.50

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'1.jpg',NULL,NULL),(2,'2.jpg',NULL,NULL),(3,'3.jpg',NULL,NULL),(4,'4.jpg',NULL,NULL),(5,'5.jpg',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_translations`
--

DROP TABLE IF EXISTS `categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_translations`
--

LOCK TABLES `categories_translations` WRITE;
/*!40000 ALTER TABLE `categories_translations` DISABLE KEYS */;
INSERT INTO `categories_translations` VALUES ('ru',1,'kompozicii','Композиции','','2017-03-31 10:27:22','2017-04-25 09:25:12'),('ua',1,'kompozytsii','Композиції','',NULL,'2017-04-25 09:25:12'),('ru',2,'vesilni-buketi','Свадебные букеты','','2017-03-31 10:27:45','2017-04-25 09:25:46'),('ua',2,'vesilni-bukety','Весільні букети','',NULL,'2017-04-25 09:25:46'),('ru',3,'korobki-z-kvitami','Коробки з цветами','','2017-03-31 10:28:07','2017-04-25 09:26:23'),('ua',3,'korobky-z-kvitamy','Коробки з квітами','',NULL,'2017-04-25 09:26:23'),('ru',4,'korobki-z-solodoschami','Коробки з сладостями','','2017-03-31 10:28:15','2017-04-25 09:28:26'),('ua',4,'korobky-z-solodoshchamy','Коробки з солодощами','',NULL,'2017-04-25 09:28:26'),('ru',5,'podarunki','Подарки','','2017-03-31 10:28:24','2017-04-25 09:28:42'),('ua',5,'podarunky','Подарунки','',NULL,'2017-04-25 09:28:42');
/*!40000 ALTER TABLE `categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
INSERT INTO `certificates` VALUES (1,'asdasd','asdasd','1@mail.com',300,'2017-04-21 11:59:04','2017-04-21 11:59:04'),(2,'12312','12312312','awdasd@mail.com',500,'2017-04-21 11:59:24','2017-04-21 11:59:24');
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commentable_id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_orders`
--

DROP TABLE IF EXISTS `coupon_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `coupon_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_orders`
--

LOCK TABLES `coupon_orders` WRITE;
/*!40000 ALTER TABLE `coupon_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oneoff` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `type` enum('*','-') COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(8,2) NOT NULL,
  `expired` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupons_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f_subscribers`
--

DROP TABLE IF EXISTS `f_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f_subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` int(255) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abonement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f_subscribers`
--

LOCK TABLES `f_subscribers` WRITE;
/*!40000 ALTER TABLE `f_subscribers` DISABLE KEYS */;
INSERT INTO `f_subscribers` VALUES (1,'Андрійченко',3333,'test@example.com','lviv','Рік','15.00-17.00','L',NULL,'2017-04-04 12:52:46','2017-04-04 12:52:46'),(2,'Sid',420,'sidthesquid@gmail.com','blue ocean','Квартал','any','S',NULL,'2017-04-04 12:57:31','2017-04-04 12:57:31'),(3,'s',2,'d','sad','Місяць','asd','S',NULL,'2017-04-04 13:00:18','2017-04-04 13:00:18');
/*!40000 ALTER TABLE `f_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fb-users`
--

DROP TABLE IF EXISTS `fb-users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb-users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fb_users_email_unique` (`email`),
  UNIQUE KEY `fb_users_facebook_id_unique` (`facebook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fb-users`
--

LOCK TABLES `fb-users` WRITE;
/*!40000 ALTER TABLE `fb-users` DISABLE KEYS */;
/*!40000 ALTER TABLE `fb-users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters`
--

DROP TABLE IF EXISTS `filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters_parent_id_foreign` (`parent_id`),
  CONSTRAINT `filters_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters`
--

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters_translations`
--

DROP TABLE IF EXISTS `filters_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`filter_id`,`locale`),
  CONSTRAINT `filters_translations_filter_id_foreign` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters_translations`
--

LOCK TABLES `filters_translations` WRITE;
/*!40000 ALTER TABLE `filters_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `filters_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `img` text COLLATE utf8_unicode_ci,
  `level` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_categories`
--

DROP TABLE IF EXISTS `galleries_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `img_width` int(11) DEFAULT NULL,
  `img_heigth` int(11) DEFAULT NULL,
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_heigth` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `galleries_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `galleries_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_categories`
--

LOCK TABLES `galleries_categories` WRITE;
/*!40000 ALTER TABLE `galleries_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_categories_translations`
--

DROP TABLE IF EXISTS `galleries_categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_categories_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_categories_translations`
--

LOCK TABLES `galleries_categories_translations` WRITE;
/*!40000 ALTER TABLE `galleries_categories_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_translations`
--

DROP TABLE IF EXISTS `galleries_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`gallery_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_translations`
--

LOCK TABLES `galleries_translations` WRITE;
/*!40000 ALTER TABLE `galleries_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_filters`
--

DROP TABLE IF EXISTS `item_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_filters_item_id_foreign` (`item_id`),
  KEY `item_filters_filter_id_foreign` (`filter_id`),
  CONSTRAINT `item_filters_filter_id_foreign` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_filters_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_filters`
--

LOCK TABLES `item_filters` WRITE;
/*!40000 ALTER TABLE `item_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_filters_translations`
--

DROP TABLE IF EXISTS `item_filters_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_filters_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_filter_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`item_filter_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_filters_translations`
--

LOCK TABLES `item_filters_translations` WRITE;
/*!40000 ALTER TABLE `item_filters_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_filters_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `filter_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `stock` tinyint(1) NOT NULL DEFAULT '1',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'{\"solodkiy-moment-102.jpg\":{\"name\":\"\"},\"charivna-prikrasa-179.jpg\":{\"name\":\"\"},\"milozvuchna-nijnist-29.jpg\":{\"name\":\"\"}}','',500.00,1,1,NULL,'2017-04-04 17:08:14',1,1,'2017-04-03 07:48:10','2017-04-04 14:08:14'),(10,'{\"solodkiy-moment-102.jpg\":{\"name\":\"\"}}','',0.00,2,1,NULL,'2017-04-03 15:16:01',1,1,'2017-04-03 08:08:21','2017-04-03 12:16:01'),(11,'{\"1.jpg\":{\"name\":\"\"}}','',3300.00,3,2,NULL,'2017-04-03 15:16:16',1,1,'2017-04-03 08:12:09','2017-04-03 12:16:16'),(12,'{\"6.jpg\":{\"name\":\"\"}}','',333.00,4,2,NULL,'2017-04-03 15:16:27',1,1,'2017-04-03 08:12:35','2017-04-03 12:16:27'),(13,'{\"malenka-korobochka-172.jpg\":{\"name\":\"\"}}','',2200.00,5,3,NULL,'2017-04-03 15:16:43',1,1,'2017-04-03 08:12:56','2017-04-03 12:16:43'),(14,'{\"malenka-korobochka-172.jpg\":{\"name\":\"\"}}','',0.00,6,3,NULL,'2017-04-03 15:16:52',1,1,'2017-04-03 08:13:08','2017-04-03 12:16:52'),(15,'{\"fruit-arabesk.jpg\":{\"name\":\"\"}}','',200.00,7,4,NULL,'2017-04-03 15:17:11',1,1,'2017-04-03 08:13:35','2017-04-03 12:17:11'),(16,'{\"fruit-arabesk.jpg\":{\"name\":\"\"}}','',1000.00,8,4,NULL,'2017-04-03 15:17:20',1,1,'2017-04-03 08:13:54','2017-04-03 12:17:20'),(17,'{\"m039iaka-igrashka-visota-50-sm-257.jpg\":{\"name\":\"\"}}','',500.00,9,5,NULL,'2017-04-03 15:17:32',1,1,'2017-04-03 08:14:08','2017-04-03 12:17:32'),(18,'{\"m039iaka-igrashka-visota-50-sm-257.jpg\":{\"name\":\"\"}}','',600.00,10,5,NULL,'2017-04-03 15:17:41',1,1,'2017-04-03 08:14:22','2017-04-03 12:17:41');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_translations`
--

DROP TABLE IF EXISTS `items_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`item_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_translations`
--

LOCK TABLES `items_translations` WRITE;
/*!40000 ALTER TABLE `items_translations` DISABLE KEYS */;
INSERT INTO `items_translations` VALUES ('ru',1,'kompoziciya-1','Композиція 1','','','',NULL,'<p>Чарівний осінній букет, наповнений яскравими фарбами.&nbsp;Букет з соняшником, антуріумом, гортензією, трояндами&nbsp;та різноманітною зеленню. Хороший подарунок для близької людини,&nbsp;який додасть тепла в останні теплі дні.</p>'),('ru',10,'kompoziciya-2','Композиція 2','','','',NULL,''),('ru',11,'vesilniy-buket-1','Весільний букет #2','','','',NULL,''),('ru',12,'vesilniy-buket-1','Весільний букет #1','','','',NULL,''),('ru',13,'korobka-z-kvitami-1','Коробка з квітами 1','','','',NULL,''),('ru',14,'korobka-z-kvitami-2','Коробка з квітами 2','','','',NULL,''),('ru',15,'korobka-z-solodoschami-1-','Коробка з солодощами 1 ','','','',NULL,''),('ru',16,'korobka-z-solodoschami-2','Коробка з солодощами 2','','','',NULL,''),('ru',17,'podarunok-1','Подарунок 1','','','',NULL,''),('ru',18,'podarunok-2','Подарунок 2','','','',NULL,'');
/*!40000 ALTER TABLE `items_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_s_subscribers`
--

DROP TABLE IF EXISTS `m_s_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_s_subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_s_subscribers`
--

LOCK TABLES `m_s_subscribers` WRITE;
/*!40000 ALTER TABLE `m_s_subscribers` DISABLE KEYS */;
INSERT INTO `m_s_subscribers` VALUES (1,'Олександр','Андрійченко',21,'хз','2017-04-04 12:19:56','2017-04-04 12:19:56'),(2,'a','b',21,'c','2017-04-04 12:20:52','2017-04-04 12:20:52'),(3,'й','ц',12,'у','2017-04-04 12:21:24','2017-04-04 12:21:24'),(4,'a','f',23,'к','2017-04-04 12:31:29','2017-04-04 12:31:29'),(5,'в','а',444,'а','2017-04-04 12:31:40','2017-04-04 12:31:40');
/*!40000 ALTER TABLE `m_s_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parent_id_foreign` (`parent_id`),
  CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,NULL,0),(2,1,0),(3,1,1),(4,1,2),(5,1,NULL),(6,1,NULL),(7,1,NULL);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus_translations`
--

DROP TABLE IF EXISTS `menus_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus_translations`
--

LOCK TABLES `menus_translations` WRITE;
/*!40000 ALTER TABLE `menus_translations` DISABLE KEYS */;
INSERT INTO `menus_translations` VALUES ('ua',1,'golovne-menu','Головне меню',NULL,'2017-04-26 07:53:17','2017-04-26 07:53:50'),('ru',2,'katalog','Каталог','',NULL,'2017-04-26 07:55:11'),('ua',2,'catalog','Каталог','','2017-04-26 07:53:24','2017-04-26 07:55:11'),('ru',3,'svadebnoe-oformlenye','Свадебное оформление','',NULL,'2017-04-26 07:56:28'),('ua',3,'vesilne-oformlennia','Весільне оформлення','','2017-04-26 07:53:37','2017-04-26 07:56:28'),('ua',4,'mayster-klas','Майстер Клас',NULL,'2017-04-26 07:53:47','2017-04-26 07:53:52'),('ua',5,'pidpyska-na-kvity','Підписка на квіти',NULL,'2017-04-26 07:54:03','2017-04-26 07:54:03'),('ua',6,'blog','Блог',NULL,'2017-04-26 07:54:08','2017-04-26 07:54:08'),('ua',7,'kontakty','Контакти',NULL,'2017-04-26 07:54:15','2017-04-26 07:54:15');
/*!40000 ALTER TABLE `menus_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `parameters` text COLLATE utf8_unicode_ci,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_item_id_foreign` (`item_id`),
  CONSTRAINT `order_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES (1,1,1,'Композиція 1','',500.00,NULL,4,'2017-04-04 13:49:06','2017-04-04 13:49:06'),(2,1,12,'Весільний букет #1','',333.00,NULL,2,'2017-04-04 13:49:06','2017-04-04 13:49:06');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `delivery` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery-date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery-time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  `amount` decimal(8,2) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'Admin','admin@example.com','333','asd',1,'Самовывоз',NULL,NULL,'ru','d',NULL,2666.00,'LiqPay',NULL,NULL,0,'127.0.0.1','58e3a4509c387',NULL,'2017-04-04 13:49:04','2017-04-04 13:49:04');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgs` text COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (3,NULL,NULL,0,3,1,'2017-04-03 09:51:00','2017-04-03 06:52:06','2017-04-26 07:58:45'),(4,'4.jpg','{\"1.png\":{\"name\":\"\"},\"2.png\":{\"name\":\"\"},\"3.png\":{\"name\":\"\"},\"4.png\":{\"name\":\"\"},\"5.png\":{\"name\":\"\"},\"4.jpg\":{\"name\":\"\"},\"7.jpg\":{\"name\":\"\"},\"9.jpg\":{\"name\":\"\"}}',2,4,1,'2017-04-03 13:35:00','2017-04-03 10:37:01','2017-04-26 08:07:14'),(5,'5.jpg','{\"1.png\":{\"name\":\"\"},\"2.png\":{\"name\":\"\"},\"3.png\":{\"name\":\"\"},\"4.png\":{\"name\":\"\"},\"5.png\":{\"name\":\"\"},\"buton.jpg\":{\"name\":\"\"}}',2,5,1,'2017-04-03 13:39:00','2017-04-03 10:43:58','2017-04-26 08:07:31'),(6,'6.jpg','{\"2015_servirovka-stola_servirovka-stola-7.jpg\":{\"name\":\"\"},\"5-novogodnij-stol-copy.jpg\":{\"name\":\"\"},\"6ffcc0d3641930e3d8980ec43343c.jpg\":{\"name\":\"\"},\"decor1collage_photocat.jpg\":{\"name\":\"\"},\"kak-ukrasit-novogodnij-stol-2016.jpg\":{\"name\":\"\"},\"zolotistoe-oformlenie.jpg\":{\"name\":\"\"},\"\\u041d\\u043e\\u0432\\u043e\\u0433\\u043e\\u0434\\u043d\\u0438\\u0439-\\u0441\\u0442\\u043e\\u043b-2015-\\u043a\\u0430\\u043a-\\u043e\\u0442\\u043f\\u0440\\u0430\\u0437\\u0434\\u043d\\u043e\\u0432\\u0430\\u0442\\u044c-\\u0433\\u043e\\u0434-\\u041a\\u043e\\u0437\\u044b.jpg\":{\"name\":\"\"}}',3,6,1,'2017-04-03 14:05:00','2017-04-03 11:06:32','2017-04-26 08:08:45'),(7,'7.jpg','{\"domashniy-zimoviy-zatishok.jpg\":{\"name\":\"\"},\"kimnatna-oranjereia.jpg\":{\"name\":\"\"},\"kompozitsiia-na-sviatkoviy-stil.jpg\":{\"name\":\"\"},\"navishcho-fotozona-na-vesilli.jpg\":{\"name\":\"\"},\"navishcho-potribna-dostavka-kvitiv-ta-chim-tse-zruchno.jpg\":{\"name\":\"\"},\"osinnia-kazka.jpg\":{\"name\":\"\"},\"vesniane-vesillia-iaki-kviti-vibrati.jpg\":{\"name\":\"\"},\"zabudte-pro-novorichni-turboti.jpg\":{\"name\":\"\"},\"zimova-klasika.jpg\":{\"name\":\"\"},\"zimovi-vesilni-buketi.jpg\":{\"name\":\"\"}}',3,7,1,'2017-04-03 14:08:00','2017-04-03 11:09:06','2017-04-26 08:09:07'),(8,'8.jpg','{\"7.jpg\":{\"name\":\"\"},\"9.jpg\":{\"name\":\"\"},\"1.png\":{\"name\":\"\"},\"2.png\":{\"name\":\"\"},\"3.png\":{\"name\":\"\"},\"4.png\":{\"name\":\"\"},\"5.png\":{\"name\":\"\"}}',2,8,1,'2017-04-03 14:58:00','2017-04-03 11:59:07','2017-04-26 08:07:54'),(9,'9.jpg','{\"9.jpg\":{\"name\":\"\"},\"1.png\":{\"name\":\"\"},\"2.png\":{\"name\":\"\"},\"3.png\":{\"name\":\"\"},\"4.png\":{\"name\":\"\"},\"5.png\":{\"name\":\"\"}}',2,9,1,'2017-04-03 15:01:00','2017-04-03 12:01:09','2017-04-26 08:08:16'),(10,'10.jpg','{\"1.png\":{\"name\":\"\"},\"2.png\":{\"name\":\"\"},\"3.png\":{\"name\":\"\"},\"4.png\":{\"name\":\"\"},\"5.png\":{\"name\":\"\"}}',2,10,1,'2017-04-04 13:33:00','2017-04-04 10:33:21','2017-04-26 08:09:35'),(11,'11.jpg','{\"4master-show-bg.png\":{\"name\":\"\"}}',1,11,1,'2017-04-04 15:44:00','2017-04-04 12:44:37','2017-04-04 12:47:26');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_translations`
--

DROP TABLE IF EXISTS `pages_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`page_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_translations`
--

LOCK TABLES `pages_translations` WRITE;
/*!40000 ALTER TABLE `pages_translations` DISABLE KEYS */;
INSERT INTO `pages_translations` VALUES ('ru',3,'contacts','Контакти','','','',NULL,''),('ua',3,'contacts','Контакти','','','',NULL,''),('ru',4,'vesilni-buketi','Весільні букети','','','',NULL,'<p>Букет нареченої є важливою складовою образу нареченої. Квіти повинні гармонійно поєднуватися з сукнею, а також<br />&nbsp;відповідати характеру нареченої. Ніжний або яскравий, мініатюрний або асиметричні - в будь-якому випадку,&nbsp;<br />букет Lacy Bird буде гармонійним, красивим і стильним. Флористика - мистецтво, доповнене емоціями і уявою.</p>'),('ua',4,'vesilni-bukety','Весільні букети','','','',NULL,'<p>Букет нареченої є важливою складовою образу нареченої. Квіти повинні гармонійно поєднуватися з сукнею, а також<br />&nbsp;відповідати характеру нареченої. Ніжний або яскравий, мініатюрний або асиметричні - в будь-якому випадку,&nbsp;<br />букет Lacy Bird буде гармонійним, красивим і стильним. Флористика - мистецтво, доповнене емоціями і уявою.</p>'),('ru',5,'butonierki','Бутоньєрки','','','',NULL,'<p>Будь яке весілля також не обходиться без бутоньєрки. Вони можуть додати фінальний штрих до образу нареченого. Однак вибираючи бутоньєрку пам&#39;ятайте, що вона повинна не тільки підходити до костюму нареченого, а й гармонійно поєднюватися з букетом нареченої. Дуже романтично, коли квіти в петлиці нареченого схожі на ті, з яких складено букет нареченої (або точно такі ж).</p>'),('ua',5,'butonierky','Бутоньєрки','','','',NULL,'<p>Будь яке весілля також не обходиться без бутоньєрки. Вони можуть додати фінальний штрих до образу нареченого. Однак вибираючи бутоньєрку пам&#39;ятайте, що вона повинна не тільки підходити до костюму нареченого, а й гармонійно поєднюватися з букетом нареченої. Дуже романтично, коли квіти в петлиці нареченого схожі на ті, з яких складено букет нареченої (або точно такі ж).</p>'),('ru',6,'stvorennya-florariumu','Створення флораріуму','','500грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.<br />&nbsp;&nbsp;</p>'),('ua',6,'stvorennia-florariumu','Створення флораріуму','','500грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.<br />&nbsp;&nbsp;</p>'),('ru',7,'vigotovlennya-pashalnogo-vinochka','Виготовлення пасхального віночка','','1700грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.<br />&nbsp;&nbsp;</p>'),('ua',7,'vygotovlennia-paskhalnogo-vinochka','Виготовлення пасхального віночка','','1700грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.</p>'),('ru',8,'buketi-dlya-drujok','Букети для дружок','','','',NULL,'<p>Все частіше ми бачимо на західних весіллях, що подружки нареченої не тільки вбрані в схожі за стилем сукні, а й кожна тримає в руках по невеликому акуратному букетику. Букет подружки нареченої - навіщо він потрібнен, і яким він повинен бути? Схоже, наречені стають все сентиментальнішими бо все частіше бажають залишити свій весільний букет на довгу пам&#39;ять про святий день одруження. Але від улюбленої, усіма без винятку дівчатами, традиції ловити весільний букет, відмовлятися ніхто не готовий, тому букет однієї з подружок часто під вечір стає Головним Букетом Свята, і летить в натовп незаміжніх подруг.</p>'),('ua',8,'ukr','Букети для дружок','','tyui7yi','',NULL,'<p>Все частіше ми бачимо на західних весіллях, що подружки нареченої не тільки вбрані в схожі за стилем сукні, а й кожна тримає в руках по невеликому акуратному букетику. Букет подружки нареченої - навіщо він потрібнен, і яким він повинен бути? Схоже, наречені стають все сентиментальнішими бо все частіше бажають залишити свій весільний букет на довгу пам&#39;ять про святий день одруження. Але від улюбленої, усіма без винятку дівчатами, традиції ловити весільний букет, відмовлятися ніхто не готовий, тому букет однієї з подружок часто під вечір стає Головним Букетом Свята, і летить в натовп незаміжніх подруг.</p>'),('ru',9,'oformlennya-zalu','Оформлення залу','','','',NULL,'<p>Одним з найважливіших пунктів підготовки до весілля є оформлення залу. І цей пункт плану вимагає особливого підходу і уваги.&nbsp;Оформлення весільного залу - це наука, яка виросла з кращих весільних традицій, як нашої країни, так і європейських країн.&nbsp;Спочатку Вам треба ретельно обговорити оформлення залу з нашими флористами, для того, щоб всі елементи декору&nbsp;поєднувалися, як з кольором, так і з тематикою Вашого свята. Крім того, оформлення банкетних залів має підкреслювати&nbsp;індивідуальність нареченого і нареченої.&nbsp;</p>'),('ua',9,'oformlennia-zalu','Оформлення залу','','','',NULL,'<p>Одним з найважливіших пунктів підготовки до весілля є оформлення залу. І цей пункт плану вимагає особливого підходу і уваги.&nbsp;Оформлення весільного залу - це наука, яка виросла з кращих весільних традицій, як нашої країни, так і європейських країн.&nbsp;Спочатку Вам треба ретельно обговорити оформлення залу з нашими флористами, для того, щоб всі елементи декору&nbsp;поєднувалися, як з кольором, так і з тематикою Вашого свята. Крім того, оформлення банкетних залів має підкреслювати&nbsp;індивідуальність нареченого і нареченої.&nbsp;</p>'),('ru',10,'oformlennya-mashin','Оформление машин','','','',NULL,'<p>Без оформлення весільної машини сьогодні не обходиться ні одне весілля. Кожна пара молодят хоче, щоб в цей урочистий&nbsp;день &nbsp;їх машина також виглядала достойно. Зараз молодята не прикріплюють ляльку на капот та не оформляють машину&nbsp;для весілля за допомогою штучних квітів. Прикраси для весільних машин призначені для того, щоб привернути увагу людей&nbsp;<br />до весільного ескорту.&nbsp;</p>'),('ua',10,'oformlennia-mashyn','Оформлення машин','','','',NULL,'<p>Без оформлення весільної машини сьогодні не обходиться ні одне весілля. Кожна пара молодят хоче, щоб в цей урочистий&nbsp;день &nbsp;їх машина також виглядала достойно. Зараз молодята не прикріплюють ляльку на капот та не оформляють машину&nbsp;для весілля за допомогою штучних квітів. Прикраси для весільних машин призначені для того, щоб привернути увагу людей&nbsp;<br />до весільного ескорту.&nbsp;</p>'),('ru',11,'kimnatna-oranjereya','Кімнатна оранжерея','','оранжерея\r\nкімнатна\r\n...','',NULL,'<p>тут контент .................</p><p>текст +</p><p><img alt=\"\" style=\"width: 250px; height: 250px;\" src=\"http://kvitochka/photos/shares/58e395d551298.png\" /></p>');
/*!40000 ALTER TABLE `pages_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (19,'frui1.jpg','/upload/photosfrui1.jpg','2017-04-25 09:10:39','2017-04-25 09:10:39'),(20,'surprise.jpg','/upload/photos/surprise.jpg','2017-04-25 10:46:49','2017-04-25 10:46:49');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubrics`
--

DROP TABLE IF EXISTS `rubrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rubrics_parent_id_foreign` (`parent_id`),
  CONSTRAINT `rubrics_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `rubrics` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubrics`
--

LOCK TABLES `rubrics` WRITE;
/*!40000 ALTER TABLE `rubrics` DISABLE KEYS */;
INSERT INTO `rubrics` VALUES (1,NULL,NULL),(2,NULL,NULL),(3,NULL,NULL);
/*!40000 ALTER TABLE `rubrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubrics_translations`
--

DROP TABLE IF EXISTS `rubrics_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubrics_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rubric_id` int(10) unsigned NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rubric_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubrics_translations`
--

LOCK TABLES `rubrics_translations` WRITE;
/*!40000 ALTER TABLE `rubrics_translations` DISABLE KEYS */;
INSERT INTO `rubrics_translations` VALUES ('ru',1,'blog','Блог','','2017-03-22 14:11:58','2017-04-26 08:03:10'),('ua',1,'blog','Блог','',NULL,'2017-04-26 08:03:10'),('ru',2,'wedding-design','Свадебное оформление','','2017-03-22 14:12:03','2017-04-26 08:03:40'),('ua',2,'wedding-design','Весільне Оформлення','',NULL,'2017-04-26 08:03:40'),('ru',3,'master-show','Майстер Клас','','2017-03-22 14:12:07','2017-04-26 08:03:54'),('ua',3,'master-show','Мастер Клас','',NULL,'2017-04-26 08:03:54');
/*!40000 ALTER TABLE `rubrics_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribers_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_social_account`
--

DROP TABLE IF EXISTS `user_social_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_social_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_social_account`
--

LOCK TABLES `user_social_account` WRITE;
/*!40000 ALTER TABLE `user_social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_social_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin',NULL,NULL,1,'$2y$10$ozTwJjFnVi8jYHPjGoxaHOj/c0QwM5Wj0sfQclg8m1zZBV3S.cJb2',NULL,NULL,NULL),(2,'Саня Андрійченко','andryichenko123@gmail.com','132490806','vk',1,NULL,NULL,'2017-04-26 13:20:34','2017-04-26 13:20:34');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-26 16:41:59
