-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: alex_kvitochka
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `cart_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `carts_user_id_foreign` (`user_id`),
  CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carts`
--

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
INSERT INTO `carts` VALUES (1,NULL,'590bb5a365df7','{\"356a192b7913b04c54574d18c28d46e6395428ab\":{\"parameters\":null,\"id\":\"1\",\"count\":2}}','127.0.0.1','2017-05-04 23:13:39','2017-05-05 00:02:59'),(2,NULL,'590bd5612b9c0','{\"f6e1126cedebf23e1463aee73f9df08783640400\":{\"parameters\":null,\"id\":\"25\",\"count\":1},\"887309d048beef83ad3eabf2a79a64a389ab1c9f\":{\"parameters\":null,\"id\":\"26\",\"count\":1}}','127.0.0.1','2017-05-05 01:29:05','2017-05-07 12:35:43'),(3,NULL,'590f9b811375a','{\"7719a1c782a1ba91c031a682a0a2f8658209adbf\":{\"parameters\":null,\"id\":\"29\",\"count\":1},\"356a192b7913b04c54574d18c28d46e6395428ab\":{\"parameters\":null,\"id\":\"1\",\"count\":1}}','127.0.0.1','2017-05-07 22:11:13','2017-05-07 22:43:01'),(4,NULL,'590fa385a0c8d','{\"887309d048beef83ad3eabf2a79a64a389ab1c9f\":{\"parameters\":null,\"id\":\"26\",\"count\":1}}','127.0.0.1','2017-05-07 22:45:25','2017-05-07 22:45:25'),(5,NULL,'590fa3c41488b','{\"356a192b7913b04c54574d18c28d46e6395428ab\":{\"parameters\":null,\"id\":\"1\",\"count\":1}}','127.0.0.1','2017-05-07 22:46:28','2017-05-07 22:46:28'),(6,NULL,'590fb77e2e3a3','{\"b37f6ddcefad7e8657837d3177f9ef2462f98acf\":{\"parameters\":null,\"id\":\"88\",\"count\":1}}','10.4.83.197','2017-05-08 00:10:38','2017-05-11 16:55:23'),(7,NULL,'591150cfe506f','{\"bd307a3ec329e10a2cff8fb87480823da114f8f4\":{\"parameters\":null,\"id\":\"13\",\"count\":1},\"7719a1c782a1ba91c031a682a0a2f8658209adbf\":{\"parameters\":null,\"id\":\"29\",\"count\":1}}','10.4.83.197','2017-05-09 05:17:03','2017-05-09 05:28:23'),(8,NULL,'59116193453d9','{\"632667547e7cd3e0466547863e1207a8c0c0c549\":{\"parameters\":null,\"id\":\"31\",\"count\":1}}','10.4.83.197','2017-05-09 06:28:35','2017-05-09 06:28:35'),(9,NULL,'591161e7d9cce','{\"632667547e7cd3e0466547863e1207a8c0c0c549\":{\"parameters\":null,\"id\":\"31\",\"count\":1}}','10.4.83.197','2017-05-09 06:29:59','2017-05-09 06:29:59'),(10,NULL,'5911660ac5d28','{\"632667547e7cd3e0466547863e1207a8c0c0c549\":{\"parameters\":null,\"id\":\"31\",\"count\":1}}','10.4.83.197','2017-05-09 06:47:38','2017-05-09 06:47:38'),(11,NULL,'591169f608ce7','{\"1574bddb75c78a6fd2251d61e2993b5146201319\":{\"parameters\":null,\"id\":\"16\",\"count\":2},\"cb4e5208b4cd87268b208e49452ed6e89a68e0b8\":{\"parameters\":null,\"id\":\"32\",\"count\":1},\"632667547e7cd3e0466547863e1207a8c0c0c549\":{\"parameters\":null,\"id\":\"31\",\"count\":1}}','10.4.83.197','2017-05-09 07:04:22','2017-05-09 12:25:00'),(12,NULL,'5911834fa4a87','{\"7719a1c782a1ba91c031a682a0a2f8658209adbf\":{\"parameters\":null,\"id\":\"29\",\"count\":1},\"b37f6ddcefad7e8657837d3177f9ef2462f98acf\":{\"parameters\":null,\"id\":\"88\",\"count\":1}}','10.4.83.197','2017-05-09 08:52:31','2017-05-10 14:29:06'),(13,NULL,'5911b5a5ba1d7','{\"7719a1c782a1ba91c031a682a0a2f8658209adbf\":{\"parameters\":null,\"id\":\"29\",\"count\":1}}','10.4.83.197','2017-05-09 12:27:17','2017-05-09 12:27:17'),(14,NULL,'5911f359188f4','{\"761f22b2c1593d0bb87e0b606f990ba4974706de\":{\"parameters\":null,\"id\":\"41\",\"count\":1}}','10.4.83.197','2017-05-09 16:50:33','2017-05-09 16:50:33'),(15,NULL,'591312c633090','{\"b37f6ddcefad7e8657837d3177f9ef2462f98acf\":{\"parameters\":null,\"id\":\"88\",\"count\":1}}','10.4.83.197','2017-05-10 13:16:54','2017-05-12 11:53:05'),(16,NULL,'59142fae5bad7','{\"8effee409c625e1a2d8f5033631840e6ce1dcb64\":{\"parameters\":null,\"id\":\"55\",\"count\":2}}','10.4.83.197','2017-05-11 09:32:30','2017-05-11 09:35:12'),(17,NULL,'591430b01ddd4','{\"8effee409c625e1a2d8f5033631840e6ce1dcb64\":{\"parameters\":null,\"id\":\"55\",\"count\":1},\"667be543b02294b7624119adc3a725473df39885\":{\"parameters\":null,\"id\":\"58\",\"count\":1}}','10.4.83.197','2017-05-11 09:36:48','2017-05-11 09:36:56'),(18,NULL,'5914315ed36f9','{\"6c1e671f9af5b46d9c1a52067bdf0e53685674f7\":{\"parameters\":null,\"id\":\"61\",\"count\":1}}','10.4.83.197','2017-05-11 09:39:42','2017-05-11 12:54:46'),(19,NULL,'591455dfd61a2','{\"b37f6ddcefad7e8657837d3177f9ef2462f98acf\":{\"parameters\":null,\"id\":\"88\",\"count\":1}}','10.4.83.197','2017-05-11 12:15:27','2017-05-11 13:57:00'),(20,NULL,'59145f72d9013','{\"59129aacfb6cebbe2c52f30ef3424209f7252e82\":{\"parameters\":null,\"id\":\"66\",\"count\":1}}','10.4.83.197','2017-05-11 12:56:18','2017-05-11 12:56:28'),(21,NULL,'59145fe781dc4','{\"40bd001563085fc35165329ea1ff5c5ecbdbbeef\":{\"parameters\":null,\"id\":\"123\",\"count\":1}}','10.4.83.197','2017-05-11 12:58:15','2017-05-11 13:23:53'),(22,NULL,'591469d620cc3','{\"98fbc42faedc02492397cb5962ea3a3ffc0a9243\":{\"parameters\":null,\"id\":\"44\",\"count\":1}}','10.4.83.197','2017-05-11 13:40:38','2017-05-11 13:59:26'),(23,NULL,'59146e7348c34','{\"98fbc42faedc02492397cb5962ea3a3ffc0a9243\":{\"parameters\":null,\"id\":\"44\",\"count\":1}}','10.4.83.197','2017-05-11 14:00:19','2017-05-11 14:00:19'),(24,NULL,'59146eb328e4e','{\"98fbc42faedc02492397cb5962ea3a3ffc0a9243\":{\"parameters\":null,\"id\":\"44\",\"count\":1}}','10.4.83.197','2017-05-11 14:01:23','2017-05-11 14:01:23'),(25,NULL,'59146ee8067a4','{\"98fbc42faedc02492397cb5962ea3a3ffc0a9243\":{\"parameters\":null,\"id\":\"44\",\"count\":1}}','10.4.83.197','2017-05-11 14:02:16','2017-05-11 14:02:16'),(26,NULL,'591496dc3c3ab','{\"fb644351560d8296fe6da332236b1f8d61b2828a\":{\"parameters\":null,\"id\":\"45\",\"count\":1}}','10.4.83.197','2017-05-11 16:52:44','2017-05-11 17:03:18'),(27,NULL,'591499a23bc01','{\"fb644351560d8296fe6da332236b1f8d61b2828a\":{\"parameters\":null,\"id\":\"45\",\"count\":1}}','10.4.83.197','2017-05-11 17:04:34','2017-05-11 17:04:34'),(28,NULL,'59157d4803cf1','{\"0ca9277f91e40054767f69afeb0426711ca0fddd\":{\"parameters\":null,\"id\":\"125\",\"count\":1}}','10.4.83.197','2017-05-12 09:15:52','2017-05-12 09:15:52'),(29,NULL,'591586b966848','{\"b37f6ddcefad7e8657837d3177f9ef2462f98acf\":{\"parameters\":null,\"id\":\"88\",\"count\":1}}','10.4.83.197','2017-05-12 09:56:09','2017-05-12 09:56:09'),(30,NULL,'59159d3006a6f','{\"1d513c0bcbe33b2e7440e5e14d0b22ef95c9d673\":{\"parameters\":null,\"id\":\"81\",\"count\":1}}','10.4.83.197','2017-05-12 11:32:00','2017-05-12 11:44:52'),(31,NULL,'5915a1b189ef0','{\"0ca9277f91e40054767f69afeb0426711ca0fddd\":{\"parameters\":null,\"id\":\"125\",\"count\":1}}','10.4.83.197','2017-05-12 11:51:13','2017-05-12 11:51:13'),(32,NULL,'5915a4146f680','{\"a17554a0d2b15a664c0e73900184544f19e70227\":{\"parameters\":null,\"id\":\"63\",\"count\":1}}','10.4.83.197','2017-05-12 12:01:24','2017-05-12 12:01:24'),(33,NULL,'5915a63b87ffb','{\"40bd001563085fc35165329ea1ff5c5ecbdbbeef\":{\"parameters\":null,\"id\":\"123\",\"count\":1}}','10.4.83.197','2017-05-12 12:10:35','2017-05-12 12:10:35'),(34,NULL,'5915b32142c41','{\"f38cfe2e2facbcc742bad63f91ad55637300cb45\":{\"parameters\":null,\"id\":\"124\",\"count\":1}}','10.4.83.197','2017-05-12 13:05:37','2017-05-12 13:05:37'),(35,NULL,'5915b534d1190','{\"b37f6ddcefad7e8657837d3177f9ef2462f98acf\":{\"parameters\":null,\"id\":\"88\",\"count\":1}}','10.4.83.197','2017-05-12 13:14:28','2017-05-12 13:14:28'),(36,NULL,'5915b648185e9','{\"05a8ea5382b9fd885261bb3eed0527d1d3b07262\":{\"parameters\":null,\"id\":\"122\",\"count\":1}}','10.4.83.197','2017-05-12 13:19:04','2017-05-12 14:02:02');
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'1.jpg',NULL,4),(2,'2.jpg',NULL,3),(3,'3.jpg',NULL,2),(4,'4.jpg',NULL,1),(5,'5.jpg',NULL,0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_translations`
--

DROP TABLE IF EXISTS `categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_translations`
--

LOCK TABLES `categories_translations` WRITE;
/*!40000 ALTER TABLE `categories_translations` DISABLE KEYS */;
INSERT INTO `categories_translations` VALUES ('en',1,'','','',NULL,'2017-05-10 11:02:07'),('ru',1,'kompozicii','Композиции','','2017-03-31 10:27:22','2017-05-10 11:02:07'),('ua',1,'kompozytsii','Композиції','',NULL,'2017-05-12 11:48:39'),('en',2,'','','',NULL,'2017-05-10 10:39:41'),('ru',2,'vesilni-buketi','Свадебные букеты','','2017-03-31 10:27:45','2017-05-10 10:39:41'),('ua',2,'vesilni-bukety','Весільні букети','',NULL,'2017-05-12 11:48:39'),('en',3,'','','',NULL,'2017-05-10 11:16:02'),('ru',3,'korobki-z-kvitami','Коробки з цветами','','2017-03-31 10:28:07','2017-05-10 11:16:02'),('ua',3,'korobky-z-kvitamy','Коробки з квітами','',NULL,'2017-05-12 11:48:39'),('en',4,'','','',NULL,'2017-05-12 12:59:53'),('ru',4,'korobki-z-solodoschami','Коробки з сладостями','','2017-03-31 10:28:15','2017-05-12 12:59:53'),('ua',4,'korobky-z-solodoshchamy','Коробки з солодощами','',NULL,'2017-05-12 12:59:53'),('en',5,'','','',NULL,'2017-05-12 09:24:44'),('ru',5,'podarunki','Подарки','','2017-03-31 10:28:24','2017-05-12 09:24:44'),('ua',5,'podarunky','Подарунки','цікаві подаруночки',NULL,'2017-05-12 11:48:39');
/*!40000 ALTER TABLE `categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
INSERT INTO `certificates` VALUES (1,'asdasd','asdasd','1@mail.com',300,'2017-04-21 11:59:04','2017-04-21 11:59:04'),(2,'12312','12312312','awdasd@mail.com',500,'2017-04-21 11:59:24','2017-04-21 11:59:24'),(3,'','','',400,'2017-05-05 03:47:32','2017-05-05 03:47:32'),(4,'','','',300,'2017-05-07 23:01:12','2017-05-07 23:01:12'),(5,'фівфів','123123','123123123@email.com',400,'2017-05-07 23:05:27','2017-05-07 23:05:27'),(6,'Марта','986986986','marta.fedoruk@redentu.com',500,'2017-05-09 09:50:06','2017-05-09 09:50:06'),(7,'Марта','66666666','marta.fedoruk@redentu.com',500,'2017-05-10 08:51:03','2017-05-10 08:51:03'),(8,'апрапрап','986986986','marta.fedoruk@redentu.com',500,'2017-05-11 09:54:31','2017-05-11 09:54:31'),(10,'Марта','+3(8777)777-77-77','marta.fedoruk@redentu.com',400,'2017-05-12 09:11:09','2017-05-12 09:11:09'),(11,'Марта','+3(8777)777-77-77','marta.fedoruk@redentu.com',400,'2017-05-12 10:02:06','2017-05-12 10:02:06');
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commentable_id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_orders`
--

DROP TABLE IF EXISTS `coupon_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `coupon_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_orders`
--

LOCK TABLES `coupon_orders` WRITE;
/*!40000 ALTER TABLE `coupon_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oneoff` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `type` enum('*','-') COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(8,2) NOT NULL,
  `expired` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupons_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f-subscribers`
--

DROP TABLE IF EXISTS `f-subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f-subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abonement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` text COLLATE utf8_unicode_ci,
  `additional_info` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f-subscribers`
--

LOCK TABLES `f-subscribers` WRITE;
/*!40000 ALTER TABLE `f-subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `f-subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f_subscribers`
--

DROP TABLE IF EXISTS `f_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f_subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` mediumtext COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abonement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `information` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f_subscribers`
--

LOCK TABLES `f_subscribers` WRITE;
/*!40000 ALTER TABLE `f_subscribers` DISABLE KEYS */;
INSERT INTO `f_subscribers` VALUES (1,'Андрійченко','3333','test@example.com','lviv','Рік','15.00-17.00','L',NULL,'2017-04-04 12:52:46','2017-04-04 12:52:46'),(2,'Sid','420','sidthesquid@gmail.com','blue ocean','Квартал','any','S',NULL,'2017-04-04 12:57:31','2017-04-04 12:57:31'),(3,'s','2','d','sad','Місяць','asd','S',NULL,'2017-04-04 13:00:18','2017-04-04 13:00:18'),(4,'iisiiiii','402040240240240','asd@maul.com','asdsaddead','Місяць','123812390','M',NULL,'2017-05-07 22:10:22','2017-05-07 22:10:22'),(5,'прварап','657567856857','marta.fedoruk@redentu.com','пропропропр','Місяць','32534','S',NULL,'2017-05-09 06:04:36','2017-05-09 06:04:36'),(6,'Marta','46756756','marta.fedoruk@redentu.com','jfghl dhfghdl ','Місяць','21:00','M',NULL,'2017-05-09 09:42:27','2017-05-09 09:42:27'),(8,'name','+3(8981)291-26-39','admin@admin.admin','saddddaeeersrss','Місяць','123123123123123','M','daddaitiino lal info','2017-05-10 09:49:39','2017-05-10 09:49:39'),(10,'ббвопр','+3(8986)986-98-6','marta.fedoruk@redentu.com','апрапр','Рік','02:10','M','авлп овдлоап длваодпл олав пдлвоап лдвадлп оавпл овдлаоп длваоплд одвла подлвадлпо двлаопдл овдлопдлво длопвалодлвпддвплдлаоплд овадлп длвопдловлдплваодлп двлапдлвадлпдлва длдвлао','2017-05-10 11:06:01','2017-05-10 11:06:01'),(11,'Віталій','+3(8777)777-77-77','marta.fedoruk@redentu.com','м. Львів','Місяць','12:00','M','привіт усім з Реденту','2017-05-10 12:59:26','2017-05-10 12:59:26'),(12,'сапр','+3(8666)666-66-66','marta.fedoruk@redentu.com','апрапр','Місяць','12:00','S','апра па пр апр','2017-05-11 09:52:36','2017-05-11 09:52:36');
/*!40000 ALTER TABLE `f_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `favorites_user_id_foreign` (`user_id`),
  KEY `favorites_item_id_foreign` (`item_id`),
  CONSTRAINT `favorites_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fb-users`
--

DROP TABLE IF EXISTS `fb-users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb-users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fb_users_email_unique` (`email`),
  UNIQUE KEY `fb_users_facebook_id_unique` (`facebook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fb-users`
--

LOCK TABLES `fb-users` WRITE;
/*!40000 ALTER TABLE `fb-users` DISABLE KEYS */;
/*!40000 ALTER TABLE `fb-users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters`
--

DROP TABLE IF EXISTS `filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters_parent_id_foreign` (`parent_id`),
  CONSTRAINT `filters_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters`
--

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters_translations`
--

DROP TABLE IF EXISTS `filters_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`filter_id`,`locale`),
  CONSTRAINT `filters_translations_filter_id_foreign` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters_translations`
--

LOCK TABLES `filters_translations` WRITE;
/*!40000 ALTER TABLE `filters_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `filters_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `img` text COLLATE utf8_unicode_ci,
  `level` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_categories`
--

DROP TABLE IF EXISTS `galleries_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `img_width` int(11) DEFAULT NULL,
  `img_heigth` int(11) DEFAULT NULL,
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_heigth` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `galleries_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `galleries_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_categories`
--

LOCK TABLES `galleries_categories` WRITE;
/*!40000 ALTER TABLE `galleries_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_categories_translations`
--

DROP TABLE IF EXISTS `galleries_categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_categories_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_categories_translations`
--

LOCK TABLES `galleries_categories_translations` WRITE;
/*!40000 ALTER TABLE `galleries_categories_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_translations`
--

DROP TABLE IF EXISTS `galleries_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`gallery_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_translations`
--

LOCK TABLES `galleries_translations` WRITE;
/*!40000 ALTER TABLE `galleries_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_filters`
--

DROP TABLE IF EXISTS `item_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_filters_item_id_foreign` (`item_id`),
  KEY `item_filters_filter_id_foreign` (`filter_id`),
  CONSTRAINT `item_filters_filter_id_foreign` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_filters_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_filters`
--

LOCK TABLES `item_filters` WRITE;
/*!40000 ALTER TABLE `item_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_filters_translations`
--

DROP TABLE IF EXISTS `item_filters_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_filters_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_filter_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`item_filter_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_filters_translations`
--

LOCK TABLES `item_filters_translations` WRITE;
/*!40000 ALTER TABLE `item_filters_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_filters_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `price` decimal(8,2) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `filter_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (44,'{\"l00FyIcyXYI.jpg\":{\"name\":\"\"}}',900.00,4,3,NULL,'2017-05-10 10:05:01',1,'2017-05-10 05:25:12','2017-05-10 13:36:33'),(45,'{\"XIQTxI0Ts78.jpg\":{\"name\":\"\"}}',800.00,3,3,NULL,'2017-05-10 10:10:49',1,'2017-05-10 05:29:02','2017-05-10 13:36:33'),(46,'{\"Suuk1XGaqbY.jpg\":{\"name\":\"\"}}',1000.00,36,3,NULL,'2017-05-10 10:11:34',1,'2017-05-10 05:31:12','2017-05-10 07:11:35'),(47,'{\"G\\u0117li\\u0173-d\\u0117\\u017eut\\u0117-Beatri\\u010d\\u0117s-G\\u0117li\\u0173-Namai-1.jpg\":{\"name\":\"\"}}',900.00,37,3,NULL,'2017-05-10 09:50:07',1,'2017-05-10 05:32:32','2017-05-10 06:50:07'),(48,'{\"rv5zV_kVwL4.jpg\":{\"name\":\"\"}}',850.00,38,3,NULL,'2017-05-10 10:12:07',1,'2017-05-10 05:54:10','2017-05-10 07:12:07'),(49,'{\"BMxF57Z93Cc.jpg\":{\"name\":\"\"}}',1000.00,39,3,NULL,'2017-05-10 10:12:46',1,'2017-05-10 06:16:19','2017-05-10 07:12:46'),(50,'{\"Yipp9ISlmEQ.jpg\":{\"name\":\"\"}}',1200.00,40,3,NULL,'2017-05-11 14:09:00',1,'2017-05-10 06:17:36','2017-05-11 11:09:00'),(51,'{\"DoJJaNAVNbE.jpg\":{\"name\":\"\"}}',800.00,41,3,NULL,'2017-05-10 10:14:05',1,'2017-05-10 06:19:18','2017-05-10 07:14:05'),(52,'{\"14.jpg\":{\"name\":\"\"}}',1000.00,42,3,NULL,'2017-05-10 10:01:24',1,'2017-05-10 06:20:12','2017-05-10 07:01:24'),(53,'{\"n_FfnemAXm4.jpg\":{\"name\":\"\"}}',800.00,43,3,NULL,'2017-05-10 10:14:37',1,'2017-05-10 06:20:58','2017-05-10 07:14:37'),(54,'{\"kIz4qwuzT0A.jpg\":{\"name\":\"\"}}',1000.00,44,3,NULL,'2017-05-10 10:15:19',1,'2017-05-10 06:26:57','2017-05-10 07:15:19'),(55,'{\"-gBpA49u3UY.jpg\":{\"name\":\"\"}}',1200.00,45,3,NULL,'2017-05-11 14:09:59',1,'2017-05-10 06:28:11','2017-05-11 11:09:59'),(56,'{\"tQsMolljqBU.jpg\":{\"name\":\"\"}}',1200.00,46,3,NULL,'2017-05-10 10:16:48',1,'2017-05-10 06:29:55','2017-05-10 07:16:48'),(57,'{\"37iL_vE83Qs.jpg\":{\"name\":\"\"}}',1200.00,17,3,NULL,'2017-05-10 10:17:18',1,'2017-05-10 06:30:41','2017-05-10 14:30:55'),(58,'{\"xmiGGBLZDbQ.jpg\":{\"name\":\"\"}}',900.00,18,3,NULL,'2017-05-10 10:17:50',1,'2017-05-10 06:32:06','2017-05-10 14:30:55'),(59,'{\"1.jpg\":{\"name\":\"\"}}',1200.00,1,1,NULL,'2017-05-10 09:33:45',1,'2017-05-10 06:33:45','2017-05-11 18:31:43'),(60,'{\"2.jpg\":{\"name\":\"\"}}',900.00,4,1,NULL,'2017-05-10 09:34:46',1,'2017-05-10 06:34:46','2017-05-11 18:31:40'),(61,'{\"3.jpg\":{\"name\":\"\"}}',600.00,5,1,NULL,'2017-05-10 09:36:34',1,'2017-05-10 06:36:34','2017-05-11 18:31:40'),(62,'{\"4.jpg\":{\"name\":\"\"}}',500.00,6,1,NULL,'2017-05-10 09:39:25',1,'2017-05-10 06:39:25','2017-05-11 18:31:40'),(63,'{\"5.jpg\":{\"name\":\"\"}}',1300.00,0,1,NULL,'2017-05-12 14:44:54',1,'2017-05-10 06:41:07','2017-05-12 11:44:54'),(64,'{\"6.jpg\":{\"name\":\"\"}}',400.00,7,1,NULL,'2017-05-10 09:42:40',1,'2017-05-10 06:41:56','2017-05-11 18:31:40'),(65,'{\"7.jpg\":{\"name\":\"\"}}',800.00,8,1,NULL,'2017-05-10 10:20:09',1,'2017-05-10 07:20:09','2017-05-11 18:31:40'),(66,'{\"8.jpg\":{\"name\":\"\"}}',800.00,9,1,NULL,'2017-05-10 10:24:56',1,'2017-05-10 07:24:56','2017-05-11 18:31:40'),(67,'{\"9.jpg\":{\"name\":\"\"}}',900.00,2,1,NULL,'2017-05-10 10:26:13',1,'2017-05-10 07:25:39','2017-05-11 18:31:27'),(68,'{\"10.jpg\":{\"name\":\"\"}}',700.00,10,1,NULL,'2017-05-10 10:27:13',1,'2017-05-10 07:27:13','2017-05-11 18:31:40'),(69,'{\"11.jpg\":{\"name\":\"\"}}',650.00,11,1,NULL,'2017-05-10 10:27:52',1,'2017-05-10 07:27:52','2017-05-11 18:31:40'),(70,'{\"12.jpg\":{\"name\":\"\"}}',550.00,12,1,NULL,'2017-05-10 10:28:38',1,'2017-05-10 07:28:38','2017-05-11 18:31:40'),(71,'{\"13.jpg\":{\"name\":\"\"}}',300.00,13,1,NULL,'2017-05-10 10:30:06',1,'2017-05-10 07:30:06','2017-05-11 18:31:40'),(72,'{\"14.jpg\":{\"name\":\"\"}}',800.00,3,1,NULL,'2017-05-10 10:31:00',1,'2017-05-10 07:31:00','2017-05-11 18:31:40'),(73,'{\"15.jpg\":{\"name\":\"\"}}',750.00,63,1,NULL,'2017-05-10 10:32:28',1,'2017-05-10 07:32:14','2017-05-10 07:32:28'),(74,'{\"16.jpg\":{\"name\":\"\"}}',750.00,64,1,NULL,'2017-05-10 10:33:32',1,'2017-05-10 07:33:32','2017-05-10 07:33:32'),(75,'{\"17.jpg\":{\"name\":\"\"}}',300.00,65,1,NULL,'2017-05-10 10:34:41',1,'2017-05-10 07:34:19','2017-05-10 07:34:41'),(76,'{\"18.jpg\":{\"name\":\"\"}}',650.00,66,1,NULL,'2017-05-10 10:39:51',1,'2017-05-10 07:39:51','2017-05-10 07:39:51'),(77,'{\"19.jpg\":{\"name\":\"\"}}',750.00,67,1,NULL,'2017-05-10 10:43:20',1,'2017-05-10 07:43:20','2017-05-10 07:43:20'),(78,'{\"20.jpg\":{\"name\":\"\"}}',650.00,68,1,NULL,'2017-05-10 10:45:08',1,'2017-05-10 07:45:08','2017-05-10 07:45:08'),(79,'{\"21.jpg\":{\"name\":\"\"}}',1000.00,69,1,NULL,'2017-05-12 11:27:51',1,'2017-05-10 07:46:07','2017-05-12 08:27:51'),(80,'{\"22.jpg\":{\"name\":\"\"}}',1000.00,70,1,NULL,'2017-05-10 10:46:55',1,'2017-05-10 07:46:55','2017-05-10 07:46:55'),(81,'{\"23.jpg\":{\"name\":\"\"}}',700.00,71,1,NULL,'2017-05-10 10:48:10',1,'2017-05-10 07:48:10','2017-05-10 07:48:10'),(82,'{\"24.jpg\":{\"name\":\"\"}}',2000.00,72,1,NULL,'2017-05-10 10:55:46',1,'2017-05-10 07:55:46','2017-05-10 07:55:46'),(83,'{\"25.jpg\":{\"name\":\"\"}}',1000.00,73,1,NULL,'2017-05-10 10:57:10',1,'2017-05-10 07:57:10','2017-05-10 07:57:10'),(84,'{\"26.jpg\":{\"name\":\"\"}}',900.00,74,1,NULL,'2017-05-10 10:58:04',1,'2017-05-10 07:58:04','2017-05-10 07:58:04'),(85,'{\"27.jpg\":{\"name\":\"\"}}',750.00,75,1,NULL,'2017-05-10 10:59:17',1,'2017-05-10 07:59:17','2017-05-10 07:59:17'),(86,'{\"28.jpg\":{\"name\":\"\"}}',800.00,76,1,NULL,'2017-05-10 11:00:32',1,'2017-05-10 08:00:32','2017-05-10 08:00:32'),(87,'{\"29.jpg\":{\"name\":\"\"}}',900.00,77,1,NULL,'2017-05-10 11:01:15',1,'2017-05-10 08:01:15','2017-05-10 08:01:15'),(88,'{\"30.jpg\":{\"name\":\"\"}}',650.00,78,1,NULL,'2017-05-10 18:16:04',1,'2017-05-10 08:02:06','2017-05-10 15:16:04'),(90,'{\"h0tKr-jJpUo.jpg\":{\"name\":\"\"}}',1500.00,80,2,NULL,'2017-05-10 11:26:43',1,'2017-05-10 08:26:43','2017-05-10 08:26:43'),(91,'{\"mPjI0bDxbRY.jpg\":{\"name\":\"\"}}',1500.00,81,2,NULL,'2017-05-10 11:34:33',1,'2017-05-10 08:28:29','2017-05-10 08:34:33'),(92,'{\"4pYTrQziasI.jpg\":{\"name\":\"\"}}',750.00,82,2,NULL,'2017-05-10 11:36:45',1,'2017-05-10 08:36:45','2017-05-10 08:36:45'),(95,'{\"nbNXCjPTSXY.jpg\":{\"name\":\"\"}}',900.00,84,2,NULL,'2017-05-10 11:41:21',1,'2017-05-10 08:40:43','2017-05-10 08:41:22'),(96,'{\"AAmBXQpTdcc.jpg\":{\"name\":\"\"}}',1200.00,85,2,NULL,'2017-05-10 11:44:31',1,'2017-05-10 08:44:31','2017-05-10 08:44:31'),(97,'{\"M7EbLhnKyec.jpg\":{\"name\":\"\"}}',1500.00,86,2,NULL,'2017-05-10 11:47:04',1,'2017-05-10 08:47:04','2017-05-10 08:47:04'),(98,'{\"Oe3ThcKoYPc.jpg\":{\"name\":\"\"}}',1300.00,87,2,NULL,'2017-05-10 11:51:31',1,'2017-05-10 08:51:31','2017-05-10 08:51:31'),(99,'{\"jae--GKKY2A.jpg\":{\"name\":\"\"}}',1200.00,88,2,NULL,'2017-05-10 12:24:04',1,'2017-05-10 09:20:39','2017-05-10 09:24:04'),(100,'{\"Vesennij-svadebnyj-buket-6-kopiya.jpg\":{\"name\":\"\"}}',1500.00,89,2,NULL,'2017-05-10 12:38:33',1,'2017-05-10 09:38:33','2017-05-10 09:38:33'),(101,'{\"04.jpg\":{\"name\":\"\"}}',1400.00,90,2,NULL,'2017-05-10 12:42:00',1,'2017-05-10 09:40:40','2017-05-10 09:42:00'),(102,'{\"3-e1478373412754.jpg\":{\"name\":\"\"}}',1500.00,91,2,NULL,'2017-05-10 12:54:26',1,'2017-05-10 09:54:26','2017-05-10 09:54:26'),(103,'{\"18350.736x1104.1443509168.jpg\":{\"name\":\"\"}}',1500.00,92,2,NULL,'2017-05-10 13:47:09',1,'2017-05-10 10:47:09','2017-05-10 10:47:09'),(104,'{\"8.jpg\":{\"name\":\"\"}}',1000.00,93,2,NULL,'2017-05-10 13:48:17',1,'2017-05-10 10:48:17','2017-05-10 10:48:17'),(105,'{\"4110745_8ggd6cqxziososowc.jpg\":{\"name\":\"\"}}',1000.00,94,2,NULL,'2017-05-10 13:49:02',1,'2017-05-10 10:48:49','2017-05-10 10:49:02'),(106,'{\"pastelowe-bukiety-\\u015blubne-3.jpg\":{\"name\":\"\"}}',800.00,95,2,NULL,'2017-05-10 13:49:36',1,'2017-05-10 10:49:36','2017-05-10 10:49:36'),(110,'{\"high-quality-80cm-font-b-huge-b-font-font-b-teddy-b-font-font-b-bear.jpg\":{\"name\":\"\"}}',370.00,96,5,NULL,'2017-05-11 13:53:37',1,'2017-05-11 10:53:37','2017-05-11 10:53:37'),(111,'{\"-5CKaM8W2FI.jpg\":{\"name\":\"\"}}',550.00,97,5,NULL,'2017-05-11 13:54:07',1,'2017-05-11 10:54:07','2017-05-11 10:54:07'),(112,'{\"12785847.esqvmztyve.JPG\":{\"name\":\"\"}}',730.00,98,5,NULL,'2017-05-11 13:54:37',1,'2017-05-11 10:54:37','2017-05-11 10:54:37'),(113,'{\"383397261.jpg\":{\"name\":\"\"}}',800.00,99,5,NULL,'2017-05-12 11:28:45',1,'2017-05-11 11:00:40','2017-05-12 08:28:45'),(114,'{\"10pc10Inch-\\u0420\\u043e\\u0437\\u043e\\u0432\\u044b\\u0439-\\u0421\\u0438\\u043d\\u0438\\u0439-\\u0411\\u0435\\u043b\\u044b\\u0439-\\u041b\\u0430\\u0442\\u0435\\u043a\\u0441\\u043d\\u044b\\u0435-\\u0428\\u0430\\u0440\\u044b-\\u041d\\u0430\\u0434\\u0443\\u0432\\u043d\\u044b\\u0435-\\u041a\\u0440\\u0443\\u0433\\u043b\\u044b\\u0439-\\u0412\\u043e\\u0437\\u0434\\u0443\\u0448\\u043d\\u044b\\u0439-\\u0428\\u0430\\u0440-\\u0421\\u0432\\u0430\\u0434\\u044c\\u0431\\u0430-\\u0421-\\u0414\\u043d\\u0435\\u043c-\\u0420\\u043e\\u0436\\u0434\\u0435\\u043d\\u0438\\u044f-\\u041f\\u0430\\u0440\\u0442\\u0438\\u044f-\\u0412\\u043e\\u0437\\u0434\\u0443\\u0448\\u043d\\u044b\\u0439.jpg\":{\"name\":\"\"}}',800.00,100,5,NULL,'2017-05-11 14:01:39',1,'2017-05-11 11:01:39','2017-05-11 11:01:39'),(115,'{\"qyt3OsRIXbI.jpg\":{\"name\":\"\"},\"r04IxchRJ-k.jpg\":{\"name\":\"\"},\"saEbatzvhGI.jpg\":{\"name\":\"\"}}',1200.00,101,2,NULL,'2017-05-11 14:18:25',1,'2017-05-11 11:18:25','2017-05-11 11:18:25'),(116,'{\"dS0QUmlL65Y.jpg\":{\"name\":\"\"}}',800.00,102,2,NULL,'2017-05-11 14:19:07',1,'2017-05-11 11:19:07','2017-05-11 11:19:07'),(117,'{\"OS2IXNtzxhY.jpg\":{\"name\":\"\"}}',1000.00,103,2,NULL,'2017-05-11 14:20:02',1,'2017-05-11 11:20:02','2017-05-11 11:20:02'),(118,'{\"EtEpFIXEXd8.jpg\":{\"name\":\"\"}}',1500.00,104,2,NULL,'2017-05-11 14:40:44',1,'2017-05-11 11:32:44','2017-05-11 11:40:44'),(119,'{\"HHbdvu9UhCA.jpg\":{\"name\":\"\"}}',1500.00,105,2,NULL,'2017-05-11 14:41:07',1,'2017-05-11 11:33:08','2017-05-11 11:41:07'),(120,'{\"WSDoHjfBjTM.jpg\":{\"name\":\"\"}}',1500.00,106,2,NULL,'2017-05-11 14:43:53',1,'2017-05-11 11:34:10','2017-05-11 11:43:53'),(121,'{\"-45ueqhWdpE.jpg\":{\"name\":\"\"}}',1200.00,107,2,NULL,'2017-05-11 14:42:16',1,'2017-05-11 11:34:56','2017-05-11 11:42:16'),(122,'{\"WByXYHOJMzw.jpg\":{\"name\":\"\"}}',1000.00,108,2,NULL,'2017-05-11 14:42:44',1,'2017-05-11 11:37:08','2017-05-11 11:42:44'),(123,'{\"jAgrHqck9Kg.jpg\":{\"name\":\"\"}}',1000.00,109,2,NULL,'2017-05-11 14:44:25',1,'2017-05-11 11:37:27','2017-05-11 11:44:26'),(124,'{\"skUosrQzi6Q.jpg\":{\"name\":\"\"}}',1000.00,110,2,NULL,'2017-05-11 14:45:22',1,'2017-05-11 11:45:22','2017-05-11 11:45:22'),(125,'{\"eodr6ij1WgU.jpg\":{\"name\":\"\"}}',1000.00,111,2,NULL,'2017-05-11 14:45:44',1,'2017-05-11 11:45:44','2017-05-11 11:45:44'),(151,'{\"box with flower delivery Los Angeles (2).jpg\":{\"name\":\"\"}}',1000.00,112,4,NULL,'2017-05-12 16:06:59',1,'2017-05-12 13:06:59','2017-05-12 13:06:59'),(152,'{\"1637123910.jpg\":{\"name\":\"\"}}',1200.00,113,4,NULL,'2017-05-12 16:12:29',1,'2017-05-12 13:12:29','2017-05-12 13:12:29'),(153,'{\"7e51b52eb945d3a9423609072c4a760c.jpg\":{\"name\":\"\"}}',800.00,114,4,NULL,'2017-05-12 16:14:38',1,'2017-05-12 13:14:38','2017-05-12 13:14:38');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_translations`
--

DROP TABLE IF EXISTS `items_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`item_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_translations`
--

LOCK TABLES `items_translations` WRITE;
/*!40000 ALTER TABLE `items_translations` DISABLE KEYS */;
INSERT INTO `items_translations` VALUES ('en',44,'','','','','',NULL,''),('ru',44,'','','','','',NULL,''),('ua',44,'rojeva-korobochka','Рожева коробочка','','Ніжна коробочка з піоновидними трояндами та різноманітною зеленню. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Ніжна коробочка з піоновидними трояндами та різноманітною зеленню. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',45,'','','','','',NULL,''),('ru',45,'','','','','',NULL,''),('ua',45,'korobochka-z-pionamy','Коробочка з піонами','','Чарівна коробочка з 7 піонами та різноманітною зеленню. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Чарівна коробочка з 7 піонами та різноманітною зеленню. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',46,'','','','','',NULL,''),('ru',46,'','','','','',NULL,''),('ua',46,'korobka-z-pionovydnymy-troiandamy','Коробка з піоновидними трояндами','','Чарівна коробочка з  піоновидними трояндами, піонами та різноманітною зеленню. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Чарівна коробочка з &nbsp;піоновидними трояндами, піонами та різноманітною зеленню. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',47,'','','','','',NULL,''),('ru',47,'','','','','',NULL,''),('ua',47,'fioletova-mriia','Фіолетова мрія','','Коробка з гортензією, 5 трояндами, 3 гвоздиками та 2 гілками еустоми. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробка з гортензією, 5 трояндами, 3 гвоздиками та 2 гілками еустоми. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',48,'','','','','',NULL,''),('ru',48,'','','','','',NULL,''),('ua',48,'rojevi-zori','Рожеві зорі','','Коробочка з 21 троянди та зелені. Оформлена атласною стрічкою Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробочка з 21 троянди та зелені. Оформлена атласною стрічкою Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',49,'','','','','',NULL,''),('ru',49,'','','','','',NULL,''),('ua',49,'vesnianyy-podykh','Весняний подих','','Коробка з 7 троянд, 3 гілок еустоми, 7 гілок кущової троянди та хризантеми. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробка з 7 троянд, 3 гілок еустоми, 7 гілок кущової троянди та хризантеми. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',50,'','','','','',NULL,''),('ru',50,'','','','','',NULL,''),('ua',50,'rojeva-radist','Бузкова радість','','Коробочка з 5 піонами, 3 гортензіями, 7 трояндами, бузком та вібурнумом. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробочка з 5 піонами, 3 гортензіями, 7 трояндами, бузком та вібурнумом. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',51,'','','','','',NULL,''),('ru',51,'','','','','',NULL,''),('ua',51,'fioletova-korobochka','Фіолетова коробочка','','Коробочка з 9 гілок еустоми та різноманітної зелені. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробочка з 9 гілок еустоми та різноманітної зелені. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',52,'','','','','',NULL,''),('ru',52,'','','','','',NULL,''),('ua',52,'korobochka-z-sukkulentom','Коробочка з суккулентом','','Коробочка з 3 трояндами, суккулентом, гіперікумом та 3 гілочками кущової троянди. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета. ','',NULL,'<p>Коробочка з 3 трояндами, суккулентом, гіперікумом та 3 гілочками кущової троянди. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',53,'','','','','',NULL,''),('ru',53,'','','','','',NULL,''),('ua',53,'korobka-z-giatsyntamy','Коробка з гіацинтами','','Коробочка з 9 гіацинтів та брунії. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробочка з 9 гіацинтів та брунії. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',54,'','','','','',NULL,''),('ru',54,'','','','','',NULL,''),('ua',54,'yaskravyy-splesk','Яскравий сплеск','','Коробочка з 7 гвоздик. 3 гілок хризантеми, 9 троянд та різноманітної зелені. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробочка з 7 гвоздик. 3 гілок хризантеми, 9 троянд та різноманітної зелені. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',55,'','','','','',NULL,''),('ru',55,'','','','','',NULL,''),('ua',55,'korobochka-v-pastelnykh-tonakh','Коробочка в пастельних тонах','','Ніжна коробочка з гортензіями, кущовою трояндою, гіперікумом, трояндами та різноманітною зеленню. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Ніжна коробочка з гортензіями, кущовою трояндою, гіперікумом, трояндами та різноманітною зеленню. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',56,'','','','','',NULL,''),('ru',56,'','','','','',NULL,''),('ua',56,'svitla-khmarynka','Світла хмаринка','','Коробка з 7 тюльпанами, 5 гіацинтами, та 3 піонами. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробка з 7 тюльпанами, 5 гіацинтами, та 3 піонами. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',57,'','','','','',NULL,''),('ru',57,'','','','','',NULL,''),('ua',57,'korobka-z-kushchovymy-troiandamy','Коробка з кущовими  трояндами','','Коробка з 15 гілок кущової троянди. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробка з 15 гілок кущової троянди. Оформлена атласною стрічкою. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',58,'','','','','',NULL,''),('ru',58,'','','','','',NULL,''),('ua',58,'korobochka-z-gortenziiamy','Коробочка з гортензіями','','Коробка з 3 гілок гортензії та зелені. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Коробка з 3 гілок гортензії та зелені. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',59,'','','','','',NULL,''),('ru',59,'','','','','',NULL,''),('ua',59,'skhidna-krasa','Східна краса','','Прекрасний букет з лілією, 3 гвоздиками, 3 гілками еустоми, кущовою трояндою, скабіозою та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Прекрасний букет з лілією, 3 гвоздиками, 3 гілками еустоми, кущовою трояндою, скабіозою та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',60,'','','','','',NULL,''),('ru',60,'','','','','',NULL,''),('ua',60,'ekzotychnyy-miks','Екзотичний мікс','','Екзотичний букет з протеєю, ерінгіумом, гілкою еустоми, 3 піонами, 5 трояндами, гвоздиками та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Екзотичний букет з протеєю, ерінгіумом, гілкою еустоми, 3 піонами, 5 трояндами, гвоздиками та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',61,'','','','','',NULL,''),('ru',61,'','','','','',NULL,''),('ua',61,'santino','Сантіно','','Чарівний букет з 3 червоними трояндами, 3 кущовими трояндами, альстромерією та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Чарівний букет з 3 червоними трояндами, 3 кущовими трояндами, альстромерією та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',62,'','','','','',NULL,''),('ru',62,'','','','','',NULL,''),('ua',62,'svitlyy-den','Світлий день','','Світлий букетик з 3 гілками еустоми, астільбою та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Світлий букетик з 3 гілками еустоми, астільбою та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',63,'','','','','',NULL,''),('ru',63,'','','','','',NULL,''),('ua',63,'rustik','Рустік','','Букет з 3 піонами, 3 гілками еустоми, ерінгіумом та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 піонами, 3 гілками еустоми, ерінгіумом та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',64,'','','','','',NULL,''),('ru',64,'','','','','',NULL,''),('ua',64,'buket-kompliment','Букет комплімент','','Маленький букет комплімент з гвоздиками, трояндами, гортензією та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Маленький букет комплімент з гвоздиками, трояндами, гортензією та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',65,'','','','','',NULL,''),('ru',65,'','','','','',NULL,''),('ua',65,'kontrast','Контраст','','Чарівний букет з 3 гілками еустоми, 3 анемонами, троядами та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Чарівний букет з 3 гілками еустоми, 3 анемонами, троядами та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',66,'','','','','',NULL,''),('ru',66,'','','','','',NULL,''),('ua',66,'pryrodna-svijist','Природна свіжість','','Прекрасний букет з тюльпанами, трояндами, діантусами та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Прекрасний букет з тюльпанами, трояндами, діантусами та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',67,'','','','','',NULL,''),('ru',67,'','','','','',NULL,''),('ua',67,'sokovyti-kolory','Соковиті кольори','','Яскравий букет з бузком, 3 герберами,3 гілками еустоми, 5 трояндами та різноманітною зеленню. Оформлений у папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Яскравий букет з бузком, 3 герберами,3 гілками еустоми, 5 трояндами та різноманітною зеленню. Оформлений у папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',68,'','','','','',NULL,''),('ru',68,'','','','','',NULL,''),('ua',68,'zakhid-sontsia','Захід сонця','','Вогняний букет з 5 герберами, кущовою гвоздикою, хамелаціумом, астільбою та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Вогняний букет з 5 герберами, кущовою гвоздикою, хамелаціумом, астільбою та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',69,'','','','','',NULL,''),('ru',69,'','','','','',NULL,''),('ua',69,'prystrast','Пристрасть','','Букет з протеєю, скабіозою, прекрасними червоними трояндами, кущовими трояндами та зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з протеєю, скабіозою, прекрасними червоними трояндами, кущовими трояндами та зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',70,'','','','','',NULL,''),('ru',70,'','','','','',NULL,''),('ua',70,'nijnist','Ніжність','','Букет з 3 трояндами, 3 тюльпанами, кущовою трояндою та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 трояндами, 3 тюльпанами, кущовою трояндою та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',71,'','','','','',NULL,''),('ru',71,'','','','','',NULL,''),('ua',71,'surpryz','Сюрприз','','Маленький букет комплімент з 3 троянд, кущових троянд та зелені. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Маленький букет комплімент з 3 троянд, кущових троянд та зелені. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',72,'','','','','',NULL,''),('ru',72,'','','','','',NULL,''),('ua',72,'aktsent','Акцент','','Чарівний букет з 3 гілками альстромерії, 5 трояндами, кущовю трояндою та величеним розмаїттям зеленв.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Чарівний букет з 3 гілками альстромерії, 5 трояндами, кущовю трояндою та величеним розмаїттям зеленв. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',73,'','','','','',NULL,''),('ru',73,'','','','','',NULL,''),('ua',73,'snijna-koroleva','Сніжна королева','','Букет з 3 піонами, гіпсофілою, 3 гілками еустоми, ерінгіумом та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 піонами, гіпсофілою, 3 гілками еустоми, ерінгіумом та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',74,'','','','','',NULL,''),('ru',74,'','','','','',NULL,''),('ua',74,'praline','Праліне','','Букет з лілією, кущовою трояндою, 5 трояндами, 3 гілками еустоми та зеленню. Оформлений у папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з лілією, кущовою трояндою, 5 трояндами, 3 гілками еустоми та зеленню. Оформлений у папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',75,'','','','','',NULL,''),('ru',75,'','','','','',NULL,''),('ua',75,'malenkyy-podarunok','Маленький подарунок','','Маленький букет комплімент з альстромерією, гвоздикою, трояндами та зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Маленький букет комплімент з альстромерією, гвоздикою, трояндами та зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',76,'','','','','',NULL,''),('ru',76,'','','','','',NULL,''),('ua',76,'madonna','Мадонна','','Ніжний букет з 3 гілками еустоми, 5 трояндами, альстромерією та різноманітною зеленню. Оформлений у папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Ніжний букет з 3 гілками еустоми, 5 трояндами, альстромерією та різноманітною зеленню. Оформлений у папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',77,'','','','','',NULL,''),('ru',77,'','','','','',NULL,''),('ua',77,'fiolet','Фіолет','','Букет з 2 хризантемами, 3 гілками еустоми, вібурнумом та великою кількістю зелені. Оформлений у папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 2 хризантемами, 3 гілками еустоми, вібурнумом та великою кількістю зелені. Оформлений у папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',78,'','','','','',NULL,''),('ru',78,'','','','','',NULL,''),('ua',78,'kasablanka','Касабланка','','Букет з гортензією, тюльпанами, еустомою, кущовими трояндами та зеленню. Оформлений у папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з гортензією, тюльпанами, еустомою, кущовими трояндами та зеленню. Оформлений у папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',79,'','','','','',NULL,''),('ru',79,'','','','','',NULL,''),('ua',79,'soliter','Симфонія весни','','Букет з 3 анемонами, 5 гілками еустоми, 3 гілками кущової троянди та різноманітною зеленню. Оформлений у папір. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 анемонами, 5 гілками еустоми, 3 гілками кущової троянди та різноманітною зеленню. Оформлений у папір. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',80,'','','','','',NULL,''),('ru',80,'','','','','',NULL,''),('ua',80,'kapuchino','Капучіно','','Букет з 7 трояндами, 3 альстромеріями, хамелаціуомом та різноманітною зеленню.  Оформлений у крафтовий папір. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 7 трояндами, 3 альстромеріями, хамелаціуомом та різноманітною зеленню. &nbsp;Оформлений у крафтовий папір. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',81,'','','','','',NULL,''),('ru',81,'','','','','',NULL,''),('ua',81,'veronika','Вероніка','','Букетз з 3 гвоздиками, 3 гілками еустоми, трояндою, матіолою та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета. ','',NULL,'<p>Букетз з 3 гвоздиками, 3 гілками еустоми, трояндою, матіолою та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.&nbsp;</p>'),('en',82,'','','','','',NULL,''),('ru',82,'','','','','',NULL,''),('ua',82,'krystal','Кристал','','Букет з 3 гортензіями, 7 анемонами, 9 трояндами, 5 гілками кущової троянди та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 гортензіями, 7 анемонами, 9 трояндами, 5 гілками кущової троянди та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',83,'','','','','',NULL,''),('ru',83,'','','','','',NULL,''),('ua',83,'karamel','Карамель','','Букет 3 піоновидними трояндами, 3 гілками еустоми, 5 трояндами та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет 3 піоновидними трояндами, 3 гілками еустоми, 5 трояндами та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',84,'','','','','',NULL,''),('ru',84,'','','','','',NULL,''),('ua',84,'buket-z-pionamy','Букет з піонами','','Букет з 3 піонами, 5 гілками еустоми, скабіозою, ерінгіумом та різноманітною зеленню.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 піонами, 5 гілками еустоми, скабіозою, ерінгіумом та різноманітною зеленню. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',85,'','','','','',NULL,''),('ru',85,'','','','','',NULL,''),('ua',85,'svitlyy-aktsent','Світлий акцент','','Букет з 3 гвоздиками, тюльпанами, 3 гілками еустоми та евкаліптом. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 гвоздиками, тюльпанами, 3 гілками еустоми та евкаліптом. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',86,'','','','','',NULL,''),('ru',86,'','','','','',NULL,''),('ua',86,'aromat','Аромат','','Букет з тюльпанами, альстромерією, кущовою гвоздикою, та великою кількістю зелені.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета. ','',NULL,'<p>Букет з тюльпанами, альстромерією, кущовою гвоздикою, та великою кількістю зелені. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.&nbsp;</p>'),('en',87,'','','','','',NULL,''),('ru',87,'','','','','',NULL,''),('ua',87,'kantri','Кантрі','','Букет з 3 трояндами, 5 гвоздиками, еустомою, ерінгіумом та різноманітною зеленню. Оформлений у папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Букет з 3 трояндами, 5 гвоздиками, еустомою, ерінгіумом та різноманітною зеленню. Оформлений у папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',88,'','','','','',NULL,''),('ru',88,'','','','','',NULL,''),('ua',88,'sokovytyy-podarunok','Соковитий подарунок','','Яскравий букет з 5 гвоздиками, еустомою, ерінгіумом та різноманітною зеленню. Оформлений у крафтовий папір.  Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Яскравий букет з 5 гвоздиками, еустомою, ерінгіумом та різноманітною зеленню. Оформлений у крафтовий папір. &nbsp;Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',90,'','','','','',NULL,''),('ru',90,'','','','','',NULL,''),('ua',90,'vesilnyy-buket-z-pionamy-ta-astilbou','Весільний букет з піонами та астільбою','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"color: rgb(0, 0, 0); font-family: -apple-system, BlinkMacSystemFont, Roboto, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості.&nbsp;</span>Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',91,'','','','','',NULL,''),('ru',91,'','','','','',NULL,''),('ua',91,'vesilnyy-buket-z-pionovydnykh-troiand','Весільний букет з піоновидних троянд','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',92,'','','','','',NULL,''),('ru',92,'','','','','',NULL,''),('ua',92,'vesilnyy-buket-z-giperikumom','Весільний букет з гіперікумом','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',95,'','','','','',NULL,''),('ru',95,'','','','','',NULL,''),('ua',95,'vesilnyy-buket-z-freziieu','Весільний букет з фрезією','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',96,'','','','','',NULL,''),('ru',96,'','','','','',NULL,''),('ua',96,'vesilnyy-buket-z-ranunkulusamy','Весільний букет з ранункулюсами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',97,'','','','','',NULL,''),('ru',97,'','','','','',NULL,''),('ua',97,'vesilnyy-buket-z-pionovydnymy-troiandamy','Весільний букет з піоновидними трояндами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',98,'','','','','',NULL,''),('ru',98,'','','','','',NULL,''),('ua',98,'vesilnyy-buket-z-pionamy','Весільний букет з піонами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',99,'','','','','',NULL,''),('ru',99,'','','','','',NULL,''),('ua',99,'vesilnyy-buket-','Весільний букет ','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',100,'','','','','',NULL,''),('ru',100,'','','','','',NULL,''),('ua',100,'vesilnyy-buket-','Весільний букет ','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',101,'','','','','',NULL,''),('ru',101,'','','','','',NULL,''),('ua',101,'vesilnyy-buket-z-kalamy','Весільний букет з калами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',102,'','','','','',NULL,''),('ru',102,'','','','','',NULL,''),('ua',102,'vesilnyy-buket-z-pionamy','Весільний букет з піонами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',103,'','','','','',NULL,''),('ru',103,'','','','','',NULL,''),('ua',103,'vesilnyy-buket-z-pionovydnymy-troiandamy','Весільний букет з піоновидними трояндами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',104,'','','','','',NULL,''),('ru',104,'','','','','',NULL,''),('ua',104,'vesilnyy-buket-z-gvozdykamy','Весільний букет з гвоздиками','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',105,'','','','','',NULL,''),('ru',105,'','','','','',NULL,''),('ua',105,'vesilnyy-buket-z-orkhideiamy','Весільний букет з орхідеями','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',106,'','','','','',NULL,''),('ru',106,'','','','','',NULL,''),('ua',106,'vesilnyy-buket-z-troiand','Весільний букет з троянд','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p><span style=\"font-family: &quot;Trebuchet MS&quot;; font-size: 14px;\">Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</span></p>'),('en',110,'','','','','',NULL,''),('ru',110,'','','','','',NULL,''),('ua',110,'miaka-igrashka-vysota-30-sm','М\'яка іграшка висота 30 см','','Чудовим доповненням до букету квітів стане м\'яка іграшка, яка завжди буде нагадувати про Вас вашому отримувачу!','',NULL,'<p>Чудовим доповненням до букету квітів стане м&#39;яка іграшка, яка завжди буде нагадувати про Вас вашому отримувачу!</p>'),('en',111,'','','','','',NULL,''),('ru',111,'','','','','',NULL,''),('ua',111,'miaka-igrashka-vysota-40-sm','М\'яка іграшка висота 40 см','','Чудовим доповненням до букету квітів стане м\'яка іграшка, яка завжди буде нагадувати про Вас вашому отримувачу!','',NULL,'<p>Чудовим доповненням до букету квітів стане м&#39;яка іграшка, яка завжди буде нагадувати про Вас вашому отримувачу!</p>'),('en',112,'','','','','',NULL,''),('ru',112,'','','','','',NULL,''),('ua',112,'miaka-igrashka-vysota-50-sm','М\'яка іграшка висота 50 см','','Чудовим доповненням до букету квітів стане м\'яка іграшка, яка завжди буде нагадувати про Вас вашому отримувачу!','',NULL,'<p>Чудовим доповненням до букету квітів стане м&#39;яка іграшка, яка завжди буде нагадувати про Вас вашому отримувачу!</p>'),('en',113,'','','','','',NULL,''),('ru',113,'','','','','',NULL,''),('ua',113,'15-kulok-z-geliiem','50 кульок з гелієм','','Прекрасним способом виразити свої почуття, також можуть стати такі чарівні кульки! 50 кульок  з гелієм в рожево-сірій гаммі зроблять звичайний день - святом!','',NULL,'<p>Прекрасним способом виразити свої почуття, також можуть стати такі чарівні кульки! 50 кульок &nbsp;з гелієм в рожево-сірій гаммі&nbsp;зроблять звичайний день - святом!</p>'),('en',114,'','','','','',NULL,''),('ru',114,'','','','','',NULL,''),('ua',114,'15-kulok-z-geliiem','15 кульок  з гелієм','','Прекрасним способом виразити свої почуття, також можуть стати такі чарівні кульки! 15 кульок  з гелієм в блакитно-білій гаммі зроблять звичайний день - святом!','',NULL,'<p>Прекрасним способом виразити свої почуття, також можуть стати такі чарівні кульки! 15 кульок &nbsp;з гелієм в блакитно-білій гаммі зроблять звичайний день - святом!</p>'),('en',115,'','','','','',NULL,''),('ru',115,'','','','','',NULL,''),('ua',115,'vesilnyy-buket-','Весільний букет ','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',116,'','','','','',NULL,''),('ru',116,'','','','','',NULL,''),('ua',116,'vesilnyy-buket-z-jorjynamy','Весільний букет з жоржинами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',117,'','','','','',NULL,''),('ru',117,'','','','','',NULL,''),('ua',117,'vesilnyy-buket-','Весільний букет ','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',118,'','','','','',NULL,''),('ru',118,'','','','','',NULL,''),('ua',118,'vesilnyy-buket-z-pionovydnymy-troiandamy','Весільний букет з піоновидними трояндами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',119,'','','','','',NULL,''),('ru',119,'','','','','',NULL,''),('ua',119,'vesilnyy-buket-z-pionovydnymy-troiandamy','Весільний букет з піоновидними трояндами','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',120,'','','','','',NULL,''),('ru',120,'','','','','',NULL,''),('ua',120,'vesilnyy-buket-','Весільний букет ','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',121,'','','','','',NULL,''),('ru',121,'','','','','',NULL,''),('ua',121,'vesilnyy-buket-z-sukkulentom','Весільний букет з суккулентом','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',122,'','','','','',NULL,''),('ru',122,'','','','','',NULL,''),('ua',122,'vesilnyy-buket-z-brassikou','Весільний букет з брассікою','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',123,'','','','','',NULL,''),('ru',123,'','','','','',NULL,''),('ua',123,'vesilnyy-buket-','Весільний букет ','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',124,'','','','','',NULL,''),('ru',124,'','','','','',NULL,''),('ua',124,'vesilnyy-buket-z-bavovnou','Весільний букет з бавовною','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',125,'','','','','',NULL,''),('ru',125,'','','','','',NULL,''),('ua',125,'vesilnyy-buket-','Весільний букет ','','Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.','',NULL,'<p>Ціна букету може змінюватись залежно від дати Вашого вессілля, вибору квітів та іх кількості. Актуальну ціну дізнавайтесь у нашого менеджера.</p>'),('en',151,'','','','','',NULL,''),('ru',151,'','','','','',NULL,''),('ua',151,'symvol-kokhannia','Символ кохання','','Що може бути краще ніж квіти? Правильно, квіти поєднані із солодощами. Прекрасні троянди разом зі смачними макарунами будуть чарівним подарунком. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Що може бути краще ніж квіти? Правильно, квіти поєднані із солодощами. Прекрасні троянди разом зі смачними макарунами будуть чарівним подарунком. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',152,'','','','','',NULL,''),('ru',152,'','','','','',NULL,''),('ua',152,'djulietta','Джульєтта','','Зробіть прекрасний подарунок своїй коханій людині. Замовте таку коробочку з квітами і макарунами, та створіть для неї невеличке свято. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.','',NULL,'<p>Зробіть прекрасний подарунок своїй коханій людині. Замовте таку коробочку з квітами і макарунами, та створіть для неї невеличке свято. Оскільки присутність в композиції кожного виду квітки залежить від сезону та наявності, ми залишаємо за собою право замінити відсутні квітами подібного стилю, якості та вартості так, щоб зберегти значення та кольорову гаму букета.</p>'),('en',153,'','','','','',NULL,''),('ru',153,'','','','','',NULL,''),('ua',153,'pryiemnyy-surpryz','Приємний сюрприз','','До букету, який ви вибрали, хорошим доповненням стане така коробочка, зі смачнющими кіндерами. Ні одна дівчина не залишиться байдужою до такого сюрпризу. ','',NULL,'<p>До букету, який ви вибрали, хорошим доповненням стане така коробочка, зі смачнющими кіндерами. Ні одна дівчина не залишиться байдужою до такого сюрпризу.&nbsp;</p>');
/*!40000 ALTER TABLE `items_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_s_subscribers`
--

DROP TABLE IF EXISTS `m_s_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_s_subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_s_subscribers`
--

LOCK TABLES `m_s_subscribers` WRITE;
/*!40000 ALTER TABLE `m_s_subscribers` DISABLE KEYS */;
INSERT INTO `m_s_subscribers` VALUES (1,'Олександр','Андрійченко',21,'хз','2017-04-04 12:19:56','2017-04-04 12:19:56'),(2,'a','b',21,'c','2017-04-04 12:20:52','2017-04-04 12:20:52'),(3,'й','ц',12,'у','2017-04-04 12:21:24','2017-04-04 12:21:24'),(4,'a','f',23,'к','2017-04-04 12:31:29','2017-04-04 12:31:29'),(5,'в','а',444,'а','2017-04-04 12:31:40','2017-04-04 12:31:40'),(7,'марта','парарапр',26,'аоапоароррап','2017-05-11 09:57:23','2017-05-11 09:57:23');
/*!40000 ALTER TABLE `m_s_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parent_id_foreign` (`parent_id`),
  CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,NULL,0),(2,1,0),(3,1,1),(4,1,2),(5,1,3),(6,1,4),(7,1,5);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus_translations`
--

DROP TABLE IF EXISTS `menus_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus_translations`
--

LOCK TABLES `menus_translations` WRITE;
/*!40000 ALTER TABLE `menus_translations` DISABLE KEYS */;
INSERT INTO `menus_translations` VALUES ('ua',1,'golovne-menu','Головне меню',NULL,'2017-04-26 07:53:17','2017-04-26 07:53:50'),('ru',2,'katalog','Каталог','',NULL,'2017-04-26 07:55:11'),('ua',2,'catalog','Каталог','','2017-04-26 07:53:24','2017-04-26 07:55:11'),('ru',3,'svadebnoe-oformlenye','Свадебное оформление','',NULL,'2017-04-26 07:56:28'),('ua',3,'vesilne-oformlennia','Весільне оформлення','','2017-04-26 07:53:37','2017-04-26 07:56:28'),('ua',4,'mayster-klas','Майстер Клас',NULL,'2017-04-26 07:53:47','2017-04-26 07:53:52'),('ua',5,'pidpyska-na-kvity','Підписка на квіти',NULL,'2017-04-26 07:54:03','2017-05-10 05:33:10'),('ua',6,'blog','Блог',NULL,'2017-04-26 07:54:08','2017-05-10 05:33:10'),('ua',7,'kontakty','Контакти',NULL,'2017-04-26 07:54:15','2017-05-10 05:33:10');
/*!40000 ALTER TABLE `menus_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2017_03_06_232006_create_carts',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `parameters` text COLLATE utf8_unicode_ci,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_item_id_foreign` (`item_id`),
  CONSTRAINT `order_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES (16,18,88,'Соковитий подарунок',NULL,650.00,NULL,1,'2017-05-10 14:30:54','2017-05-10 14:30:54'),(17,19,55,'Коробочка в пастельних тонах',NULL,1200.00,NULL,2,'2017-05-11 09:36:00','2017-05-11 09:36:00'),(18,20,55,'Коробочка в пастельних тонах',NULL,1200.00,NULL,1,'2017-05-11 09:37:19','2017-05-11 09:37:19'),(19,20,58,'Коробочка з гортензіями',NULL,900.00,NULL,1,'2017-05-11 09:37:19','2017-05-11 09:37:19'),(20,21,61,'Сантіно',NULL,600.00,NULL,1,'2017-05-11 12:55:15','2017-05-11 12:55:15'),(21,22,66,'Природна свіжість',NULL,800.00,NULL,1,'2017-05-11 12:57:22','2017-05-11 12:57:22'),(22,23,123,'Весільний букет ',NULL,1000.00,NULL,1,'2017-05-11 13:24:19','2017-05-11 13:24:19'),(23,24,88,'Соковитий подарунок',NULL,650.00,NULL,1,'2017-05-11 13:57:45','2017-05-11 13:57:45'),(24,25,44,'Рожева коробочка',NULL,900.00,NULL,1,'2017-05-11 13:59:58','2017-05-11 13:59:58'),(25,26,44,'Рожева коробочка',NULL,900.00,NULL,1,'2017-05-11 14:00:59','2017-05-11 14:00:59'),(26,27,44,'Рожева коробочка',NULL,900.00,NULL,1,'2017-05-11 14:01:55','2017-05-11 14:01:55'),(27,28,44,'Рожева коробочка',NULL,900.00,NULL,1,'2017-05-11 14:02:39','2017-05-11 14:02:39'),(28,29,88,'Соковитий подарунок',NULL,650.00,NULL,1,'2017-05-11 17:03:33','2017-05-11 17:03:33'),(29,30,45,'Коробочка з піонами',NULL,800.00,NULL,1,'2017-05-11 17:03:38','2017-05-11 17:03:38'),(30,31,45,'Коробочка з піонами',NULL,800.00,NULL,1,'2017-05-11 17:05:18','2017-05-11 17:05:18'),(31,32,125,'Весільний букет ',NULL,1000.00,NULL,1,'2017-05-12 09:16:31','2017-05-12 09:16:31'),(32,37,125,'Весільний букет ',NULL,1000.00,NULL,1,'2017-05-12 11:57:03','2017-05-12 11:57:03'),(33,38,63,'Рустік',NULL,1300.00,NULL,1,'2017-05-12 12:06:34','2017-05-12 12:06:34'),(34,39,123,'Весільний букет ',NULL,1000.00,NULL,1,'2017-05-12 12:35:47','2017-05-12 12:35:47'),(35,45,88,'Соковитий подарунок',NULL,650.00,NULL,1,'2017-05-12 13:18:14','2017-05-12 13:18:14');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `delivery` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  `amount` decimal(8,2) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'Admin','admin@example.com','333','asd',1,'Самовывоз',NULL,NULL,NULL,NULL,NULL,'ru','d',NULL,2666.00,'LiqPay',NULL,NULL,0,'127.0.0.1','58e3a4509c387',NULL,'2017-04-04 13:49:04','2017-04-04 13:49:04'),(3,'Імя замовника','мейл замовника','телефон замовника','Адреса',1,NULL,NULL,NULL,NULL,NULL,NULL,'ua',NULL,NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','590bc68b07922',NULL,'2017-05-05 00:25:47','2017-05-05 00:25:47'),(4,'Імя замовника','мейл замовника','телефон замовника','Адреса',1,NULL,'05/31/2017 3:22 AM',NULL,NULL,NULL,NULL,'ua',NULL,NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','590bc83fe1faf',NULL,'2017-05-05 00:33:03','2017-05-05 00:33:03'),(5,'Імя замовника','мейл замовника','телефон замовника','Адреса',1,NULL,'05/31/2017 3:22 AM',NULL,NULL,NULL,NULL,'ua',NULL,NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','590bc8495b1ad',NULL,'2017-05-05 00:33:13','2017-05-05 00:33:13'),(6,'Імя замовника','мейл замовника','телефон замовника','Адреса',1,NULL,'05/31/2017 3:22 AM',NULL,NULL,NULL,NULL,'ua',NULL,NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','590bc8b6a6ed8',NULL,'2017-05-05 00:35:02','2017-05-05 00:35:02'),(7,'Імя замовника','мейл замовника','телефон замовника','Адреса',1,NULL,'05/31/2017 3:22 AM','09:30',NULL,NULL,'+3(8000)000-00-00','ua',NULL,NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','590bc920e1b8e',NULL,'2017-05-05 00:36:48','2017-05-05 00:36:48'),(8,'Імя замовника','мейл замовника','телефон замовника','Адреса',1,NULL,'05/31/2017 3:22 AM','09:30',NULL,NULL,'+3(8000)000-00-00','ua',NULL,NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','590bc92ba8364',NULL,'2017-05-05 00:36:59','2017-05-05 00:36:59'),(9,'Імя замовника','мейл замовника','телефон замовника','Адреса',1,NULL,'05/31/2017 3:22 AM','09:30',NULL,NULL,'+3(8000)000-00-00','ua',NULL,NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','590bc9889fede',NULL,'2017-05-05 00:38:32','2017-05-05 00:38:32'),(10,'Admin123123','admin121212312','1231231212','sadasdasdasd',1,NULL,'05/10/2017','09:30',NULL,NULL,'+3(88)123-23-23','ua','Подарункова доставка',NULL,546558.00,'PayPal',NULL,NULL,0,'127.0.0.1','590fa30ad719e',NULL,'2017-05-07 22:43:22','2017-05-07 22:43:22'),(12,'Admin','admin','+3(8756)875-87-58','рпорпа о пор пор',1,NULL,'05/10/2017','10:55',NULL,NULL,'+3(8986)986-98-69','ua','Я отримувач',NULL,2212.00,'Cash',NULL,NULL,0,'10.4.83.197','591153a973daf',NULL,'2017-05-09 05:29:13','2017-05-09 05:29:13'),(14,'Марта Федорук','marta.fedoruk@redentu.com','+3(8068)009-95-95','м. Львів, вул. Навроцього 1а',1,NULL,'05/17/2017','23:00',NULL,NULL,'+3(8000)000-00-00','ua',NULL,NULL,205.00,'Cash',NULL,NULL,0,'10.4.83.197','591163ddc77fd',NULL,'2017-05-09 06:38:21','2017-05-09 06:38:21'),(15,'Admin','admin','+3(8777)777-77-77','арпар',1,'','05/24/2017','21:45',NULL,NULL,'+3(8666)666-66-66','ua','','',205.00,'Bank','wait_accept','',1,'10.4.83.197','591166b8e2630',NULL,'2017-05-09 06:50:32','2017-05-09 07:01:46'),(16,'Мартуська','admin','+3(8777)777-77-77','м. Львів',1,NULL,'05/30/2017','23:00',NULL,NULL,'+3(8777)777-77-77','ua','Я отримувач',NULL,2455.00,'Готівка',NULL,NULL,0,'10.4.83.197','5911b5751807c',NULL,'2017-05-09 12:26:29','2017-05-09 12:26:29'),(17,'Admin','admin','+3(8666)666-66-66','г',1,NULL,'05/02/2017','00:55',NULL,NULL,'+3(8777)777-77-77','ua','Я отримувач',NULL,12.00,'Готівка',NULL,NULL,0,'10.4.83.197','5911b5c06dfc2',NULL,'2017-05-09 12:27:44','2017-05-09 12:27:44'),(18,'asdasdasd','asdasd@mail.com','+3(8231)312-21-83','asdasdasdasdasd',NULL,NULL,'05/09/2017','09:45',NULL,NULL,'+3(8123)213-12-31','ua','Я отримувач',NULL,650.00,'Готівка',NULL,NULL,0,'10.4.83.197','5913241e3531b',NULL,'2017-05-10 14:30:54','2017-05-10 14:30:54'),(19,'Admin','admin','+3(8666)666-66-66','рапр апр апр апр па',NULL,NULL,'05/31/2017','00:00',NULL,NULL,'+3(8777)777-77-87','ua','Подарункова доставка',NULL,2400.00,'Готівка',NULL,NULL,0,'10.4.83.197','59143080a6499',NULL,'2017-05-11 09:36:00','2017-05-11 09:36:00'),(20,'Admin','admin','+3(8666)666-66-66','екнке',NULL,NULL,'05/17/2017','22:49',NULL,NULL,'+3(8888)888-88-88','ua',NULL,NULL,2100.00,'Банківский платіж',NULL,NULL,0,'10.4.83.197','591430cf89687',NULL,'2017-05-11 09:37:19','2017-05-11 09:37:19'),(21,'Марта Федорук','admin','+3(8444)444-44-44','мпв',NULL,NULL,'05/16/2017','09:45',NULL,'па','+3(8333)333-33-33','ua',NULL,NULL,600.00,'Готівка',NULL,NULL,0,'10.4.83.197','59145f33cc4f3',NULL,'2017-05-11 12:55:15','2017-05-11 12:55:15'),(22,'Admin','admin','+3(8666)666-66-66','привіт',NULL,NULL,'05/10/2017','10:00',NULL,'Женя','+3(8666)666-66-66','ua',NULL,NULL,800.00,'Банківский платіж',NULL,NULL,0,'10.4.83.197','59145fb22fbbc',NULL,'2017-05-11 12:57:22','2017-05-11 12:57:22'),(23,'Admin','admin','+3(8999)999-99-99','00',NULL,NULL,'05/03/2017','До',NULL,'9','+3(8999)999-99-99','ua',NULL,NULL,1000.00,NULL,NULL,NULL,0,'10.4.83.197','5914660380b1f',NULL,'2017-05-11 13:24:19','2017-05-11 13:24:19'),(24,'Admin test','admin@test.mail.com','+3(8333)333-33-33','asdasdasdasd',NULL,NULL,'05/08/2017','10:05',NULL,'11111111111aaaaaaaa','+3(8333)333-33-33','ua',NULL,NULL,650.00,'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комісія)',NULL,NULL,0,'10.4.83.197','59146dd941935',NULL,'2017-05-11 13:57:45','2017-05-11 13:57:45'),(25,'Adminддддд','adminддддддддд','+3(8777)777-77-77','6756',NULL,NULL,'05/01/2017','09:05',NULL,'67567','+3(8566)666-66-66','ua','Я отримувач',NULL,900.00,'Готівка',NULL,NULL,0,'10.4.83.197','59146e5e38b83',NULL,'2017-05-11 13:59:58','2017-05-11 13:59:58'),(26,'Admin','admin','+3(8777)777-77-77','777',NULL,NULL,'05/16/2017','23:55',NULL,'7777777777777','+3(8777)777-77-77','ua',NULL,NULL,900.00,NULL,NULL,NULL,0,'10.4.83.197','59146e9acfe93',NULL,'2017-05-11 14:00:58','2017-05-11 14:00:58'),(27,'Admin','admin','+3(8666)666-66-66','666666666666666666',NULL,NULL,'05/09/2017','22:45',NULL,'666666666666','+3(8666)666-66-66','ua',NULL,NULL,900.00,NULL,NULL,NULL,0,'10.4.83.197','59146ed3bedbe',NULL,'2017-05-11 14:01:55','2017-05-11 14:01:55'),(28,'Admin','admin','+3(8777)777-77-77','777777777777',NULL,NULL,'05/02/2017','10:55',NULL,'Липей','+3(8777)777-77-77','ua',NULL,NULL,900.00,'Готівка',NULL,NULL,0,'10.4.83.197','59146effb07e6',NULL,'2017-05-11 14:02:39','2017-05-11 14:02:39'),(29,'Adminеуіеadmin','admin@amin.com','+3(8222)222-22-22','bbbbbbbbbbbbb',NULL,NULL,'05/16/2017','12:30','12:00','aaaaaaaaa','+3(8222)222-22-22','ua','Я отримувач',NULL,650.00,'Готівка',NULL,NULL,0,'10.4.83.197','5914996505452',NULL,'2017-05-11 17:03:33','2017-05-11 17:03:33'),(30,'Admin','admin','+3(8777)777-77-77','апрапр',NULL,NULL,'05/01/2017','23:00','12:00','апрапр','+3(8666)666-66-66','ua','Я отримувач',NULL,800.00,NULL,NULL,NULL,0,'10.4.83.197','5914996a3f68d',NULL,'2017-05-11 17:03:38','2017-05-11 17:03:38'),(31,'Федорук','marta.fedoruk@redentu.com','+3(8777)777-77-77','паропроп',NULL,NULL,'05/02/2017','23:00','12:00','Липей','+3(8666)666-66-66','ua','Я отримувач',NULL,800.00,NULL,NULL,NULL,0,'10.4.83.197','591499ce7f1e7',NULL,'2017-05-11 17:05:18','2017-05-11 17:05:18'),(32,'Adminaaaaa','admin@admikn.comj','+3(8222)222-22-22','aaaaaaaaaaaa',NULL,'','05/16/2017','10:15','12:30','asdddddddddd','+3(8222)222-22-22','ua','','',1000.00,'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комісія)','success','',0,'10.4.83.197','59157d6fb3fd9',NULL,'2017-05-12 09:16:31','2017-05-12 10:32:17'),(33,'Admin','admin','+3(8435)455-55-55','555555555555555555555',NULL,NULL,'05/23/2017','До','Від','5555','+3(8555)555-55-55','ua',NULL,NULL,1000.00,'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комісія)',NULL,NULL,0,'10.4.83.197','5915a29a2ab2e',NULL,'2017-05-12 11:55:06','2017-05-12 11:55:06'),(34,'Admin','admin','+3(8435)455-55-55','555555555555555555555',NULL,NULL,'05/23/2017','До','Від','5555','+3(8555)555-55-55','ua',NULL,NULL,1000.00,'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комісія)',NULL,NULL,0,'10.4.83.197','5915a2a5f0fd2',NULL,'2017-05-12 11:55:17','2017-05-12 11:55:17'),(35,'Admin','admin','+3(8435)455-55-55','555555555555555555555',NULL,NULL,'05/23/2017','До','Від','5555','+3(8555)555-55-55','ua',NULL,NULL,1000.00,'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комісія)',NULL,NULL,0,'10.4.83.197','5915a3030cd24',NULL,'2017-05-12 11:56:51','2017-05-12 11:56:51'),(36,'Admin','admin','+3(8435)455-55-55','555555555555555555555',NULL,NULL,'05/23/2017','До','Від','5555','+3(8555)555-55-55','ua',NULL,NULL,1000.00,'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комісія)',NULL,NULL,0,'10.4.83.197','5915a30542f7c',NULL,'2017-05-12 11:56:53','2017-05-12 11:56:53'),(37,'Admin','admin','+3(8435)455-55-55','555555555555555555555',NULL,NULL,'05/23/2017','До','Від','5555','+3(8555)555-55-55','ua',NULL,NULL,1000.00,'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комісія)',NULL,NULL,0,'10.4.83.197','5915a30fb1018',NULL,'2017-05-12 11:57:03','2017-05-12 11:57:03'),(38,'Admin','admin','+3(8555)555-55-55','прпрарапрапр',NULL,NULL,'05/26/2017','До','Від','апапавп','+3(883)556-56-56','ua',NULL,NULL,1300.00,'Liqpay','failure',NULL,0,'10.4.83.197','5915a54ab37c5',NULL,'2017-05-12 12:06:34','2017-05-12 12:07:08'),(39,'Admin','admin','+3(8555)555-55-55','55555555555555555555',NULL,NULL,'05/24/2017','06:30','03:15','55555','+3(8555)555-55-55','ua',NULL,NULL,1000.00,'Liqpay','sandbox',NULL,0,'10.4.83.197','5915ac2365dc6',NULL,'2017-05-12 12:35:47','2017-05-12 12:35:59'),(40,'Admin','admin','+3(8777)777-77-77','л пвда рпдв арпвраолдрлов',NULL,NULL,'05/16/2017','23:58','12:00','Хмиз Віталій','+3(8666)666-66-66','ua',NULL,NULL,700.00,'Готівка',NULL,NULL,0,'10.4.83.197','5915aef5994c6',NULL,'2017-05-12 12:47:49','2017-05-12 12:47:49'),(41,'Марта','marta.fedoruk@redentu.com','+3(8777)777-77-77','л пвда рпдв арпвраолдрлов',NULL,NULL,'05/16/2017','23:58','12:00','Хмиз Віталій','+3(8666)666-66-66','ua',NULL,NULL,700.00,'Готівка',NULL,NULL,0,'10.4.83.197','5915af1645240',NULL,'2017-05-12 12:48:22','2017-05-12 12:48:22'),(42,'Марта','marta.fedoruk@redentu.com','+3(8777)777-77-77','л пвда рпдв арпвраолдрлов',NULL,NULL,'05/16/2017','23:58','12:00','Хмиз Віталій','+3(8666)666-66-66','ua','Я отримувач',NULL,700.00,'Готівка',NULL,NULL,0,'10.4.83.197','5915af30860bf',NULL,'2017-05-12 12:48:48','2017-05-12 12:48:48'),(43,'Марта','marta.fedoruk@redentu.com','+3(8777)777-77-77','л пвда рпдв арпвраолдрлов',NULL,NULL,'05/16/2017','23:58','12:00','Хмиз Віталій','+3(8666)666-66-66','ua','Я отримувач',NULL,700.00,'Готівка',NULL,NULL,0,'10.4.83.197','5915af65b0ae9',NULL,'2017-05-12 12:49:41','2017-05-12 12:49:41'),(44,'Марта','marta.fedoruk@redentu.com','+3(8777)777-77-77','л пвда рпдв арпвраолдрлов',NULL,NULL,'05/16/2017','23:58','12:00','Хмиз Віталій','+3(8666)666-66-66','ua','Я отримувач',NULL,700.00,'Готівка',NULL,NULL,0,'10.4.83.197','5915af6bf12c1',NULL,'2017-05-12 12:49:47','2017-05-12 12:49:47'),(45,'hfg h fg','marta.fedoruk@redentu.com','+3(8666)666-66-66','dgfdgdfgdf',NULL,NULL,'05/03/2017','23:00','11:00','fgjfghjfgh','+3(8777)777-77-77','ua','Я отримувач',NULL,650.00,'Liqpay',NULL,NULL,0,'10.4.83.197','5915b6165922d',NULL,'2017-05-12 13:18:14','2017-05-12 13:18:14');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgs` text COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (3,NULL,NULL,0,3,1,'2017-04-03 09:51:00','2017-04-03 06:52:06','2017-05-04 22:26:54'),(4,'4.jpg','{\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/3.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/7.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/5.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/8.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/mPjI0bDxbRY.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/3-e1478373412754.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/04.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/4-e1478373626161.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/4pYTrQziasI.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/18350.736x1104.1443509168.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/\\/upload\\/pages\\/4\\/big\\/25.jpg\":{\"name\":\"\"},\"-45ueqhWdpE.jpg\":{\"name\":\"\"},\"dS0QUmlL65Y.jpg\":{\"name\":\"\"},\"EtEpFIXEXd8.jpg\":{\"name\":\"\"},\"HHbdvu9UhCA.jpg\":{\"name\":\"\"},\"jAgrHqck9Kg.jpg\":{\"name\":\"\"},\"qyt3OsRIXbI.jpg\":{\"name\":\"\"},\"WSDoHjfBjTM.jpg\":{\"name\":\"\"}}',2,4,1,'2017-04-03 13:35:00','2017-04-03 10:37:01','2017-05-11 11:50:47'),(5,'5.jpg','{\"\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/1.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/2.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/3.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/4.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/\\/upload\\/pages\\/5\\/big\\/5.png\":{\"name\":\"\"},\"67f300fcdf.jpg\":{\"name\":\"\"},\"14455.736x1104.1410502669.jpg\":{\"name\":\"\"},\"4391461_9r52nyu3224o40g8w.jpg\":{\"name\":\"\"},\"13227512_wag62a0vfuo0g4g4.jpg\":{\"name\":\"\"},\"c411ee25d3dc06375f3959ce55d7743d.jpg\":{\"name\":\"\"},\"s800 (1).jpg\":{\"name\":\"\"},\"s800.jpg\":{\"name\":\"\"}}',2,5,1,'2017-04-03 13:39:00','2017-04-03 10:43:58','2017-05-10 11:52:51'),(6,'6.jpg','{\"2015_servirovka-stola_servirovka-stola-7.jpg\":{\"name\":\"\"},\"5-novogodnij-stol-copy.jpg\":{\"name\":\"\"},\"6ffcc0d3641930e3d8980ec43343c.jpg\":{\"name\":\"\"},\"decor1collage_photocat.jpg\":{\"name\":\"\"},\"kak-ukrasit-novogodnij-stol-2016.jpg\":{\"name\":\"\"},\"zolotistoe-oformlenie.jpg\":{\"name\":\"\"},\"\\u041d\\u043e\\u0432\\u043e\\u0433\\u043e\\u0434\\u043d\\u0438\\u0439-\\u0441\\u0442\\u043e\\u043b-2015-\\u043a\\u0430\\u043a-\\u043e\\u0442\\u043f\\u0440\\u0430\\u0437\\u0434\\u043d\\u043e\\u0432\\u0430\\u0442\\u044c-\\u0433\\u043e\\u0434-\\u041a\\u043e\\u0437\\u044b.jpg\":{\"name\":\"\"}}',3,6,1,'2017-04-03 14:05:00','2017-04-03 11:06:32','2017-04-26 08:08:45'),(7,'7.jpg','{\"domashniy-zimoviy-zatishok.jpg\":{\"name\":\"\"},\"kimnatna-oranjereia.jpg\":{\"name\":\"\"},\"kompozitsiia-na-sviatkoviy-stil.jpg\":{\"name\":\"\"},\"navishcho-fotozona-na-vesilli.jpg\":{\"name\":\"\"},\"navishcho-potribna-dostavka-kvitiv-ta-chim-tse-zruchno.jpg\":{\"name\":\"\"},\"osinnia-kazka.jpg\":{\"name\":\"\"},\"vesniane-vesillia-iaki-kviti-vibrati.jpg\":{\"name\":\"\"},\"zabudte-pro-novorichni-turboti.jpg\":{\"name\":\"\"},\"zimova-klasika.jpg\":{\"name\":\"\"},\"zimovi-vesilni-buketi.jpg\":{\"name\":\"\"}}',3,7,1,'2017-04-03 14:08:00','2017-04-03 11:09:06','2017-04-26 08:09:07'),(8,'8.jpg','{\"00bafb9e115381abb46f80b62cc086ec.jpg\":{\"name\":\"\"},\"6d08118cfaef35b1bcd57d904c73e941.jpg\":{\"name\":\"\"},\"73e8dee78f15b52e9a6a583234dc9182.jpg\":{\"name\":\"\"},\"482dd5109c3d0bdf3c0c52a0900672d6.jpg\":{\"name\":\"\"},\"5028aaae8eb16677efa03b48954ad64c.jpg\":{\"name\":\"\"},\"br1.jpg\":{\"name\":\"\"},\"bracelet_bride_from_beige_roses.JPG\":{\"name\":\"\"},\"braslety_na_ruku_iz_zhivykh_tsvetov_dlya_podruzhek_nevesty.jpg\":{\"name\":\"\"},\"c22f540a5681ded98e1e82952db6e003.jpg\":{\"name\":\"\"},\"lilybridesmaid.jpg\":{\"name\":\"\"},\"resizes_braslety-dlya-podruzhek-nevesty19.jpg\":{\"name\":\"\"},\"v7YlHayUXjY.jpg\":{\"name\":\"\"}}',2,8,1,'2017-04-03 14:58:00','2017-04-03 11:59:07','2017-05-10 12:47:05'),(9,'9.jpg','{\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/__s1tGbtUSk.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/4Tc57dXcuvQ.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/akuBHiEkBIw.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/IBKKGnr3fEA.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/K7iAH0p0wc4.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/nunta-29-1024x681.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/oGjpCpaDA_0.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/s800.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/svadebnaya-floristika.jpg\":{\"name\":\"\"},\"\\/upload\\/pages\\/9\\/big\\/\\/upload\\/pages\\/9\\/big\\/wimTqgcFn8E.jpg\":{\"name\":\"\"},\"3.jpg\":{\"name\":\"\"},\"10.jpg\":{\"name\":\"\"},\"24 (2).jpg\":{\"name\":\"\"},\"49.jpg\":{\"name\":\"\"},\"55.jpg\":{\"name\":\"\"}}',2,9,1,'2017-04-03 15:01:00','2017-04-03 12:01:09','2017-05-11 12:15:01'),(10,'10.jpg','{\"15svadebnyeukrashenijanamashinu.jpg\":{\"name\":\"\"},\"43a3f3c782bdc7b203cf869e539daa80.png\":{\"name\":\"\"},\"875fc790c65fd9bf17977ee7cdabdd76.jpg\":{\"name\":\"\"},\"b2a580fc34d70eb71921463e581e7e82.jpg\":{\"name\":\"\"},\"BER_0016-1024x717.jpg.pagespeed.ce.JlbxwSkPNd.jpg\":{\"name\":\"\"},\"BER_9992.jpg\":{\"name\":\"\"}}',2,10,1,'2017-04-04 13:33:00','2017-04-04 10:33:21','2017-05-11 12:23:08'),(11,'11.jpg','{\"\\/upload\\/pages\\/11\\/big\\/\\/upload\\/pages\\/11\\/big\\/4master-show-bg.png\":{\"name\":\"\"}}',1,11,1,'2017-04-04 15:44:00','2017-04-04 12:44:37','2017-05-04 23:58:02'),(12,'12.jpg','{\"58a483fa3ccfa_w7bsm6stvqg.jpg\":{\"name\":\"\"},\"587bd1d06ea19_s1si0fmavjy.jpg\":{\"name\":\"\"},\"57f2acc69a7b5_0.02.01.a40f512087959db8fc95a68e43d871ac2845491a75985f9495f9e025730de430_full.jpg\":{\"name\":\"\"},\"58a482f228f2f_cive2yp3nvk.jpg\":{\"name\":\"\"},\"587bd16a07042_4xmfw5eqovg.jpg\":{\"name\":\"\"},\"587bd1e6f32b8_vnuvjttnxmk.jpg\":{\"name\":\"\"},\"587bd1a51ef18_fgw3baxrrcg.jpg\":{\"name\":\"\"},\"587bd18336f47_alrca2fezxi.jpg\":{\"name\":\"\"},\"5828ac7fb35b8_0-02-03-6c89bdac8dc265d1adc34d85a79d3fd0861d093b978d340d4648a90b56ee6786_full.jpg\":{\"name\":\"\"},\"5828ad9e36327_0-02-05-6e72d0d89fed202372651f9a0fff3c4df2ea5b0a038a49ea0d77369e2c3ea418_full.jpg\":{\"name\":\"\"},\"5845d0f2bb48d_0-02-05-3c6b1e126d799b77f4c1229ba1549970bebd4ba514bd02132592b69332bb21b7_full.jpg\":{\"name\":\"\"}}',0,12,1,'2017-05-04 17:59:00','2017-05-04 14:59:46','2017-05-11 12:36:55'),(14,'14.jpg','{\"\\/upload\\/pages\\/14\\/big\\/6.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/14\\/big\\/7.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/14\\/big\\/8.png\":{\"name\":\"\"}}',1,13,1,'2017-05-09 08:41:00','2017-05-09 05:42:00','2017-05-09 05:51:35'),(15,'15.jpg','{\"\\/upload\\/pages\\/15\\/big\\/Big_Pinnacle_of_Pilot_Mountain_high_res.jpg\":{\"name\":\"\"}}',1,14,1,'2017-05-10 12:13:00','2017-05-10 09:13:25','2017-05-10 10:45:21'),(16,'16.jpg',NULL,4,15,1,'2017-05-10 18:54:00','2017-05-10 15:55:32','2017-05-10 15:55:33'),(17,'17.jpg','{\"\\/upload\\/pages\\/17\\/big\\/2.png\":{\"name\":\"\"},\"\\/upload\\/pages\\/17\\/big\\/2\\u0430.png\":{\"name\":\"\"}}',1,16,1,'2017-05-11 13:16:00','2017-05-11 10:16:34','2017-05-11 10:16:56'),(18,'18.jpg',NULL,1,17,1,'2017-05-11 15:36:00','2017-05-11 12:36:53','2017-05-11 12:37:38'),(19,'19.jpg',NULL,4,18,1,'2017-05-11 16:12:00','2017-05-11 13:12:37','2017-05-12 09:03:10'),(20,NULL,NULL,1,19,1,'2017-05-11 16:56:00','2017-05-11 13:56:52','2017-05-11 13:56:52'),(21,'21.jpg',NULL,4,20,1,'2017-05-12 13:13:00','2017-05-12 10:14:11','2017-05-12 10:14:48');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_translations`
--

DROP TABLE IF EXISTS `pages_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`page_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_translations`
--

LOCK TABLES `pages_translations` WRITE;
/*!40000 ALTER TABLE `pages_translations` DISABLE KEYS */;
INSERT INTO `pages_translations` VALUES ('ru',3,'contacts','Контакти','','','',NULL,''),('ua',3,'contacts','Контакти','','','',NULL,''),('en',4,'','','','','',NULL,''),('ru',4,'vesilni-buketi','Весільні букети','','','',NULL,'<p>Букет нареченої є важливою складовою образу нареченої. Квіти повинні гармонійно поєднуватися з сукнею, а також<br />&nbsp;відповідати характеру нареченої. Ніжний або яскравий, мініатюрний або асиметричні - в будь-якому випадку,&nbsp;<br />букет Lacy Bird буде гармонійним, красивим і стильним. Флористика - мистецтво, доповнене емоціями і уявою.</p>'),('ua',4,'vesilni-bukety','Весільні букети','','','',NULL,'<p>Букет нареченої є важливою складовою образу нареченої. Квіти повинні гармонійно поєднуватися з сукнею, а також<br />&nbsp;відповідати характеру нареченої. Ніжний або яскравий, мініатюрний або асиметричні - в будь-якому випадку,&nbsp;<br />букет від Квіточки буде гармонійним, красивим і стильним. Флористика - мистецтво, доповнене емоціями і уявою.</p>'),('en',5,'','','','','',NULL,''),('ru',5,'butonierki','Бутоньєрки','','','',NULL,'<p>Будь яке весілля також не обходиться без бутоньєрки. Вони можуть додати фінальний штрих до образу нареченого. Однак вибираючи бутоньєрку пам&#39;ятайте, що вона повинна не тільки підходити до костюму нареченого, а й гармонійно поєднюватися з букетом нареченої. Дуже романтично, коли квіти в петлиці нареченого схожі на ті, з яких складено букет нареченої (або точно такі ж).</p>'),('ua',5,'butonierky','Бутоньєрки','','','',NULL,'<p>Будь яке весілля також не обходиться без бутоньєрки. Вона може додати фінальний штрих до образу нареченого. Однак вибираючи бутоньєрку пам&#39;ятайте, що вона повинна не тільки підходити до костюму нареченого, а й гармонійно поєднюватися з букетом нареченої. Дуже романтично, коли квіти в петлиці нареченого схожі на ті, з яких складено букет нареченої (або точно такі ж).</p>'),('ru',6,'stvorennya-florariumu','Створення флораріуму','','500грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.<br />&nbsp;&nbsp;</p>'),('ua',6,'stvorennia-florariumu','Створення флораріуму','','500грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.<br />&nbsp;&nbsp;</p>'),('ru',7,'vigotovlennya-pashalnogo-vinochka','Виготовлення пасхального віночка','','1700грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.<br />&nbsp;&nbsp;</p>'),('ua',7,'vygotovlennia-paskhalnogo-vinochka','Виготовлення пасхального віночка','','1700грн','',NULL,'<p>На майстер класах Ви:</p><p>- дізнаєтесь про особливості різних квітів та рослин, та їх використання у флористиці<br />- навчитесь складати гармонійні квіткові композиції різноманітних форм, розмірів&nbsp;<br />- гарантовано отримаєте позитивний заряд гарного гастрою.</p>'),('en',8,'','','','','',NULL,''),('ru',8,'buketi-dlya-drujok','Букети для дружок','','','',NULL,'<p>Все частіше ми бачимо на західних весіллях, що подружки нареченої не тільки вбрані в схожі за стилем сукні, а й кожна тримає в руках по невеликому акуратному букетику. Букет подружки нареченої - навіщо він потрібнен, і яким він повинен бути? Схоже, наречені стають все сентиментальнішими бо все частіше бажають залишити свій весільний букет на довгу пам&#39;ять про святий день одруження. Але від улюбленої, усіма без винятку дівчатами, традиції ловити весільний букет, відмовлятися ніхто не готовий, тому букет однієї з подружок часто під вечір стає Головним Букетом Свята, і летить в натовп незаміжніх подруг.</p>'),('ua',8,'ukr','Букети для дружок','','tyui7yi','',NULL,'<p>Все частіше ми бачимо на західних весіллях, що подружки нареченої не тільки вбрані в схожі за стилем сукні, а й кожна тримає в руках по невеликому акуратному букетику. Букет подружки нареченої - навіщо він потрібнен, і яким він повинен бути? Схоже, наречені стають все сентиментальнішими бо все частіше бажають залишити свій весільний букет на довгу пам&#39;ять про святий день одруження. Але від улюбленої, усіма без винятку дівчатами, традиції ловити весільний букет, відмовлятися ніхто не готовий, тому букет однієї з подружок часто під вечір стає головним букетом свята, і летить в натовп незаміжніх подруг.</p>'),('en',9,'','','','','',NULL,''),('ru',9,'oformlennya-zalu','Оформлення залу','','','',NULL,'<p>Одним з найважливіших пунктів підготовки до весілля є оформлення залу. І цей пункт плану вимагає особливого підходу і уваги.&nbsp;Оформлення весільного залу - це наука, яка виросла з кращих весільних традицій, як нашої країни, так і європейських країн.&nbsp;Спочатку Вам треба ретельно обговорити оформлення залу з нашими флористами, для того, щоб всі елементи декору&nbsp;поєднувалися, як з кольором, так і з тематикою Вашого свята. Крім того, оформлення банкетних залів має підкреслювати&nbsp;індивідуальність нареченого і нареченої.&nbsp;</p>'),('ua',9,'oformlennia-zalu','Оформлення залу','','','',NULL,'<p>Одним з найважливіших пунктів підготовки до весілля є оформлення залу. І цей пункт плану вимагає особливого підходу і уваги.&nbsp;Оформлення весільного залу - це наука, яка виросла з кращих весільних традицій, як нашої країни, так і європейських країн.&nbsp;Спочатку Вам треба ретельно обговорити оформлення залу з нашими флористами, для того, щоб всі елементи декору&nbsp;поєднувалися, як з кольором, так і з тематикою Вашого свята. Крім того, оформлення банкетних залів має підкреслювати&nbsp;індивідуальність нареченого і нареченої.&nbsp;</p>'),('en',10,'','','','','',NULL,''),('ru',10,'oformlennya-mashin','Оформление машин','','','',NULL,'<p>Без оформлення весільної машини сьогодні не обходиться ні одне весілля. Кожна пара молодят хоче, щоб в цей урочистий&nbsp;день &nbsp;їх машина також виглядала достойно. Зараз молодята не прикріплюють ляльку на капот та не оформляють машину&nbsp;для весілля за допомогою штучних квітів. Прикраси для весільних машин призначені для того, щоб привернути увагу людей&nbsp;<br />до весільного ескорту.&nbsp;</p>'),('ua',10,'oformlennia-mashyn','Оформлення машин','','','',NULL,'<p>Без оформлення весільної машини сьогодні не обходиться ні одне весілля. Кожна пара молодят хоче, щоб в цей урочистий&nbsp;день &nbsp;їх машина також виглядала достойно. Зараз молодята не прикріплюють ляльку на капот та не оформляють машину&nbsp;для весілля за допомогою штучних квітів. Прикраси для весільних машин призначені для того, щоб привернути увагу людей&nbsp;<br />до весільного ескорту.&nbsp;</p>'),('ru',11,'kimnatna-oranjereya','Кімнатна оранжерея','','оранжерея\r\nкімнатна\r\n...','',NULL,'<p>тут контент .................</p><p>текст +</p><p><img alt=\"\" style=\"width: 250px; height: 250px;\" src=\"http://kvitochka/photos/shares/58e395d551298.png\" /></p>'),('ua',11,'tggfhfghfghfh','vbnvbnvbnvbnvbn','','','',NULL,'<h1 style=\"text-align: center;\">dfghdfhfghfh</h1><p>dsfsdfsd sf sdfs</p>'),('en',12,'','','','','',NULL,''),('ru',12,'12','','','','',NULL,''),('ua',12,'our-works','Майстер клас (наші роботи)','our-works','','',NULL,''),('en',14,'14','','','','',NULL,''),('ru',14,'14','','','','',NULL,''),('ua',14,'testova-storinka','тестова сторінка2','','','',NULL,'<p>очар лочвр поіврп оліврпоірпор олівр ір впрі лірвплд ілдврп лдіврплд рівдлпр ідлрп лдівдлпірвдл ділв пдлдплвр длпірдл ідлпв д</p>'),('en',15,'15','','','','',NULL,''),('ru',15,'15','Страница','','','',NULL,''),('ua',15,'storinka','Сторінка','','пара ап ','',NULL,'<p>апр апр ап апр апр апр&nbsp;</p><p><img alt=\"\" style=\"width: 6760px; height: 3523px;\" src=\"http://kvitochka.srv.lionservice.pro/photos/shares/5912eef91edcd.jpg\" /></p>'),('en',16,'16','','','','',NULL,''),('ru',16,'16','','','','',NULL,''),('ua',16,'s','S','','Абонемент на місяць -\r\nАбонемент на пів  року - \r\nАбонемент на рік - ','',NULL,'<p>test content</p>'),('en',17,'17','','','','',NULL,''),('ru',17,'17','','','','',NULL,''),('ua',17,'ra-ap-arp-','ра ап арп ','','','',NULL,'<p>апр апапр ап рапр апр</p>'),('en',18,'18','','','','',NULL,''),('ru',18,'18','','','','',NULL,''),('ua',18,'nova-storinka','Нова сторінка','','','',NULL,'<p>впао допвд длвоп длвоп овдп одпдв п&nbsp;</p><p><img alt=\"\" style=\"width: 6760px; height: 3523px;\" src=\"http://kvitochka.srv.lionservice.pro/photos/shares/59145afac0972.jpg\" /></p>'),('en',19,'19','','','','',NULL,''),('ru',19,'19','','','','',NULL,''),('ua',19,'m','M','','Абонемент на місяць -432432\r\nАбонемент на пів року - \r\nАбонемент на рік -','',NULL,'<p>dfg</p>'),('en',20,'20','','','','',NULL,''),('ru',20,'20','','','','',NULL,''),('ua',20,'nova','нова','','','',NULL,'<p><img alt=\"\" src=\"http://kvitochka.srv.lionservice.pro/photos/shares/59146d9466752.jpg\" /></p>'),('en',21,'21','','','','',NULL,''),('ru',21,'21','','','','',NULL,''),('ua',21,'l','L','','Абонемент на місяць -\r\nАбонемент на пів  року - \r\nАбонемент на рік - ','',NULL,'<p>опис можна міняти через адмінку</p>');
/*!40000 ALTER TABLE `pages_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `name` mediumtext COLLATE utf8_unicode_ci,
  `email` mediumtext COLLATE utf8_unicode_ci,
  `phone` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (19,'/upload/photosfrui1.jpg',NULL,NULL,NULL,'2017-04-25 09:10:39','2017-04-25 09:10:39'),(21,'/upload/photos/biliy-vodospad-44.jpg',NULL,NULL,NULL,'2017-05-04 13:46:54','2017-05-04 13:46:54'),(22,'/upload/photos/happy-box.jpg',NULL,NULL,NULL,'2017-05-04 13:50:37','2017-05-04 13:50:37'),(24,'/upload/photos/4master-show-callback-bg.png','asdasasd','asd@maul.com','123123123','2017-05-04 14:01:11','2017-05-04 14:01:11'),(25,'4master-show-1.png','asdasdas','sadsad@mil.com','123123','2017-05-04 14:15:45','2017-05-04 14:15:45'),(26,'7.jpg','Марта','marta.fedoruk@redentu.com','986986986','2017-05-09 05:20:07','2017-05-09 05:20:07'),(27,'1371452485_002.jpg','мриами','marta.fedoruk@redentu.com','67567','2017-05-09 05:23:20','2017-05-09 05:23:20'),(28,'1371452485_002.jpg','Марта','marta.fedoruk@redentu.com','986986986','2017-05-09 05:23:45','2017-05-09 05:23:45'),(29,'6.png','Marta','marta.fedoruk@redentu.com','456747767','2017-05-09 08:27:53','2017-05-09 08:27:53'),(30,'1371452485_002.jpg','Marta','marta.fedoruk@redentu.com','657567','2017-05-09 08:29:12','2017-05-09 08:29:12'),(31,'Big_Pinnacle_of_Pilot_Mountain_high_res.jpg','Марта','marta.fedoruk@redentu.com','456456456456','2017-05-10 11:01:31','2017-05-10 11:01:31'),(32,'download.jpg','апр вар','marta.fedoruk@redentu.com','667567567','2017-05-10 11:08:18','2017-05-10 11:08:18'),(33,'1371452485_002.jpg','h ggh','marta.fedoruk@redentu.com','+3(8777)777-77-77','2017-05-11 09:31:59','2017-05-11 09:31:59'),(34,'10.png','Марта','marta.fedoruk@redentu.com','+3(8888)888-88-88','2017-05-12 10:33:37','2017-05-12 10:33:37');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubrics`
--

DROP TABLE IF EXISTS `rubrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rubrics_parent_id_foreign` (`parent_id`),
  CONSTRAINT `rubrics_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `rubrics` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubrics`
--

LOCK TABLES `rubrics` WRITE;
/*!40000 ALTER TABLE `rubrics` DISABLE KEYS */;
INSERT INTO `rubrics` VALUES (1,NULL,NULL),(2,NULL,NULL),(3,NULL,NULL),(4,NULL,NULL);
/*!40000 ALTER TABLE `rubrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubrics_translations`
--

DROP TABLE IF EXISTS `rubrics_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubrics_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rubric_id` int(10) unsigned NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rubric_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubrics_translations`
--

LOCK TABLES `rubrics_translations` WRITE;
/*!40000 ALTER TABLE `rubrics_translations` DISABLE KEYS */;
INSERT INTO `rubrics_translations` VALUES ('ru',1,'blog','Блог','','2017-03-22 14:11:58','2017-04-26 08:03:10'),('ua',1,'blog','Блог','',NULL,'2017-04-26 08:03:10'),('ru',2,'wedding-design','Свадебное оформление','','2017-03-22 14:12:03','2017-04-26 08:03:40'),('ua',2,'wedding-design','Весільне Оформлення','',NULL,'2017-04-26 08:03:40'),('ru',3,'master-show','Майстер Клас','','2017-03-22 14:12:07','2017-04-26 08:03:54'),('ua',3,'master-show','Мастер Клас','',NULL,'2017-04-26 08:03:54'),('ua',4,'pidpysky-na-kvity','Підписки на квіти',NULL,'2017-05-10 15:54:35','2017-05-10 15:54:35');
/*!40000 ALTER TABLE `rubrics_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribers_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_social_account`
--

DROP TABLE IF EXISTS `user_social_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_social_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_social_account`
--

LOCK TABLES `user_social_account` WRITE;
/*!40000 ALTER TABLE `user_social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_social_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin',NULL,NULL,1,'$2y$10$ozTwJjFnVi8jYHPjGoxaHOj/c0QwM5Wj0sfQclg8m1zZBV3S.cJb2',NULL,NULL,NULL),(2,'Саня Андрійченко','andryichenko123@gmail.com','132490806','vk',1,NULL,NULL,'2017-04-26 13:20:34','2017-05-09 09:52:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-12 22:19:24
