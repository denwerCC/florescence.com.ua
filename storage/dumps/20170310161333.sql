-- MySQL dump 10.13  Distrib 5.5.50, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: kvitochka
-- ------------------------------------------------------
-- Server version	5.5.50

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,NULL,NULL),(2,NULL,NULL,NULL),(3,NULL,NULL,NULL),(4,NULL,NULL,NULL),(5,NULL,NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_translations`
--

DROP TABLE IF EXISTS `categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_translations`
--

LOCK TABLES `categories_translations` WRITE;
/*!40000 ALTER TABLE `categories_translations` DISABLE KEYS */;
INSERT INTO `categories_translations` VALUES ('ru',1,'podarunki','Подарунки',NULL,'2017-03-06 14:15:12','2017-03-06 14:15:12'),('ru',2,'kompoizcii','Композиції','','2017-03-06 14:22:08','2017-03-06 15:12:19'),('ru',3,'vesilni-buketi','Весільні Букети',NULL,'2017-03-06 14:22:17','2017-03-06 14:22:17'),('ru',4,'koshiki-z-solodoschami','Кошики з солодощами',NULL,'2017-03-06 14:22:26','2017-03-06 14:22:26'),('ru',5,'korobki-z-kvitami','Коробки з квітами',NULL,'2017-03-06 14:22:35','2017-03-06 14:22:35');
/*!40000 ALTER TABLE `categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commentable_id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_orders`
--

DROP TABLE IF EXISTS `coupon_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `coupon_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_orders`
--

LOCK TABLES `coupon_orders` WRITE;
/*!40000 ALTER TABLE `coupon_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oneoff` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `type` enum('*','-') COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(8,2) NOT NULL,
  `expired` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupons_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f-subscribers`
--

DROP TABLE IF EXISTS `f-subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f-subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abonement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` text COLLATE utf8_unicode_ci,
  `additional_info` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f-subscribers`
--

LOCK TABLES `f-subscribers` WRITE;
/*!40000 ALTER TABLE `f-subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `f-subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `favorites_user_id_foreign` (`user_id`),
  KEY `favorites_item_id_foreign` (`item_id`),
  CONSTRAINT `favorites_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters`
--

DROP TABLE IF EXISTS `filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters_parent_id_foreign` (`parent_id`),
  CONSTRAINT `filters_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters`
--

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters_translations`
--

DROP TABLE IF EXISTS `filters_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`filter_id`,`locale`),
  CONSTRAINT `filters_translations_filter_id_foreign` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters_translations`
--

LOCK TABLES `filters_translations` WRITE;
/*!40000 ALTER TABLE `filters_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `filters_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `img` text COLLATE utf8_unicode_ci,
  `level` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_categories`
--

DROP TABLE IF EXISTS `galleries_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `img_width` int(11) DEFAULT NULL,
  `img_heigth` int(11) DEFAULT NULL,
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_heigth` int(11) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `galleries_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `galleries_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_categories`
--

LOCK TABLES `galleries_categories` WRITE;
/*!40000 ALTER TABLE `galleries_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_categories_translations`
--

DROP TABLE IF EXISTS `galleries_categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_categories_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_categories_translations`
--

LOCK TABLES `galleries_categories_translations` WRITE;
/*!40000 ALTER TABLE `galleries_categories_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_translations`
--

DROP TABLE IF EXISTS `galleries_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`gallery_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_translations`
--

LOCK TABLES `galleries_translations` WRITE;
/*!40000 ALTER TABLE `galleries_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_filters`
--

DROP TABLE IF EXISTS `item_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_filters_item_id_foreign` (`item_id`),
  KEY `item_filters_filter_id_foreign` (`filter_id`),
  CONSTRAINT `item_filters_filter_id_foreign` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_filters_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_filters`
--

LOCK TABLES `item_filters` WRITE;
/*!40000 ALTER TABLE `item_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_filters_translations`
--

DROP TABLE IF EXISTS `item_filters_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_filters_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_filter_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`item_filter_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_filters_translations`
--

LOCK TABLES `item_filters_translations` WRITE;
/*!40000 ALTER TABLE `item_filters_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_filters_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` text COLLATE utf8_unicode_ci,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `filter_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `stock` tinyint(1) NOT NULL DEFAULT '1',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (2,'{\"m039iaka-igrashka-visota-40-sm-258.jpg\":{\"name\":\"\"}}','',500.00,1,1,NULL,'2017-03-06 16:19:56',1,1,'2017-03-06 14:15:52','2017-03-06 14:19:56'),(3,'{\"m039iaka-igrashka-visota-50-sm-257.jpg\":{\"name\":\"\"}}','',0.00,2,1,NULL,'2017-03-06 17:10:19',1,1,'2017-03-06 15:10:16','2017-03-06 15:10:19'),(4,'{\"m039iaka-igrashka-visota-30-sm-254.jpg\":{\"name\":\"\"}}','',0.00,3,1,NULL,'2017-03-06 17:11:05',1,1,'2017-03-06 15:11:05','2017-03-06 15:11:05'),(5,'{\"kolorovi.jpg\":{\"name\":\"\"}}','',0.00,4,1,NULL,'2017-03-06 17:12:50',1,1,'2017-03-06 15:12:50','2017-03-06 15:12:50'),(6,'{\"korobka-z-ekzotichnimi-fruktami-252.jpg\":{\"name\":\"\"}}','',0.00,5,1,NULL,'2017-03-06 17:13:26',1,1,'2017-03-06 15:13:26','2017-03-06 15:13:26'),(7,'{\"kulku.jpg\":{\"name\":\"\"}}','',0.00,6,1,NULL,'2017-03-06 17:13:50',1,1,'2017-03-06 15:13:50','2017-03-06 15:13:50'),(8,'{\"rittersport.jpg\":{\"name\":\"\"}}','',0.00,7,1,NULL,'2017-03-06 17:14:38',1,1,'2017-03-06 15:14:23','2017-03-06 15:14:38'),(9,'{\"velika-korobka-z-ekzotichnimi-fruktami-253.jpg\":{\"name\":\"\"}}','',0.00,8,1,NULL,'2017-03-06 17:15:00',1,1,'2017-03-06 15:15:00','2017-03-06 15:15:00'),(10,'{\"chista-lubov-74.jpg\":{\"name\":\"\"}}','',0.00,9,5,NULL,'2017-03-06 17:21:09',1,1,'2017-03-06 15:21:09','2017-03-06 15:21:09'),(11,'{\"biliy-vodospad-44.jpg\":{\"name\":\"\"}}','',0.00,10,2,NULL,'2017-03-06 17:40:22',1,1,'2017-03-06 15:40:22','2017-03-06 15:40:22'),(13,'{\"happy-box.jpg\":{\"name\":\"\"}}','',0.00,11,5,NULL,'2017-03-06 18:42:16',1,1,'2017-03-06 16:42:16','2017-03-06 16:42:17'),(14,'{\"rojeva-khmarka-189.jpg\":{\"name\":\"\"}}','',0.00,12,5,NULL,'2017-03-06 18:42:54',1,1,'2017-03-06 16:42:54','2017-03-06 16:42:54'),(15,'{\"frui1.jpg\":{\"name\":\"\"}}','',0.00,13,4,NULL,'2017-03-06 18:43:22',1,1,'2017-03-06 16:43:22','2017-03-06 16:43:22'),(16,'{\"fruit-arabesk.jpg\":{\"name\":\"\"}}','',0.00,14,4,NULL,'2017-03-06 18:43:45',1,1,'2017-03-06 16:43:45','2017-03-06 16:43:45'),(17,'{\"kinder.jpg\":{\"name\":\"\"}}','',0.00,15,4,NULL,'2017-03-06 18:44:23',1,1,'2017-03-06 16:44:23','2017-03-06 16:44:23'),(18,'{\"prestij-147.jpg\":{\"name\":\"\"}}','',0.00,16,2,NULL,'2017-03-06 18:45:06',1,1,'2017-03-06 16:45:06','2017-03-06 16:45:06'),(19,'{\"nijni-pochuttia-177.jpg\":{\"name\":\"\"}}','',0.00,17,2,NULL,'2017-03-06 18:45:25',1,1,'2017-03-06 16:45:25','2017-03-06 16:45:25'),(20,'{\"1.jpg\":{\"name\":\"\"}}','',0.00,18,3,NULL,'2017-03-06 18:48:25',1,1,'2017-03-06 16:45:58','2017-03-06 16:48:25'),(21,'{\"2.jpg\":{\"name\":\"\"}}','',0.00,19,3,NULL,'2017-03-06 18:48:53',1,1,'2017-03-06 16:46:14','2017-03-06 16:48:53'),(22,'{\"3.jpg\":{\"name\":\"\"}}','',0.00,20,3,NULL,'2017-03-06 18:49:08',1,1,'2017-03-06 16:46:27','2017-03-06 16:49:08');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_translations`
--

DROP TABLE IF EXISTS `items_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`item_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_translations`
--

LOCK TABLES `items_translations` WRITE;
/*!40000 ALTER TABLE `items_translations` DISABLE KEYS */;
INSERT INTO `items_translations` VALUES ('ru',2,'igrashka-40sm','М\'яка іграшка висота 40см','','','',NULL,''),('ru',3,'myaka-igrashka-visota-50sm','М\'яка іграшка висота 50см','','','',NULL,''),('ru',4,'myaka-igrashka-visota-30sm','М\'яка іграшка висота 30см','','','',NULL,''),('ru',5,'kolorovi-kulki','Кольорові кульки','','','',NULL,''),('ru',6,'ekzotichni-frukti','Екзотичні фрукти','','','',NULL,''),('ru',7,'kulki-z-geliiem','Кульки з гелієм','','','',NULL,''),('ru',8,'shokoladka-rottersport','Шоколадка RotterSport','','','',NULL,''),('ru',9,'ekzotichni-frukti-2','Екзотичні фрукти #2','','','',NULL,''),('ru',10,'chista-lyubov','Чиста любов','','','',NULL,''),('ru',11,'biliy-vodospad','Білий водоспад','','','',NULL,''),('ru',13,'schasliva-korobka','Щаслива Коробка','','','',NULL,''),('ru',14,'rojeva-hmarka','Рожева хмарка','','','',NULL,''),('ru',15,'ekzotika','Екзотика','','','',NULL,''),('ru',16,'arabesk','Арабеск','','','',NULL,''),('ru',17,'korobka-z-kinderami','Коробка з кіндерами','','','',NULL,''),('ru',18,'prestij','Престиж','','','',NULL,''),('ru',19,'nijni-pochuttya','Ніжні почуття','','','',NULL,''),('ru',20,'vesilniy-buket-1','Весільний букет #1','','','',NULL,''),('ru',21,'vesilniy-buket-2','Весільний букет #2','','','',NULL,''),('ru',22,'vesilniy-buket-3','Весільний букет #3','','','',NULL,'');
/*!40000 ALTER TABLE `items_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parent_id_foreign` (`parent_id`),
  CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus_translations`
--

DROP TABLE IF EXISTS `menus_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus_translations`
--

LOCK TABLES `menus_translations` WRITE;
/*!40000 ALTER TABLE `menus_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `menus_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_filters_table',1),(2,'2014_10_12_000000_create_users_table',1),(3,'2014_10_12_000001_create_categories_table',1),(4,'2014_10_12_000001_create_menus_table',1),(5,'2014_10_12_100000_create_orders',1),(6,'2014_10_12_100000_create_password_resets_table',1),(7,'2016_08_09_085740_create_comments_table',1),(8,'2016_08_14_125130_create_rubrics_table',1),(9,'2016_08_19_102435_create_subscribers_table',1),(10,'2016_08_22_220908_create_coupons_table',1),(11,'2016_08_22_221506_create_coupon_orders_table',1),(12,'2016_10_28_192500_create_items',1),(13,'2016_10_28_192533_create_item_filters',1),(14,'2016_10_28_192818_create_pages',1),(15,'2016_10_28_192832_create_order_items_table',1),(16,'2016_11_30_231401_create_galleries_table',1),(17,'2016_12_02_030735_create_favorites_table',1),(18,'2017_03_10_141755_create_flowers_subscription',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `parameters` text COLLATE utf8_unicode_ci,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_item_id_foreign` (`item_id`),
  CONSTRAINT `order_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES (1,24,5,'Кольорові кульки','',0.00,NULL,2,'2017-03-10 12:02:49','2017-03-10 12:02:49'),(2,24,18,'Престиж','',0.00,NULL,1,'2017-03-10 12:02:49','2017-03-10 12:02:49'),(3,25,2,'М\'яка іграшка висота 40см','',500.00,NULL,1,'2017-03-10 12:05:25','2017-03-10 12:05:25'),(4,26,6,'Екзотичні фрукти','',0.00,NULL,1,'2017-03-10 12:09:15','2017-03-10 12:09:15'),(5,27,2,'М\'яка іграшка висота 40см','',500.00,NULL,1,'2017-03-10 12:11:34','2017-03-10 12:11:34'),(6,28,2,'М\'яка іграшка висота 40см','',500.00,NULL,1,'2017-03-10 12:13:59','2017-03-10 12:13:59');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `delivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  `amount` decimal(8,2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (25,'Я','andryichenko123@gmail.com','38093 720 2021','LVIV',1,'Курьер (по Киеву + 40 грн)','ru','yo',NULL,500.00,'Cash',NULL,NULL,0,'127.0.0.1','58c29684d538d','2017-03-10 12:05:24','2017-03-10 12:05:24'),(26,'Admin','admin@example.com','asdasd','asdd',1,'','ru','dasdasd',NULL,0.00,'Cash',NULL,NULL,0,'127.0.0.1','58c2976b07d12','2017-03-10 12:09:15','2017-03-10 12:09:15'),(27,'Admin','admin@example.com','ввв','asdd',1,'InTime (от 30 грн)','ru','dasdasdasd',NULL,500.00,'Cash',NULL,NULL,0,'127.0.0.1','58c297f657de4','2017-03-10 12:11:34','2017-03-10 12:11:34'),(28,'Admin','admin@example.com','asd','lvl',1,'','ru','',NULL,500.00,'LiqPay',NULL,NULL,0,'127.0.0.1','58c29887a2f3a','2017-03-10 12:13:59','2017-03-10 12:13:59');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `public` tinyint(1) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (3,'3.jpg',1,1,1,'2017-03-09 15:00:00','2017-03-09 13:01:19','2017-03-09 14:12:11'),(12,'12.jpg',1,1,10,'2017-03-09 15:15:00','2017-03-09 13:28:31','2017-03-09 13:28:31'),(13,'13.jpg',1,1,11,'2017-03-09 15:55:00','2017-03-09 13:56:21','2017-03-09 13:56:21');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_translations`
--

DROP TABLE IF EXISTS `pages_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`page_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_translations`
--

LOCK TABLES `pages_translations` WRITE;
/*!40000 ALTER TABLE `pages_translations` DISABLE KEYS */;
INSERT INTO `pages_translations` VALUES ('ru',3,'kimnatna-oranjereya','Кімнатна оранжерея','','Квіти доповняють і прикрашають інтер’єр будинку, особливо в поєднанні зі стильними аксесуарами. У статті ми розповімо про сучасні підставки для квітів. Квітів у будинку не буває занадто багато. ','',NULL,'<h1 style=\"text-align: center;\">Кімнатна оранжерея</h1><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 694px;\" src=\"http://kvitochka/photos/shares/58c1612112be2.jpg\" /></p><p>Квіти доповняють і прикрашають інтер&rsquo;єр будинку, особливо в поєднанні зі стильними аксесуарами. У статті ми розповімо про сучасні підставки для квітів. Квітів у будинку не буває занадто багато.</p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 725px;\" src=\"http://kvitochka/photos/shares/58c16196e2c1d.jpg\" /></p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 593px;\" src=\"http://kvitochka/photos/shares/58c161bf8e87c.jpg\" /></p><div class=\"col-lg-12\"><p>Скільки б їх не було, вони лише прикрашають кімнату і радують погляд, до того ж, вони корисні для навколишнього середовища і для всіх, хто поряд з ними проживає. Проблема розташування квіткових горщиків вирішується досить просто &ndash; за допомогою підставок.</p><p>Сьогодні майже у всіх будинках та дачних дворах є красиві і в той же час досить оригінально висаджені квіти. Такі вазонки здатні повністю оживити простір в будинку, можуть зробити повітря набагато чистіше, а також здатні підняти настрій. Для цього відкритого простору або створення композицій з вазонів зазвичай застосовують підставки для рослин. Можна вибрати як підлогові, так і підвісні, виходячи з побажань господарів будинку. Квіти в таких підставках виглядають досить оригінально і в той же час дуже красиво. Вони досить міцні, надійні і володіють елегантним зовнішнім виглядом.</p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 756px;\" src=\"http://kvitochka/photos/shares/58c161e3a1dc5.jpg\" /></p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 842px;\" src=\"http://kvitochka/photos/shares/58c161fb31e59.jpg\" /></p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 843px;\" src=\"http://kvitochka/photos/shares/58c16206dfccd.jpg\" /></p><div class=\"col-lg-12\"><p>Квіти &ndash; це гарно. Навіть невелика домашня оранжерея здатна прикрасити кімнату, перетворивши її в мініатюрний ботанічний сад. У такій квартирі навіть дихається легше. Якщо ви пам&rsquo;ятаєте шкільний курс ботаніки, то всі рослини виробляють кисень, поглинаючи вуглекислий газ, який ми видихаємо. Так що квіти несуть користь для здоров&rsquo;я.</p><p>Але як розмістити всі ці горщики і вазони з фіалками і азаліями, які на підвіконнях вже не поміщаються? Правильно, розставити їх на підлозі. Але зробити це можна дуже креативно, при цьому займаючи мало місця. Використовуйте спеціальні підлогові підставки, які можна вписати в будь-який інтер&rsquo;єр.</p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 625px;\" src=\"http://kvitochka/photos/shares/58c1623cb5a82.jpg\" /></p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 717px;\" src=\"http://kvitochka/photos/shares/58c1625a423f8.jpg\" /></p><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 845px;\" src=\"http://kvitochka/photos/shares/58c162694ddb6.jpg\" /></p><div class=\"col-lg-12\"><div class=\"h4\">На що звернути увагу при покупці підставки:</div><ul><li>По-перше, виріб <strong>повинен вам сподобатися</strong>, навіть на фотографії. Бувають випадки, коли невдалий ракурс зображення налаштовує на скептицизм, а насправді все навпаки. Так от: якщо на фото гарно, то і в житті теж.</li><li>По-друге, поцікавтеся, хто <strong>виробник</strong>. На ринку можна зустріти сотні красивих виробів від невідомих компаній. Судити про якість підставок при цьому вкрай складно. Хто знає, раптом вона зламається, якщо на неї поставити 20-кілограмовий горщик із екзотичною пальмою. Непогано б перевірити цей момент перед покупкою.</li><li><strong>Розміри і габарити</strong> також значно впливають на вибір. Заміряйте заздалегідь площу, на якій ви хочете розмістити підставку з квітами. Додатково виміряйте висоту самої великої квітки. Вона не повинна розташовуватися дуже високо, щоб до неї було легко підійти і полити/удобрити.</li><li><strong>Кількість тарілок і їх діаметр</strong>. Якщо на велику площину поставити маленький горщик &ndash; не проблема, то в зворотній ситуації вже нічого не поробиш. Визначтеся, які саме квіти ви хочете поставити на підставку, заміряючи їх діаметр. На підставі цих нехитрих обчислень і робіть свій вибір.</li><li><strong>Колір, фактура і стиль</strong> &ndash; це вже суто індивідуальна справа смаку. Тут немає якихось побажань, переваг і вказівок.</li></ul><p style=\"text-align: center;\"><img alt=\"\" style=\"width: 1000px; height: 738px;\" src=\"http://kvitochka/photos/shares/58c162a51724a.jpg\" /><img alt=\"\" style=\"width: 1000px; height: 693px;\" src=\"http://kvitochka/photos/shares/58c162b76f977.jpg\" /></p><p>&nbsp;</p></div></div></div>'),('ru',12,'kompoziciya-na-svyatkoviy-stil','Композиція на святковий стіл','','Вважається, що квіти не надто доречні на новорічному столі. Але чи так це? З квітів, хвої, ялинкових прикрас, стрічок, мішури і ялинкових іграшок навіть приклавши мінімальні зусилля можна створити чудові композиції, які зроблять новорічну н','',NULL,'<p>СЩТЕУТЕ</p>'),('ru',13,'navischo-potribna-dostvka-kvitiv-ta-chim-ce-zruchno','Навіщо потрібна доствка квітів? Та чим це зручно?','','Невід\'ємною частиною кожного свята і важливої ​​події є квіти. При цьому букет квітів можна використовувати не тільки в ролі подарунка або знака уваги, але і ефективного методу прикраси заходи. У разі якщо ви хочете здивувати і пора','',NULL,'');
/*!40000 ALTER TABLE `pages_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubrics`
--

DROP TABLE IF EXISTS `rubrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rubrics_parent_id_foreign` (`parent_id`),
  CONSTRAINT `rubrics_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `rubrics` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubrics`
--

LOCK TABLES `rubrics` WRITE;
/*!40000 ALTER TABLE `rubrics` DISABLE KEYS */;
INSERT INTO `rubrics` VALUES (1,NULL,NULL);
/*!40000 ALTER TABLE `rubrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubrics_translations`
--

DROP TABLE IF EXISTS `rubrics_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubrics_translations` (
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rubric_id` int(10) unsigned NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rubric_id`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubrics_translations`
--

LOCK TABLES `rubrics_translations` WRITE;
/*!40000 ALTER TABLE `rubrics_translations` DISABLE KEYS */;
INSERT INTO `rubrics_translations` VALUES ('ru',1,'blog','Блог',NULL,'2017-03-09 11:45:20','2017-03-09 11:45:20');
/*!40000 ALTER TABLE `rubrics_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribers_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin',NULL,NULL,1,'$2y$10$hQ0ZZ4p/kVLJmDim0Tsp/OvOMGJld5eC/6k4kzErrXAMYv21xK0eW',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-10 16:13:34
