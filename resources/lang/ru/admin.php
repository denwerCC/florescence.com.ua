<?php

return [

	'Login' => 'Логин',
	'Forgot Password' => 'Забыл пароль',
	'Enter your username and password' => 'Введите логин и пароль',
	'Enter your valid e-mail' => 'Введите правильный E-mail',
	'Remember Me' => 'Запомнить меня',
	'Username' => 'Логин',
	'Password' => 'Пароль'
];