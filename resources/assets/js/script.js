/**
 * @requires ../../node_modules/jquery/dist/jquery.js
 */
(function ($) {

    $.fn.submitter = function (options) {

        var form = $(this);

        var defaults = {
            form: form,
            send: function () {

                var m_method = this.form.attr('method');
                var m_action = this.form.attr('action');
                var m_data = this.form.serialize();

                $.ajax({
                    type: m_method,
                    url: m_action,
                    data: m_data,
                    success: function (response) {
                        if (options.onSuccess) {
                            options.onSuccess(response);
                        }
                    },
                    error: function (response) {
                        options.onError(response)
                    }
                });
            },
            onSubmit: function () {
                this.send();
            },
            onSuccess: function (response) {
            },
            onError: function (response) {
                alert(response.responseText);
            }
        };

        var options = $.extend({}, defaults, options);

        options.form.submit(function (e) {
            options.onSubmit();
            e.preventDefault();
        });

        return this;

    };

    $.fn.menu = function () {
        // menu
        var menu = $("li", this);
        $("ul:not(:has(.active))", menu).hide();
        $(".active", this).parent('ul').show();

        menu.each(function () {
            if ($(this).children('ul').length > 0) {
                $(this).addClass('hassub');
            }
        });

        menu.click(function () {

            $('ul',menu).not($(this).parent("ul")).slideUp(500);

            if ($("ul", this).css("display") == "none") {
                $("ul", this).slideDown(500);
            }
            if ($(this).children('ul').length > 0) {
                return false;
            }
        });

        $("li a", menu).click(function (event) {
            event.stopPropagation();
        });
    };

}(jQuery));

$(function () {

    SocialShareKit.init();

    cartPopup = function () {
        $.colorbox({html:'<div id="cart-splash"><h1>Покупка успешно добавлена в корзину!</h1><a href="" class="btn close">Продолжить покупки</a><a href="/cart" class="btn">Перейти в корзину</a></div>'});

        $('#colorbox .close').click(function(){
            $('#cboxClose').click();
            return false;
        });
    };

    $('a[href*=".jpg"],a[href*=".jpeg"]').colorbox({'rel':'gall','maxHeight':'95%'});

    $('.add-to-cart-f form').submitter({
        onSuccess: function (data) {
            $('.cart-data').html(data.count + ' покупок - ' + data.total + ' грн');
            cartPopup();
        }
    });

    $('.add-to-cart').click(function () {
       $.get(this.href,function (data) {
           $('.cart-data').html(data.count + ' покупок - ' + data.total + ' грн');
       });

        cartPopup();

        return false;
    });

    $('#sortForm').change(function () {
        $(this).submit();
    });

    $('.menu-catalog').menu();

    $('.callbackform').submitter({
        onError: function (response) {
            $.colorbox({html: '<div class="box">Ошибка! Неверно указаны данные</div>'});
        },
        onSuccess: function (response) {
            $.colorbox({html: '<div class="box">' + response.message + '</div>'});
        }
    });

    $('.callback, .oneclick').colorbox({inline: true}).click(function () {
        $('#callback input[name=type]').val($(this).attr('id'))
    });
    $('.popup-iframe').colorbox({iframe: true, width: '90%', height: '100%'});

    // TO TOP
    $("<a class='arrows' id='up' href=''>▲</a>").insertAfter('footer');
    var up = $("#up");
    var down = $("#down");
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 400) {
            up.fadeIn('slow');
        }
        else {
            up.fadeOut('slow');
        };
    });
    up.click(function(a) {
        a.preventDefault();
        $('html, body').animate( {
            scrollTop: 0
        }, 'slow');
    })
});