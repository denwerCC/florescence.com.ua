@extends('layouts.app')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('content')

    <div id="container">
        <div class="container">
            <div>
                <h1>{{l('home:h1')}}</h1>
            </div>
            <div class="row">
                @foreach($categories as  $n => $category)
                    @if($n == 2)
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 col-xs-offset-1 main_page_item_holder photo_flower_block">
                            <div class="layer-main"></div>
                            <div class="photo_flower_block_inner">
                                <div class="text-center" style="margin-bottom: 15px;">{{l('Завантажте фото сюди')}}</div>
                                <span><i class="fa fa-arrow-circle-down fa-3x download-i" aria-hidden="true"></i></span>
                                <div class="text-center" style="margin-top: 15px;">{{l('І ми зробимо такий букет')}}</div>
                            </div>
                            <div class="h3 col-lg-6">{{l('Букет по фото')}}</div>
                            {{--<img src="{{asset('/img/main-page-photo6.png')}}" alt="" id="main-page-img">--}}
                            {{--<img src="{{asset('/img/main-page-subphoto.png')}}" alt="" id="main-page-img-sub">--}}
                            <img src="/img/main-page-photo6.png" alt="" id="main-page-img">
                            <img src="/img/main-page-subphoto.png" alt="" id="main-page-img-sub">
                        </div>
                    @endif
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 col-xs-offset-1 main_page_item_holder">
                        <a href="{{route('catalog.category',$category->slug)}}">
                            <div class="h3 col-lg-6">{{$category->title}}</div>
                            <img src="{{$category->image(350,350)}}" alt="" id="main-page-img">
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="content page-content">
                {!! $page->content !!}
            </div>

        </div>
    </div>

@endsection
@push('scripts')
<script>
    $(document).on("click", ".download-icon", function () {
        $('.upload-button').click();
        $(this).removeClass('download-icon')
            .addClass('submit-button');
        $('.photo_flower_block').addClass('hover');

        });
        $(document).on("click", ".submit-button", function () {
            $('.input-sumbit').click();
            $(this).removeClass('sumbit-button')
                .addClass('download-button');
            $('.photo_flower_block').removeClass('hover');
        });
</script>
@endpush