<form id='contacts' action="{{route('send')}}" method="post" class="contactform">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <h2>Отправить сообщение</h2>

    <div class="form-groups">
        <div class="form-group form-group-name">
            <label class="up-top" for="">Имя</label>
            <input type="text" name="name" placeholder="" required>
        </div>
        <div class="form-group form-group-email">
            <label class="up-top" for="">Ваш e-mail</label>
            <input type="email" name="email" placeholder="" required>
        </div>
        <div class="form-group form-group-phone">
            <label class="up-top" for="">Номер телефона</label>
            <input type="text" name="phone" placeholder="" required>
        </div>
    </div>
    <div class="form-group form-group-message">
        <textarea name="message" placeholder="" required></textarea> <label class="up-top" for=""></label>
    </div>

    <button class="btn" type="submit" name="button">{{l('Надіслати')}}</button>
</form>
