@extends('layouts.app')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('content')

    <div id="subheader">
        <div class="layer"></div>
        <video poster="" id="bgvid" playsinline autoplay muted loop>
            <source src="{{asset('video/' . $video[0])}}" type="video/webm">
            <source src="{{asset('video/'. $video[1])}}" type="video/mp4">
        </video>
        <div class="col-lg-12 subheader-top text-center">
            <a href="/"><img src="{{asset('img/LOGO.png')}}" alt="logo"></a>
            <span>{{l('Запрошує на майстер класи')}}</span>
            <span class="second">{{l('різні тематики')}}</span>
        </div>
        <div class="h1 text-center subheader-middle"><p class="fldata">{{l('Найближчий майстер клас')}} <span>15</span></p><!-- end --></div>
        <div class="h1 text-center subheader-middle second"><p class="fldata">{{l('тема')}} <span>1111a</span></p><!-- end --></div>
        <div class="h4 text-center subheader-bottom fldata">{{l('Львів, вул. Г. УПА, 15')}}</div><!-- end -->
        <a href="{{route('callback.ms')}}" id="callback-btn-master-show"><button type="button" class="btn btn-default">{{l('записатись')}}</button></a>
    </div>

    <div id="container">
        <div class="container master-show">
           <div class="h2 text-center fldata">{{l('Теми найближчих майстер класів')}}</div><!-- end -->
            @foreach($rubric->pages as $num => $page)
                @if ($num % 2 == 0)
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 block-odd">
                <div class="col-lg-4" style="text-align: right;">
                    <img src="{{$page->image(250,250)}}" alt="">
                </div>
                <div class="col-lg-8 description" style="padding-top: 0;">
                    <h4>{{$page->title}}</h4>
                    <p style="font-size: 18px;">{{$page->description}}</p>
                    <span class="master_price">{!!$page->content!!}</span>
                    <a href="{{route('callback.ms')}}" class="callback-btn-master-show1"><button type="button" class="btn ms-button">записатись</button></a>
                </div>
            </div>
                @else
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 block-even">
                        <div class="col-lg-4 pull-right">
                            <img src="{{$page->image(376,250)}}" alt="">
                        </div>
                        <div class="col-lg-8 description" style="padding-top: 0;">
                            <h4>{{$page->title}}</h4>
                            <p style="font-size: 18px;">{{$page->description}}</p>
                            <span class="master_price">{!!$page->content!!}</span>
                            <a href="{{route('callback.ms')}}" class="callback-btn-master-show1"><button type="button" class="btn ms-button">{{l('записатись')}}</button></a>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 no-padding additional-description">
                <p style="font-size: 16px;" class="fldata">{{l('У вартість всіх майстер класів входять усі матеріали, і звичайно ж свої роботи Ви зможете забрати із собою. Невеликі групи, приємна атмосфера. Майстер класи проходять регулярно. Якщо Ви хочете зробити подарунок близькій людині у нас є ПОДАРУНКОВІ СЕТРИФІКАТИ на суму 300, 400, 500 грн або без номіналу')}}.</p><!-- end --></div>
            <div class="row sertificate-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-3 col-md-offset-1 col-sm-4 col-xs-6 sertificate-block-single">
                    <img src="{{asset('/img/sertificate/300.png')}}" alt="">
                    <p>{{l('Подарунковий сертифікат на суму')}} <span>300 грн.</span></p>
                    <button type="button" id="callback-btn-certificate" data-price="300" class="btn sertificate-btn">{{l('придбати')}}</button>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 sertificate-block-single">
                    <img src="{{asset('/img/sertificate/400.png')}}" alt="">
                    <p>{{l('Подарунковий сертифікат на суму ')}}<span>400 грн.</span></p>
                    <button type="button" id="callback-btn-certificate2" class="btn sertificate-btn">{{l('придбати')}}</button>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 sertificate-block-single">
                    <img src="{{asset('/img/sertificate/500.png')}}" alt="">
                    <p>{{l('Подарунковий сертифікат на суму ')}}<span>500 грн.</span></p>
                    <button type="button" id="callback-btn-certificate3" class="btn sertificate-btn">{{l('придбати')}}</button>
                </div>
            </div>

            <div class="h3 text-center col-lg-12" style="margin-top: 25px;">{{l('Наші роботи')}}:</div>
            <div class="our-works-photos-holder col-lg-12">
                <div><img src="{{asset('/img/master-show-works/1.png')}}" alt=""></div>
                <div><img src="{{asset('/img/master-show-works/2.png')}}" alt=""></div>
                <div><img src="{{asset('/img/master-show-works/3.png')}}" alt=""></div>
                <div><img src="{{asset('/img/master-show-works/4.png')}}" alt=""></div>
                <div><img src="{{asset('/img/master-show-works/5.png')}}" alt=""></div>
                <div><img src="{{asset('/img/master-show-works/6.png')}}" alt=""></div>
                <div><img src="{{asset('/img/master-show-works/7.png')}}" alt=""></div>
                <div><img src="{{asset('/img/master-show-works/8.png')}}" alt=""></div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')

    {{--<script src="{{asset('/js/jquery.shapeshift.min.js')}}"></script>--}}
<script>
    (function ($) {
        $(document).on('ready', function () {
            // $('.our-works-photos-holder').shapeshift({
            //     // enableDrag:false,
            // });
        });
    })(jQuery);
</script>
@endpush
