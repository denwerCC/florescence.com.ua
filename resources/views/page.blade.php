@extends('layouts.florescence')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('page_class',  'page')
@section('content')

    <div id="container">
        <div class="container">

            <div id="breadcrumb">
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                </div>

                @if($page->rubric)
                    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="{{route('page',$page->rubric->slug)}}" itemprop="url">
                            <span itemprop="title">{{$page->rubric->title}}</span> </a> »
                    </div>
                @endif

                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('page',$page->slug)}}" itemprop="url">
                        <span itemprop="title">{{$page->title}}</span> </a>
                </div>

            </div>

            <div class="content blog-page-holder">
                {!! $page->content !!}
            </div>

            @if(count($page->images) > 0)
            <div id="page_thumbs">
                <hr>
                    @foreach($page->images->splice(1) as $image)
                            <a class="gallery" href="{{$page->image(0,0,$image)}}">
                                {{--<img itemprop='image' src="{{$item->image(100,100,$item->dir . "big/" . $image)}}" alt="{{$item->title}}" title="{{$item->category->title}} - {{$item->title}}"></a>--}}
                                <img itemprop='image' src="{{$page->image(100,100,$image)}}" alt="{{$page->title}}" title="{{$page->title}}"></a>
                    @endforeach
            </div>
            @endif

            @if($page->rubric)
                <div class="text-right">
                    <a href="{{route('page',$page->rubric->slug)}}" class="more_news">
                        <<< {{l('Повернутися назад')}}
                    </a>
                </div>
            @endif

        </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(function () {
            $('.gallery').colorbox({rel: 'gall', maxHeight: '95%', maxWidth: '95%'});

            // $('#page_thumbs').slick({
            //     dots: false,
            //     slidesToShow: 8,
            //     slidesToScroll: 8,
            //     speed: 500
            // });

        })

    </script>
@endpush