<style>
    form label[for='name']{
        display: block;
        float: left;
        width: 20%;
        padding: 5px;
        font-family: "Century Gothic";
        margin:5px 0;
    }
    form input[type='text']{
        display: block;
        width: 95%;
        margin:0 auto;
        border: 1px solid gainsboro!important;
        border-radius: 0;
    }
    form label[for='phone']{
        display: block;
        width: 20%;
        padding: 5px;
        font-family: "Century Gothic";
        height: 20px;
        margin:5px 0;
        border-radius: 0;
    }
    form input[type='phone']{
        display: block;
        width: 70%;
        float: left;
        margin:5px 0;
        border-radius: 0;
    }
    form label[for='email']{
        display: block;
        float: left;
        width: 20%;
        padding: 5px;
        font-family: "Century Gothic";
        margin:5px 0;
        border-radius: 0;
    }
    form input[type='email']{
        display: block;
        width: 95%;
        margin:0 auto;
        margin-bottom: 12px;
        border: 1px solid gainsboro!important;
        border-radius: 0;
    }
     form button{
        display: block;
        width: 150px;
        margin: 0 auto;
        border-radius: 0px;
        color: white;
        font-size: 15px;
    }
    form div.h3{
        color: gray;
        margin-bottom: 15px;
        padding: 15px;
        border-bottom: 1px solid gainsboro;
    }
    form button[type='submit']{
        width: 60%;
        display: block;
        margin: 0 auto;
        border: 1px solid!important;
        border-radius: 0;
    }
</style>
<form action="{{route('certificate.store',[$price_id])}}" method="post" id="certificate-form">
    {{csrf_field()}}
    <div class="h3 text-center">{{l('Подарунковий сертифікат')}}</div>
    <div class="form-group" >
        <input type="text" class="form-control" name="name" placeholder="{{l('Ваше ім’я')}}">
    </div>
    <div class="form-group" >
        <input type="text" id="int" class="form-control phone-input" name="phone" placeholder="{{l('Ваш контактний номер')}}">
    </div>
    <div class="form-group" >
        <input type="email" class="form-control" name="email" placeholder="{{l('Ваш email')}}">
    </div>
    <button type="submit" class="form-control">{{l('Придбати')}}</button>
</form>