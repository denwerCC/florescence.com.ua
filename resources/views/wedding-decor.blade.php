@extends('layouts.app')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('content')
    <div id="container">
        <div class="container">
            <div class="wedding_item_holder">
                <div class="description_block row">
                    <div class="outer_block">
                        <div class="col-lg-10 col-lg-offset-1 item_title">
                            <span>{{l('Весільні букети')}}</span>
                        </div>
                        <div class="col-lg-10 col-lg-offset-1 item_description">
                            <p>{{l('Букет нареченої є важивою складовою образу нареченої. Квіти повинні гармонійно поєднуватися з сукнею, а також відповідати характеру нареченої. Ніжний або яскравий, мініатюрний або асиметричні - в будь-якому випадку букет Lacy Bird буде гармонійним, красивим і стильним. Флопристика - мистецтво, доповнене емоціями і уявою')}}.</p>
                        </div>
                    </div>
                </div>
                <div class="wedding_slider">
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/1.jpg')}}"><img src="{{asset('/img/wedding-flowers/1.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/2.jpg')}}"><img src="{{asset('/img/wedding-flowers/2.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/3.jpg')}}"><img src="{{asset('/img/wedding-flowers/3.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/4.jpg')}}"><img src="{{asset('/img/wedding-flowers/4.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/5.jpg')}}"><img src="{{asset('/img/wedding-flowers/5.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/6.jpg')}}"><img src="{{asset('/img/wedding-flowers/6.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/7.jpg')}}"><img src="{{asset('/img/wedding-flowers/7.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/8.jpg')}}"><img src="{{asset('/img/wedding-flowers/8.jpg')}}" alt=""></a></div>
                    <div><a class="wedding_slide" href="{{asset('/img/wedding-flowers/9.jpg')}}"><img src="{{asset('/img/wedding-flowers/9.jpg')}}" alt=""></a></div>
                </div>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    @endsection

@push('scripts')
@endpush