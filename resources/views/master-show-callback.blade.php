<style>
    form div.form-group input{
        border-radius: 0;
    }
    form button[type='submit']{
        border-radius: 0;
    }
</style>

<form action="{{route('msubs.store')}}" method="post" id="ms-callback" class="callbackform">
    <div class="form-group input-wrap">
        {{--<label for="name" class="up-top">Ім'я</label>--}}
        <input type="text" id="name" name="name" class="form-control"  placeholder="{{l('Ім’я')}}">
    </div>
    <div class="form-group input-wrap">
        {{--<label for="surname" class="up-top">Прізвище</label>--}}
        <input type="text" id="surname" class="form-control" name="surname" placeholder="{{l('Прізвище')}}">
    </div>
    <div class="form-group input-wrap">
        {{--<label for="age" class="up-top">Вік</label>--}}
        {{--<input type="text" id="age" class="form-control" name="age" placeholder="Вік">--}}
        <i class="fa fa-ellipsis-v" style="z-index: 99;position: absolute; left: 84.2%;font-size: 26px;color: rgb(57,49,133);top: 48.4%;"></i>
        {{--<label for="age">yo age</label>--}}
        <select name="age" id="age" class="form-control">
            <option value="disabled" disabled selected>{{l('Вік')}}</option>
            <option value="18">{{l('До')}} 18</option>
            <option value="25">{{l('До')}} 25</option>
            <option value="35">{{l('До')}} 35</option>
            <option value="45">{{l('До')}} 45</option>
            <option value="55">{{l('До')}} 55</option>
        </select>
    </div>
    <div class="form-group input-wrap">
        {{--<label for="from" class="up-top">Звідки дізнались</label>--}}
        {{--<input type="text" id="from" class="form-control" name="from" placeholder="Звідки дізнались">--}}
        <i class="fa fa-ellipsis-v" style="z-index: 99;    position: absolute;
    left: 84.2%;
    font-size: 26px;
    color: rgb(57,49,133);
    top: 55.4%;"></i>
        <select name="from" id="from" class="form-control">
            <option value="" disabled selected>{{l('Звідки дізнались')}}</option>
            <option value="соц.мережі">{{l('Соц. мережі')}}</option>
            <option value="від знайомих">{{l('Від знайомих')}}</option>
            <option value="google">Google</option>
            <option value="інше">{{l('інше')}}</option>
        </select>
    </div>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn ms-callback-btn">{{l('Надіслати')}}</button>
</form>