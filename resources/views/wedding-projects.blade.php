@extends('layouts.florescence')
@section('title',  $rubric->meta_title)
@section('description', $rubric->meta_description)
@section('content')

    <div id="projects">

        <div class="container">
            <div id="breadcrumb">
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                </div>

                @if($rubric)
                    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="{{route('page',$rubric->slug)}}" itemprop="url">
                            <span itemprop="title">{{$rubric->title}}</span> </a>
                    </div>
                @endif

            </div>
            @if (isset($rubric->h1) && !empty($rubric->h1))
                <h1>{{$rubric->h1}}</h1>
            @endif
            <div class="grid">
                @foreach($rubric->pages as $page)
                    <div class="grid-item">
                        <div class="gitem text-center">
                            @if (!empty($page->image))
                                <div class="img">
                                    <a href="{{route('page',$page->slug)}}">
                                        <img src="{{$page->image}}" alt="">
                                    </a>
                                </div>
                            @endif
                            <div class="header">{{$page->title}}</div>
                            <div class="description">
                                {!! getWordsFromText($page->description, 30) !!}
                            </div>
                            <div class="more_btn">
                                <a href="{{route('page',$page->slug)}}" class="vdz_btn vdz_btn_dark">
                                    <span>{{l('Детальніше')}}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="content">
                <br>
                <hr>
                <br>
                {!! $rubric->content !!}
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script src="/vendor/masonry.pkgd.min.js"></script>
    <script>
        (function ($) {
            $(document).ready(function () {
                console.log('projects');
                $('.grid').masonry();
            });
        })(jQuery);
    </script>

@endsection
