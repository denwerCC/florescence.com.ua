@extends('layouts.app')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)

@section('content')
    <style>
        #map {
            height: 500px;
            width: 100%;
            margin-top: 30px;
        }
        #container .col-lg-6 p{
            padding: 4px;
        }
    </style>
    <div id="container">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="map"></div>
                    <script>
                        function initMap() {
                            var uluru2 = {lat: 49.80209547, lng:24.02962491};
                            var uluru3 = {lat: 49.8079759, lng:24.0177906};
                            var map = new google.maps.Map(document.getElementById('map'), {
                                zoom:10,
                                center: uluru2
                            });
                            var marker = new google.maps.Marker({
                                position: uluru2,
                                map: map
                            });
                            var marker2 = new google.maps.Marker({
                                position: uluru3,
                                map: map
                            });
                        }
                    </script>
                </div>
                <div class="col-lg-6 contacts-description">
                    <div class="h3 text-center subheader">
                        KVITOCHKAgroup
                        <span class="subheader-text">{{l('магазин флористики та декору')}}</span>
                    </div>
                    <div class="text">
                        <p>{{l(' Наша адреса')}}:<br/>
                            {{l('м. Львів, вул. Героїв УПА 15')}}</p><br/>
                        <p>{{l('Магазин працює')}}:<br/>
                            {{l('Пн.-Пт.')}}: 10.00 - 20.00<br/>
                            {{l('Сб.-Нд.')}}: 10.00 - 17.00
                        </p>
                        <p>{{l('Телефони для замовлення букетів та композицій, а також для запису на майстер класи чи для придбання подарункових сертифікатів')}}:<br/>
                            <br/>
                            {!! get_config('phone') !!}
                            <br/>
                            <span class="bold" style="font-size: 18px;">{{l('Дзвінки приймаємо цілодобово')}}.</span></p>
                        <p>e-mail: {{get_config('email')}}</p>

                        <p style="font-size: 17px;">{{l('Зв’язатись з нами Ви можете також через соц. мережі')}}:
                            <a href="https://www.facebook.com/flowers.delivery/" class="ssk ssk-facebook ssk-xs"></a>
                            <a href="https://www.instagram.com/kvitochka.lviv/" class="ssk ssk-instagram ssk-xs"></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe6G4AeGUZ9rwrf8RkWi9hJ-ICBN4GZk8&callback=initMap">
</script>
@endsection

