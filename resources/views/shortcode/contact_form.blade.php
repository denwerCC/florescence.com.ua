<form action="{{route('send')}}" method="post" class="shortcode_form">
    {{csrf_field()}}
    <div class="form-group">
        <input type="text" placeholder="{{l('Ваше ім’я')}}" name="name" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="text" name="phone" placeholder="{{l('Телефон')}}" class="form-control shortcode_form_phone" required>
    </div>
    <div class="form-group">
        <textarea type="text" name="message" class="form-control" placeholder="{{l('Ваш коментар')}}" ></textarea>
    </div>
    <input type="hidden" name="type" value="shortcodeForm">
    <input type="hidden" name="send_from" value="{{url()->current()}}">
    <button type="submit" class="form-control">{{l('Відправити')}}</button>
</form>
<!--[[shortcode_form]]END-->