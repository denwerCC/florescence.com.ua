@extends('account.index')
@section('account.content')
    {!! Form::open(array('route' => ['user.update',Auth::user()->id],'method'=>'put','id'=>'checkout-form')) !!}
    <div class="form-group">
        {{Form::label("name",'Имя Фамилия', ['class'=>'control-label'])}}
        {{Form::text("name",Auth::user()['name'],['required'=>true, 'class'=>'form-control'])}}
    </div>
    <div class="form-group col-xs-6 first">
        {{Form::label("phone",'Телефон', ['class'=>'control-label'])}}
        {{Form::text("phone",Auth::user()['phone'],['required'=>true, 'class'=>'form-control'])}}
    </div>
    <div class="form-group col-xs-6 second">
        {{Form::label("email",'Email', ['class'=>'control-label'])}}
        {{Form::email("email",Auth::user()['email'],['required'=>true, 'class'=>'form-control'])}}
    </div>

    <div class="form-group">
        {{Form::label("address",'Адрес доставки', ['class'=>'control-label'])}}
        {{Form::textarea("address",Auth::user()['address'],['placeholder'=>'Город, адрес или номер отделения','required'=>true, 'class'=>'form-control'])}}
    </div>

    <p>
        При смене E-mail на новый - используйте его для авторизации на сайте
    </p>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">
            Сохранить
        </button>
    </div>
    {{Form::close()}}
@endsection