@extends('layouts.app')
@section('title',  isset($title) ? $title : '')
@section('description', isset($description) ? $description : '')
@section('content')
    <h1 class="text-center">{{$title}}</h1>
    <div id="account">
        <div class="col-lg-2 col-sm-3">
            <div class="block form1">
                <ul class="menu">
                    <li><a href="/account">Мои покупки</a></li>
                    <li><a href="/account/wishlist">Избранное</a></li>
                    <li><a href="/account/profile">Профиль</a></li>
                    <li><a href="/logout">Выход</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-10 col-sm-9">
            <div class="block form2">
                @yield('account.content')
            </div>
        </div>
    </div>
@endsection