@extends('account.index')
@section('account.content')
    <div class="box-body">
        <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
            <thead>
            <tr>
                <th class="hidden-xs">ID</th>
                <th>Img</th>
                <th class="hidden-xs">Артикул</th>
                <th>Название</th>
                <th>Параметры</th>
                <th>Стоимость</th>
                <th>Количество</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>

            @foreach($order->items as $item)

                <tr>
                    <td class="sort text-center hidden-xs">{{$item->id}}</td>
                    <td><img class="img-order" src="{{$item->attr->image(225,340)}}" alt="{{$item->title}}"></td>
                    <td class="hidden-xs">{{$item->sku}}</td>
                    <td><a href="{{$item->attr->link}}" target="blank">{{$item->title}}</a></td>
                    <td class="text-center">
                        <ul>
                            @foreach($item->parameters as $type => $parameters)
                                <li><b>{{$type}}</b> : {{$parameters}}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td class="text-center">{{$item->price}}</td>
                    <td class="text-center">{{$item->count}}</td>
                    <td class="text-center">{{$item->count * $item->price}}</td>
                </tr>

            @endforeach

            <tr>
                <td colspan="7"><h3>Всего:</h3></td>
                <td><h3 class="text-center">{{$order->amount}}</h3></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection