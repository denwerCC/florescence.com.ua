@extends('account.index')
@section('account.content')
    @foreach($items as $item)

        <div class="row wishlist">
            <div class="col-sm-1 col-xs-4">
                <a href="{{$item->link}}">
                    <div class="item-pic">
                        <img src="{{$item->image(225,340)}}" class="img-responsive" alt="{{$item->title}}"/>
                    </div>
                </a>
            </div>
            <div class="col-sm-9 col-xs-6">
                <div class="item">
                    <div class="title">{{$item->title}}</div>
                    <ul class="params">
                        @if($item->sku)
                            <li><span><span>{{l('Артикул')}}</span></span>
                                <div>{{$item->sku}}</div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 col-xs-2 item-total">
                {{$item->price}} грн
            </div>
        </div>

    @endforeach



@endsection