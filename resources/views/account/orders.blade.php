@extends('account.index')
@section('account.content')

    <table>
        <thead>
        <tr>
            <th>№ заказа</th>
            <th>Дата</th>
            <th>Сумма</th>
            <th class="hidden-xs">Статус</th>
            <th>Просмотр</th>
        </tr>
        </thead>
       <tbody>

       @foreach($orders as $order)
       <tr>
           <td>{{$order->id}}</td>
           <td>{{$order->created_at}}</td>
           <td>{{$order->amount}}</td>
           <td class="hidden-xs">{{$order->statusname}}</td>
           <td><a class="btn" href="{{route('account.order',$order->id)}}">Смотреть</a></td>
       </tr>
       @endforeach
       </tbody>

    </table>


@endsection