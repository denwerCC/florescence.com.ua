@extends('layouts.florescence')
@section('title', l(':blog:title'))
@section('description', l(':blog:description'))
@section('page_class',  'blog')
@section('content')
    <section id="blog">
        <div class="blog-content container">

            <div id="breadcrumb">
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                </div>

                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('page',$rubric->slug)}}" itemprop="url"> <span
                                itemprop="title">{{$rubric->title}}</span> </a>
                </div>

            </div>
            <h2>{{l('Блог')}}</h2>
            <div id="articles">
                @if(isset($articles))
                    @foreach($articles as $article)
                        @if($article->public == 1)
                            <div class="blog_item">
                                <div class="row no-gutters">

                                    <div class="col-md-6 col-sm-12 item_img">
                                        <a href="{{route('page',$article->slug)}}"><img class="blog_img"
                                                                                        src="{{$article->image}}"
                                                                                        alt="{{$article->title}}"/></a>
                                    </div>
                                    <div class="col-md-6 col-sm-12 ">
                                        <div class="item_text">

                                            <h3>{{$article->title}}</h3>
                                            <span class="blog-item-time time">{{dateFormat($article->date)}}</span>
                                            <p>
                                                {{getWordsFromText($article->description, 28)}} <a
                                                        href="{{route('page',$article->slug)}}">>>></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="row">
                @if($paginate)
                    {!! $paginate->links() !!}
                @endif
            </div>
        </div>
    </section>


@endsection