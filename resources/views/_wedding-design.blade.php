@extends('layouts.florescence')
@section('title',  $rubric->meta_title)
@section('description', $rubric->meta_description)
@section('style')
@endsection
@section('content')
@if(isset($rubric->video_bg) && !empty($rubric->video_bg))
    <div id="video_bg">
        <video src="/{!! $rubric->video_bg !!}"></video>
    </div>
@endif
    <div id="wedding_design">
        <div class="container">
            <div id="breadcrumb">
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                </div>

                @if($rubric)
                    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="{{route('page',$rubric->slug)}}" itemprop="url">
                            <span itemprop="title">{{$rubric->title}}</span> </a>
                    </div>
                @endif

            </div>
            @if (isset($rubric->h1) && !empty($rubric->h1))
                <h1>{{$rubric->h1}}</h1>
            @endif
            <div class="wedding_item_holder">
                @foreach($rubric->pages as $page)
                    <div class="description_block row">
                        <img src="{{$page->image}}" alt="" class="wd-bg-img">
                        <div class="outer_block">
                            <div class="col-lg-10 col-lg-offset-1 item_title">
                                <h2>{{$page->title}}</h2>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item_description">
                                <p>{!! $page->description !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="wedding_slider">
                        @foreach($page->images as $image)
                            <div style="width: 160px!important;">
                                <a class="wedding_slide" href="{{$image}}">
                                    <img src="{{$page->image(330,330,$image)}}" alt=""> </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="description_short">
                        {{--                        {!! $page->content !!}--}}
                    </div>
                    <div class="more_btn text-right">
                        <a href="{{route('page',$page->slug)}}">
                            <span>{{l('Детальніше')}}</span>
                        </a>
                    </div>
                    <div class="divider"><img src="{{asset('/img/divider.png')}}" alt=""></div>
                @endforeach
            </div>
            <div class="content">
                <hr>
                {!! $rubric->content !!}
            </div>
        </div>
    </div>
@endsection