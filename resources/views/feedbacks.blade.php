@extends('layouts.app')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('content')

    <div id="comments" class="container content">

        <h1>Відгуки</h1>

        <a class="btn" href="#create-feedback">Створити відгук</a>

        <h2>Відгуки наших клієнтів</h2>

        <div class="col-sm-12">

            @foreach(\App\Models\Comment::whereCommentableType('menu')->whereCommentableId(1)->wherePublic(1)->with('answer')->latest()->get() as $comment)
                <div class="commbox">
                    <div class="user">
                        <div class="col-sm-3 col-xs-12 lcol text-center">
                            <img src="{{$comment->img}}" alt="{{$comment->name}}">

                            <span class="name">{{$comment->name}}</span>
                        </div>

                        <div class="col-sm-9 col-xs-12 rcol">
                            <div class="combody">
                                {{$comment->text}}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    @if($comment->answer)
                        <div class="admin">
                            <div class="feedautor col-sm-3"><span class="name">Адміністратор</span></div>
                            <div class="feedansw col-sm-9">
                                <div class="combody">{{$comment->answer->text}}</div>
                            </div>
                            <i class="fa fa-caret-down"></i>
                        </div>
                    @endif
                </div>
            @endforeach
        </div>

<div class="clearfix"></div>

        <h2 id="create-feedback">Створити відгук</h2>
        <div id="id"></div>
        <div class="col-sm-6 offset-sm-3">

            {!! Form::open(array('route' => ['comments.store','menu',1], 'files' => true, 'method' => 'post','class'=>'form-horizontal')) !!}

            <div class="form-group">

                <div class="col-sm-12">
                    <input name="name" placeholder="{{l('Ім’я')}}" class="form-control" type="text">
                </div>
            </div>

            <div class="form-group">
                <label for="" class="col-sm-1 control-label">
                    Фото
                </label>
                <div class="col-sm-9">
                    <input name="img" type="file">
                </div>

            </div>
            <div class="form-group">
                <div class="col-sm-12">
                        <textarea name="text" cols="30" rows="5" placeholder="Відгук ..."
                                  class="form-control">
                        </textarea>
                </div>
            </div>
            <button type="submit" class="btn pink mx-auto ">Надіслати</button>
            {!! Form::close() !!}
        </div>

        <div class="clearfix"></div>


    </div>

@endsection