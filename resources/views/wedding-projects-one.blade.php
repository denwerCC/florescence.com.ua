@extends('layouts.florescence')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('content')

    <div id="projects_item">
        <div class="container">
        <div id="breadcrumb">
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
            </div>

            @if($rubric)
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('page',$rubric->slug)}}" itemprop="url">
                        <span itemprop="title">{{$rubric->title}}</span> </a> »
                </div>
            @endif

            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="{{route('page',$page->slug)}}" itemprop="url">
                    <span itemprop="title">{{$page->title}}</span> </a>
            </div>

        </div>
        <div id="project_one_content" class=" row">
            <div class="col-sm-12">
                <h1>{!! $page->title !!}</h1>
                {!! $page->content !!}
            </div>



            @if (count($page->images) > 0)
                <div class="col-sm-12">
                    <h3>
                        {!! trans('new_app.Фотогалерея') !!}
                    </h3>
                    <div id="project_one_gallery" class="row">
                        @foreach($page->images as $image)
                            <a class="gallery_item col-12 col-sm-6 col-md-6 col-lg-4" href="{{$image}}">
                                <span>
                                    <img src="{{$image}}" alt="">
                                </span>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif

        </div>
        <div class="description_short">
        </div>
    </div>
    </div>
@endsection
@section('head_styles')
    <link rel="stylesheet" href="/vendor/lightgallery/css/lightgallery.min.css">
    <link rel="stylesheet" href="/vendor/lightgallery/css/lg-transitions.min.css">
@endsection
@section('footer_scripts')
<script src="/vendor/lightgallery/js/lightgallery-all.min.js"></script>

<script>
    (function ($) {
        $(document).ready(function () {
            console.log('projectItem');
            $('#project_one_gallery').lightGallery({
                pager: true,
                selector: '.gallery_item',
                download: false
            });
        });
    })(jQuery);
</script>
@endsection