@extends('layouts.florescence')
@section('title',  isset($title) ? $title : l('Корзина'))
@section('description', isset($description) ? $description : '')
@section('content')

    <div class="header_bg_line"></div>
    <div id="cart">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <div id="breadcrumb">

                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('home')}}" itemprop="url">
                                <span itemprop="title">{{l('Головна')}}</span>
                            </a> »
                        </div>
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('cart')}}" itemprop="url">
                                <span itemprop="title">{{trans('new_app.Кошик')}}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1>{{trans('new_app.Кошик замовлення')}}</h1>
                </div>
                <div class="col-sm-12">
                    {{--{!! dd($cart) !!}--}}
                    @if($count_items>=1)
                        <form action="{!! route('cart.update') !!}" method="post">
                            {!! csrf_field() !!}
                            <table id="cart_table">
                                <thead>
                                <tr>
                                    <th class="photo">{{trans('new_app.Фото')}}</th>
                                    <th class="name">{{trans('new_app.Назва')}}</th>
                                    <th class="quantity">{{trans('new_app.Кількість')}}</th>
                                    <th class="price">{{trans('new_app.Ціна')}}</th>
                                    <th class="remove"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cart as $cart_item)
                                    <tr>
                                        <td class="photo">
                                            <a href="{{$cart_item->model->link}}">
                                                <img src="{{$cart_item->model->image(0,315)}}"
                                                     alt="{{$cart_item->model->title}}"
                                                     title="{{$cart_item->model->category->title}} - {{$cart_item->model->title}}">
                                            </a>
                                        </td>
                                        <td class="name">
                                            <a href="{{$cart_item->model->link}}">
                                            {{$cart_item->model->title}}
                                            </a>
                                        </td>
                                        <td class="quantity">
                                            <div class="input_count_wrap input-group">
                                                <span class="count_down">-</span>
                                                <input name="product_id[{!! $cart_item->model->id !!}]" type="text" value="{!! $cart_item->qty !!}" min="1" class="product_count" pattern="[0-9]+">
                                                <span class="count_add">+</span>
                                            </div>

                                        </td>
                                        <td class="price">
                                            {{--{!! $cart_item->price !!}--}}
                                            {{$cart_item->model->priceFormatted}} {!!$cart_item->model->currency !!}</td>
                                        <td class="remove">
                                            <a href="{!! route('cart.delete', $cart_item->model->id) !!}">×</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5" class="text-right">
                                        <button id="cart_recalc" type="submit">{{trans('new_app.Перерахувати')}}</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right total">
                                        {{trans('new_app.Всього')}}: {!! $total !!} {!!$cart_item->model->currency !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="text-right btns">
                                        <a href="{!! route('catalog') !!}" class="vdz_btn">{{trans('new_app.Повернутися в каталог')}}</a><a id="cart_checkout" href="{!! route('order') !!}"  class="vdz_btn">{{trans('new_app.Оформити замовлення')}}</a>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>

                    @else
                        <h3>
                            {{trans('new_app.Ваш кошик пустий')}}
                        </h3>
                        <a href="{!! route('catalog') !!}">{{trans('new_app.Повернутися в каталог')}}</a>

                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_scripts')
    <script>
        (function ($) {
            $(document).ready(function () {
                console.log('cart');
                $('#cart .count_add, #cart .count_down').on('click', function(){
                    var $input = $(this).siblings('input');
                    var input_val = Number($input.val());

                    if($(this).hasClass('count_add')){
                        $input.val(input_val + 1);
                    }else{
                        //Не менее единицы
                        if(input_val == 1) return;

                        $input.val(input_val - 1);
                    }
                });
            });
        })(jQuery);
    </script>
@endsection