@extends('layouts.florescence')
@section('title',  $rubric->meta_title)

@section('description', $rubric->meta_description)
@section('style')
@endsection
@section('content')
    @if(isset($rubric->video_bg) && !empty($rubric->video_bg))
        <div id="video_bg">
            <i id="video_p" class="fas fa-play"></i>
            <i id="video_s" class="fas fa-volume-off"></i>

            {{--<video src="/{{ $rubric->video_bg }}" poster="/img/weddings_bg.png" loop autoplay>--}}
            <video src="/{{ $rubric->video_bg }}" poster="/img/weddings_bg.png" loop muted>
                Sorry, your browser doesn't support embedded videos,
                but don't worry, you can <a href="/{{ $rubric->video_bg }}">download it</a>
                and watch it with your favorite video player!
            </video>
            <a href="#wedding_design"><img id="video_btn" src="/img/wedding_btn.png" alt=""
                                           class="bounce animated infinite"></a>
        </div>
        @section('page_class',  'home wedding_page')
    @else
        @section('page_class',  'wedding_page')
        <div id="wedding_design_line"></div>
    @endif
    <div id="wedding_design">
        <div class="container">
            <div id="breadcrumb">
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                </div>

                @if($rubric)
                    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="{{route('page',$rubric->slug)}}" itemprop="url">
                            <span itemprop="title">{{$rubric->title}}</span> </a>
                    </div>
                @endif

            </div>
            @if (isset($rubric->h1) && !empty($rubric->h1))
                <h1>{{$rubric->h1}}</h1>
            @endif
            <div class="wedding_item_holder">
                @foreach($rubric->pages as $page)
                    <div class="wedding_item">
                        <div class="row no-gutters">

                            @if($loop->iteration % 2)

                                <div class="col-md-6 col-sm-12 item_img">
                                    <a href="{{route('page',$page->slug)}}">
                                        <img class="wedding_item_img" src="{{$page->image}}" alt="{{$page->title}}">
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-12 ">
                                    <div class="item_text_w">

                                        <h3>{{$page->title}}</h3>
                                        <div class="item_text">
                                            {{getWordsFromText($page->description, 28)}}
                                        </div>

                                        <a class="more_btn vdz_btn"  href="{{route('page',$page->slug)}}">{{l('Детальніше')}}</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            @else

                                <div class="col-md-6 col-sm-12 ">
                                    <div class="item_text_w">

                                        <h3>{{$page->title}}</h3>
                                        <div class="item_text">
                                            {{getWordsFromText($page->description, 28)}}
                                        </div>
                                        <a class="more_btn vdz_btn" href="{{route('page',$page->slug)}}">{{l('Детальніше')}}</a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 item_img">
                                    <a href="{{route('page',$page->slug)}}">
                                        <img class="wedding_item_img" src="{{$page->image}}" alt="{{$page->title}}">
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            @endif
                        </div>
                    </div>

                @endforeach
            </div>
        </div>

        <div id="projects_block">
            <div class="container">
                <div class="header">{!! trans('new_app.Кожне весілля - окрема історія.') !!}</div>
                <div class="header">{!! trans('new_app.Ти маєш це побачити на власні очі!') !!}</div>
                <a href="{!! url('/projects') !!}" class="vdz_btn">{!! trans('new_app.Наші проекти') !!}</a>
            </div>
        </div>

        <div class="container">
            <div class="content">
                {!! $rubric->content !!}
            </div>
        </div>
    </div>

    @include('parts.contacts_block')

@endsection
