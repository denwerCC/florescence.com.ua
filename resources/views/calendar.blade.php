@extends('layouts.florescence')
@section('title',  isset($title) ? $title : '')
@section('description', isset($description) ? $description : '')
@section('page_class',  'calendar')
@section('head_styles')
    <link rel="stylesheet" href="/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/vendor/lightgallery/css/lightgallery.min.css">
    <link rel="stylesheet" href="/vendor/lightgallery/css/lg-transitions.min.css">
    <link rel="stylesheet" href="/vendor/slick/slick-theme.css">
    <link rel="stylesheet" href="/vendor/slick-slider-1.8.0/slick.css">
@endsection
@section('content')
    <div class="header_bg_line"></div>
    <div id="calendar">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div id="breadcrumb">

                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('home')}}" itemprop="url">
                                <span itemprop="title">{{l('Головна')}}</span>
                            </a> »
                        </div>
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('cart')}}" itemprop="url">
                                <span itemprop="title">{{trans('new_app.Кошик')}}</span>
                            </a> »
                        </div>
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('page',$page->slug)}}" itemprop="url">
                                <span itemprop="title">{{$page->title}}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1>{{$page->title}}</h1>
                </div>
                <div class="col-sm-12 content_img">
                    @if(!empty($page->image))
                        <img src="{{$page->image}}" alt="">
                    @endif
                </div>
                <div class="col-sm-12 content_text">
                    {!! $page->content !!}
                </div>
                <div id="stages">
                    <div class="col-sm-12">
                        @if(isset($calendar_settings['e_title'][App::getLocale()]))
                            <h3>{!! $calendar_settings['e_title'][App::getLocale()] !!}</h3>
                        @endif
                        <div class="row">
                            @if(isset($calendar_settings['e_header_1'][App::getLocale()]) && isset($calendar_settings['e_text_1'][App::getLocale()]))
                                <div class="col-lg-3">
                                    <div class="stage_item">
                                        <div class="n">1</div>
                                        <div class="s_desc">
                                            <div class="header">
                                                {!! $calendar_settings['e_header_1'][App::getLocale()] !!}
                                            </div>
                                            <div class="text">
                                                {!! $calendar_settings['e_text_1'][App::getLocale()] !!}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($calendar_settings['e_header_2'][App::getLocale()]) && isset($calendar_settings['e_text_2'][App::getLocale()]))
                                <div class="col-lg-3">
                                    <div class="stage_item">
                                        <div class="n">2</div>
                                        <div class="s_desc">
                                            <div class="header">
                                                {!! $calendar_settings['e_header_2'][App::getLocale()] !!}
                                            </div>
                                            <div class="text">
                                                {!! $calendar_settings['e_text_2'][App::getLocale()] !!}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($calendar_settings['e_header_3'][App::getLocale()]) && isset($calendar_settings['e_text_3'][App::getLocale()]))
                                <div class="col-lg-3">
                                    <div class="stage_item">
                                        <div class="n">3</div>
                                        <div class="s_desc">
                                            <div class="header">
                                                {!! $calendar_settings['e_header_3'][App::getLocale()] !!}
                                            </div>
                                            <div class="text">
                                                {!! $calendar_settings['e_text_3'][App::getLocale()] !!}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($calendar_settings['e_header_4'][App::getLocale()]) && isset($calendar_settings['e_text_4'][App::getLocale()]))
                                <div class="col-lg-3">
                                    <div class="stage_item">
                                        <div class="n">4</div>
                                        <div class="s_desc">
                                            <div class="header">
                                                {!! $calendar_settings['e_header_4'][App::getLocale()] !!}
                                            </div>
                                            <div class="text">
                                                {!! $calendar_settings['e_text_4'][App::getLocale()] !!}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="col-sm-12">
                    <form id="order_form" action="{!! route('order.calendar_save') !!}" method="post">
                        <h3>{!! trans('new_app.Оформлення замовлення') !!}</h3>
                        {!! csrf_field() !!}
                        <section>
                            <div class="slabel">1</div>
                            <div class="s_content">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="s_header">{{trans('new_app.Оберіть дату привітання')}}</div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                    <span class="delivery_date_wrap">
                                                <input type="text" name="delivery_date" required="required"
                                                       value="{{ old('delivery_date') }}"
                                                       placeholder="{{trans('new_app.Дата')}}">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="slabel">2</div>
                            <div class="s_header">{{trans('new_app.Привід для привітання')}}</div>
                            <div class="s_content">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <select id="occasion" class="selectpicker" name="occasion" required>
                                            <option value="" readonly="readonly">---</option>
                                            <option value="{{trans('new_app.Ювілей')}}">{{trans('new_app.Ювілей')}}</option>
                                            <option value="{{trans('new_app.День народження')}}">{{trans('new_app.День народження')}}</option>
                                            <option value="{{trans('new_app.Річниця весілля')}}">{{trans('new_app.Річниця весілля')}}</option>
                                            <option value="{{trans('new_app.Річниця Знайомства')}}">{{trans('new_app.Річниця Знайомства')}}</option>
                                            <option value="{{trans('new_app.День Закоханих')}}">{{trans('new_app.День Закоханих')}}</option>
                                            <option value="{{trans('new_app.8-е березня')}}">{{trans('new_app.8-е березня')}}</option>
                                            <option value="{{trans('new_app.Сюрприз')}}">{{trans('new_app.Сюрприз')}}</option>
                                            <option class="custom_occasion"
                                                    value="{{trans('new_app.Свій привід')}}">{{trans('new_app.Свій привід')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <input id="occasion_input" type="text" name="occasion_text" value=""
                                               placeholder="{!! trans('new_app.Вкажіть свій привід') !!}"
                                               style="display:none;">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="slabel">3</div>
                            <div class="s_content">
                                <div class="s_header">{{trans('new_app.Контактна інформація отримувача')}}</div>

                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <label for="">{{trans('new_app.Замовник')}}</label>
                                        <input type="text" name="name" required="required"
                                               value="{{ old('name', Session::get('order.name', Auth::user()['name'])) }}"
                                               placeholder="{{trans('new_app.П.І.Б.')}}">
                                        <input type="text" name="phone" required="required"
                                               value="{{ old('phone', Session::get('order.phone', Auth::user()['phone'])) }}"
                                               placeholder="{{trans('new_app.Телефон')}}">
                                        <input type="email" name="email" required="required"
                                               value="{{ old('email', Session::get('order.email', Auth::user()['email'])) }}"
                                               placeholder="E-mail">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="slabel">4</div>
                            <div class="s_content">
                                <div class="s_header">{{trans('new_app.Букет')}}</div>
                                <div class="row">

                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <label for="">{{trans('new_app.Оберіть розмір букету')}}</label>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row">
                                    <div id="subscribe_sizes" class="row">
                                    @if(isset($calendar_settings['sizes']) && is_array($calendar_settings['sizes']))
                                        @foreach($calendar_settings['sizes'] as $size => $size_arr)
                                            <div class="size_item_wrapper col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                <div class="size_item {!! ($size == 's') ? 'active' : '' !!}" data-size="{{$size}}" data-price="{!! (isset($size_arr['price']) ? $size_arr['price'] : 0)!!}">
                                                    <div class="size text-center">
                                                        {{$size}}
                                                    </div>
                                                    <div class="size_label text-center">
                                                        {!! trans('new_app.Розмір') !!}
                                                    </div>
                                                    <div class="size_desc size_price text-center">
                                                        @if(isset($size_arr['price']))
                                                            {!!round($size_arr['price'] / App\Http\Controllers\CurrencyController::getCurrency(), 2) .' '.trans('new_app.'.getCurrentCurrency())!!}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                        <input id="order_bsize" type="hidden" name="order_bouquet_size" value="s">
                                        <input id="order_bsize_product" type="hidden" name="order_product_id" value="">
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <label for="">{{trans('new_app.Оберіть букет')}}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(isset($calendar_settings['sizes']) && is_array($calendar_settings['sizes']))
                                            @foreach($calendar_settings['sizes'] as $size => $size_arr)
                                                {{--{!! dump($size_arr) !!}--}}
                                                @if(isset($size_arr['products']) &&($size_arr['products'] instanceof \Illuminate\Database\Eloquent\Collection))
                                                    <div class="bouquets_slider row {!! ($size == 's') ? 'active' : '' !!}" data-size="{!! $size !!}">
                                                        @foreach($size_arr['products'] as $product)
                                                        <div class="product_item_slide">
                                                            <div id="p{!! $product->id !!}" class="product_item">
                                                                <div class="p_img">
                                                                    @if(!empty($product->image))
                                                                       <img src="{!! $product->image !!}" alt="{!! $product->title !!}" class="gallery" data-src="{{ $product->image }}" data-sub-html="#p{!! $product->id !!}_caption" alt="{!! $product->title !!}">
                                                                    @endif
                                                                </div>
                                                                <div id="p{!! $product->id !!}_caption" class="p_desc">
                                                                    <div class="p_name">
                                                                        {!! $product->title !!}
                                                                    </div>
                                                                    <span data-product_id="{!! $product->id !!}">{!! trans('new_app.Обрати') !!}</span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <br>
                                        <label for="">{{trans('new_app.Листівка')}}</label>
                                        <label class="postcard_label"><input type="checkbox"
                                                                             name="postcard"> {{trans('new_app.Додати листівку')}}
                                        </label>
                                        <textarea name="postcard_text" id="" cols="30" rows="10"
                                                  placeholder="{{trans('new_app.Текст')}}"
                                                  style="display: none;">{{ old('postcard_text', '') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="slabel">5</div>
                            <div class="s_content">
                                <div class="s_header">{{trans('new_app.Дані для доставки')}}</div>

                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <label for="">{{trans('new_app.Отримувач')}}</label>
                                        <input type="text" name="delivery_name" required="required"
                                               value="{{ old('delivery_name') }}"
                                               placeholder="{{trans('new_app.П.І.Б.')}}*">
                                        <input type="text" name="delivery_phone" required="required"
                                               value="{{ old('delivery_phone') }}"
                                               placeholder="{{trans('new_app.Телефон')}}*">
                                        <input type="email" name="delivery_email"
                                               value="{{ old('delivery_email') }}"
                                               placeholder="E-mail">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="delivery_anonym_label"><input type="checkbox"
                                                                                    name="delivery_anonym"> {{trans('new_app.Доставити анонімно')}}

                                        </label>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <label for="">{{trans('new_app.Адреса доставки')}}</label>
                                        <input type="text" name="address" required="required"
                                               value="{{ old('address') }}"
                                               placeholder="{{trans('new_app.Адреса')}}">
                                        {{--<textarea name="note" id="" cols="30" rows="10" placeholder="{{trans('new_app.Текст')}}">{{ old('note', Session::get('order.note')) }}</textarea>--}}
                                    </div>
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                        <label for="">{{trans('new_app.Час доставки')}}</label>
                                        <div class="row">
                                            <div class="col">
                                                    <span class="time_wrap">
                                                    <input type="text" name="time" required="required"
                                                           value="{{ old('time') }}"
                                                           placeholder="{{trans('new_app.Від')}}">
                                                    </span>
                                            </div>
                                            <div class="col">
                                                     <span class="delivery_time_wrap">
                                                    <input type="text" name="delivery_time" required="required"
                                                           value="{{ old('delivery_time') }}"
                                                           placeholder="{{trans('new_app.До')}}">
                                                     </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                @php($oheader = config('order_form.header'))
                                @php($otext = config('order_form.text'))
                                @if(is_array($oheader) && is_array($otext))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="order_content">
                                                @if(is_array($oheader) && isset($oheader[App::getLocale()]))
                                                    <div class="oc_header">
                                                        {!! $oheader[App::getLocale()] !!}
                                                    </div>
                                                @endif
                                                @if(is_array($otext) && isset($otext[App::getLocale()]))
                                                    <div class="oc_text">
                                                        {!! $otext[App::getLocale()] !!}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif


                            </div>
                        </section>
                        <section>
                            <div class="slabel">6</div>
                            <div class="s_content">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="s_header">{{l('Спосіб оплати')}}</div>
                                        <br>
                                        <label class="payment_method_label"><input type="radio" required
                                                                                   name="payment_method" value="Liqpay"
                                                                                   checked>{{trans('new_app.Онлайн-платіж (Privat24, LiqPay, Visa, MasterCard)')}}
                                        </label>
                                        <label class="payment_method_label"><input type="radio" required
                                                                                   name="payment_method"
                                                                                   value="Privat24">{{trans('new_app.Переказ на картку ПриватБанку')}}
                                        </label>
                                        {{--<label class="payment_method_label"><input type="radio" required name="payment_method" value="MoneyGram">{{trans('new_app.Переказ через MoneyGram')}}</label>--}}
                                        {{--<label class="payment_method_label"><input type="radio" required name="payment_method" value="PayPal">{{trans('new_app.Онлайн-платіж PayPal')}}</label>--}}
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="slabel">7</div>
                            <div class="s_content">
                                <button id="order_confirm" class="vdz_btn vdz_btn_dark"
                                        type="submit">{{trans('new_app.Підтвердити замовлення')}}</button>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_scripts')
    <script src="/vendor/momentjs/moment.min.js"></script>
    <script src="/vendor/momentjs/moment-with-locales.min.js"></script>
    <script src="/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="/vendor/lightgallery/js/lightgallery-all.min.js"></script>
    <script src="/vendor/slick-slider-1.8.0/slick.min.js"></script>
    <script>
        (function ($) {
            $(document).ready(function () {
                console.log('order');
                $('input[name="time"]').datetimepicker({
                    locale: '{!! App::getLocale() !!}',
                    format: 'HH:mm',
                });
                $('input[name="delivery_time"]').datetimepicker({
                    locale: '{!! App::getLocale() !!}',
                    format: 'HH:mm',
                });
                $('input[name="delivery_date"]').datetimepicker({
                    locale: '{!! (App::getLocale() === 'ua') ? 'ru' : App::getLocale()!!}',
                    format: 'DD/MM/YYYY',
                });

                /*
                function selecte_recipient_check() {
                    // console.log($('#srecipient').val());
                    if (parseInt($('#srecipient').val()) === 0) {
                        $('input[name="name"]').on('input', function () {
                            $('input[name="delivery_name"]').val($(this).val()).trigger('input');
                        })
                        $('input[name="phone"]').on('input', function () {
                            $('input[name="delivery_phone"]').val($(this).val()).trigger('input');
                        })
                        $('input[name="email"]').on('input', function () {
                            $('input[name="delivery_email"]').val($(this).val()).trigger('input');
                        })
                    }
                    $('input#recipient_input').val($('#srecipient').val());
                }

                selecte_recipient_check();
                $('#srecipient').on('change', function () {
                    selecte_recipient_check();
                });
                */

                $('#occasion').on('change', function () {
                    console.log($(this).find('option:selected').val());
                    if ($(this).find('option:selected').hasClass('custom_occasion')) {
                        $('#occasion_input').show().val('');
                    } else {
                        $('#occasion_input').hide().val('');
                    }
                });

                $('input[name="postcard"]').on('change', function () {
                    if ($(this).prop('checked')) {
                        $('textarea[name="postcard_text"]').show();
                    } else {
                        $('textarea[name="postcard_text"]').hide();
                    }
                });

                var slickSliderSettings = {
                    dots: false,
                    arrows: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    speed: 500,
                    responsive: [
                        {
                            breakpoint: 680,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                };
                //Букеты
                $('.bouquets_slider').slick(slickSliderSettings);
                //Выбор размера
                $('#subscribe_sizes .size_item').on('click', function () {
                    $(this).addClass('active').parent('.size_item_wrapper').siblings().find('.size_item').removeClass('active');
                    var size = $(this).data('size');
                    //отключаем выбранные ранее цветы
                    $('.bouquets_slider .product_item').removeClass('active');
                    //Показываем нужную галерею
                    $('.bouquets_slider[data-size="'+size+'"]').slick('unslick').slick(slickSliderSettings).addClass('active').siblings().removeClass('active');
                    $('#order_bsize').val($(this).data('size'));
                });
                $('.gallery').lightGallery({
                    pager: false,
                    download: false,
                    selector: 'this',
                });
                $('.bouquets_slider .product_item span').on('click', function(){
                    $('.bouquets_slider .product_item').removeClass('active');
                    $(this).parents('.product_item').addClass('active');
                    $('#order_bsize_product').val( $(this).data('product_id'));
                });

            });
        })(jQuery);
    </script>
@endsection