<style>
    form div.form-group input {
        border-radius: 0;
    }
    form button[type='submit']{
        border-radius: 0;
    }
</style>

<form action="{{route('send')}}" method="post" class="callback-form" id="mp-callback-form">
    {{csrf_field()}}
    <input type="hidden" name="type" value="CallbackMe">
    <div class="h3 text-center">{{l('Зв’язатись з організатором')}}</div>
    <div class="form-group">
        <input type="text" placeholder="{{l('Ваше ім’я')}}" name="name" class="form-control">
    </div>
    <div class="form-group">
        <input type="text" name="phone" placeholder="{{l('Ваш контактний номер')}}" class="form-control phone-input">
    </div>
    <button type="submit" class="form-control">{{l('Зв’язатись')}}</button>
</form>