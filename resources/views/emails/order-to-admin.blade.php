<div style="font-family: Arial;">
    <h1 style="border-bottom: 6px solid #3C8DBC; padding-bottom: 10px;margin-bottom: 10px;">{{request()->getHost()}} Інформація про замовлення <a style="color:#3C8DBC;text-decoration: none;" href="http://{{request()->getHost() . '/admincab/orders/' . $order->id }}/edit">№ {{$order->id}}</a>:</h1>

    <div style="float: left; width: 50%">

        <h2>Дані замовника</h2>
        Імя: {{$order->name}} <br> E-mail: {{$order->email}} <br> Телефон: {{$order->phone}} <br>

    </div>

    <div style="float: left; width: 50%">
        <h2>Дані отримувача</h2>
        Імя: {{$order->delivery_name}} <br> Телефон: {{$order->delivery_phone}} <br> Адреса: {{$order->address}}
        <br> Дата, час: {{$order->delivery_date}} {{$order->delivery_time}} - {{$order->time}} <br>

    </div>

    <div style="padding: 10px 0; margin-bottom: 10px; border-bottom: 3px solid #3C8DBC;">
    <h2>Дані про замовлення</h2>

    Сума: {{$order->amount}} <br> Спосіб оплати: {{$order->payment_method}}
    <br> Код замовлення: {{$order->code}} <br>
</div>

    <h2>Товар</h2>

    @foreach($order->items as $item)
        <a style="display: block; padding: 10px 0; margin-bottom: 10px; border-bottom: 2px solid #3C8DBC;text-decoration: none;font-size: 20px;overflow: hidden;" href="{{$item->attr->link}}"><img src="http://{{request()->getHost() . $item->attr->thumb}}" style="float: left;margin: 5px;border: 2px solid #3C8DBC;" alt="{{$item->title}}"> {{$item->title}} - {{$item->price}} грн</a>
    @endforeach
</div>