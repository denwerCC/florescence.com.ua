@extends('layouts.florescence')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('style')
    <style>
        .wedding_slider {
            position: relative;
            z-index: 99;
        }

        .slick-current {
            width: 300px;
        }

        .slick-current {
            position: relative;
            top: -149px;
            width: 320px !important;
            height: 320px !important;
        }

        .slick-current img {
            width: 100%;
            height: 100%;
            /*position: relative;*/
            /*top: -149px;*/
            /*width: 300px !important;*/
            /*height: 300px !important;*/
            /*left: -65px;*/
        }
    </style>
@endsection
@section('content')

    <div id="container">
        <div class="container">
            <div id="breadcrumb">
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                </div>

                @if($rubric)
                    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="{{route('page',$rubric->slug)}}" itemprop="url">
                            <span itemprop="title">{{$rubric->title}}</span> </a> »
                    </div>
                @endif

                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('page',$page->slug)}}" itemprop="url">
                        <span itemprop="title">{{$page->title}}</span> </a>
                </div>

            </div>

            <div class="wedding_item_holder">
                    <div class="description_block row">
                        <img src="{{$page->image}}" alt="" class="wd-bg-img">
                        <div class="outer_block">
                            <div class="col-lg-10 col-lg-offset-1 item_title">
                                <h1>{{$page->title}}</h1>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item_description">
                                <p>{!! $page->description !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="wedding_slider">
                        @foreach($page->images as $image)
                            <div style="width: 160px!important;">
                                <a class="wedding_slide" href="{{$image}}">
                                    <img src="{{$page->image(330,330,$image)}}" alt=""> </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="description_short">
                        {!! $page->content !!}
                    </div>
                    <div class="divider"><img src="{{asset('/img/divider.png')}}" alt=""></div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function () {
            console.log('popupshowed');
            {{--@if(!session('popupshowed'))--}}
            @if(false)
            console.log('popupshowedDo');
            setTimeout(function () {
                $.colorbox({
                    width: 500, height: 400, html: '<div id="form-holder" style="padding:15px;">' +
                    '<h3 style="font-size:18px;">{{l('Вкажіть, будь ласка, дату весілля і наш флорист зв’яжеться з вами найближчим часом')}}</h3>' +
                    '<form action="{{route('send')}}" method="post">' +
                    '{{csrf_field()}}' +
                    '<input type=hidden name=type value=master-show>' +
                    '<label for="name">{{l('Ім’я')}}</label><input style="margin-bottom:15px;" type="text" name=name class="form-control">' +
                    '<label for="phone">{{l('Контактний номер')}}</label><input style="margin-bottom:15px;" type="text" name=phone class="form-control phone-input">' +
                    '<label for="date">{{l('Дата')}}</label><input style="margin-bottom:15px;" type="text" name=date class="form-control date-input">' +
                    '<button type="submit">{{l('Надіслати')}}</button>' +
                    '</form></div>',
                    onComplete: function () {
                        $('#form-holder form').submitter({
                            onError: function (response) {
                                formError($('#form-holder form'), response.responseJSON);
                                $.colorbox.resize();
                            },
                            onSuccess: function (response) {
                                $('#form-holder form').html(response.message);
                                $.colorbox.resize();
                            }
                        });
                    }
                });
            }, 10000);
            @endif
        });
    </script>
@endpush
@php(session(['popupshowed'=>true]))