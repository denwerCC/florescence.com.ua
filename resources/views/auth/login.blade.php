@extends('layouts.app')
@section('title',  'Авторизация')
@section('description', isset($description) ? $description : '')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading"><h1 class="text-center">Авторизация</h1></div>
            <div class="panel-body userform">
                @include('auth.loginform')
            </div>
        </div>
    </div>
@endsection
