@extends('layouts.florescence')
@section('title',  isset($title) ? $title :trans('new_app.Оформлення замовлення'))
@section('description', isset($description) ? $description : '')
@section('head_styles')
    <link rel="stylesheet" href="/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
@endsection
@section('content')
    <div id="cart">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <div id="breadcrumb">

                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('home')}}" itemprop="url">
                                <span itemprop="title">{{l('Головна')}}</span>
                            </a> »
                        </div>
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('cart')}}" itemprop="url">
                                <span itemprop="title">{{trans('new_app.Кошик')}}</span>
                            </a> »
                        </div>
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('order')}}" itemprop="url">
                                <span itemprop="title">{{trans('new_app.Оформлення замовлення')}}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1>{{trans('new_app.Оформлення замовлення')}}</h1>
                </div>
                <div class="col-sm-12">
                    {{--{!! dd($cart) !!}--}}
                    @if($count_items>=1)
                        <form id="order_form" action="{!! route('order.save') !!}" method="post"
                              onsubmit="ga('send', 'event', 'zamoviti','buket'); yaCounter44630368.reachGoal('zamoviti-buket');">
                            {!! csrf_field() !!}
                            <section>
                                <div class="slabel">1</div>
                                <div class="s_content">
                                    <div class="s_header">{{trans('new_app.Оберіть отримувача')}}</div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <select id="srecipient" class="selectpicker" name="recipient" required>
                                                <option value="0">{{trans('new_app.Отримувач я')}}</option>
                                                <option value="1" selected>{{trans('new_app.Отримувач інша людина')}}</option>
                                            </select>
                                            <input id="recipient_input" type="hidden" name="text">
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="slabel">2</div>
                                <div class="s_content">
                                    <div class="s_header">{{trans('new_app.Контактна інформація')}}</div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <label for="">{{trans('new_app.Замовник')}}</label>
                                            <input type="text" name="name" required="required"
                                                   value="{{ old('name', Session::get('order.name', Auth::user()['name'])) }}"
                                                   placeholder="{{trans('new_app.П.І.Б.')}}">
                                            <input type="text" name="phone" required="required"
                                                   value="{{ old('phone', Session::get('order.phone', Auth::user()['phone'])) }}"
                                                   placeholder="{{trans('new_app.Телефон')}}">
                                            <input type="email" name="email" required="required"
                                                   value="{{ old('email', Session::get('order.email', Auth::user()['email'])) }}"
                                                   placeholder="E-mail">
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <label for="">{{trans('new_app.Отримувач')}}</label>
                                            <input type="text" name="delivery_name" required="required"
                                                   value="{{ old('delivery_name', Session::get('order.delivery_name')) }}"
                                                   placeholder="{{trans('new_app.П.І.Б.')}}">
                                            <input type="text" name="delivery_phone" required="required"
                                                   value="{{ old('delivery_phone', Session::get('order.delivery_phone')) }}"
                                                   placeholder="{{trans('new_app.Телефон')}}">
                                            <input type="email" name="delivery_email"
                                                   value="{{ old('delivery_email', Session::get('order.delivery_email')) }}"
                                                   placeholder="E-mail">
                                        </div>
                                        <div class="col-md-12">
                                            <label class="delivery_anonym_label"><input type="checkbox"
                                                                                        name="delivery_anonym"> {{trans('new_app.Доставити анонімно')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="slabel">3</div>
                                <div class="s_content">
                                    <div class="s_header">{{trans('new_app.Дата та час')}}</div>
                                    <div class="row">

                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <label for="">{{trans('new_app.Дата доставки')}}</label>
                                            <span class="delivery_date_wrap">
                                                <input type="text" name="delivery_date" required="required"
                                                   value="{{ old('delivery_date', Session::get('order.delivery_date')) }}"
                                                   placeholder="{{trans('new_app.Дата')}}">
                                            </span>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <label for="">{{trans('new_app.Час доставки')}}</label>
                                            <div class="row">
                                                <div class="col">
                                                    <span class="time_wrap">
                                                    <input type="text" name="time" required="required"
                                                           value="{{ old('time', Session::get('order.time')) }}"
                                                           placeholder="{{trans('new_app.Від')}}">
                                                    </span>
                                                </div>
                                                <div class="col">
                                                     <span class="delivery_time_wrap">
                                                    <input type="text" name="delivery_time" required="required"
                                                           value="{{ old('delivery_time', Session::get('order.delivery_time')) }}"
                                                           placeholder="{{trans('new_app.До')}}">
                                                     </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="slabel">4</div>
                                <div class="s_content">
                                    <div class="s_header">{{trans('new_app.Доставка')}}</div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <label for="">{{trans('new_app.Адреса доставки')}}</label>
                                            <input type="text" name="address" required="required"
                                                   value="{{ old('address', Session::get('order.address')) }}"
                                                   placeholder="{{trans('new_app.Адреса')}}">
                                            <label for="">{{trans('new_app.Коментар до замовлення')}}</label>
                                            <textarea name="note" id="" cols="30" rows="10" placeholder="{{trans('new_app.Текст')}}">{{ old('note', Session::get('order.note')) }}</textarea>
                                        </div>

                                    </div>

                                    @php($oheader = config('order_form.header'))
                                    @php($otext = config('order_form.text'))
                                    @if(is_array($oheader) && is_array($otext))
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="order_content">
                                                    @if(is_array($oheader) && isset($oheader[App::getLocale()]))
                                                        <div class="oc_header">
                                                            {!! $oheader[App::getLocale()] !!}
                                                        </div>
                                                    @endif
                                                    @if(is_array($otext) && isset($otext[App::getLocale()]))
                                                        <div class="oc_text">
                                                            {!! $otext[App::getLocale()] !!}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <br>
                                            <label for="">{{trans('new_app.Листівка')}}</label>
                                            <label class="postcard_label"><input type="checkbox"
                                                                                        name="postcard"> {{trans('new_app.Додати листівку')}}
                                            </label>
                                            <textarea name="postcard_text" id="" cols="30" rows="10" placeholder="{{trans('new_app.Текст')}}" style="display: none;">{{ old('postcard_text', '') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="slabel">5</div>
                                <div class="s_content">
                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="s_header">{{trans('new_app.Спосіб оплати')}}</div>
                                    <br>
                                    <label class="payment_method_label"><input type="radio" required name="payment_method" value="Liqpay" checked>{{trans('new_app.Онлайн-платіж (Privat24, LiqPay, Visa, MasterCard)')}}</label>
                                    <label class="payment_method_label"><input type="radio" required name="payment_method" value="Privat24">{{trans('new_app.Переказ на картку ПриватБанку')}}</label>
                                    {{--<label class="payment_method_label"><input type="radio" required name="payment_method" value="MoneyGram">{{trans('new_app.Переказ через MoneyGram')}}</label>--}}
                                    {{--<label class="payment_method_label"><input type="radio" required name="payment_method" value="PayPal">{{trans('new_app.Онлайн-платіж PayPal')}}</label>--}}
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="slabel">6</div>
                                <div class="s_content">
                                    <button id="order_confirm" class="vdz_btn vdz_btn_dark"
                                            type="submit">{{trans('new_app.Підтвердити замовлення')}}</button>
                                </div>
                            </section>
                        </form>

                    @else
                        <h3>
                            {{trans('new_app.Ваш кошик пустий')}}
                        </h3>
                        <a href="{!! route('catalog') !!}">{{trans('new_app.Повернутися в каталог')}}</a>

                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_scripts')
    <script src="/vendor/momentjs/moment.min.js"></script>
    <script src="/vendor/momentjs/moment-with-locales.min.js"></script>
    <script src="/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script>
        (function ($) {
            $(document).ready(function () {
                console.log('order');
                $('input[name="time"]').datetimepicker({
                    locale: '{!! App::getLocale() !!}',
                    format: 'HH:mm',
                });
                $('input[name="delivery_time"]').datetimepicker({
                    locale: '{!! App::getLocale() !!}',
                    format: 'HH:mm',
                });
                $('input[name="delivery_date"]').datetimepicker({
                    locale: '{!! (App::getLocale() === 'ua') ? 'ru' : App::getLocale()!!}',
                    format: 'DD/MM/YYYY',
                });
                function selecte_recipient_check(){
                    // console.log($('#srecipient').val());
                    if(parseInt($('#srecipient').val()) === 0){
                        $('input[name="name"]').on('input', function () {
                            $('input[name="delivery_name"]').val($(this).val()).trigger('input');
                        })
                        $('input[name="phone"]').on('input', function () {
                            $('input[name="delivery_phone"]').val($(this).val()).trigger('input');
                        })
                        $('input[name="email"]').on('input', function () {
                            $('input[name="delivery_email"]').val($(this).val()).trigger('input');
                        })
                    }
                    $('input#recipient_input').val($('#srecipient').val());
                }
                selecte_recipient_check();
                $('#srecipient').on('change',function () {
                    selecte_recipient_check();
                });
                $('input[name="postcard"]').on('change',function () {
                    if($(this).prop('checked')){
                        $('textarea[name="postcard_text"]').show();
                    }else{
                        $('textarea[name="postcard_text"]').hide();
                    }
                });
            });
        })(jQuery);
    </script>
@endsection