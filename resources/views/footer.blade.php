
<div class="container">
    <div id="footer">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="social-bar">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pull-left" id="social">
                    @foreach(config('social') as $social => $link)
                        <div class="col-lg-1 col-md-2 col-sm-3 col-xs-6">
                            <a href="{{$link}}" target="_blank" rel="nofollow">
                                <i class="fa fa-{{$social}} fa-2x"></i>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 footer-phone-block pull-right">
                <span class="footer_number">{!! get_config('phone') !!}</span>
            </div>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12-col-xs-12" id="payment-icons">
          <div class="footer-payment-header">
            <img src="/img/payment-icons/sprite.png" alt="">
            </div>
        </div>
    </div>
</div>