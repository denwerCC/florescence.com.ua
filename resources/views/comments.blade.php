<div id="comments">
    <div class="pane">
        <div class="col-sm-6 col-sm-offset-3">
            <h4>Создать комментарий</h4>

            {!! Form::open(array('route' => ['comments.store','item',$item->id], 'files' => true, 'method' => 'post','class'=>'form-horizontal')) !!}

            <div class="form-group">
                <input name="name" placeholder="Имя" class="form-control" type="text">
            </div>
            <div class="form-group">
                <textarea name="text" cols="30" rows="5" placeholder="Сообщение ..." class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary btn-flat">Сохранить</button>
            {!! Form::close() !!}
        </div>
    </div>

    <h3>Отзывы о товаре</h3>

    <div class="col-sm-12">

        @foreach(\App\Models\Comment::whereCommentableType('item')->whereCommentableId($item->id)->wherePublic(1)->with('answer')->get() as $comment)
            <div class="commbox">
                <div class="user">
                    <div class="col-sm-3 col-xs-12 lcol">
                        <span class="name">{{$comment->name}}</span>
                    </div>

                    <div class="col-sm-9 col-xs-12 rcol">
                        <div class="combody">
                            {{$comment->text}}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                @if($comment->answer)
                    <div class="admin">
                        <div class="feedautor col-sm-3"><span class="name">Администратор</span></div>
                        <div class="feedansw col-sm-9">
                            <div class="combody">{{$comment->answer->text}}</div>
                        </div>
                        <i class="fa fa-caret-down"></i>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</div>