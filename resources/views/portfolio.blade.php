@extends('layouts.app')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)

@section('content')

    <div id="container">
        <div class="container">
            <div class="h2">{{l('Наше портфоліо')}}</div>

            <div class="row">
                <div class="col-lg-4 gallery">
                    <a class="portfolio" href="img/buket.jpg" id="inner-img"><img src="img/buket.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/biliy-vodospad-44.jpg"><img src="img/biliy-vodospad-44.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/florentsiia.jpg"><img src="img/florentsiia.jpg" id="gallery-img"></a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 gallery">
                    <a class="portfolio" href="img/happy-box.jpg"><img src="img/happy-box.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/sertse-z-troiand-175.jpg"><img src="img/sertse-z-troiand-175.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/prestij-147.jpg"><img src="img/prestij-147.jpg" id="gallery-img"></a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 gallery">
                    <a class="portfolio" href="img/box-flowers/chista-lubov-74.jpg"><img src="img/box-flowers/chista-lubov-74.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/box-flowers/korobochka-z-troiandami-ta-orkhideiami-182.jpg"><img src="img/box-flowers/korobochka-z-troiandami-ta-orkhideiami-182.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/box-flowers/romantyka.jpg"><img src="img/box-flowers/romantyka.jpg" id="gallery-img"></a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 gallery">
                    <a class="portfolio" href="img/wedding-flowers/1.jpg"><img src="img/wedding-flowers/1.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/wedding-flowers/2.jpg"><img src="img/wedding-flowers/2.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/wedding-flowers/3.jpg"><img src="img/wedding-flowers/3.jpg" id="gallery-img"></a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 gallery">
                    <a class="portfolio" href="img/wedding-flowers/4.jpg"><img src="img/wedding-flowers/4.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/wedding-flowers/5.jpg"><img src="img/wedding-flowers/5.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/wedding-flowers/6.jpg"><img src="img/wedding-flowers/6.jpg" id="gallery-img"></a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 gallery">
                    <a class="portfolio" href="img/wedding-flowers/7.jpg"><img src="img/wedding-flowers/7.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/wedding-flowers/8.jpg"><img src="img/wedding-flowers/8.jpg" id="gallery-img"></a>
                </div>
                <div class="col-lg-4">
                    <a class="portfolio" href="img/wedding-flowers/9.jpg"><img src="img/wedding-flowers/9.jpg" id="gallery-img"></a>
                </div>
            </div>
        </div>
    </div>

    @endsection



