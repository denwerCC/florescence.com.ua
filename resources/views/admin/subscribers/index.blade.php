@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('javascript')
    <link rel="stylesheet" href="/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(function () {
            $('.date').datetimepicker({
                locale: 'uk',
                format: 'YYYY-MM-DD HH:mm',
            });
        });
    </script>
@endsection

@section('content')

    <div class="box box box-info">
        <div class="box-header">
            <h3 class="box-title">Подписчики</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>E-mail</th>
                    <th>Дата подписки</th>
                    <th>IP</th>
                    <th></th>
                </tr>

                @foreach($subscribers as $n => $subscriber)
                    <tr>
                        <td>{{$n}}.</td>
                        <td>{{$subscriber->email}}</td>
                        <td>{{$subscriber->created_at}}</td>
                        <td>{{$subscriber->ip}}</td>
                        <td><a class="btn btn-danger btn-xs confirm pull-right" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="{{route('admin.subscribers.delete',$subscriber->id)}}"><i class="glyphicon glyphicon-trash"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection