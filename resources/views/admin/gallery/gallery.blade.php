@extends('admin.admin')
@section('style')
    <style>
        .ajax-file-upload-filename {
            width: 300px;
            height: auto;
            margin: 0 5px 5px 0px;

        }

        .ajax-file-upload-filesize {
            width: 50px;
            height: auto;
            margin: 0 5px 5px 0px;
            display: inline-block;
            vertical-align: middle;
        }

        .ajax-file-upload-progress {
            margin: 5px 10px 5px 0px;
            position: relative;
            width: 250px;
            border: 1px solid #ddd;
            padding: 1px;
            border-radius: 3px;
            display: inline-block;
            color: #FFFFFF;

        }

        .ajax-file-upload-bar {
            background-color: #0ba1b5;
            width: 0;
            height: 20px;
            border-radius: 3px;
            color: #FFFFFF;

        }

        .ajax-file-upload-percent {
            position: absolute;
            display: inline-block;
            top: 3px;
            left: 48%
        }

        .ajax-file-upload-red {
            -moz-box-shadow: inset 0 39px 0 -24px #e67a73;
            -webkit-box-shadow: inset 0 39px 0 -24px #e67a73;
            box-shadow: inset 0 39px 0 -24px #e67a73;
            background-color: #e4685d;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            display: inline-block;
            color: #fff;
            font-family: arial;
            font-size: 13px;
            font-weight: normal;
            padding: 4px 15px;
            text-decoration: none;
            text-shadow: 0 1px 0 #b23e35;
            cursor: pointer;
            vertical-align: top;
            margin: 5px 10px 5px 0px;
        }

        .ajax-file-upload-green {
            background-color: #77b55a;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            margin: 0;
            padding: 0;
            display: inline-block;
            color: #fff;
            font-family: arial;
            font-size: 13px;
            font-weight: normal;
            padding: 4px 15px;
            text-decoration: none;
            cursor: pointer;
            text-shadow: 0 1px 0 #5b8a3c;
            vertical-align: top;
            margin: 5px 10px 5px 0px;
        }

        .ajax-upload-dragdrop {

            border: 2px solid #F5F5F5;
            width: 420px;
            color: #DADCE3;
            text-align: left;
            vertical-align: middle;
            padding: 10px;
        }

        .ajax-upload-dragdrop span {
            padding-left: 10px;
        }

        .state-hover {
            border: 2px solid #A5A5C7;
        }

        .ajax-file-upload-container {
            margin: 20px 0px 20px 0px;
        }
    </style>
@endsection
@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <div class="col-lg-12 form-group">

                <div id="fileuploader" class="bg-light dk"></div>
                <div class="progress progress-striped active">
                    <div style="width: 100%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="progress-bar" data-original-title="" title="">

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="box">
        <div class="box-body">
            <div id="gallform">

                @foreach ($images as $image)
                    <div class="clearfix bg-light lter">

                        <form class='list-img' data-id='{{$image->id}}' method='post' action="{{route('admin.gallery.adddata',$image->id)}}">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <img class="img-thumbnail" src='{{$image->thumb}}' alt='{{$image->title}}'/>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type='text' class='form-control' name='title' placeholder='Название...' value='{{$image->title}}'>
                                </div>
                                <div class="form-group">
                                    <input type='text' class='form-control' name='link' placeholder='Ссылка...' value='{{$image->link}}'>
                                </div>
                                <div class="form-group">
                                    <textarea name='description' class='form-control' placeholder='Описание...'>{{$image->description}}</textarea>
                                </div>
                            </div>

                            <div class="col-sm-1">
                                <a class='del confirm' data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="{{route('admin.gallery.delimage',$image->id)}}"><i class='glyphicon glyphicon-trash'></i>
                                </a>
                            </div>
                        </form>

                    </div>
                @endforeach
            </div>
        </div>
    </div>


@endsection
@section('javascript')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="/admin/uploader/js/jquery.uploadfile.js"></script>
    <script>
        $(function () {
            var lang = '';

            $("#gallform").sortable({
                placeholder: "ui-state-highlight",
                cursor: 'move',
                update: function (event, ui) {

                    var result = new Array();
                    $('.list-img').each(function () {
                        result.push($(this).data('id'));
                    });

                    $.ajax({
                        type: "POST",
                        url: "{{route('admin.gallery.sort')}}",
                        data: {data: result},
                        cache: false,
                        success: function (data) {
                            toastr.success(data);
                        },
                        error: function (error) {
                            alert("Error was " + error['status']);
                        }
                    });
                }
            });
//$( "#gallform" ).disableSelection();
// END JQUERY

            $('.list-img').change(function () {

                var m_method = $(this).attr('method');
                var m_action = $(this).attr('action');
                var m_data = $(this).serialize();

                $.ajax({
                    type: m_method,
                    url: m_action,
                    data: m_data,
                    success: function (data) {
                        toastr.success(data);
                    },
                    error: function (result) {
                        alert(result.responseText);
                    }
                });
            });

            // uploader
            fileuploader = $("#fileuploader");
            progress = $('.progress');

            fileuploader.uploadFile({
                url: "{{route('admin.gallery.upload')}}<?//=$category?>",
                fileName: "img",
                showFileCounter: true,
                showFileSize: true,
                showProgress: true,
                showDownload: true,
                processData: true,
                statusBarWidth: 'auto',
                dragdropWidth: 'auto',
                previewWidth: 'auto',
                statusBarWidth: 'auto',
                uploadStr: "Добавить изображения",
                uploadButtonClass: "ajax-file-upload btn btn-success btn-lg btn-rect",
                dragDropStr: "<span><b class='text-info'>Перетащить файлы</b></span>",
                onLoad: function (files) {
                    progress.hide()
                },
                onSubmit: function (files) {

                    progress.slideDown(500);
                    $('.text-info', fileuploader).text(files);

                },
                onSuccess: function (files, response, xhr, pd) {
                    if (response.status == 'error') {
                        alert(response.message);
                    }
                },
                afterUploadAll: function (files, response, xhr, pd) {
                    //form.append("<input type='hidden' name='imgs["+response+"][name]'>");
                    window.location.reload();
                }
            });
        });
    </script>
    @endsection