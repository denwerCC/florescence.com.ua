@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
@endsection
@section('javascript')
    <script>
        $(function () {

            $('#showTranslationBtn').click(function () {

                $('.form-group').hide();

                $('.box-body input').each(function () {

                    if($(this).val()==''){
                        $(this).parents('.form-group').show();
                    }

                });

                return false;
            });

            $('#showAllBtn').click(function () {

                $('.form-group').show();

                return false;
            })

        })

    </script>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="text-center">Перевод сайта завершен на {{$progress['all']}}% ({{$progress['allcount']}})
                позиций</h3>

            @foreach($progress['pers'] as $lang => $pers)
                <div class="progress">
                    <div style="width: {{$pers}}%;" class="progress-bar progress-bar-success"><span>{{$lang}} {{$pers}}% ({{$progress['data'][$lang]}} позицій)</span></div>
                </div>
            @endforeach

        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {{Form::open(array('route' => ['admin.translation.store'],'class'=>"form-horizontal",'method'=>'post'))}}
        <div class="box-header">
            <button type="submit" class="btn btn-info pull-right">Сохранить</button>

            <a id="showTranslationBtn" href="" class="btn btn-primary btn-xs">Отобразить только непереведенные</a>
            <a id="showAllBtn" class="btn btn-info btn-xs" href="">Отобразить все</a>
        </div>
        <div class="box-body">

            @foreach($translation as $text=>$data)

                <div class="form-group">
                    @foreach($data as $lang => $value)
                        {{Form::label($lang, $lang, array('class' => 'col-sm-1 control-label'))}}

                        <div class="col-sm-11">
                            {{Form::text("translation[$lang][$text]",$value,['class'=>'form-control','placeholder'=>$text])}}
                        </div>
                    @endforeach
                </div>

            @endforeach

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
        </div>
        <!-- /.box-footer -->
        {{Form::close()}}
    </div>

@endsection