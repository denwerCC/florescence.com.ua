<ul class="sidebar-menu">
    <li class="header">Настройки</li>
    <li>
        <a href="{{route('admin.config.edit','liqpay')}}">
            <i class="fa fa-file"></i> <span>Настройки оплаты</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','email')}}">
            <i class="fa fa-envelope"></i> <span>Настройки писем</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.translation.show')}}">
            <i class="fa fa-language"></i> <span>Перевод</span>
        </a>
    </li>
</ul>