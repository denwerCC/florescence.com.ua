@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('javascript')
    <link rel="stylesheet" href="/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script type="text/javascript"
            src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
@endsection

@section('content')


        <div class="row">
                <div class="col-md-12">
                        <!-- Default box -->
                        <div class="box">
                                <div class="box-body">
                                        <table aria-describedby="pages-table" role="grid" id="pages-table"
                                                                           class="table table-bordered table-hover dataTable  table-striped">
                                                <thead>
                                                <tr role="row">
                                                        <th>id</th>
                                                        <th>image</th>
                                                        <th>name</th>
                                                        <th>phone</th>
                                                        <th>email</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                @foreach($photos as $n => $photo)
                                                        <tbody>
                                                        <tr>
                                                                <td>{{$n}}</td>
                                                            <td><img src="/upload/photos/{{$photo->img}}" alt="" style="width: 300px;height: 300px;"> </td>
                                                                <td>{{$photo->name}} </td>
                                                                <td>{{$photo->phone}}</td>
                                                                <td>{{$photo->email}} </td>
                                                                <td><a class="btn btn-danger btn-xs confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="{{route('photos.destroy', $photo->id)}}"> <i class="glyphicon glyphicon-trash"></i></a> </td>
                                                            </tr>
                                                        </tbody>
                                                    @endforeach
                                            </table>
                                    </div>
                            </div>
                    </div>
            </div>

        @endsection

