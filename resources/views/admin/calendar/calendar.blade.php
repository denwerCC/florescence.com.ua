@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

@endsection
@section('content')
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Amount</th>
            <th>Payment Method</th>
            <th>Delivery Date</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Status</th>
        </tr>
        </thead>
    </table>
@endsection

@section('javascript')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {
            $('#users-table').DataTable({ "language": {
                    //"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Ukrainian.json"
                    "url": "/admin/plugins/datatables/i18n/Russian.json"
                },
                processing: true,
                serverSide: true,
                pageLength: 50,
                // searching: false,
                ordering: true,
                ajax: '{!! route('calendar.data') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'phone', name: 'phone' },
                    { data: 'email', name: 'email' },
                    { data: 'amount', name: 'amount' },
                    { data: 'payment_method', name: 'payment_method' },
                    { data: 'delivery_date', name: 'delivery_date' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    {"data": "status","render": function (data, type, row) {

                            return '<a class="btn btn-success btn-xs" href="/admincab/orders/' + row['id'] + '/edit"> <i class="fa fa-pencil "></i></a> <a class="btn btn-danger btn-xs confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="/admincab/orders/delete/' + row['id'] + '"> <i class="glyphicon glyphicon-trash"></i> </a>';
                        }},
                ]
            });
        });
    </script>
@endsection
