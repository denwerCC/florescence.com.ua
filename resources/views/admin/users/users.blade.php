@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('javascript')

    <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
		$(function () {

			$('#pages-table').DataTable({
				"language": {
					//"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Ukrainian.json"
					"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json"
				},
				"processing": true,
				"paging": true,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": true,
				"autoWidth": true,
				"serverSide": true,
				"ajax": "/admincab/users",
				"columns": [
					{"data": "id"},
					{"data": "name"},
					{"data": "email"},
					{"data": "updated_at"},
					{"data": "id"},
					//{"data": "delete"}
				],
				"columnDefs": [
					{

						"targets": 4,
						"render": function (data, type, row) {
							if (row['active'] == 1) {
								btn = ' <a class="btn btn-default btn-xs" href="/admincab/users/activate/' + row['id'] + '"><i class="fa fa-circle-o"></i> Отменить доступ</a>';
							} else {
								btn = ' <a class="btn btn-success btn-xs" href="/admincab/users/activate/' + row['id'] + '"><i class="fa fa-check-circle"></i> Предоставить доступ</a>';
							}

							return '<a class="btn btn-danger btn-xs confirm pull-right" data-original-title="Удалить пользователя" data-toggle="tooltip" data-placement="top" href="/admincab/users/delete/' + row['id'] + '"><i class="glyphicon glyphicon-trash"></i> </a>  <a class="btn btn-primary btn-xs" data-original-title="Просмотр" data-toggle="tooltip" data-placement="top" href="/admincab/users/' + row['id'] + '"><i class="fa fa-eye"></i></a> ' + btn;
						}
					}
				]
			});
		});
    </script>
@endsection

@section('content')

    <table aria-describedby="pages-table" role="grid" id="pages-table"
            class="table table-bordered table-hover dataTable  table-striped">
        <thead>
        <tr role="row">
            <th>id</th>
            <th>Имя</th>
            <th>E-mail</th>
            <th>Дата</th>
            <th>Управление</th>
        </tr>
        </thead>
    </table>

@endsection