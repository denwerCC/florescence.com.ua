@extends('admin.admin')
@section('title', $title)
@section('content')

    <section class="content">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="/admin/img/default-user-avatar.png"
                     alt="User profile picture">

                <h3 class="profile-username text-center">{{$model->name}} {{$model->last_name}}</h3>

                <p class="text-muted text-center">Анкета</p>

                <ul class="list-group list-group-unbordered">

                    @foreach($model->toarray() as $key =>$data)

                        <li class="list-group-item"><b>{{$key}}</b>: <a class="">{{$data}}</a></li>
                    @endforeach
                </ul>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
@endsection