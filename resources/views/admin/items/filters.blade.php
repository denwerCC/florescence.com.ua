<div class="row">

    @foreach(\App\Models\Filter::with('childs','parent')->get()->sortBy('level') as $filter)

        @if(!$filter->childs->isEmpty())
            <div class="form-group col-sm-12">
                {!! Form::label('', $filter->title,['class'=>'center-block']) !!}
                @foreach($filter->childs->load('parent') as $filter)
                    @if(isset($filter->parent->type) and $filter->parent->type==3)
                        <div class="form-group col-sm-3">
                            <div class="form-input-box">

                                <?
                                $filter_active = $Item->filters->where('filter_id', $filter->id)->first();
                                ?>
                                {!! Form::label("filter[$filter->id][$lang][value]", $filter->title) !!}
                                {!! Form::text("filter[$filter->id][$lang][value]", $filter_active ? $filter_active->value : '', array('class'=>'form-control')) !!}

                            </div>
                        </div>

                    @else
                        <?
                        $Item->filters ? $filter_active = $Item->filters->where('filter_id', $filter->id)->first() : $filter_active = false;
                        ?>
                        <div class="checkbox-inline">
                            {{Form::checkbox("filter[$filter->id][$lang][value]",$filter->id,$filter_active ? true : false, ['class'=>'synchronize','data-name'=>'filt'.$filter->id])}}
                            {!! Form::label("filter[$filter->id][$lang][value]", $filter->title) !!}
                        </div>
                    @endif

                @endforeach
            </div>
        @elseif(empty($filter->parent))

            @if(isset($filter->type) and $filter->type==4)
                <div class="form-group col-sm-12">
                    <div class="form-input-box">
                        <?
                        $filter_active = $Item->filters->where('filter_id', $filter->id)->first();
                        ?>
                        {!! Form::label("filter[$filter->id][$lang][value]", $filter->title) !!}
                        {!! Form::file("filter[$filter->id][$lang][value]") !!}

                        @if($filter_active)
                            {!! Form::hidden("filter[$filter->id][$lang][value]",$filter_active->value,['id'=>'id'.$filter->id]) !!}
                            <p class="help-block"><i class="fa fa-file" aria-hidden="true"></i>
                                <a href="/{{$Item->fileDir.$filter_active->value}}">{{$filter_active->value}}</a>
                                <a href="" class="del text-danger confirm delFile" data-id="id{{$filter->id}}">×</a></p>
                        @endif
                    </div>
                </div>
            @else
                <div class="form-group col-sm-3">
                    <div class="form-input-box">

                        <?
                        $filter_active = $Item->filters->where('filter_id', $filter->id)->first();
                        ?>
                        {!! Form::label("filter[$filter->id][$lang][value]", $filter->title) !!}
                        {!! Form::text("filter[$filter->id][$lang][value]", $filter_active ? $filter_active->value : '', array('class'=>'form-control')) !!}

                    </div>
                </div>
            @endif
        @endif
    @endforeach
</div>
<script>
    window.onload = function () {
        $('.delFile').click(function () {
            $('#'+$(this).data('id')).detach();
            $(this).parent().detach();
            return false;
        })
    };

</script>