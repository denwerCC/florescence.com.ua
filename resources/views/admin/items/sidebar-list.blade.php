<ul class="treeview-menu">
    @foreach($category->childrens->sortBy('level') as $category)
        @if($category->childrens->isEmpty())
            <li><a href="{{route('admin.items.category',$category->id)}}">{{$category->title}}
                    <small class="label label-primary pull-right">{{$allitems->where('category_id',"$category->id")->count()}}</small>
                </a>
            </li>
        @else

            <li><a href="#"><i class="fa fa-caret-down"></i>{{$category->title}}
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @include('admin.items.sidebar-list')
            </li>
        @endif

    @endforeach
</ul>