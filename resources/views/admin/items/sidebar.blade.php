<ul class="sidebar-menu">
    <li class="header">Товары</li>
    <li>
        <a href="{{route('admin.items.create')}}">
            <i class="fa fa-file"></i> <span>Нова позиція</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.items.index')}}">
            <i class="fa fa-list-alt"></i> <span>Всі позиції</span>
            <small class="label label-primary pull-right">{{$document['datacount']}}</small>
        </a>
    </li>

    @if($count=$allitems->where('category_id',0)->count())
        <li>
            <a href="{{route('admin.items.category',0)}}">
                <i class="fa fa-list-alt"></i> <span>Без категории</span>
                <small class="label label-primary pull-right">{{$count}}</small>
            </a>
        </li>
    @endif

    @foreach($categories->where('parent_id',NULL)->sortBy('level') as $category)
        @if($category->childrens->isEmpty())
            <li>
                <a href="{{route('admin.items.category',$category->id)}}">
                    <i class="fa fa-list-alt"></i> <span>{{$category->title}}</span>
                    <small class="label label-primary pull-right">{{$allitems->where('category_id',"$category->id")->count()}}</small>
                </a>
            </li>
        @else

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i> <span>{{$category->title}}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                @include('admin.items.sidebar-list')
            </li>
        @endif
    @endforeach

    <li class="header">Настройки</li>
    <li>
        <a href="{{route('admin.categories.index')}}">
            <i class="fa fa-calendar"></i> <span>Категории</span>
        </a>
    </li>
</ul>