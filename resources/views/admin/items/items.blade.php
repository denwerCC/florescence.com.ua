@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('sidebar')
    @include('admin.items.sidebar')
@endsection
@section('javascript')

    <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript"
            src="/admin/plugins/datatables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
    <script>
		$(function () {

			$('#pages-table').DataTable({
				"language": {
					//"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Ukrainian.json"
					"url": "/admin/plugins/datatables/i18n/Russian.json"
				},
				"rowReorder": {
					update: false
				},
				"processing": true,
				"paging": true,
				"pageLength": 50,
				"lengthChange": true,
				"searching": true,
				"order": [],
				"ordering": true,
				"info": true,
				"autoWidth": true,
				"serverSide": true,
				"ajax": document.location,
				"columns": [
					{"data": "id"},
					{
						"data": "thumb_html", "render": function (data, type, row) {

						return "<img class='direct-chat-img' src='" + row['thumb'] + "'>";
						;
					}
					},
					{"data": "title"},
					{"data": "categoryTitle"},
					{"data": "updated_at"},
//                    {"data": "stock"},
//                    {"data": "sku"},
					{
						"data": "price", "render": function (data, type, row) {
						return '<input class="priceedit" data-id="'+row.id+'" value="'+row.price+'">'
					}
					},
					{
						"data": "id", "render": function (data, type, row) {

						return '<div class="box-tools pull-right">'
							+ '<a class="btn btn-success btn-xs" data-original-title="Редактировать" data-toggle="tooltip" data-placement="top" href="/admincab/items/' + row['id'] + '/edit"><i class="fa fa-pencil "></i> </a> '
							+ '<a class="btn btn-info btn-xs" data-original-title="Клонировать вещь" data-toggle="tooltip" data-placement="top" href="/admincab/items/clone/' + row['id'] + '"><i class="fa fa-files-o"></i></a> '
							+ '<a class="btn delete btn-danger btn-xs confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="/admincab/items/delete/' + row['id'] + '"><i class="glyphicon glyphicon-trash"></i> </a> '
							+ '</div>';
					}
					},
				],
				"columnDefs": [
					{orderable: false, targets: 4}
				]

			}).on('row-reorder', function (e, diff, edit) {

				data = [];
				diff.forEach(function (item, i, arr) {

					data.push({'id': item.node.id, 'level': item.newPosition});

				});

				$.post('{{route('admin.items.sort')}}', {'sort': data});

			});

			$(document).on('input', '.priceedit', function () {
				$.post('{{route('admin.items.price')}}', {
					id: $(this).data('id'),
					price: $(this).val()
				}, function (data) {

				});
			});


			$(document).on('click', '.delete', function () {
				$.get(this.href, function (data) {
					table.draw(false);
					toastr[data.status](data.message);
				});
				return false;
			});

		});
    </script>
@endsection

@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <table aria-describedby="pages-table" role="grid" id="pages-table"
                    class="table table-bordered table-hover dataTable  table-striped">
                <thead>
                <tr role="row">
                    <th>id</th>
                    <th>Миниатюра</th>
                    <th>Название</th>
                    <th>Категория</th>
                    <th>Дата</th>
                    <th>Цена</th>
                    <th>Управление</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div id="masschange" class="well well-small dark">

                <p>
                    <i class="fa fa-info-circle"></i> Перетащите номер позиции для изменения сортировки (только внутри категории)
                </p>
                {{--<p>
                    <i class="fa fa-info-circle"></i> Массовое изменение цены: (не забудьте сделать <a href="/admincab/index.php?a=backup">резервную копию</a>)
                </p>--}}

                {!! Form::open(array('route' => ['admin.items.changeprice',$category_id])) !!}
                <p>
                    <i class="fa fa-info-circle"></i> Массовое изменение цены: введите формулу, используя математические знаки */+-() Например, уменьшить цену на 20% + 30:
                </p>
                <p class="help-block">
                    При обновлении цены в любой категории - изменения затронут только ее, при работе во вкладке "Все позиции" - будет изменена цена всех товаров.
                </p>

                <div class="form-group">
                    <div class="checkbox-inline">
                        <input id="price_old" name="price_old" type="checkbox">
                        <label for="price_old">Отобразить текущие цены в блоке "старая цена"?</label>
                    </div>
                </div>
                <div class="form-group form-inline">
                    <input class="form-control" name="formula" placeholder="* 0.8 + 30" required="" type="text">
                    <input class="btn btn-metis-6 btn-default btn-flat " value="Отправить" type="submit">
                </div>
                {!! Form::close() !!}

            </div>
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection