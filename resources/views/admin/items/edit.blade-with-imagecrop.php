@extends('admin.admin')
@section('sidebar')
    @include('admin.items.sidebar')
@endsection
@section('style')
    <link rel="stylesheet" href="/admin/uploader/css/uploadfile.css">
    <link rel="stylesheet" href="/admin/js/jquery.fileapi/main.css">
    <link rel="stylesheet" href="/admin/js/jquery.fileapi/jcrop/jquery.Jcrop.min.css">

    <style>

        #additional-images {
            overflow: hidden;
        }

        #additional-images > div {
            height: 60px !important;
            float: left;
            position: relative;
            margin: 0.5px;
        }

        #additional-images > div img {
            height: 60px !important;
            display: block;
        }

        #additional-images > div input {
            display: none;
        }

        #additional-images .ajax-file-upload-filename, #additional-images .ajax-file-upload-progress {
            display: none !important;
        }

        #additional-images .ui-state-highlight {
            width: 60px;
            height: 60px;
            border: 1px solid #E4E4E4;
            background: #F5F5F5;
        }

        #additional-images .del {
            position: absolute;
            text-decoration: none;
            display: none;
        }

        #additional-images > div:hover .del {
            display: inline-block;
            color: red;

        }
    </style>

@endsection

@section('content')
    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <div class="nav-tabs-custom negativemargin">
                <ul class="nav nav-tabs pull-right">
                    @foreach ($items as $lang=>$page)
                        <li @if (App::getLocale()==$lang) class="active" @endif ><a href="#tab_{{$lang}}"
                                                                                    data-toggle="tab">{{$lang}}</a>
                        </li>
                    @endforeach

                    <li class="pull-left header"><i class="fa fa-th"></i> ...</li>
                </ul>
                <div class="tab-content">
                    @foreach ($items as $lang=>$page)

                        <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">

                            {!! Form::open(array('route' => $route,'files' => true, 'method' => $method)) !!}
                            {!! Form::hidden('locale', $page->locale, array('class'=>'form-control','placeholder'=>'Название страницы')) !!}
                            <div class="form-group">
                                {!! Form::label('name', 'Название', array('class' => '')) !!}
                                {!! Form::text('name', $page->title, array('class'=>'form-control','placeholder'=>'Название','required'=>'required')) !!}
                            </div>

                            <div class="row">

                                <div class="form-group col-sm-6">
                                    {!! Form::label('category_id', 'Категория', array('class' => '')) !!}
                                    {!! Form::select('category_id', ['1'=>'items',2=>'Hostess'],$page->category_id,array('class'=>'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-6 col-sm-offset-1">
                                    <div class="checkbox">
                                        {!! Form::checkbox('public', $page->public, array('class'=>'form-control')) !!}
                                        {!! Form::label('public', 'Опубликовано', array('class' => '')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="clear:both">
                                <div>
                                    <label class="control-label"><?= l('Image') ?></label><br>
                                </div>
                                <div>

                                    <div id="additional-images">
                                        @foreach($page->images as $img => $params)
                                            <div><a class='del confirm'
                                                    href='{{route('admin.items.deleteimg',['id'=>$page->id,'image'=>$img])}}'>✕</a><img
                                                        src='{{$page->image(60,60,$img)}}'><input type='text'
                                                                                               name='imgs[{{$img}}][name]'
                                                                                               value='{{$params['name']}}'>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div>

                                    <div id="fileuploader"></div>
                                    <div id="userpic" class="userpic b-upload b-upload_dnd">

                                        <div class="b-upload__dnd">
                                            <div class="js-browse">
                                                <label class="add-self"><span class='upload'><i class="fa fa-camera fa-1x"></i> Загрузить фото</span>
                                                    <input name="filedata" type="file">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="js-preview userpic__preview"
                                             style="position: absolute; top: 166px; right: 13px; z-index: 9999999; border:2px solid #367FA9;display:none"></div>
                                        <div id="popup" class="popup" style="display: none;">
                                            <div class="popup__body">
                                                <div class="js-img"></div>
                                            </div>
                                            <div style="margin: 0 0 5px; text-align: center;">
                                                <div class="js-upload btn btn_browse btn_browse_small">Загрузить</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div><!-- /.form-group -->


                            {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}

                            {!! Form::close() !!}
                        </div>
                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

@endsection

@section('javascript')

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <!-- include summernote css/js-->
    {!! Html::style(asset('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}
    {!! Html::script(asset('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
    <script>
        $(document).ready(function () {
            $('#page-content').summernote({height: 400});
        });
    </script>

    <script src="/admin/js/jquery.fileapi/FileAPI/FileAPI.min.js"></script>
    <script src="/admin/js/jquery.fileapi/FileAPI/FileAPI.exif.js"></script>
    <script src="/admin/js/jquery.fileapi/jquery.fileapi.min.js"></script>
    <script src="/admin/js/jquery.fileapi/jcrop/jquery.Jcrop.min.js"></script>
    <script src="/admin/js/jquery.fileapi/jquery.modal.js"></script>

    <script>

        $(function () {

            $(document).on('click', '#additional-images .del', function () {

                if (!$(this).hasClass('inline')) {
                    $.get(this.href);
                }

                $(this).parent('div').detach();

                return false;

            });

            var imgContainer = $('#additional-images');
            // sortable
            $(imgContainer).sortable({
                placeholder: "ui-state-highlight",
                cursor: 'move',
                revert: true // animation
            });

            preview = $('.userpic__preview');

            $('#userpic').fileapi({
                url: '/admincab/model/uploadimg',
                data: {_token: '{{csrf_token()}}'},
                paramName: 'img',
                accept: 'image/*',
                //autoUpload: true,
                imageSize: {minWidth: 230, minHeight: 230},
                elements: {
                    active: {show: '.js-upload', hide: '.js-browse'},
                    preview: {
                        el: '.js-preview',
                        width: 60,
                        height: 80
                    },
                    progress: '.js-progress',
                    dnd: {
                        el: '.b-upload__dnd',
                        hover: 'b-upload__dnd_hover',
                        fallback: '.b-upload__dnd-not-supported',
                    }
                },
                onSelect: function (evt, ui) {
                    modalcrop(evt, ui);
                    preview.fadeIn(500)
                },
                onDrop: function (evt, ui) {
                    modalcrop(evt, ui)
                    preview.fadeIn(500)
                },
                onBeforeUpload: function (evt, uiEvt) {
                    preview.fadeOut(500)
                },
                onFileComplete: function (evt, ui) {

                    response = ui.result;
                    if (response.status == 'error') {
                        alert(response.message);
                    } else {
                        imgContainer.prepend("<div><a href='#' class='del inline confirm'>✕</a> <img src='/upload/tmp/" + response.file + "'> <input type='hidden' name='imgs[" + response.file + "][name]'></div>");
                    }
                },
            });

            function modalcrop(evt, ui) {

                var file = ui.files[0];
                var filetype = file.type;

                if (!filetype.match(/image\/(gif|png|jpg|jpeg)$/)) {
                    alert('Помилка! Використовуйте тільки зображення в форматі gif, png, jpg або jpeg')
                } else if (!FileAPI.support.transform) {
                    alert('Your browser does not support Flash :(');
                }
                else if (file) {
                    $('#popup').modal({
                        closeOnEsc: true,
                        closeOnOverlayClick: false,
                        onOpen: function (overlay) {
                            $(overlay).on('click', '.js-upload', function () {
                                $.modal().close();

                                $('#userpic').fileapi('upload');
                            });
                            $('.js-img', overlay).cropper({
                                file: file,
                                bgColor: '#fff',
                                aspectRatio: 3 / 4,
                                maxSize: [$(window).width() - 100, $(window).height() - 40],
                                minSize: [100, 100],
                                selection: '100%',
                                onSelect: function (coords) {
                                    $('#userpic').fileapi('crop', file, coords);
                                }
                            });
                        }
                    }).open();
                }
            }


        });
    </script>

@endsection