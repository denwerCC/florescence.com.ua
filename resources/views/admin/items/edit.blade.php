@extends('admin.admin')
@section('sidebar')
    @include('admin.items.sidebar')
@endsection
@section('style')
    <link rel="stylesheet" href="/admin/plugins/uploader/css/uploadfile.css">
@endsection
@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header">
                        <i class="fa fa-external-link"></i>
                        <a class='inline-block' href="{{$Item->link}}" target="_blank">{{$Item->title}}</a>
                    </li>
                </ul>
            </div>

            {!! Form::open(array('route' => $route,'files' => true, 'method' => $method)) !!}
            {{csrf_field()}}

            <div class="col-sm-9">
                <div class="tab-content">
                    @foreach ($items as $lang=>$page)

                        <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">

                            <div class="form-group">
                                {!! Form::label($lang."[title]", 'Название позиции', array('class' => '')) !!}
                                {!! Form::text($lang."[title]", $page->title, array('class'=>'form-control','placeholder'=>'Название позиции')) !!}
                            </div>

                            <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$lang}}"> Meta </a>
                                        </h4>
                                    </div>
                                    <div id="collapse-{{$lang}}" class="panel-collapse collapse">
                                        <div class="box-body">
                                            <div class="form-group">
                                                {!! Form::label($lang."[meta_title]", 'Meta title', array('class' => '')) !!}
                                                {!! Form::text($lang."[meta_title]", $page->meta_title, array('class'=>'form-control','placeholder'=>'Название позиции')) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label($lang."[meta_description]", 'Meta description', array('class' => '')) !!}
                                                {!! Form::text($lang."[meta_description]", $page->meta_description, array('class'=>'form-control','placeholder'=>'Название позиции')) !!}
                                            </div>

                                            {{--                                            <div class="form-group">
                                                                                            {!! Form::label($lang."[keywords]", 'Keywords', array('class' => '')) !!}
                                                                                            {!! Form::text($lang."[keywords]", $page->keywords, array('class'=>'form-control','placeholder'=>'Ключевые слова')) !!}
                                                                                        </div>--}}

                                            <div class="form-group">
                                                {!! Form::label($lang."[slug]", 'Ссылка', array('class' => '')) !!}
                                                {!! Form::text($lang."[slug]", $page->slug, array('class'=>'form-control','placeholder'=>'Ссылка')) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            @include('admin.items.filters')

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[description]", 'Описание', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[description]", $page->description, array('class'=>'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[content]", 'Контент', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[content]", $page->content, array('class'=>'form-control editor')) !!}
                                </div>
                            </div>

                        </div>

                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>

            <div class="col-sm-3">

                <div class="form-group">
                    {!! Form::label('category_id', 'Категория', array('class' => '')) !!}
                    {!! Form::select('category_id', [0=>'Без категории'] + $categories->pluck('title', 'id')->toArray(),$Item->category_id,array('class'=>'form-control')) !!}
                </div>
                <div class="form-group">

                    {{--{!! Form::checkbox('stock', 1, $Item->stock) !!}--}}
                    {{--{!! Form::label('stock', 'Наличие', array('class' => '')) !!}--}}

                    {{--                    {!! Form::label('stock', 'Наличие', array('class' => '')) !!}
                                        {!! Form::text('stock', $Item->stock,array('class'=>'form-control')) !!}--}}
                </div>
                {{--<div class="form-group">--}}
                {{--{!! Form::label('sku', 'Артикул', array('class' => '')) !!}--}}
                {{--{!! Form::text('sku', $Item->sku,array('class'=>'form-control')) !!}--}}
                {{--</div>--}}

                <div class="form-group">
                    {!! Form::label('price', 'Цена UAH', array('class' => '')) !!}
                    {!! Form::text('price', $Item->price,array('class'=>'form-control')) !!}
                </div>

                {{--<div class="form-group">--}}
                {{--{!! Form::label('price_old', 'Старая цена', array('class' => '')) !!}--}}
                {{--{!! Form::text('price_old', $Item->price_old,array('class'=>'form-control')) !!}--}}
                {{--</div>--}}

                {{--                <div class="form-group">
                                    {!! Form::label('date', 'Дата поступлення', array('class' => '')) !!}
                                    <div class="input-group input-append date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        {!! Form::text('date', $Item->date, array('class'=>'form-control')) !!}
                                    </div>
                                </div>--}}

                <div class="form-group">
                    <div id="additional-images">
                        @foreach($Item->imgs as $img)

                            <div><a class='del confirm'
                                        href='{{route('admin.items.deleteimg',['id'=>$Item->id,'image'=>$img])}}'>✕</a>
                                <img src='{{$Item->image(60,60,$img)}}'>
                                <input type='text' name='imgs[{{$img}}][name]' value=''>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <div>

                        <div id="fileuploader"></div>

                    </div>
                </div><!-- /.form-group -->

                <div class="form-group">

                    {!! Form::checkbox('public', 1, $Item->public) !!}

                    {!! Form::label('public', 'Опубликовано', array('class' => '')) !!}
                </div>
                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>

            <div class="clearfix"></div>

            <div class="col-sm-12">

                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>


            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection

@section('javascript')

    <link rel="stylesheet" href="/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript" src="/admin/plugins/ckeditor/ckeditor.js"></script>
    <script src="/admin/plugins/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
		$('.editor').ckeditor({
			filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
			filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
			filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
			filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
		});
    </script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script src="/admin/plugins/uploader/js/jquery.uploadfile.js"></script>
    <script>

		$(function () {

			$('.date').datetimepicker({
				locale: 'uk',
				format: 'YYYY-MM-DD HH:mm',
			});

			$(document).on('click', '#additional-images .del', function () {

				if (!$(this).hasClass('inline')) {
					$.get(this.href);
				}

				$(this).parent('div').detach();

				return false;

			});

			var imgContainer = $('#additional-images');

			$("#fileuploader").uploadFile({
				url: "{{route('admin.pages.uploadimg')}}",
				fileName: "img",
				formData: {_token: '{{csrf_token()}}'},
				showFileCounter: false,
				showFileSize: false,
				showProgress: false,
				statusBarWidth: 'auto',
				dragdropWidth: 'auto',
				previewWidth: 'auto',
				statusBarWidth: 'auto',
				uploadStr: "<?=l('Изображения')?>",
				uploadButtonClass: "ajax-file-upload btn btn-default",
				dragDropStr: "<span><b><?=l('Drag & Drop Files')?></b></span>",
				successCallback: function (pd, data) {
					pd.preview
						.addClass('loaded')
						.before("<a href='#' class='del inline confirm'>✕</a><input type='hidden' name='imgs[" + data.file + "][name]'>");
					pd.progressDiv.hide(200);
				},
				onError: function (files, status, message, pd) {
					toastr.error('Ошибка! Файл ' + files[0] + ' не загружен!');
					console.log(files, status, message, pd);
				}
			});

			// sortable
			$(imgContainer).sortable({
				placeholder: "ui-state-highlight",
				cursor: 'move',
				revert: true // animation
			});

		});
    </script>

@endsection