@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/uploader/css/uploadfile.css">
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}
@endsection
@section('javascript')
    {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
    <script>
        $(document).ready(function () {
            $('#page-content').summernote({height: 400});
        });
    </script>
{{--    {!! Html::script(asset('https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js')) !!}--}}
    {{--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>--}}
    <script type="text/javascript" src="/admin/plugins/ckeditor/ckeditor.js"></script>
    <script src="/admin/plugins/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">

        (function ($) {
            $(document).on('ready', function () {
                $('.editor').ckeditor({
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                });
            });
        })(jQuery);
    </script>
@endsection
@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header"><i class="fa fa-th"></i> ...</li>
                </ul>
            </div>
            {!! Form::open(array('route' => $route,'files' => true, 'method' => $method)) !!}
            <div class="col-sm-9">
                <div class="tab-content">

                    @foreach ($rubrics as $lang=>$rubric)
                        <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">

                            <div class="form-group">
                                {!! Form::label($lang."[title]", 'Название категории', array('class' => '')) !!}
                                {!! Form::text($lang."[title]", $rubric->title, array('class'=>'form-control','placeholder'=>'Название категории','required'=>'required')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label($lang."[h1]", 'H1', array('class' => '')) !!}
                                {!! Form::text($lang."[h1]", $rubric->h1, array('class'=>'form-control','placeholder'=>'H1')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[meta_title]", 'Meta title', array('class' => '')) !!}
                                {!! Form::text($lang."[meta_title]", $rubric->meta_title, array('class'=>'form-control','placeholder'=>'Meta title')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[meta_description]", 'Meta description', array('class' => '')) !!}
                                {!! Form::textarea($lang."[meta_description]", $rubric->meta_title, array('class'=>'form-control','placeholder'=>'Meta description')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[slug]", 'Ссылка', array('class' => '')) !!}
                                {!! Form::text($lang."[slug]", $rubric->slug, array('class'=>'form-control','placeholder'=>'Ссылка')) !!}
                            </div>

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[description]", 'Описание', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[description]", $rubric->description, array('class'=>'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[content]", 'Контент', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[content]", $rubric->content, array('class'=>'form-control editor')) !!}
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
                <!-- /.tab-content -->
                @if(in_array($Rubric->id, array(2,6)))
                    <div class="form-group col-sm-12">
                        <label for="">Video background</label>
                        {!! Form::text('video_bg', $Rubric->video_bg, array('class'=>'form-control','placeholder'=>'Video src')) !!}
                        <iframe src="/vendor/elFinder-2.1.33/elfinder.html" frameborder="0" width="100%" height="420px"></iframe>
                    </div>
                @endif

            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('parent_id', 'Категория', array('class' => '')) !!}
                    {!! Form::select('parent_id', $Rubrics, $Rubric->parent_id,array('class'=>'form-control')) !!}
                </div>

                <img class="thumbnail img-responsive" src="{{$Rubric->thumb}}">

                <div class="form-group">
                    <div class="form-input-box">
                        {!! Form::label('img', 'Изображение', array('class' => '')) !!}
                        {!! Form::file('img', null, array('class'=>'form-control')) !!}
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-sm-12">

                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>

            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection