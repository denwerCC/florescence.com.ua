@extends('admin.admin')
@section('content')

    @include('admin.rubrics.create')

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Список разделов</h3>
        </div>
        <div class="box-body">
            <div class="dd" id="menu">
                @include('admin.rubrics.list')
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="/admin/plugins/nestable/jquery.nestable.js"></script>
    <script>
        $(document).ready(function () {

            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target);

                if (window.JSON) {

                    var data = {
                        '_token' : '{{ csrf_token() }}',
                        'data' : list.nestable('serialize')
                    };

                    $.post('{{route('admin.rubrics.sort')}}',data);

                } else {
                    alert('JSON browser support required for sorting.')
                }
            };

            $('#menu').nestable({
                group: 1
            }).on('change', updateOutput);

        });
    </script>
@endsection