<ol class="dd-list">

    @foreach ($rubrics as $rubric)

        <li class="dd-item" data-id="{{$rubric->id}}">

         <span class="btns pull-right">

				<a data-original-title="Редактировать" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.rubrics.edit',['rubrics'=>$rubric->id])}}"><i class="fa fa-cog"></i></a>
				<a data-original-title="Видимость" data-toggle="tooltip" data-placement="top"
                   href="/admincab/menu/result.php?command=change_visible&amp;type=1&amp;id=6"><i
                            class="fa fa-check"></i></a>
				<a class="del confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.rubrics.delete',['rubrics'=>$rubric->id])}}"><i
                            class="fa fa fa-times text-danger"></i></a>
			</span>
            <div class="dd-handle">
                <span class="label label-info">{{$rubric->id}}</span> {{$rubric->title}}
            </div>
            @if($rubrics=$rubric->childrens)
                @include('admin.rubrics.list')
            @endif
        </li>

    @endforeach

</ol>