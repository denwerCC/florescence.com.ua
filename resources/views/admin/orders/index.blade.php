@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('sidebar')
    @include('admin.orders.sidebar')
@endsection
@section('javascript')

    <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {

            $('#pages-table').DataTable({
                "language": {
                    //"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Ukrainian.json"
                    "url": "/admin/plugins/datatables/i18n/Russian.json"
                },
                "processing": true,
                "paging": true,
                "pageLength": 50,
                //"lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "serverSide": true,
                "ajax": window.location,
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "email"},
                    {"data": "created_at"},
                    {"data": "amount"},
                    {"data": "statusname"},
                    {"data": "payment_status_value"},
                    {"data": "status","render": function (data, type, row) {

                        return '<a class="btn btn-success btn-xs" href="/admincab/orders/' + row['id'] + '/edit"> <i class="fa fa-pencil "></i></a> <a class="btn btn-danger btn-xs confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="/admincab/orders/delete/' + row['id'] + '"> <i class="glyphicon glyphicon-trash"></i> </a>';
                        ;
                    }},
                    //{"data": "delete"}
                ]
            });
        });
    </script>
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">

            <!-- Default box -->
            <div class="box">

                <div class="box-body">
                    <table aria-describedby="pages-table" role="grid" id="pages-table"
                           class="table table-bordered table-hover dataTable  table-striped">
                        <thead>
                        <tr role="row">
                            <th>id</th>
                            <th>Покупатель</th>
                            <th>Контакт</th>
                            <th>Дата</th>
                            <th>Сумма</th>
                            <th>Статус</th>
                            <th>Статус оплаты</th>
                            <th>Детали</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
{{--                <div class="box-footer">

                    <i class="fa fa-file-excel-o"></i> <span>Экспорт. Выбрать диапазон</span>
                    <div class="row">
                        {{Form::open(['route'=> ['admin.export','order'],'class'=>'form-horizontal','method'=>'get'])}}

                        <div class="col-xs-3">
                            {{Form::date('from','',['class'=>'form-control','placeholder'=>'От'])}}
                        </div>
                        <div class="col-xs-3">
                            {{Form::date('to','',['class'=>'form-control','placeholder'=>'До'])}}
                        </div>
                        {{Form::submit('Экспорт',['class'=>'btn'])}}

                        {{Form::close()}}
                    </div>
                </div>--}}
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
        </div>

    </div>
@endsection