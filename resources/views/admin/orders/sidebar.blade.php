<ul class="sidebar-menu">
    <li class="header">Заказы</li>

@foreach((new \App\Models\Order)->statuses as $status => $statusname)
    <li>
        <a href="{{route('admin.orders.status',$status)}}">
            <i class="fa fa-file"></i> <span>{{$statusname}}</span>
        </a>
    </li>
    @endforeach
<li class="header">Настройки</li>
    <li>
        <a href="{{route('admin.config.edit','email')}}">
            <i class="fa fa-envelope"></i> <span>Настройки писем</span>
        </a>
    </li>
</ul>