@extends('admin.admin')

@section('style')
@endsection
@section('javascript')
    <script type="text/javascript">
        $(function () {

            $('#status').change(function () {
                if (this.value == 2) {
                    $('#modal').modal();
                }
            });

        })
    </script>
@endsection
@section('content')

    <div class="row">

        {!! Form::open(array('route' => ['admin.orders.update',$order->id],'class'=>'form-horizontal','method'=>'PUT')) !!}

        <div class="col-md-8">
            <!-- Default box -->
            <div class="box box-info">

                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user "></i> Покупатель</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label("name", 'Имя', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::text("name", $order->name, array('class'=>'form-control','placeholder'=>'Имя')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label("email", 'E-mail', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-4">
                            {!! Form::text("email", $order->email, array('class'=>'form-control','placeholder'=>'E-mail')) !!}
                        </div>

                        {!! Form::label("email", 'Телефон', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-4">
                            {!! Form::text("phone", $order->phone, array('class'=>'form-control','placeholder'=>'Телефон')) !!}
                        </div>

                    </div>

                    <div class="form-group">
                        {!! Form::label("delivery", 'Доставка', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::text("delivery", $order->delivery, array('class'=>'form-control','placeholder'=>'Доставка')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label("address", 'Адрес', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::text("address", $order->address, array('class'=>'form-control','placeholder'=>'Адрес')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label("text", 'Комментарий', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::textarea("text", $order->text, array('class'=>'form-control','placeholder'=>'Комментарий')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('date', 'Дата доставки', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::text("date", $order->delivery_date, array('class'=>'form-control','placeholder'=>'дата')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('timeFrom', 'Час доставки', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::label('time', 'Від') !!}
                            {!! Form::text("time", $order->time, array('class'=>'form-control','placeholder'=>'Час доставки')) !!}
                            {!! Form::label('delivery_time', 'Дo') !!}
                            {!! Form::text("delivery_time", $order->delivery_time, array('class'=>'form-control','placeholder'=>'Час доставки')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('order[delivery_name]', 'ПІБ отримувача', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::text("order[delivery_name]", $order->delivery_name, array('class'=>'form-control','placeholder'=>'')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('order[delivery_phone]', 'Контактний номер отримувача', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::text("order[delivery_phone]", $order->delivery_phone, array('class'=>'form-control','placeholder'=>'')) !!}
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    {!! Form::submit('Сохранить', array('class'=>'btn btn-info pull-right')) !!}
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-4">

            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Сумма</span> <span class="info-box-number">{{$order->amount}}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                    IP: {{$order->ip}} Lang: {{$order->lang}}
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>

            <!-- Default box -->
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-calendar"></i> {{$order->created_at}}</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">
                    @if($order->coupon)
                        <div class="form-group">
                            <div class="col-md-4 text-right"><b>Скидка</b></div>
                            <div class="col-md-8">
                                {{$order->coupon->discount}}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 text-right"><b>Купон</b></div>
                            <div class="col-md-8">
                                {{$order->coupon->code}}
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="control-label col-md-4">Статус</label>
                        <div class="col-md-8">

                            {{Form::select('status',$order->statuses,$order->status,['id'=>'status','class'=>'form-control'])}}

                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-4 text-right"><b>Оплата</b></div>
                        <div class="col-md-8">

                            {{$order->payment_method}}

                        </div>

                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4">Статус оплаты</label>
                        <div class="col-md-8">
                            {{Form::select('payment_status',$order->payment_statuses,$order->payment_status,['class'=>'form-control'])}}
                        </div>
                    </div>

                    @if($order->tracking_number)
                        <div class="form-group">
                            <label class="control-label col-md-4">Номер декларации</label>
                            <div class="col-md-8">
                                {{Form::text('tracking_number',$order->tracking_number,['class'=>'form-control'])}}
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        {!! Form::label("text", 'Примечание', array('class' => 'control-label col-md-4')) !!}
                        <div class="col-md-8">
                            {!! Form::textarea("note", $order->note, array('class'=>'form-control','rows'=>4,'placeholder'=>'Примечание')) !!}
                        </div>
                    </div>

                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    {!! Form::submit('Сохранить', array('class'=>'btn btn-primary pull-right')) !!}
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </div>
        @if($order->status<2)
            <div id="modal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Ввести номер декларации</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    {!! Form::text("tracking_number", $order->tracking_number, array('class'=>'form-control','placeholder'=>'Номер')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    {!! Form::textarea("text", false, array('class'=>'form-control','rows'=>4,'placeholder'=>'Сообщение для покупателя (' . $order->lang . ')')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <input type="submit" name="sendtouser" class="btn btn-primary" value="Отправить пользователю">
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endif
        {!! Form::close() !!}
        <div class="col-lg-12">
            <div class="box box-danger">

                <div class="box-header with-border">
                    <h3 class="box-title">Продукция</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">
                    <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th class="hidden-xs">ID</th>
                            <th>Img</th>
                            <th class="hidden-xs">Артикул</th>
                            <th>Название</th>
                            <th>Параметры</th>
                            <th>Стоимость</th>
                            <th>Количество</th>
                            <th>Сумма</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($order->items as $item)

                            <tr>
                                <td class="sort text-center hidden-xs">{{$item->id}}</td>
                                <td><img class="direct-chat-img" src="{{$item->attr->thumb}}" alt="{{$item->title}}">
                                </td>
                                <td class="hidden-xs">{{$item->sku}}</td>
                                <td><a href="{{$item->attr->link}}" target="blank">{{$item->title}}</a></td>
                                <td class="text-center">
                                    <ul>
                                        @foreach($item->parameters as $type => $parameters)
                                            <li><b>{{$type}}</b> : {{$parameters}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td class="text-center">{{$item->price}}</td>
                                <td class="text-center">{{$item->count}}</td>
                                <td class="text-center">{{$item->count * $item->price}}</td>
                            </tr>

                        @endforeach

                        <tr>
                            <td colspan="7"><h3>Всего:</h3></td>
                            <td><h3 class="text-center">{{$order->amount}}</h3></td>
                        </tr>
                        </tbody>
                    </table>

                    <!-- /.box-body -->
                    <div class="box-footer">
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>

        </div>

    </div>
@endsection