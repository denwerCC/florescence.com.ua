@extends('admin.admin')
@section('sidebar')
    @include('admin.coupons.sidebar')
@endsection
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('javascript')
    <link rel="stylesheet" href="/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(function () {
            $('.date').datetimepicker({
                locale: 'uk',
                format: 'YYYY-MM-DD HH:mm',
            });
        });
    </script>
@endsection

@section('content')

    <div class="box box box-success">
        <div class="box-body">

            {!! Form::open(array('route' => 'admin.coupons.store')) !!}

            <div class="row">
                <div class="col-xs-5">
                    <input class="form-control" placeholder="Название купона" name="code" required>
                </div>

                <div class="col-xs-1">
                    <select class="form-control" name="type" required>
                        <option value="">Формула</option>
                        <option value="*">*</option>
                        <option value="-">-</option>
                    </select>
                </div>

                <div class="col-xs-2">
                    <input class="form-control" placeholder="Скидка" name="discount" required>
                </div>

                <div class="col-xs-2">
                    <div class="input-group input-append date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input name="expired" class="form-control" placeholder="Дата завершения">
                    </div>
                </div>

                <div class="col-xs-2">
                    <input class="btn btn-success btn-block" value="Создать" type="submit">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
    </div>


    <div class="box box box-info">
        <div class="box-header">
            <h3 class="box-title">Все купоны</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Купон</th>
                    <th>Скидка</th>
                    <th>Дата завершения</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th style="width: 40px">Покупки</th>
                    <th></th>
                </tr>

                @foreach($coupons as $n => $coupon)
                    <tr>
                        <td>{{$n}}.</td>
                        <td>{{$coupon->code}}</td>
                        <td>{{$coupon->discount_value}}</td>
                        <td>{{$coupon->expired}}</td>
                        <td>{{$coupon->oneoff==1?'одноразовый':''}}</td>
                        <td>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-danger" style="width: {{$coupon->orders->count()}}%"></div>
                            </div>
                        </td>
                        <td>
                            @if($coupon->orders->count()>0)
                                <a href="{{route('admin.orders.search',["in",implode(",",$coupon->orders->pluck('order_id')->toArray())])}}"><span class="badge bg-green">{{$coupon->orders->count()}}</span></a>
                            @endif
                        </td>
                        <td>
                            <span class="text-{{$coupon->active==1?'success':'info'}}" data-original-title="{{$coupon->active==1?'Активен':'Неактивен. Уже активирован'}}" data-toggle="tooltip" data-placement="top"><i class="fa fa-{{$coupon->active==1?'check-circle':'ban'}}"></i></span>
                        </td>
                        <td>
                            <a class="btn btn-danger btn-xs confirm pull-right" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="{{route('admin.coupons.delete',$coupon->id)}}"><i class="glyphicon glyphicon-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection