@foreach($form as $name=>$data)
    <?php isset($data['class'])?:$data['class']='' ?>
    <div class="form-group">
        {{Form::label($name, $data['label'], array('class' => 'col-sm-2 control-label'))}}

        <div class="col-sm-10">
            @if(isset($lang))
                @if($data['type']=='clone')
                    {{Form::text($lang."[$name]",1,$data['value'])}}
                @elseif($data['type']=='checkbox')
                    {{Form::checkbox($lang."[$name]",1,$data['value'])}}
                @else
                    {{Form::{$data['type']}($lang."[$name]",$data['value'],['class'=>'form-control ' . $data['class']])}}
                @endif
            @else
                @if($data['type']=='clone')
                    @foreach($data['value'][$name] as $value)
                        <div class="clone-block">
                            <div class="col-sm-11">
                                {{Form::text($name.'[]',$value,['class'=>'form-control'])}}
                            </div>
                            <div class="col-sm1 text-center control">
                                <a href="" class="add">+</a> <a href="" class="remove">-</a>
                            </div>
                        </div>
                    @endforeach

                @elseif($data['type']=='checkbox')
                    {{Form::checkbox($name,1,$data['value'])}}
                @else
                    {{Form::{$data['type']}($name,$data['value'],['class'=>'form-control ' . $data['class']])}}
                @endif
            @endif
        </div>
    </div>
@endforeach