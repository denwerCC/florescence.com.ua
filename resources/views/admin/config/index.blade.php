@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}

    <style>
        .clone-block:first-of-type .remove{
            visibility: hidden;
        }

        .control .add,.control .remove{
            font-size: 26px;
            font-weight: bolder;
            color: green;
            padding: 0 2px;
        }
        .control .remove{
            color: red;
        }
    </style>

@endsection
@section('javascript')
    {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
    <script>
        $(document).ready(function () {
            $('textarea').not('.noeditor').summernote({height: 120});

            $(document).on('click','.control .add',function () {
                var parent = $(this).parents('.clone-block');
                parent.after(parent.clone());
                return false;
            });
            $(document).on('click','.control .remove',function () {
                $(this).parents('.clone-block').remove();
                return false;
            })

        });
    </script>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header"><i class="fa fa-gear"></i> {{$title}}
                    </li>
                </ul>
            </div>

            {{Form::open(array('route' => ['admin.config.update',$type],'class'=>"form-horizontal",'method'=>'put'))}}

            <div class="box-body">
                <div class="tab-content">
                    @if(isset($form))
                        @include('admin.config.form')
                    @else
                        @foreach($dataform as $lang => $form)
                            <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">
                                @include('admin.config.form')
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Сохранить</button>
            </div>
            <!-- /.box-footer -->
            {{Form::close()}}
        </div>
    </div>
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class=" text-center">Администратор</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {{Form::open(array('route' => ['user.update',auth()->user()->id],'class'=>"form-horizontal","method"=>"put"))}}
        <div class="box-body">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Login</label>

                <div class="col-sm-10">
                    <input class="form-control" id="inputEmail3" placeholder="Login" name="email" type="text">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                <div class="col-sm-10">
                    <input class="form-control" id="inputPassword3" placeholder="Password" name="password"
                           type="password">
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
        </div>
        <!-- /.box-footer -->
        {{Form::close()}}
    </div>
@endsection