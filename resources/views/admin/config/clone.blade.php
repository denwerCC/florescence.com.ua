@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}

    <style>
        .clone-block:first-of-type .remove {
            visibility: hidden;
        }

        .control .add, .control .remove {
            font-size: 26px;
            font-weight: bolder;
            color: green;
            padding: 0 2px;
        }

        .control .remove {
            color: red;
        }
    </style>

@endsection
@section('javascript')
    {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
    <script>
        $(document).ready(function () {
            $('textarea').summernote({height: 120});

            $(document).on('click', '.control .add', function () {
                var parent = $(this).parents('.clone-block');
                parent.after(parent.clone());
                return false;
            });
            $(document).on('click', '.control .remove', function () {
                $(this).parents('.clone-block').remove();
                return false;
            })

        });
    </script>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- form start -->

            {{Form::open(array('route' => ['admin.config.update',$type],'class'=>"form-horizontal",'method'=>'put'))}}

            <div class="box-body">

                <div class="form-group">

                    <div class="col-xs-6">
                        Перенаправлення з
                    </div>
                    <div class="col-xs-6">
                        На
                    </div>

                    @foreach($form[$type]['value'] as $name=>$value)
                        <div class="clone-block">
                            <div class="col-sm-6">
                                {{Form::text('key[]',$name,['class'=>'form-control'])}}
                            </div>
                            <div class="col-sm-5">
                                {{Form::text('value[]',$value,['class'=>'form-control'])}}
                            </div>
                            <div class="col-sm1 text-center control">
                                <a href="" class="add">+</a> <a href="" class="remove">-</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
        </div>
        <!-- /.box-footer -->
        {{Form::close()}}
    </div>
    </div>
@endsection