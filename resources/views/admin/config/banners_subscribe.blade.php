@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}

    <style>
        .clone-block:first-of-type .remove {
            visibility: hidden;
        }

        .control .add, .control .remove {
            font-size: 26px;
            font-weight: bolder;
            color: green;
            padding: 0 2px;
        }

        .control .remove {
            color: red;
        }
    </style>

@endsection
@section('javascript')
{{--    {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}--}}
    <script>
        $(document).ready(function () {
            //$('textarea').summernote({height: 120});

            $(document).on('click', '.control .add', function (e) {
                e.preventDefault();
                $clones = $('#banner_clones').find('.clone-block');
                var $parent = $(this).parents('.clone-block');
                $clone = $parent.clone().appendTo($('#banner_clones'));
                $clone.find('input,textarea').each(function () {
                    var name = $(this).attr('name').replace(/\[[0-9]+\]/, '[' + ($clones.length + 1) + ']');
                    $(this).attr('name', name);
                    $(this).val('')
                    //console.log('name', name);
                })
                return false;
            });
            $(document).on('click', '.control .remove', function () {
                $(this).parents('.clone-block').remove();
                return false;
            })

        });
    </script>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div id="banner_form" class="box-body">
            <!-- form start -->

            {{Form::open(array('route' => ['admin.banners_subscribe.update'],'class'=>"form-horizontal",'method'=>'post'))}}

            <div class="box-body">

                @if(isset($form[$type]) && is_array($form[$type]))
                    <div id="banner_clones" class="form-group">

                        @foreach($form[$type] as $key=>$banner_arr)
                            <div class="clone-block">
                                @if(isset($banner_arr['img']))
                                    <div class="col-sm-12">
                                        <label for="">Image</label>
                                        {{Form::text($type.'['.$key.'][img]',$banner_arr['img'],['class'=>'form-control'])}}
                                    </div>
                                @endif
                                {{--@if(isset($banner_arr['link']))--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<label for="">Link</label>--}}
                                        {{--{{Form::url($type.'['.$key.'][link]',$banner_arr['link'],['class'=>'form-control'])}}--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                                @if(isset($banner_arr['text']) && is_array($banner_arr['text']))
                                    @foreach($banner_arr['text'] as $lang=>$text)
                                        <div class="col-sm-12">
                                            <label for="">{!! $lang !!}</label>
                                            {{Form::text($type.'['.$key.'][text]['.$lang.']',$text,['class'=>'form-control'])}}
                                        </div>
                                    @endforeach
                                @endif
                                <div class="col-sm1 text-center control">
                                    <a href="" class="add">+</a> <a href="" class="remove">-</a>
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
        </div>
        <!-- /.box-footer -->
        {{Form::close()}}
        <iframe src="/vendor/elFinder-2.1.33/elfinder.html" frameborder="0" width="100%" height="600px"></iframe>
    </div>
@endsection