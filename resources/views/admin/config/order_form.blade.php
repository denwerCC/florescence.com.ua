@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}
@endsection
@section('javascript')
        {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
        <script>
            $(document).ready(function () {
                $('textarea').not('.noeditor').summernote({height: 120});
            });
        </script>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div id="order_form_wrapper" class="box-body">
            <!-- form start -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header"><i class="fa fa-gear"></i> {{$title}}
                    </li>
                </ul>
            </div>
            {{Form::open(array('route' => ['admin.order_form.update'],'class'=>"form-horizontal",'method'=>'post'))}}

            <div class="box-body">
                {{--{!! dd($form) !!}--}}
                <div class="box-body">
                    <div class="tab-content">
                            @foreach(array_keys(LaravelLocalization::getSupportedLocales()) as $lang_tab)
                                <div class="tab-pane @if (App::getLocale()==$lang_tab) active @endif" id="tab_{{$lang_tab}}">

                                    @if(isset($form[$type]) && is_array($form[$type]))
                                        <div class="order_form" class="form-group">

                                            @if(isset($form[$type]['header'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Заголовок</label>
                                                        {{Form::text($type.'[header]['.$lang_tab.']',$form[$type]['header'][$lang_tab],['class'=>'form-control', 'placeholder' => 'Заголовок'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['text'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Текст</label>
                                                        {{Form::textarea($type.'[text]['.$lang_tab.']',$form[$type]['text'][$lang_tab],['class'=>'form-control'])}}
                                                    </div>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
        </div>
        <!-- /.box-footer -->
        {{Form::close()}}
    </div>
@endsection