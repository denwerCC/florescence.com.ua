<ul class="sidebar-menu">
    <li class="header"><a href="/admincab/config">Налаштування</a></li>
    <li>
        <a href="{{route('admin.config.edit','liqpay')}}"> <i class="fa fa-usd"></i> <span>Налаштування LiqPay</span> </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','bank')}}"> <i class="fa fa-university"></i>
            <span>Платіжні системи</span> </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','email')}}"> <i class="fa fa-envelope"></i> <span>Налаштування повідомлень</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.translation.index')}}"> <i class="fa fa-language"></i> <span>Переклад</span> </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','social')}}"> <i class="fa fa fa-thumbs-o-up"></i>
            <span>Соціальні мережі</span> </a>
    </li>
    <li>
        <a href="{{route('admin.backup')}}"> <i class="fa fa-life-saver"></i> <span>Резервні копії</span> </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','redirect')}}"> <i class="fa fa-random"></i> <span>Перенаправлення</span> </a>
    </li>
    <li>
        <a href="{{route('admin.banners','index')}}"> <i class="fa fa-instagram"></i> <span>Банери</span> </a>
    </li>
    <li>
        <a href="{{route('admin.banners_subscribe','index')}}"> <i class="fa fa-instagram"></i> <span>Банери - Подписка</span> </a>
    </li>
    <li>
        <a href="{{route('admin.order_form','index')}}"> <i class="fa fa-instagram"></i> <span>Оформлення замовлення</span> </a>
    </li>
    <li>
        <a href="{{route('admin.calendar_settings','index')}}"> <i class="fa fa-instagram"></i> <span>Календарь событий настройки</span> </a>
    </li>
</ul>