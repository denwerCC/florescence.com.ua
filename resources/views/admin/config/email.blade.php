@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}
@endsection
@section('javascript')
    {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
    <script>
        $(document).ready(function () {
            $('textarea').summernote({height: 170});
        });
    </script>
@endsection
@section('content')

    <div class="box box-info">

        <!-- form start -->

        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-3">
                    <div class="box-header with-border">
                        <h3 class="box-title">Шаблон</h3>
                    </div>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">@{{$name}}<a class="pull-right">имя</a></li>
                        <li class="list-group-item">@{{$email}}<a class="pull-right">логин входа</a></li>
                        <li class="list-group-item">@{{$password}}<a class="pull-right">пароль входа</a></li>
                    </ul>

                </div>

                <div class="col-sm-9">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            @include('admin.lang-pagination')
                            <li class="pull-left header"><i class="fa fa-envelope"></i> Настройка письма подтверждения регистрации
                            </li>
                        </ul>
                    </div>
                    {{Form::open(array('route' => ['admin.config.update','email'],'class'=>"form-horizontal",'method'=>'put'))}}
                    <div class="tab-content">
                        @foreach ($emails as $lang=>$email)
                            <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{Form::text("email[$lang][registration][subject]",$email['order']['subject'],['class'=>'form-control','placeholder'=>'Тема письма'])}}
                                    </div>
                                    <div class="form-group">
                                        {{Form::textarea("email[$lang][registration][text]",$email['order']['text'],['class'=>'form-control'])}}
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer -->

    </div>

    <div class="box box-info">

        <!-- form start -->

        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-3">
                    <div class="box-header with-border">
                        <h3 class="box-title">Шаблон</h3>
                    </div>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">@{{$name}}<a class="pull-right">имя</a></li>
                        <li class="list-group-item">@{{$amount}}<a class="pull-right">сумма</a></li>
                        <li class="list-group-item">@{{$created_at}}<a class="pull-right">дата заказа</a></li>
                        <li class="list-group-item">@{{$link}}<a class="pull-right">ссылка заказа</a></li>
                    </ul>

                </div>

                <div class="col-sm-9">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            @include('admin.lang-pagination')
                            <li class="pull-left header"><i class="fa fa-envelope"></i> Настройка письма подтверждения
                                покупки
                            </li>
                        </ul>
                    </div>
                    {{Form::open(array('route' => ['admin.config.update','email'],'class'=>"form-horizontal",'method'=>'put'))}}
                    <div class="tab-content">
                        @foreach ($emails as $lang=>$email)
                            <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{Form::text("email[$lang][order][subject]",$email['order']['subject'],['class'=>'form-control','placeholder'=>'Тема письма'])}}
                                    </div>
                                    <div class="form-group">
                                        {{Form::textarea("email[$lang][order][text]",$email['order']['text'],['class'=>'form-control'])}}
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer -->

    </div>


    <div class="box box-warning">
        <!-- form start -->

        <div class="box-body">

            <div class="form-group">
                <div class="col-sm-3">
                    <div class="box-header with-border">
                        <h3 class="box-title">Шаблон</h3>
                    </div>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">@{{$name}}<a class="pull-right">имя</a></li>
                        <li class="list-group-item">@{{$amount}}<a class="pull-right">сумма</a></li>
                        <li class="list-group-item">@{{$created_at}}<a class="pull-right">дата заказа</a></li>
                        <li class="list-group-item">@{{$link}}<a class="pull-right">ссылка заказа</a></li>
                        <li class="list-group-item">@{{$tracking_number}}<a class="pull-right">номер декларации</a></li>
                        <li class="list-group-item">@{{$text}}<a class="pull-right">личное сообщение</a></li>
                    </ul>

                </div>

                <div class="col-sm-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            @foreach ($emails as $lang=>$email)
                                <li @if (App::getLocale()==$lang) class="active" @endif ><a href="#tab3_{{$lang}}"
                                                                                            data-toggle="tab">{{$lang}}</a>
                                </li>
                            @endforeach

                            <li class="pull-left header"><i class="fa fa-envelope"></i> Настройка уведомлений об
                                отправке заказа
                            </li>
                        </ul>
                    </div>
                    {{Form::open(array('route' => ['admin.config.update','email'],'class'=>"form-horizontal",'method'=>'put'))}}
                    <div class="tab-content">
                        @foreach ($emails as $lang=>$email)
                            <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab3_{{$lang}}">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{Form::text("email[$lang][delivery][subject]",$email['delivery']['subject'],['class'=>'form-control','placeholder'=>'Тема письма'])}}
                                    </div>
                                    <div class="form-group">
                                        {{Form::textarea("email[$lang][delivery][text]",$email['delivery']['text'],['class'=>'form-control'])}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-warning pull-right">Сохранить</button>
                    {{Form::close()}}
                </div>
            </div>


        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer -->

    </div>
@endsection