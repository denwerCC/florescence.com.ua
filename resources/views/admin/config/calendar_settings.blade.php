@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}
    <style>
        hr{
            border-bottom: 1px solid rgba(0,0,0,0.3)
        }
        textarea{
            max-height: 60px;
            margin-bottom: 20px;
        }
        input.form-control{
            margin-bottom: 10px;
        }
        label{
            cursor: pointer;
        }
        #size_products label{
            display: block;
            vertical-align: middle;
        }
        #size_products label:hover{
            background-color: rgba(60,141,188,0.4);
        }
        #size_products input[type="checkbox"]{
            width: 30px;
            height: 30px;
            margin-top: 16px;
            margin-right: 10px;
            position: relative;
            top: 10px;
        }
        #size_products input[type="checkbox"]:checked + span + a{
            border-left: 10px solid #3c8dbc;
        }
    </style>
@endsection
@section('javascript')
        {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
        <script>
            $(document).ready(function () {
                //$('textarea').not('.noeditor').summernote({height: 120});
            });
        </script>
@endsection
@section('content')

    <div class="box box-info">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div id="order_form_wrapper" class="box-body">
            <!-- form start -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header"><i class="fa fa-gear"></i> {{$title}}
                    </li>
                </ul>
            </div>
            {{Form::open(array('route' => ['admin.calendar_settings.update'],'class'=>"form-horizontal",'method'=>'post'))}}

            <div class="box-body">
                {{--{!! dd($form) !!}--}}
                <div class="box-body">
                    <div class="tab-content">
                            @foreach(array_keys(LaravelLocalization::getSupportedLocales()) as $lang_tab)
                                <div class="tab-pane @if (App::getLocale()==$lang_tab) active @endif" id="tab_{{$lang_tab}}">

                                    @if(isset($form[$type]) && is_array($form[$type]))
                                        <div class="order_form" class="form-group">

                                            @if(isset($form[$type]['e_title'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Заголовок "Эпаты"</label>
                                                        {{Form::text($type.'[e_title]['.$lang_tab.']',$form[$type]['e_title'][$lang_tab],['class'=>'form-control', 'placeholder' => 'Заголовок'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_header_1'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Заголовок этапа 1</label>
                                                        {{Form::text($type.'[e_header_1]['.$lang_tab.']',$form[$type]['e_header_1'][$lang_tab],['class'=>'form-control', 'placeholder' => 'Заголовок'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_text_1'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Текст этапа 1</label>
                                                        {{Form::textarea($type.'[e_text_1]['.$lang_tab.']',$form[$type]['e_text_1'][$lang_tab],['class'=>'form-control'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_header_2'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Заголовок этапа 2</label>
                                                        {{Form::text($type.'[e_header_2]['.$lang_tab.']',$form[$type]['e_header_2'][$lang_tab],['class'=>'form-control', 'placeholder' => 'Заголовок'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_text_2'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Текст этапа 2</label>
                                                        {{Form::textarea($type.'[e_text_2]['.$lang_tab.']',$form[$type]['e_text_2'][$lang_tab],['class'=>'form-control'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_header_3'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Заголовок этапа 3</label>
                                                        {{Form::text($type.'[e_header_3]['.$lang_tab.']',$form[$type]['e_header_3'][$lang_tab],['class'=>'form-control', 'placeholder' => 'Заголовок'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_text_3'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Текст этапа 3</label>
                                                        {{Form::textarea($type.'[e_text_3]['.$lang_tab.']',$form[$type]['e_text_3'][$lang_tab],['class'=>'form-control'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_header_4'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Заголовок этапа 4</label>
                                                        {{Form::text($type.'[e_header_4]['.$lang_tab.']',$form[$type]['e_header_4'][$lang_tab],['class'=>'form-control', 'placeholder' => 'Заголовок'])}}
                                                    </div>
                                            @endif
                                            @if(isset($form[$type]['e_text_4'][$lang_tab]))
                                                    <div class="col-sm-12">
                                                        <label for="">Текст этапа 4</label>
                                                        {{Form::textarea($type.'[e_text_4]['.$lang_tab.']',$form[$type]['e_text_4'][$lang_tab],['class'=>'form-control'])}}
                                                    </div>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-12">
            <hr>
            <h3>Размеры</h3>
            <div class="col-md-12">
                <div id="size_products" class="row">
                        {{--{!! dump($form) !!}--}}
                    @if(isset($form[$type]['sizes']))
                        @foreach($form[$type]['sizes'] as $label_s => $size)
                        <div class="col-md-4 size_item">
                            <div id="price">

                            <label for="">Размер "<strong style="text-transform: uppercase">{!! $label_s !!}</strong>" цена</label>
                            {{Form::number($type.'[sizes]['.$label_s.'][price]',$form[$type]['sizes'][$label_s]['price'],['class'=>'form-control'])}}
                            </div>
                            <div class="poroducts">
                                @if(isset($form[$type]['products']))
                                    @foreach($form[$type]['products'] as $item)
                                        <label for="{!! $label_s . $loop->index !!}">
                                            {{--isset($form[$type]['sizes'][$label_s]['items'][$item->id]) ? $form[$type]['sizes'][$label_s]['items'][$item->id] : ''--}}
                                            {{Form::checkbox(
                                            $type.'[sizes]['.$label_s.'][items]['.$item->id.']',
                                            $item->id,
                                            isset($form[$type]['sizes'][$label_s]['items'][$item->id]) ? $form[$type]['sizes'][$label_s]['items'][$item->id] : false,
                                            ['id' => $label_s.$loop->index]
                                            )}}
                                                <span>{!! $item->title !!}</span>
                                            <a href="/admincab/items/{!! $item->id !!}/edit" target="_blank" class="pull-right"><img src="{!! $item->image !!}" alt="" style="width: 80px"></a>
                                            <div class="clearfix"></div>
                                        </label>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Сохранить</button>
        </div>
        <!-- /.box-footer -->
        {{Form::close()}}
    </div>
@endsection