<ul class="sidebar-menu">
    <li class="header">Страницы</li>
    <li>
        <a href="{{route('admin.pages.create')}}">
            <i class="fa fa-file"></i> <span>Нова сторінка</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.pages.index')}}">
            <i class="fa fa-list-alt"></i> <span>Всі сторінки</span>
            <small class="label label-primary pull-right">{{$document['pagescount']}}</small>
        </a>
    </li>
    <li class="header">Рубрики</li>

    @if($count=$allpages->where('category_id',0)->count())
        <li>
            <a href="{{route('admin.pages.category',0)}}">
                <i class="fa fa-list-alt"></i> <span>Без категорії</span>
                <small class="label label-primary pull-right">{{$count}}</small>
            </a>
        </li>
    @endif

    @foreach($categories as $category)

        @if(!$category->childrens->isEmpty())

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>{{$category->title}}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu menu-open">
                    @foreach($category->childrens as $category)
                        <li><a href="{{route('admin.pages.category',$category->id)}}">{{$category->title}}<small class="label label-primary pull-right">{{$allpages->where('category_id',"$category->id")->count()}}</small></a></li>
                    @endforeach
                </ul>
            </li>

        @else
            <li>
                <a href="{{route('admin.pages.category',$category->id)}}">
                    <i class="fa fa-list-alt"></i> <span>{{$category->title}}</span>
                    <small class="label label-primary pull-right">{{$allpages->where('category_id',"$category->id")->count()}}</small>
                </a>
            </li>
        @endif
    @endforeach
    <li class="header">Налаштування</li>
    <li>
        <a href="{{route('admin.rubrics.index')}}">
            <i class="fa fa-calendar"></i> <span>Управління рубриками</span>
        </a>
    </li>
</ul>