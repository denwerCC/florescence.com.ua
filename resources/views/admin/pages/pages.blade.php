@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
    <style>
        table .reorder {
            cursor: move;
        }

        table.dt-rowReorder-float {
            position: absolute !important;
            opacity: 0.8;
            table-layout: fixed;
            outline: 1px solid #3C8DBC;
            outline-offset: -2px;
            z-index: 2001
        }

        tr.dt-rowReorder-moving {
            outline: 1px solid #3C8DBC;
            outline-offset: -2px
        }

        body.dt-rowReorder-noOverflow {
            overflow-x: hidden
        }

        table.dataTable td.reorder {
            text-align: center;
            cursor: move
        }
    </style>
@endsection
@section('sidebar')
    @include('admin.pages.sidebar')
@endsection
@section('javascript')

    <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="/admin/plugins/datatables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
    <script>
		$(function () {

			var table = $('#pages-table').DataTable({
				"language": {
					//"url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Ukrainian.json"
					"url": "/admin/plugins/datatables/i18n/Russian.json"
				},
				"rowReorder": {
					update: false
				},
				"processing": true,
				"paging": true,
				"pageLength": 50,
				"lengthChange": true,
				"searching": true,
				"order": [],
				"ordering": true,
				"info": true,
				"autoWidth": true,
				"serverSide": true,
				"ajax": document.location,
				"columns": [
					{"data": "id"},
					{"data": "thumb_html"},
					{"data": "title"},
					{"data": "rubricTitle"},
					{"data": "slug"},
					{"data": "updated_at"},
					{"data": "slug"}
					//{"data": "delete"}
				],
				"columnDefs": [
					{
						// The `data` parameter refers to the data for the cell (defined by the
						// `data` option, which defaults to the column being worked with, in
						// this case `data: 0`.

						"targets": 1,
						"render": function (data, type, row) {
							return "<a href='/" + row['slug'] + "' target='_blank'><img class='direct-chat-img' src='" + row['thumb'] + "'></a>";
						}
					},
					{
						"targets": 4,
						"render": function (data, type, row) {
							return "<a href='/" + row['slug'] + "' target='_blank'>" + row['slug'] + "</a>";
						}
					},
					//{"visible": false, "targets": [3]}
					{
						"targets": 6,
						"render": function (data, type, row) {

							addbutton = '';

							if (row['public'] == 1) {
								icon = 'fa-eye';
								iconclass = 'primary';
							} else {
								icon = 'fa-eye-slash';
								iconclass = 'default';
							}

						if (row['category_id'] == 5) {
								addbutton = '<a class="btn btn-info btn-xs" target="_blank" href="/admincab/pages/' + row['id'] + '/viewedit" data-original-title="Edit View" data-toggle="tooltip" data-placement="top" ><i class="fa fa-object-group "></i></a> ';
							}

							return addbutton + '<a class="btn btn-success btn-xs" href="/admincab/pages/' + row['id'] + '/edit"><i class="fa fa-pencil "></i></a> <a class="btn btn-' + iconclass + ' btn-xs visibility" data-original-title="Видимость" data-toggle="tooltip" data-placement="top" href="/admincab/pages/' + row['id'] + '/public"><i class="fa ' + icon + '"></i></a> <a class="btn delete btn-danger btn-xs confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="/admincab/pages/' + row['id'] + '/delete"><i class="glyphicon glyphicon-trash"></i> </a>';
						}
					}
				]
			}).on('row-reorder', function (e, diff, edit) {

				data = [];
				diff.forEach(function (item, i, arr) {
					console.log(item);
					data.push({'id': item.node.id, 'level': item.newPosition});

				});

				$.post('{{route('admin.pages.sort')}}', {'sort': data});

			});

			$(document).on('click', '.visibility', function () {

				if ($(this).hasClass('btn-primary')) {
					$(this).removeClass('btn-primary').addClass('btn-default');
					$('.fa', this).removeClass('fa-eye').addClass('fa-eye-slash');
				} else {
					$(this).removeClass('btn-default').addClass('btn-primary');
					$('.fa', this).removeClass('fa-eye-slash').addClass('fa-eye');
				}

				$.get(this.href);

				return false;
			});

			$(document).on('click', '.delete', function () {
				$.get(this.href, function (data) {
					table.draw(false);
					toastr[data.status](data.message);
				});
				return false;
			});

		});
    </script>
@endsection

@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <table aria-describedby="pages-table" role="grid" id="pages-table"
                    class="table table-bordered table-hover dataTable  table-striped">
                <thead>
                <tr role="row">
                    <th>id</th>
                    <th>Миниатюра</th>
                    <th>Название</th>
                    <th>Категория</th>
                    <th>Ссылка</th>
                    <th>Дата</th>
                    <th>Управление</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection