<style>
 @import "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css";

 .wow{
  visibility:visible!important;
 }
</style>

<div id="data-edit">
 {!! $content !!}
</div>

<style>
 #admin-toolbar{
  width: 100%;
  padding: 3px 0;
  position: fixed;
  left:0;
  bottom: 0;
  text-align: center;
  background-color: rgba(0,0,0,0.5);
  z-index: 999999999999999;
 }
 #admin-toolbar a{
  text-transform: uppercase;
  color: white;
 }
</style>

<div id="admin-toolbar">
 <a href="{{route('admin.view.clear',[$page->id,app()->getLocale()])}}" title="Обнулить шаблон"> <i class="fa fa-trash-o"></i>
 </a>
 {{--  <a id="undo" href="" title="Undo"> <i class="fa fa-undo"></i>  </a>
   <a id="redo" href="" title="Redo"> <i class="fa fa-repeat"></i>  </a>--}}
 <a href="" id="save" title="Save"> <i class="fa fa-save"></i> </a>
 <a href="{{route('admin.pages.index')}}" class="fa" title="Exit"> <i class="fa fa-sign-out"></i>  </a>
 |
 @foreach(langswitch() as $localeCode => $properties)
  <a class="lang-selector" rel="alternate" hreflang="{{$localeCode}}" href="{{$properties['url'] }}">
   {{ $properties['lang'] }}
  </a>
 @endforeach

</div>
<!-- Toastr script -->
<link href="/admin/plugins/toastr/toastr.min.css" rel="stylesheet">
<script src="/admin/plugins/toastr/toastr.min.js"></script>
<!-- / Toastr script -->

<script>
	$(function(){

		edits =  $('.fldata').attr('contenteditable',true);

		$('#redo').click(function (e) {
			edits.froalaEditor('commands.redo');
			return false;
		});

		$('#undo').click(function (e) {
			edits.froalaEditor('commands.undo');
			return false;
		});

		$('#save').click(function (e) {

			var k=0;
			var text = [];
			edits.each(function(){
				text.push($(this).html());
				k++;
			});

			$.ajax({
				type: "post",
				url: "{{route('admin.view.save',$view)}}",
				data: {
					_token : '{{csrf_token()}}',
					content : text
				},
				cache: false,
				beforeSend: function(){},
				success: function(msg){
					toastr.success('Успішно', msg);
				},
				error: function(error){
					toastr.error('Помилка!', error['status']);
				}
			});

			return false;
		});

	});

</script>