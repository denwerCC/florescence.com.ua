@extends('admin.admin')
@section('sidebar')
    @include('admin.pages.sidebar')
@endsection
@section('style')
    <link rel="stylesheet" href="/admin/plugins/uploader/css/uploadfile.css">
@endsection
@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')

                    <li class="pull-left header"><i class="fa fa-th"></i> <a class='inline-block' href="{{$Page->link}}"
                                                                             target="_blank">{{$Page->title}}</a></li>
                </ul>
            </div>

            {!! Form::open(array('route' => $route,'files' => true, 'method' => $method)) !!}
            <div class="col-sm-9">
                <div class="tab-content">
                    @foreach ($pages as $lang=>$page)

                        <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">

                            <div class="form-group">
                                {!! Form::label($lang."[title]", 'Название страницы', array('class' => '')) !!}
                                {!! Form::text($lang."[title]", $page->title, array('class'=>'form-control','placeholder'=>'Название страницы')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[meta_title]", 'Meta title', array('class' => '')) !!}
                                {!! Form::text($lang."[meta_title]", $page->meta_title, array('class'=>'form-control','placeholder'=>'Название страницы')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[meta_description]", 'Meta description', array('class' => '')) !!}
                                {!! Form::text($lang."[meta_description]", $page->meta_description, array('class'=>'form-control','placeholder'=>'Название страницы')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[slug]", 'Ссылка', array('class' => '')) !!}
                                {!! Form::text($lang."[slug]", $page->slug, array('class'=>'form-control','placeholder'=>'Ссылка')) !!}
                            </div>

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[description]", 'Описание', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[description]", $page->description, array('class'=>'form-control', 'rows'=>5)) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[content]", 'Контент', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[content]", $page->content, array('class'=>'form-control editor')) !!}
                                </div>
                            </div>

                        </div>

                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>

            <div class="col-sm-3">

                <div class="form-group">
                    {!! Form::label('category_id', 'Категория', array('class' => '')) !!}
                    {!! Form::select('category_id', $RubricTree,$Page->category_id,array('class'=>'form-control')) !!}
                </div>

                <img class="thumbnail img-responsive" src="{{$Page->thumb}}">

                <div class="form-group">
                    <div class="form-input-box">
                        {!! Form::label('img', 'Изображение', array('class' => '')) !!}
                        {!! Form::file('img', null, array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-append date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        {!! Form::text('date', $Page->date, array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div id="additional-images">

                        @foreach($Page->images as $img)
                            <div><a class='del confirm'
                                    href='{{route('admin.pages.deleteimg',['id'=>$Page->id,'image'=>$img])}}'>✕</a><img
                                        src='{{$Page->thumb($img)}}'><input type='text'
                                                                               name='imgs[{{$img}}][name]' value=''>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <div>

                        <div id="fileuploader"></div>

                    </div>
                </div><!-- /.form-group -->

                <div class="form-group">

                    {!! Form::checkbox('public', 1, $Page->public) !!}

                    {!! Form::label('public', 'Опубликовано', array('class' => '')) !!}
                </div>
                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>

            <div class="clearfix"></div>

            <div class="col-sm-12">

                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>

            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection
@section('javascript')
    <script type="text/javascript" src="/admin/plugins/ckeditor/ckeditor.js"></script>
    <script src="/admin/plugins/ckeditor/adapters/jquery.js"></script>

    <link rel="stylesheet" href="/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script type="text/javascript"
            src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script src="/admin/plugins/uploader/js/jquery.uploadfile.js"></script>
    <script type="text/javascript">

        $(function () {

            $('.date').datetimepicker({
                locale: 'uk',
                format: 'YYYY-MM-DD HH:mm',
            });


            {{--$('.editor').ckeditor({--}}
                {{--filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',--}}
                {{--filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',--}}
                {{--filebrowserBrowseUrl: '/laravel-filemanager?type=Files',--}}
                {{--filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'--}}
            {{--});--}}
            $('.editor').ckeditor({
                filebrowserBrowseUrl: '/vendor/elFinder-2.1.33/elfinder.html?type=Files'
            });

            // additional images
            $(document).on('click', '#additional-images .del', function () {

                if (!$(this).hasClass('inline')) {
                    $.get(this.href);
                }

                $(this).parent('div').detach();

                return false;

            });

            var imgContainer = $('#additional-images');

            $("#fileuploader").uploadFile({
                url: "{{route('admin.pages.uploadimg')}}",
                fileName: "img",
                formData: {_token: '{{csrf_token()}}'},
                showFileCounter: false,
                showFileSize: false,
                showProgress: false,
                statusBarWidth: 'auto',
                dragdropWidth: 'auto',
                previewWidth: 'auto',
                statusBarWidth: 'auto',
                uploadStr: "<?=l('Изображения')?>",
                uploadButtonClass: "ajax-file-upload btn btn-default",
                dragDropStr: "<span><b><?=l('Drag & Drop Files')?></b></span>",
                successCallback: function (pd,data) {

                    pd.preview
                        .addClass('loaded')
                        .before("<a href='#' class='del inline confirm'>✕</a><input type='hidden' name='imgs[" + data.file + "][name]'>");
                    pd.progressDiv.hide(200);
                },
                onError: function (files, status, message, pd) {
                    toastr.error('Ошибка! Файл ' + files[0] + ' не загружен!');
                    console.log(files, status, message, pd);
                }
            });

            // sortable
            $(imgContainer).sortable({
                placeholder: "ui-state-highlight",
                cursor: 'move',
                revert: true // animation
            });

        });
    </script>

@endsection