@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('javascript')
    <link rel="stylesheet" href="/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script type="text/javascript"
            src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <table aria-describedby="pages-table" role="grid" id="pages-table"
                           class="table table-bordered table-hover dataTable  table-striped">
                        <thead>
                        <tr role="row">
                            <th>id</th>
                            <th>{{l('Ім’я')}}</th>
                            <th>{{l('Прізвище')}}</th>
                            <th>{{l('Вік')}}</th>
                            <th>{{l('Звідки дізнались')}}</th>
                            <th>{{l('Дії')}}</th>
                        </tr>
                        </thead>
                        @foreach($subs as $n => $subscriber)
                            <tbody>
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{$subscriber->name}}</td>
                                <td>{{$subscriber->surname}}</td>
                                <td>{{$subscriber->age}}</td>
                                <td>{{$subscriber->from}}</td>
                                <td><a class="btn btn-danger btn-xs confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="{{route('ms.delete', $subscriber->id)}}"> <i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection