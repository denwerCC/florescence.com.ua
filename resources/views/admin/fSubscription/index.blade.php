@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('javascript')
    <link rel="stylesheet" href="/admin/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script type="text/javascript"
            src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <table aria-describedby="pages-table" role="grid" id="pages-table"
                           class="table table-bordered table-hover dataTable  table-striped">
                        <thead>
                        <tr role="row">
                            <th>id</th>
                            <th>{{l('Ім’я')}}</th>
                            <th>{{l('Телефон')}}</th>
                            <th>{{l('Дата оформлення підписки')}}</th>
                            <th>{{l('Адреса')}}</th>
                            <th>Email</th>
                            <th>{{l('Абонемент')}}</th>
                            <th>{{l('Час для доставки')}}</th>
                            <th>SIZE</th>
                            <th>{{l('Додаткова інформація')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        @foreach($subscribers as $n => $subscriber)
                        <tbody>
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{$subscriber->name}}</td>
                                <td>{{$subscriber->phone}}</td>
                                <td>{{$subscriber->created_at}}</td>
                                <td>{{$subscriber->address}}</td>
                                <td>{{$subscriber->email}}</td>
                                <td>{{$subscriber->abonement}}</td>
                                <td>{{$subscriber->time}}</td>
                                <td>{{$subscriber->size}}</td>
                                <td>{{$subscriber->information}}</td>
                                <td><a class="btn btn-danger btn-xs confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top" href="{{route('f.delete',$subscriber->id)}}"> <i class="glyphicon glyphicon-trash"></i></a></td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection