<ol class="dd-list">

    @foreach ($menus->load('childrens') as $menu)

        <li class="dd-item" data-id="{{$menu->id}}">

         <span class="btns pull-right">

				<a data-original-title="Редактировать" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.menus.edit',['menus'=>$menu->id])}}"><i class="fa fa-cog"></i></a>
				{{--<a data-original-title="Видимость" data-toggle="tooltip" data-placement="top"
                   href="/admincab/menu/result.php?command=change_visible&amp;type=1&amp;id=6"><i
                            class="fa fa-check"></i></a>--}}
				<a class="del confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.menus.delete',['menus'=>$menu->id])}}"><i
                            class="fa fa fa-times text-danger"></i></a>
			</span>
            <div class="dd-handle">
                <span class="label label-info">{{$menu->id}}</span> {{$menu->title}}
            </div>
            @if($menus=$menu->childrens)
                @include('admin.menus.list')
            @endif
        </li>

    @endforeach

</ol>