@extends('admin.admin')
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}
@endsection
@section('javascript')
    {!! Html::script(asset('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
    <script>
        $(document).ready(function () {
            $('#page-content').summernote({height: 400});
        });
    </script>
@endsection
@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header"><i class="fa fa-th"></i> ...</li>
                </ul>
            </div>
            {!! Form::open(array('route' => $route,'files' => true, 'method' => $method)) !!}
            <div class="col-sm-9">
                <div class="tab-content">

                    @foreach ($menus as $lang => $menu)
                        <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">

                            <div class="form-group">
                                {!! Form::label($lang."[title]", 'Название пункта меню', array('class' => '')) !!}
                                {!! Form::text($lang."[title]", $menu->title, array('class'=>'form-control','placeholder'=>'Название пункта меню','required'=>'required')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[slug]", 'Ссылка', array('class' => '')) !!}
                                {!! Form::text($lang."[slug]", $menu->slug, array('class'=>'form-control','placeholder'=>'Ссылка')) !!}
                            </div>
                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[description]", 'Описание', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[description]", $menu->description, array('class'=>'form-control')) !!}
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
                <!-- /.tab-content -->
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('parent_id', 'Категория', array('class' => '')) !!}
                    {!! Form::select('parent_id', $categories,$Menu->parent_id,array('class'=>'form-control')) !!}
                </div>
                {{--                <div class="form-group">
                                    <div class="form-input-box">
                                        {!! Form::label('img', 'Изображение', array('class' => '')) !!}
                                        {!! Form::file('img', null, array('class'=>'form-control')) !!}
                                    </div>
                                </div>--}}

            </div>

            <div class="clearfix"></div>

            <div class="col-sm-12">

                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>

            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection