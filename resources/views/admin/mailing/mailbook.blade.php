@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
@endsection
@section('javascript')
@endsection
@section('content')

    <div class="box box-info">

        <!-- form start -->

        <div class="box-body">

            <div class="form-group">

                <div class="col-sm-12">

                    {{Form::open(array('route' => 'admin.mailbook.update','class'=>"form-horizontal"))}}
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{Form::textarea("emails",$emails,['class'=>'form-control'])}}
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info pull-right">Сохранить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer -->

    </div>

@endsection