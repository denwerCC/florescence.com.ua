@extends('admin.admin')
@section('sidebar')
    @include('admin.config.sidebar')
@endsection
@section('style')
    <!-- include summernote css/js-->
    {!! Html::style(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css')) !!}
@endsection
@section('javascript')
    {!! Html::script(asset('//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')) !!}
    <script>
        $(document).ready(function () {
            $('textarea').summernote({height: 170});
        });
    </script>
@endsection
@section('content')

    <div class="box box-info">

        <!-- form start -->

        <div class="box-body">
            {{Form::open(array('route' => ['admin.mailing.update','email'],'class'=>"form-horizontal",'method'=>'put'))}}
            <div class="form-group">
                <div class="col-sm-3">
                    <div class="box-header with-border">
                        <h3 class="box-title">Адресаты</h3>
                    </div>

                    <ul class="list-group list-group-unbordered">
                        @foreach($targets as $key=>$target)
                            <li class="list-group-item">
                                <label>
                                    {{Form::radio("email[target]",$key)}}
                                    {!! $target !!}
                                </label>
                            </li>
                        @endforeach
                    </ul>

                </div>

                <div class="col-sm-9">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            @include('admin.lang-pagination')
                            <li class="pull-left header"><i class="fa fa-envelope"></i> Массовая рассылка писем
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            {{Form::text("email[subject]",old('email.subject'),['class'=>'form-control','placeholder'=>'Тема письма'])}}
                        </div>
                        <div class="form-group">
                            {{Form::textarea("email[text]",old('email.text'))}}
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info pull-right">Отправить</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer -->

    </div>

@endsection