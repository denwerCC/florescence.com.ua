<ul class="sidebar-menu">
    <li class="header">Настройки</li>
    <li>
        <a href="{{route('admin.config.edit','liqpay')}}">
            <i class="fa fa-usd"></i> <span>Настройки LiqPay</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','bank')}}">
            <i class="fa fa-university"></i> <span>Оплата через банк</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','delivery')}}">
            <i class="fa fa-truck"></i> <span>Настройки доставки</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','uapost')}}">
            <i class="fa fa-usd"></i> <span>Тарифы Укрпочты</span>
        </a>
    </li>
    <li>
        <a href="{{route('admin.config.edit','email')}}">
            <i class="fa fa-envelope"></i> <span>Настройки писем</span>
        </a>
    </li>
{{--    <li>
        <a href="{{route('admin.translation.index')}}">
            <i class="fa fa-language"></i> <span>Перевод</span>
        </a>
    </li>--}}
    <li>
        <a href="{{route('admin.config.edit','social')}}">
            <i class="fa fa fa-thumbs-o-up"></i> <span>Социальные сети</span>
        </a>
    </li>
</ul>