@foreach (Config::get('app.locales') as $lang=>$langData)
    <li @if (App::getLocale()==$lang) class="active" @endif ><a href="#tab_{{$lang}}"
                                                                data-toggle="tab">{{$langData[1]}}</a>
    </li>
@endforeach