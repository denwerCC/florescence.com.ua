@extends('admin.admin')
@section('sidebar')
    @include('admin.items.sidebar')
@endsection
@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header"><i class="fa fa-th"></i> ...</li>
                </ul>
            </div>

            {!! Form::open(array('route' => $route,'files' => true, 'method' => $method)) !!}

            <div class="col-sm-9">
                <div class="tab-content">
                    @foreach ($filters as $lang=>$filter)

                        <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">

                            <div class="form-group">
                                {!! Form::label($lang."[title]", 'Название параметра', array('class' => '')) !!}
                                {!! Form::text($lang."[title]", $filter->title, array('class'=>'form-control','placeholder'=>'Название параметра')) !!}
                            </div>

                            @if(isset($filter->parent->type))
                                @if($filter->childs->isEmpty())
                                    {{--todo Create dynamic parameters selections--}}
                                @endif
                                @if($filter->parent->type==1)
                                    @include('admin.filters.type.color')
                                @elseif($filter->parent->type==2)
                                    @include('admin.filters.type.image')
                                @endif
                            @endif

                            {{--                            <div class="form-group">
                                                            <div class="form-input-box">
                                                                {!! Form::label('img', 'Изображение', array('class' => '')) !!}
                                                                {!! Form::file('img', null, array('class'=>'form-control')) !!}
                                                            </div>
                                                        </div>--}}

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang.'[description]', 'Описание', array('class' => '')) !!}
                                    {!! Form::textarea($lang.'[description]', $filter->description, array('class'=>'form-control')) !!}
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
                <!-- /.tab-content -->
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('parent_id', 'Категория', array('class' => '')) !!}
                    {!! Form::select('parent_id', [null=>'Без категории']+ $allFilters,$Filter->parent_id,array('class'=>'form-control')) !!}
                </div>

                @if(empty($filter->parent->type))
                    <div class="form-group">
                        {!! Form::label('type', 'Тип', array('class' => '')) !!}
                        {!! Form::select('type', $filter->types,$filter->type,array('class'=>'form-control')) !!}
                    </div>
                @endif
                {{--                <div class="form-group">
                                    <div class="form-input-box">
                                        {!! Form::label('img', 'Изображение', array('class' => '')) !!}
                                        {!! Form::file('img', null, array('class'=>'form-control')) !!}
                                    </div>
                                </div>--}}

            </div>

            <div class="clearfix"></div>

            <div class="col-sm-12">

                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>

            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
    </div>
@endsection