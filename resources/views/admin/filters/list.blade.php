<ol class="dd-list">

    @foreach ($filters as $filter)

        <li class="dd-item" data-id="{{$filter->id}}">

         <span class="btns pull-right">

				<a data-original-title="Редактировать" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.filters.edit',['filters'=>$filter->id])}}"><i class="fa fa-cog"></i></a>
				{{--<a data-original-title="Видимость" data-toggle="tooltip" data-placement="top"
                   href="/admincab/menu/result.php?command=change_visible&amp;type=1&amp;id=6"><i
                            class="fa fa-check"></i></a>--}}
				<a class="del confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.filters.delete',['filters'=>$filter->id])}}"><i
                            class="fa fa fa-times text-danger"></i></a>
			</span>
            <div class="dd-handle">
                <span class="label label-info">{{$filter->id}}</span> {{$filter->title}}
            </div>
            @if($filters=$filter->childs->load('childs'))
                @include('admin.filters.list')
            @endif
        </li>

    @endforeach

</ol>