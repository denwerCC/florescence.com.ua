@section('style')
    <link rel="stylesheet" href="/admin/uploader/css/uploadfile.css">
    <link rel="stylesheet" href="/admin/js/jquery.fileapi/main.css">
    <link rel="stylesheet" href="/admin/js/jquery.fileapi/jcrop/jquery.Jcrop.min.css">

    <style>
        #image {
            text-align: center;
        }

        #img-preview{
            margin-top: 15px;
        }

        #img-preview,.image__preview{
            position: relative;
            width: 230px;

        }

        #img-preview img{
            width: 100%;
        }

        #img-preview div{
            position: absolute;
            top:0;
            left: 0;
        }

        .b-upload__dnd {
            height: 46px;
        }

    </style>

@endsection


<div class="form-group">

    <div style="width: 230px;">

        <div id="image" class="image b-upload b-upload_dnd">
            <div class="b-upload__dnd">
                <div class="js-browse">
                    <label class="add-self"><span class='upload'><i
                                    class="fa fa-camera fa-1x"></i> Загрузить фото</span>
                        <input name="filedata" type="file">
                    </label>
                    {!! Form::hidden($lang.'[slug]', $filter->slug, array('class'=>'synchronize')) !!}
                </div>
            </div>

            <div id="popup" class="popup" style="display: none;">
                <div class="popup__body">
                    <div class="js-img"></div>
                </div>
                <div style="margin: 0 0 5px; text-align: center;">
                    <div class="js-upload btn btn_browse btn_browse_small">Загрузить</div>
                </div>
            </div>
            <div id="img-preview">
                @if($filter->slug)
                    <img src="{{$filter->image}}" alt="">
                @endif
                    <div class="js-preview image__preview">

                </div>
            </div>

        </div>
    </div>
</div><!-- /.form-group -->



@section('javascript')

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    {!! Html::script(asset('/admin/js/jquery.fileapi/FileAPI/FileAPI.min.js')) !!}
    {!! Html::script(asset('/admin/js/jquery.fileapi/FileAPI/FileAPI.exif.js')) !!}
    {!! Html::script(asset('/admin/js/jquery.fileapi/jquery.fileapi.min.js')) !!}
    {!! Html::script(asset('/admin/js/jquery.fileapi/jcrop/jquery.Jcrop.min.js')) !!}
    {!! Html::script(asset('/admin/js/jquery.fileapi/jquery.modal.js')) !!}

    <script>

        $(function () {

            var imgContainer = $('#additional-images');

            preview = $('.image__preview');

            $('#image').fileapi({
                url: '{{route('admin.filters.uploadimg',$filter->id)}}',
                data: {_token: '{{csrf_token()}}'},
                paramName: 'img',
                accept: 'image/*',
                multiple: false,
                //autoUpload: true,
                imageSize: {minWidth: 230, minHeight: 230},
                elements: {
                    active: {show: '.js-upload', hide: '.js-browse'},
                    preview: {
                        el: '.js-preview',
                        width: 230,
                        height: 230
                    },
                    progress: '.js-progress',
                    dnd: {
                        el: '.b-upload__dnd',
                        hover: 'b-upload__dnd_hover',
                        fallback: '.b-upload__dnd-not-supported',
                    }
                },
                onSelect: function (evt, ui) {
                    modalcrop(evt, ui);
                },
                onDrop: function (evt, ui) {
                    modalcrop(evt, ui);
                },
                onBeforeUpload: function (evt, uiEvt) {
                },
                onFileComplete: function (evt, ui) {

                    response = ui.result;
                    if (response.status == 'error') {
                        alert(response.message);
                    } else {
                        $('.synchronize').val(response.file).change();
                    }
                },
            });

            function modalcrop(evt, ui) {

                var file = ui.files[0];
                var filetype = file.type;

                if (!filetype.match(/image\/(gif|png|jpg|jpeg)$/)) {
                    alert('Помилка! Використовуйте тільки зображення в форматі gif, png, jpg або jpeg')
                } else if (!FileAPI.support.transform) {
                    alert('Your browser does not support Flash :(');
                }
                else if (file) {
                    $('#popup').modal({
                        closeOnEsc: true,
                        closeOnOverlayClick: false,
                        onOpen: function (overlay) {
                            $(overlay).on('click', '.js-upload', function () {
                                $.modal().close();
                                $('#image').fileapi('upload');
                            });
                            $('.js-img', overlay).cropper({
                                file: file,
                                bgColor: '#fff',
                                //aspectRatio: 3 / 4,
                                maxSize: [$(window).width() - 100, $(window).height() - 40],
                                minSize: [20, 20],
                                selection: '100%',
                                onSelect: function (coords) {
                                    $('#image').fileapi('crop', file, coords);
                                }
                            });
                        }
                    }).open();
                }
            }


        });
    </script>

@endsection