@extends('admin.admin')
@section('style')
    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection
@section('javascript')
@endsection

@section('content')

    <a href="{{route('admin.backup.create')}}" class="btn btn-block btn-primary">Создать резервную копию</a>

    <table aria-describedby="pages-table" role="grid" id="pages-table"
           class="table table-bordered table-hover dataTable  table-striped">
        <thead>
        <tr role="row">
            <th>№</th>
            <th>Файл</th>
            <th>Время</th>
            <th>Размер</th>
            <th>Управление</th>
        </tr>
        </thead>

        <tbody>

        @foreach($files as $n=>$file)
        <tr>
            <td>{{$n++}}</td>
            <td>{{$file['name']}}</td>
            <td>{{$file['date']}}</td>
            <td>{{$file['size']}} mb</td>
            <td>
                <a class="btn btn-success btn-xs" href="{{route('admin.backup.restore',$file['name'])}}"><i class="fa fa-upload"></i> Восстановить</a>

                <a class="btn btn-danger btn-xs confirm pull-right" data-original-title="Удалить резервную копию" data-toggle="tooltip" data-placement="top" href="{{route('admin.backup.delete',$file['name'])}}"><i class="glyphicon glyphicon-trash"></i> </a>

               {{-- <a class="btn btn-primary btn-xs" data-original-title="Скачать" data-toggle="tooltip" data-placement="top" href="#"><i class="fa fa fa-download"></i></a>--}}
            </td>
        </tr>

@endforeach

        </tbody>

    </table>

@endsection