<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>LightAdmin2 CMS Login Page</title>
    <meta name="msapplication-TileColor" content="#5bc0de"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link rel="icon" href="/admin/img/favicon.png">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css">

    <style>

        body.login {
            padding-top: 40px;
            padding-bottom: 40px;
            background: url("/admin/img/low_contrast_linen.png") repeat #444;
        }

        .login * {
            border-radius: 0
        }

        .login .form-signin {
            box-shadow: 0px 0px 15px black;
            -webkit-box-shadow: 0px 0px 15px black;
            -moz-box-shadow: 0px 0px 15px;
            max-width: 330px;
            padding: 20px;
            margin: 0 auto;
            background-color: #fff;
        }

        .login .form-signin .form-signin-heading, .login .form-signin .checkbox {
            margin-bottom: 10px
        }

        .login .form-signin .checkbox {
            font-weight: normal
        }

        .login .form-signin input[type="text"], .login .form-signin input[type="password"], .login .form-signin input[type="email"] {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            margin-bottom: 10px
        }

        .login .form-signin input[type="text"]:focus, .login .form-signin input[type="password"]:focus, .login .form-signin input[type="email"]:focus {
            z-index: 2
        }

        .login .form-signin input.top {
            margin-bottom: -1px;
        }

        .login .form-signin input.middle {
            margin-bottom: -1px;
        }

        .login .form-signin input.bottom {
        }

        .navbar-brand > img {
            display: block;
            margin-top: 6px;
            width: 238px;
            margin-left: 5px;
        }

        .logo {
            background: url(/admin/img/low_contrast_linen.png);
            padding: 10px;
        }

        .logo img {
            width: 100%;
        }
    </style>
</head>
<body class="login">
<div class="form-signin">
    <div class="logo text-center">
        <img src="/admin/img/logo.png" alt="CMS Logo">
    </div>
    <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active">

            <form action="{{ url('/login') }}" method='post'>
                {{ csrf_field() }}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                @if ($errors->has('password'))
                    <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

                <p class="text-muted text-center">
                    {{trans('admin.Enter your username and password')}}
                </p>
                <input type="text" name="email" value="{{ old('email') }}" placeholder="{{trans('admin.Username')}}"
                       class="form-control top">
                <input type="password" name="password" placeholder="{{trans('admin.Password')}}"
                       class="form-control bottom">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> {{trans('admin.Remember Me')}}
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">{{trans('admin.Login')}}</button>
            </form>
        </div>
        <div id="forgot" class="tab-pane">
            <form action="">
                <p class="text-muted text-center">{{trans('admin.Enter your valid e-mail')}}</p>
                <input type="email" placeholder="mail@{{$_SERVER['SERVER_NAME']}}" class="form-control">
                <br>
                <button class="btn btn-lg btn-danger btn-block"
                        type="submit">{{trans('admin.Recover Password')}}</button>
            </form>
        </div>

    </div>
    <hr>
    <div class="text-center">
        <ul class="list-inline">
            <li><a class="text-muted" href="#login" data-toggle="tab">{{trans('admin.Login')}}</a></li>
            <li><a class="text-muted" href="#forgot" data-toggle="tab">{{trans('admin.Forgot Password')}}</a></li>
        </ul>
    </div>
</div>

<!--jQuery -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<!--Bootstrap -->
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('.list-inline li > a').click(function () {
                var activeForm = $(this).attr('href') + ' > form';
                //console.log(activeForm);
                $(activeForm).addClass('animated fadeIn');
                //set timer to 1 seconds, after that, unload the animate animation
                setTimeout(function () {
                    $(activeForm).removeClass('animated fadeIn');
                }, 1000);
            });
        });
    })(jQuery);
</script>
</body>
</html>

