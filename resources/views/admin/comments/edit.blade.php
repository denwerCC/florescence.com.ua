@extends('admin.admin')
@section('content')

    <div class="row">

        <div class="col-md-8">
        {!! Form::open(array('route' => ['admin.comments.update',$comment->id],'files' => true, 'method' => 'put','class'=>'form-horizontal')) !!}
        <!-- Default box -->
            <div class="box box-info">

                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user "></i> Покупатель</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label("name", 'Имя', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-4">
                            {!! Form::text("name", $comment->name, array('class'=>'form-control','placeholder'=>'Имя')) !!}
                        </div>

                        {!! Form::label("public", 'Опубликован', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-4" style="padding-top: 8px;">
                            {!! Form::checkbox("public", 1,$comment->public) !!}
                        </div>

                    </div>

                    <div class="form-group">
                        {!! Form::label("email", 'E-mail', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-4">
                            {!! Form::text("email", $comment->email, array('class'=>'form-control','placeholder'=>'E-mail')) !!}
                        </div>

                        {!! Form::label("email", 'Телефон', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-4">
                            {!! Form::text("phone", $comment->phone, array('class'=>'form-control','placeholder'=>'Телефон')) !!}
                        </div>

                    </div>

                    <div class="form-group">
                        {!! Form::label("text", 'Комментарий', array('class' => 'control-label col-lg-2')) !!}
                        <div class="col-lg-10">
                            {!! Form::textarea("text", $comment->text, array('class'=>'form-control','placeholder'=>'Комментарий')) !!}
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    {!! Form::submit('Сохранить', array('class'=>'btn btn-info pull-right')) !!}
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {!! Form::close() !!}
        </div>

        <div class="col-md-4">

            <!-- Default box -->
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-calendar"></i> {{$comment->created_at}}</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            @if(isset($comment->item))
                                <tr>
                                    <th style="width:50%">Страница комментария:</th>
                                    <td><a href="{{$comment->item->link}}">{{$comment->item->title}}</a></td>
                                </tr>
                            @endif
                            <tr>
                                <th>IP пользователя:</th>
                                <td>{{$comment->ip}}</td>
                            </tr>
                            <tr>
                                <th>Статус пользователя:</th>
                                <td>{{$comment->user? 'зарегистрирован' : 'не зарегистрирован' }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <!-- Default box -->
        <div class="col-md-8">
            <div class="box">

                @if($comment->answer)
                    {!! Form::open(array('route' => ['admin.comments.update',$comment->answer->id], 'files' => true, 'method' => 'put','class'=>'form-horizontal')) !!}
                @else
                    {!! Form::open(array('route' => ['comments.store','comment',$comment->id], 'files' => true, 'method' => 'post','class'=>'form-horizontal')) !!}
                @endif

                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-th"></i> Ответить от имени администратора</h3>
                </div>
                <div class="box-body">

                    <div class="form-input-box">
                        {{Form::textarea('text',$comment->answer ? $comment->answer->text : "",['class'=>'form-control'])}}
                    </div>

                    <div class="clearfix"></div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {!! Form::submit('Ответить', array('class'=>'btn btn-info pull-right')) !!}
                </div>
                <!-- /.box-footer-->
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection