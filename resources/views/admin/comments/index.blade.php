@extends('admin.admin')
@section('style')
    <style>
        .direct-chat-messages {
            height: 100%;
        }
    </style>
@endsection
@section('javascript')

    <script>
        $(function () {

        });
    </script>
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="box box-primary direct-chat direct-chat-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Комментарии</h3>

                    <div class="box-tools pull-right">
                        <span data-original-title="3 New Messages" data-toggle="tooltip" title=""
                                class="badge bg-light-blue">{{$comments->count()}}</span>

                    </div>
                </div>
                <!-- /.box-header -->
                <div style="display: block;" class="box-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">

                    @foreach($comments as $comment)

                        <!-- Message. Default to the left -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name">{{$comment->name}}</span>
                                    <span class="direct-chat-name  pull-right">
                                <a class="btn btn-xs" href="{{route('admin.comments.edit',$comment->id)}}"> <i class="fa fa-pencil  text-success"></i> </a>
                                        <a class="btn btn-xs confirm"
                                                href="{{route('admin.comments.delete',$comment->id)}}"><i
                                                    class="glyphicon glyphicon-trash text-danger"></i> </a>
</span>
                                    <span class="direct-chat-timestamp">{{$comment->created_at}}</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <a href="{{$comment->email}}" target="_blank"><img class="direct-chat-img" src="{{$comment->image}}" alt="User Image"></a>
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    {{$comment->text}}
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>

                    @endforeach

                    <!-- /.direct-chat-msg -->

                    </div>
                    <!--/.direct-chat-messages-->

                    <!-- /.direct-chat-pane -->
                </div>
                <!-- /.box-body -->
            {{--<div style="display: block;" class="box-footer">

                <h4>Создать комментарий</h4>

                {!! Form::open(array('route' => 'admin.comments.store', 'files' => true, 'method' => 'post','class'=>'form-horizontal')) !!}

                <div class="form-group">
                    <div class="col-sm-4">
                        <input name="name" placeholder="Имя" class="form-control" type="text">
                    </div>
                    <div class="col-sm-4">
                        <input name="email" placeholder="Ссылка на Facebook" class="form-control" type="text">
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-photo"></i></span>
                            <input class="form-control" name="img" type="file">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea name="text" cols="30" rows="5" placeholder="Сообщение ..." class="form-control"></textarea>
                    </div>

                </div>
                <button type="submit" class="btn btn-primary btn-flat">Сохранить</button>

                {!! Form::close() !!}
            </div>--}}
            <!-- /.box-footer-->
            </div>
        </div>
    </div>
@endsection