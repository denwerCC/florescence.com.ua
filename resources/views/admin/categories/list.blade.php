<ol class="dd-list">

    @foreach ($cats->load('childrens')->sortBy('level') as $category)

        <li class="dd-item" data-id="{{$category->id}}">

         <span class="btns pull-right">

				<a data-original-title="Редактировать" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.categories.edit',['categories'=>$category->id])}}"><i class="fa fa-cog"></i></a>
				{{--<a data-original-title="Видимость" data-toggle="tooltip" data-placement="top"
                   href="/admincab/menu/result.php?command=change_visible&amp;type=1&amp;id=6"><i
                            class="fa fa-check"></i></a>--}}
				<a class="del confirm" data-original-title="Удалить" data-toggle="tooltip" data-placement="top"
                   href="{{route('admin.categories.delete',['categories'=>$category->id])}}"><i
                            class="fa fa fa-times text-danger"></i></a>
			</span>
            <div class="dd-handle">
                <span class="label label-info">{{$category->id}}</span> {{$category->title}}
            </div>
            @if($cats=$category->childrens)
                @include('admin.categories.list')
            @endif
        </li>

    @endforeach

</ol>