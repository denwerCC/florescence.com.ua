@extends('admin.admin')
@section('sidebar')
    @include('admin.items.sidebar')
@endsection
@section('content')

    <!-- Default box -->
    <div class="box">

        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @include('admin.lang-pagination')
                    <li class="pull-left header"><i class="fa fa-th"></i> ...</li>
                </ul>
            </div>
            {!! Form::open(array('route' => $route,'files' => true, 'method' => $method)) !!}
            <div class="col-sm-9">
                <div class="tab-content">

                    @foreach ($Categories as $lang => $category)
{{--                        {!! var_dump($category); !!}--}}
                        <div class="tab-pane @if (App::getLocale()==$lang) active @endif" id="tab_{{$lang}}">

                            <div class="form-group">
                                {!! Form::label($lang."[h1]", 'Заголовок h1', array('class' => '')) !!}
                                {!! Form::text($lang."[h1]", $category->h1, array('class'=>'form-control','placeholder'=>'Заголовок h1')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label($lang."[title]", 'Название категории', array('class' => '')) !!}
                                {!! Form::text($lang."[title]", $category->title, array('class'=>'form-control','placeholder'=>'Название категории')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[slug]", 'Ссылка', array('class' => '')) !!}
                                {!! Form::text($lang."[slug]", $category->slug, array('class'=>'form-control','placeholder'=>'Ссылка')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[meta_title]", 'Meta title', array('class' => '')) !!}
                                {!! Form::text($lang."[meta_title]", $category->meta_title, array('class'=>'form-control','placeholder'=>'Название позиции')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label($lang."[meta_description]", 'Meta description', array('class' => '')) !!}
                                {!! Form::text($lang."[meta_description]", $category->meta_description, array('class'=>'form-control','placeholder'=>'Название позиции')) !!}
                            </div>

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[description]", 'Описание', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[description]", $category->description, array('class'=>'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-input-box">
                                    {!! Form::label($lang."[content]", 'Контент', array('class' => '')) !!}
                                    {!! Form::textarea($lang."[content]", $category->content, array('class'=>'form-control editor')) !!}
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
                <!-- /.tab-content -->
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('parent_id', 'Категория', array('class' => '')) !!}
                    {!! Form::select('parent_id', [ null =>'Без категории']+ array_except($categories->pluck('title', 'id')->toArray(),$Category->id),$Category->parent_id,array('class'=>'form-control')) !!}
                </div>

                <img class="thumbnail img-responsive" src="{{$Category->thumb}}?rand={{rand(1,999999)}}">

                <div class="form-group">
                    <div class="form-input-box">
                        {!! Form::label('img', 'Изображение', array('class' => '')) !!}
                        {!! Form::file('img', null, array('class'=>'form-control')) !!}
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-sm-12">

                {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}
            </div>

            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            ...
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
@endsection
@section('javascript')
<script type="text/javascript" src="/admin/plugins/ckeditor/ckeditor.js"></script>
<script src="/admin/plugins/ckeditor/adapters/jquery.js"></script>
<script>
    $('.editor').ckeditor({
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
    });
</script>
@endsection