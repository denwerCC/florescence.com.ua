@extends('admin.admin')
@section('sidebar')
    @include('admin.items.sidebar')
@endsection
@section('content')

    @include('admin.categories.create')
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Список разделов</h3>
        </div>
        <div class="box-body">
            <div class="dd" id="menu">
                @include('admin.categories.list')
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="/admin/plugins/nestable/jquery.nestable.js"></script>
    <script>
        $(document).ready(function () {

            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target);

                if (window.JSON) {

                    var data = {
                        '_token' : '{{ csrf_token() }}',
                        'data' : list.nestable('serialize')
                    };

                    $.post('{{route('admin.categories.sort')}}',data);

                } else {
                    alert('JSON browser support required for sorting.')
                }
            };

            $('#menu').nestable({
                group: 1
            }).on('change', updateOutput);


            $('.del').unbind('click').click(function () {
                if (!confirm("При уадалении категории будут удалены все дочерние категории и все товары в этих категориях. Вы уверены?")) {
                    return false;
                }
            })

        });
    </script>
@endsection