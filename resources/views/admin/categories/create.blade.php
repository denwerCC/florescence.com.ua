<div class="box">
    <div class="box-body">

        {!! Form::open(array('route' => 'admin.categories.store','files' => true, 'method' => 'post')) !!}
        <div class="row">
            <div class="col-xs-5">
                <input class="form-control" placeholder="Новый раздел" name="title" required/>
            </div>
            <div class="col-xs-5">
                {!! Form::select('parent_id', [''=>'Без категории'] + $categories->pluck('title','id')->toArray(),'',array('class'=>'form-control')) !!}
            </div>
            <div class="col-xs-2">
                <input class="btn btn-primary btn-block" type="submit" value="Сохранить">
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box-body -->
</div>