@extends('layouts.florescence')
@section('title',  $page->meta_title)
@section('page_class',  'subscribe_page')
@section('description', $page->meta_description)


@section('content')
    <div id="wedding_design_line"></div>
    <div id="container">
        <div class="container">
            <div id="breadcrumb">
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                </div>

                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('flowers-sub')}}" itemprop="url">
                        <span itemprop="title">{{$page->title}}</span> </a>
                </div>

            </div>

            @if (isset($page->h1) && !empty($page->h1))
                <h1>{{$page->h1}}</h1>
            @endif

            @if(!empty($banners) && is_array($banners))
                <div id="subscribe_slider" class="carousel slide" data-ride="carousel">
                    <div class="container">
                        <ol class="carousel-indicators">
                            @foreach($banners as $paginate_index => $banner)
                                <li data-target="#subscribe_slider" data-slide-to="{!! $paginate_index !!}"
                                    class="{!! ($paginate_index == 0) ? 'active' : '' !!}"></li>
                            @endforeach
                        </ol>

                    </div>
                    <div class="carousel-inner" role="listbox">
                        @foreach($banners as $banner_index => $banner)
                            <div class="carousel-item {!! ($banner_index == 0) ? 'active' : '' !!}">
                                @if(isset($banner['img']))
                                    <img src="{!! $banner['img'] !!}" alt="">
                                @endif

                                <div class="carousel-caption">
                                    @if(isset($banner['text'][App::getLocale()]))
                                        <div class="title">{!! $banner['text'][App::getLocale()] !!}</div>
                                    @endif
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <div class="container d-none">
                        <a class="carousel-control-prev" href="#subscribe_slider" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#subscribe_slider" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            @endif

            <div class="col-md-12">
                {!!$page->content!!}
            </div>

            <div id="subscribe_sizes" class="row">
                @foreach($pages as $page)
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <a class="size_item" href="{{route('flower.subscribe',$page->slug)}}">
                            {{--<img src="{{$page->image}}" alt="">--}}
                            <div class="size text-center">
                                {{str_replace('size-', '', $page->slug)}}
                            </div>
                            <div class="size_label text-center">
                                {!! trans('new_app.Розмір') !!}
                            </div>
                            <div class="size_desc text-center">
                                {{$page->description}}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>


    @include('parts.contacts_block')
@endsection