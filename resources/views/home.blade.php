@extends('layouts.florescence')
@section('title',  $page->meta_title)
@section('page_class',  'home')
@section('description', $page->meta_description)
@section('head_styles')
@endsection
@section('content')
    @if(!empty($banners) && is_array($banners))
        <div id="home_slider" class="carousel slide" data-ride="carousel">

            <div id="home_slider_arrows" class="container">
                <a class="carousel-control-prev" href="#home_slider" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#home_slider" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="carousel-inner" role="listbox">
                @foreach($banners as $banner_index => $banner)
                    <div class="carousel-item {!! ($banner_index == 0) ? 'active' : '' !!}"
                         style="background-image: url('{!! isset($banner['img']) ? $banner['img'] : '' !!}')">
                        <div class="container">
                            <div class="row">
                                <div class="carousel-caption">
                                    @if(isset($banner['text'][App::getLocale()]))
                                        <div class="title">{!! $banner['text'][App::getLocale()] !!}</div>
                                    @endif
                                    @if(isset($banner['link']) && !empty($banner['link']))
                                        <a class="link" href="{!! $banner['link'] !!}">{!!  l('В каталог') !!}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="container">
                <ol class="carousel-indicators">
                    @foreach($banners as $paginate_index => $banner)
                        <li data-target="#home_slider" data-slide-to="{!! $paginate_index !!}"
                            class="{!! ($paginate_index == 0) ? 'active' : '' !!}"></li>
                    @endforeach
                </ol>

            </div>
        </div>
    @endif



    <section id="categories">
        <div class="container">
            <h2 class="section-title">{{l('Послуги')}}</h2>
            <div class="row">
                @foreach($categories as  $n => $category)
                    @if($n == 2)
                        <div class="col-md-4 col-sm-12">
                            <div class="category_item">
                                <div class="photo_flower_block_inner">
                                    <div class="text-center"
                                         style="margin-bottom: 10px;">{{l('Завантажте фото сюди')}}
                                        <br>
                                        {{l('І ми зробимо такий букет')}}
                                    </div>
                                    <span data-toggle="modal" data-target="#upload_photo_form"><i class="fas fa-download"></i></span>
                                </div>
                                <img src="/img/main-page-photo6.png" alt="" class="category_item_img"><div class="title"><img src="/img/category_title_left.png" alt="">{{l('Букет по фото')}}<img src="/img/category_title_right.png" alt=""></div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-4 col-sm-12">
                        <div class="category_item">
                            <a href="{{route('catalog.category',$category->slug)}}">
                                <img src="{{$category->image(350,350)}}" alt="" class="category_item_img"><div class="title"><img src="/img/category_title_left.png" alt="">{{$category->title}}<img src="/img/category_title_right.png" alt=""></div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>


        </div>
    </section>
    <div id="container">
    </div>

    <section id="blog">
        <div class="container d-none d-md-block">
            <div class=" position-relative">
                <img src="/public/img/blog_section.png" alt="" class="blog_u">
            </div>
        </div>
        <div class="container">
            <h2 class="section-title">{{l('Блог')}}</h2>
            <div id="articles">
                @foreach($articles as $num_article => $article)
                    @if($article->public == 1)
                        <div class="blog_item">
                            <div class="row no-gutters">

                                <div class="col-md-6 col-sm-12 item_img">
                                    <a href="{{route('page',$article->slug)}}"><img class="blog_img"
                                                                                    src="{{$article->image}}"
                                                                                    alt="{{$article->title}}"/></a>
                                </div>
                                <div class="col-md-6 col-sm-12 ">
                                    <div class="item_text">

                                    <h3 onclick="window.location='{{route('page',$article->slug)}}';">{{$article->title}}</h3>
                                    <span class="blog-item-time time">{{dateFormat($article->date)}}</span>
                                    <p>
                                        {{getWordsFromText($article->description, 20)}} <a href="{{route('page',$article->slug)}}">>>></a>
                                    </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    @endif
                @endforeach
                <div class="text-right mt-3">
                    <a href="{!! route('blog') !!}" class="more_news">{!! l('Більше новин') !!} >>></a>
                </div>
            </div>
        </div>
    </section>
    <section id="main_content">
        <div class="container">
            <div>
                <h1>{{l('home:h1')}}</h1>
            </div>
            <div class="content page-content" data-read_more="content" data-more_btn_text="{!! trans('new_app.Читати далі') !!}">
                {!! $page->content !!}
            </div>
        </div>
    </section>


    <!-- Modal -->
    <div class="modal fade" id="upload_photo_form" tabindex="-1" role="dialog" aria-labelledby="upload_photo_form_title" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('photo.upload')}}" method="post"
                          enctype="multipart/form-data" id="upload-photo">
                        {{csrf_field()}}

                        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3>{{l('Завантажити фото')}}</h3>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="clabel">{{l('Оберіть фото..')}}</div>
                            <button id="falseinput">{{l('Оберіть фото..')}}</button>
                            <label for="file" id="label-file">{{l('Не вибрано')}}</label>
                            <input type="file" id="file" class="upload-button form-control" name="file" required multiple accept="image/*,image/jpeg">
                        </div>

                        <div class="clabel">{{trans('new_app.Ваше ім`я')}}</div>
                        <div class="form-group">
                            <input type="text" name="name" placeholder="{{l('Ім`я')}}" class="form-control" required>
                        </div>
                        <div class="clabel">{{trans('new_app.Електронна адреса')}}</div>
                        <div class="form-group">
                            <input type="email" name="email" placeholder="Email" class="form-control" required>
                        </div>
                        <div class="clabel">{{trans('new_app.Вкажіть свій номер телефону')}}</div>
                        <div class="form-group">
                            <input type="text" name="phone" placeholder="{{trans('new_app.Телефон')}}" class="form-control phone-mask" required>
                        </div>
                        <button type="submit" class="input-sumbit form-control">{{l('Підтвердити')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection