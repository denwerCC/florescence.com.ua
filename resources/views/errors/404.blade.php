@extends('layouts.florescence')
@section('title',  isset($title) ? $title : '')
@section('description', isset($description) ? $description : '')
@section('content')
    <br><br><br>
<h1 class="text-center">
    404. {{l('Сторінку не знайдено')}}
</h1>
@endsection
