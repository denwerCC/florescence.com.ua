<!doctype html>
<html lang="">
<head>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PL4R5XD');</script>
<!-- End Google Tag Manager -->
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @if(substr_count($_SERVER['REQUEST_URI'],'?'))
        <meta name="robots" content="noindex,follow">
    @endif

    <meta name="description" content="@yield('description')">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{url()->full()}}"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:description" content="@yield('description')"/>
    <meta property="og:image" content=""/>

    <link rel="canonical" href="{{url()->full()}}" />
    <link hreflang="x-default" href="{{langswitch()[config('app.locale')]['url']}}" rel="alternate"/>
    @foreach(langswitch() as $localeCode => $properties)
        @if ($localeCode === 'ua')
            <link rel="alternate" href="{{$properties['url']}}" hreflang="uk"/>
        @else
            <link rel="alternate" href="{{$properties['url']}}" hreflang="{{$localeCode}}"/>
        @endif
    @endforeach
    <link rel="stylesheet" href="/vendor/bootstrap-4.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/vendor/fontawesome-free-5.0.8/css/fontawesome-all.min.css" />
    <link rel="stylesheet" href="/vendor/bootstrap-select-1.13.0-beta/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="/css/fonts.css" />
    <link rel="stylesheet" href="/css/app.css?{{ sha1(microtime(true)) }}" />
    <link rel="stylesheet" href="/css/mobile.css?{{ sha1(microtime(true)) }}" />
    @section('head_styles')
    @show


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    {{--<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>--}}
    @section('head_scripts')
    @show

</head>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PL4R5XD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->	
<body class="@yield('page_class')">
<div id="scroll_line"></div>
{{--@if(!in_array($_SERVER['REMOTE_ADDR'], ['188.163.81.170','188.163.48.111','127.0.0.1','94.244.17.9']))--}}
    {{--|| ($_SERVER['SERVER_NAME'] != 'florescence.online-services.org.ua.loc')--}}
    {{--<h1 class="text-center">Сайт в разработке</h1>--}}
{{--@else--}}
{{--@endif--}}
@include('parts.header')

<section id="content_section">
@yield('content')
</section>

@include('parts.footer')

{{--BuyOneClick--}}<!-- Modal -->
<div class="modal fade" id="buy_one_click_modal" tabindex="-1" role="dialog" aria-labelledby="upload_photo_form_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>{{trans('new_app.Замовлення в 1 клік')}}</h3>
                <div class="clearfix"></div>
                <table>
                    <tr>
                        <td id="popup_product_img"></td>
                        <td id="popup_product_name"></td>
                        <td id="popup_product_price"></td>
                        <td>
                            <div id="popup_product_count_box">
                                <span id="popup_product_count_down">-</span>
                                <input id="popup_product_count" name="count" type="number" value="1" min="1" step="1">
                                <span id="popup_product_count_up">+</span>
                            </div>
                        </td>
                    </tr>
                </table>
                <form action="{{route('buy.one_click')}}" method="post"
                      enctype="multipart/form-data" id="buy_one_click_form">
                    {{csrf_field()}}
                    <input id="form_product_id" type="hidden" value="" name="product_id">
                    <input id="form_product_count" type="hidden" value="" name="product_count">
                    <div class="clabel">{{trans('new_app.Ваше ім`я')}}</div>
                    <div class="form-group">
                        <input type="text" name="name" placeholder="{{l('Ім`я')}}" class="form-control" required>
                    </div>
                    <div class="clabel">{{trans('new_app.Вкажіть свій номер телефону')}}</div>
                    <div class="form-group">
                        <input type="text" name="phone" placeholder="{{trans('new_app.Телефон')}}" class="form-control phone-mask" required>
                    </div>
                    <button type="submit" class="input-sumbit form-control">{{l('Замовити')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>



<script src="/vendor/jquery-3.3.1.min.js"></script>
<script src="/vendor/popper.min.js"></script>
<script src="/vendor/bootstrap-4.0.0/js/bootstrap.min.js"></script>
<script src="/vendor/bootstrap-select-1.13.0-beta/js/bootstrap-select.min.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="/js/app.js?{{ sha1(microtime(true)) }}"></script>
@section('footer_scripts')
@show
<script>
    (function ($) {
        $(document).ready(function () {

            var $count = $("#popup_product_count");
            var $input_count = $('#form_product_count');
            $("#popup_product_count_down").on("click", function () {
                var count = (parseInt($count.val(), 10) > 1) ? parseInt($count.val(), 10) - 1 : 1;
                $count.val(count);
                $count.trigger("input");
            });
            $("#popup_product_count_up").on("click", function () {
                var count = parseInt($count.val(), 10) + 1;
                $count.val(count);
                $count.trigger("input");
            });
            $count.on("input", function () {
                $input_count.val( $(this).val() );
            });

            $('#buy_one_click_modal').on('show.bs.modal', function (e) {
                console.log(e);
                var $a = $(e.relatedTarget);
                if($a != undefined){
                    $input_count.val(1);
                    $('#form_product_id').val($a.data('product_id'));
                    $('#popup_product_name').text($a.data('product_name'));
                    $('#popup_product_price').text($a.data('product_price'));
                    $('#popup_product_img').html($('<img src="" alt="">').attr({
                        src: $a.data('product_img_src')
                    }));
                }
            });
            // $('#buy_one_click_modal').on('hide.bs.modal', function (e) {
            //     console.log(e);
            // })
        });
    })(jQuery);
</script>

@if(Session::has('message'))
    <script>
        (function ($) {
            $(document).ready(function () {
                swal({
                    title: "Успішно",
                    text: "{!! str_replace("\n", "", Session::get('message'))!!}",
                    icon: "success",
                    html: true
                });
            });
        })(jQuery);
    </script>
@endif

@if ((isset($errors) and count($errors) > 0) || (Session::has('error')))
    <script>
        (function ($) {
            $(document).ready(function () {
                swal({
                    title: "Помилка!",
                    text: "{!! implode("<br>",$errors->all()) !!} {!! str_replace("\n", "", Session::get('error'))!!}",
                    icon: "error",
                    html: true
                });

            });
        })(jQuery);
    </script>
@endif

</body>
</html>