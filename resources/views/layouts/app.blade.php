<!DOCTYPE html>
<html>

<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="description" content="@yield('description')">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{url()->full()}}"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:description" content="@yield('description')"/>
    <meta property="og:image" content=""/>

    <link rel="canonical" href="{{url()->full()}}" />
    <link hreflang="x-default" href="{{langswitch()[config('app.locale')]['url']}}" rel="alternate"/>
    @foreach(langswitch() as $localeCode => $properties)
        @if ($localeCode === 'ua')
            <link rel="alternate" href="{{$properties['url']}}" hreflang="uk"/>
        @else
            <link rel="alternate" href="{{$properties['url']}}" hreflang="{{$localeCode}}"/>
        @endif
    @endforeach
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="icon" href="{{asset('/img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('/css/normalize.css')}}">
    {{--<link rel="stylesheet" href="/css/bootstrap.min.css">--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>--}}
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="{{asset('/js/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('/js/slick/slick-theme.css')}}">
    {{--<script src="https://use.fontawesome.com/104139b571.js"></script>--}}
    <link rel="stylesheet" href="/css/social-share-kit.css" type="text/css">
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
    <link href="/css/clockpicker.css" rel="stylesheet" type="text/css">
    <link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    {{--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>--}}
    <link rel="stylesheet" href="{{asset('/js/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('/js/slick/slick-theme.css')}}">
    @yield('style')
    <link rel="stylesheet" href="/css/fast.css">)
</head>
<body id='page-{{Route::currentRouteName()}}'>

@if(Route::getCurrentRoute() and Route::getCurrentRoute()->parameter('slug') !== 'master-show')
    @include('layouts.header')
@endif

    @yield('content')
    @include('footer')

<!-- jquery ---->
<script src="{{asset('/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('/js/moment.js')}}"></script>
<script src="{{asset('/js/moment-with-locales.js')}}"></script>
<script src='/js/bootstrap.min.js'></script>
<script src="{{asset('/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('/js/clockpicker.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
{{--<script type="text/javascript" src="https://vk.com/js/api/share.js?94" charset="windows-1251"></script>--}}
<link rel="stylesheet" href="/js/colorbox/colorbox.css">
<script src='/js/colorbox/jquery.colorbox-min.js'></script>
<script type="text/javascript" src="/js/social-share-kit.min.js"></script>
<script src="{{asset('/js/jquery.lazyload.js')}}"></script>
<!----- SLICK SLIDER ------>
{{--<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>--}}
<script src="{{asset('/js/slick/slick.js')}}"></script>
<!---- VK LIKE BTN ----->

<!---- MASK PLUGIN ---->
{{--<script src="{{asset('/js/jquery.mask.js')}}"></script>--}}
<script src="{{asset('//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js')}}"></script>
<!-- Sweet alert -->
<link rel="stylesheet" href="/js/sweetalert/sweetalert.css">
<script src="/js/sweetalert/sweetalert.min.js"></script>
<!--- script ---->
<script src="{{asset('/js/script.js')}}"></script>

<script type="text/javascript">
    console.log('for shortcode_form_phone');
    (function ($) {
        console.log('for shortcode_form_phone');
        // $('.shortcode_form_phone').mask("(00) 0000-0000");
        $('.shortcode_form_phone').mask('+38(000)000-00-00');
        console.log($('.shortcode_form_phone'));
    })(jQuery);
</script>

@stack('scripts')
<script>
    $(function () {
        $('.item.lazy').lazyload({
            effect:'fadeIn',
        });
    });
</script>
@if(Session::has('message'))
    <script>
        $(function () {
            swal({
                title: "Успішно",
                text: "{!! str_replace("\n", "", Session::get('message'))!!}",
                type: "success",
                html: true
            });
        })
    </script>
@endif

@if (isset($errors) and count($errors) > 0)
    <script>
        $(function () {
            swal({
                title: "Помилка!",
                text: "{!! implode("<br>",$errors->all()) !!}",
                type: "error",
                html: true
            });
        })
    </script>

@endif

</body>
</html>
