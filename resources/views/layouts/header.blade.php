<div class="header">
    <div class="header-row">
        <div class="top_menu_holder">
            <div class="container">
                <div class="row top_menu">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 header-contacts">
                        <div id="header-phone-block" style="width: 300px;height: 25px;position: absolute;"></div>
                        {!! get_config('phone') !!}
                        <p class="visible">{{l('Зворотній зв’язок')}}</p>
                        <i class="btn fa fa-phone callback-btn2" id="callback-btn2"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" id="social-autorization" style="">
                        @if(Auth::check())
                            <p>{{l('Доброго дня')}}, {{explode(' ',Auth::user()->name)[0]}}.
                                <a href="{{route('logout')}}">{{l('Вийти')}}</a>
                            </p>
                        @else
                            <p>{{l('Увійти через')}}:</p>
                            <a href="{{route('social-login', 'facebook')}}" class="auth-link">
                                <i class="fa fa-facebook"></i> </a>
                            <a href="{{route('social-login', 'instagram')}}" class="auth-link">
                                <i class="fa fa-instagram"></i> </a>
                        @endif
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 top-header-links-holder">
                        <ul class="top-header">
                            {!! menu(7,['class' => 'top-header', 'li' => ['class'=>'top-header-link']]) !!}
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 logo-img">
                    <a href="{{route('home')}}"><img id="logo-image" src="/img/LOGO-cropped.png"></a>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" id="menu">
                    <div class="row pull-right" id="additional-menu">
                        <div class="col-lg-6" id="languages">
                            <ul class="language_bar_chooser lang-list">

                                @foreach(langswitch() as $localeCode => $properties)
                                    <li>
                                        <a class="lang-selector" rel="alternate" hreflang="{{( ($localeCode == 'ua') ? 'uk' : $localeCode)}}" href="{{$properties['url'] }}">
                                            {{ $properties['lang'] }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-lg-6" id="currency">
                            <div id="currency_list">
                            {!! currency() !!}
                            </div>
                        </div>
                    </div>
                    <div class="row main-menu-holder col-xs-12 col-sm-12">
                        <ul class="nav hidden-xs">
                            {!! menu(1) !!}
                        </ul>
                        <div class="dropdown hidden-lg hidden-md hidden-sm visible-xs-block">
                            <button class="btn mobile-menu-btn dropdown-toggle" type="button" data-toggle="dropdown">{{l('Меню')}}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                {!! menu(1) !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
