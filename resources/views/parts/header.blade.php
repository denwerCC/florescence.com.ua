<header>
    <div class="container">
        <div class="row">

            <div id="logo_block" class="col-lg-2 col-md-3 col-sm-5 text-left">
                <a id="logo" href="{{route('home')}}"><img src="/img/logo.png" alt="logo"></a>
            </div>
            <div id="menu_block" class="col-lg-10 col-md-9 col-sm-7 text-right">
                <div id="menu_line">
                    <ul id="site_menu" class="nav d-none d-lg-inline-flex">
                        {!! menu(1) !!}
                    </ul>
                    <div id="site_menu_mobile" class="dropdown d-inline-flex d-lg-none">
                        <button class="btn mobile-menu-btn dropdown-toggle" type="button"
                                data-toggle="dropdown">
{{--                            {{l('Меню')}}--}}
                            {{--<span class="caret"></span>--}}
                            <i class="fas fa-bars"></i>
                        </button>
                        <ul class="dropdown-menu">
                            {!! menu(1) !!}
                        </ul>
                    </div>
                    <ul id="params_menu" class="nav d-inline-flex">
                        <li id="phone_item">
                            <a href="#" class="dropdown-toggle" href="#" role="button" id="header_phone"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-phone"></i>
                                <i class="fas fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="header_phone">
                                {!! get_config('phone') !!}
                            </div>
                        </li>
                        <li id="cart_item">
                            <a href="{!! route('cart') !!}"><span>{!! getCartCount() !!}</span>
                            <img src="/img/cart.png" alt="cart">
                            </a>
                        </li>
                        <li id="currencies_item">
                            <span class="dropdown-toggle" role="button" id="currency_menu_btn" data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false">{!! getCurrentCurrency() !!}

                                <i class="fas fa-angle-down"></i>
                            </span>
                            {!! getCurrencyMenu() !!}
                        </li>
                        <li id="langs_item">
                            <span class="dropdown-toggle" role="button" id="lang_menu_btn" data-toggle="dropdown"
                                  aria-haspopup="true" aria-expanded="false">{!! App::getLocale() !!}
                                <i class="fas fa-angle-down"></i>
                            </span>
                            @includeIf('parts.langs_menu')
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfx"></div>
</header>