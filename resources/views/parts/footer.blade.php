<footer>
    <div class="container">
        <div class="row">
            <div id="phones" class="col-sm-6 text-sm-left text-center">
                {!! get_config('phone') !!}
            </div>
            <div class="col-sm-6 text-sm-left text-md-right text-center">
                <ul id="footer_social" class="list-unstyled list-inline-item m-0 mr-4">
                    @foreach(config('social') as $social => $link)
                        <li class="list-inline-item">
                            <a href="{{$link}}" target="_blank" rel="nofollow">
                                @if($social == 'facebook')
                                    <i class="fab fa-{{$social}}-square"></i>
                                @else
                                    <i class="fab fa-{{$social}}"></i>
                                @endif
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="list-inline-item">
                    All rights reserved {{date('Y')}}
                </div>
            </div>
        </div>
        <div type="button" class="callback-bt">
            <div class="text-call">
                <a href="tel:+38(099)082-51-52">
                    <i class="fa fa-phone"></i>
                    <span>Замовити <br> дзвінок</span>
                </a>
            </div>
        </div>
        <div type="button" class="email-bt" data-toggle="modal" data-target="#send_contact">
            <div class="text-call">
                <a href="#" class="call-modal">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>Замовити <br> дзвінок</span>
                </a>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
</footer>
@include('parts.send_contact_modal')
@include('parts.analytics')

<style>
    .callback-bt {
        background:#38a3fd;
        border:2px solid #38a3fd;
        border-radius:50%;
        box-shadow:0 8px 10px rgba(56,163,253,0.3);
        cursor:pointer;
        height:68px;
        text-align:center;
        width:68px;
        position: fixed;
        right: 8%;
        bottom: 18%;
        z-index:999;
        transition:.3s;
        -webkit-animation:hoverWave linear 1s infinite;
        animation:hoverWave linear 1s infinite;
    }

    .callback-bt .text-call{
        height:68px;
        width:68px;
        border-radius:50%;
        position:relative;
        overflow:hidden;
    }

    .callback-bt .text-call span {
        text-align: center;
        color:#38a3fd;
        opacity: 0;
        font-size: 0;
        position:absolute;
        right: 4px;
        top: 22px;
        line-height: 14px;
        font-weight: 600;
        text-transform: uppercase;
        transition: opacity .3s linear;
        font-family: 'montserrat', Arial, Helvetica, sans-serif;
    }

    .callback-bt .text-call:hover span {
        opacity: 1;
        font-size: 11px;
    }
    .callback-bt:hover i {
        display:none;
    }

    .callback-bt:hover {
        z-index:1;
        background:#fff;
        color:transparent;
        transition:.3s;
    }
    .callback-bt:hover i {
        color:#38a3fd;
        font-size:40px;
        transition:.3s;
    }
    .callback-bt i {
        color:#fff;
        font-size:34px;
        transition:.3s;
        line-height: 66px;transition: .5s ease-in-out;
    }

    .callback-bt i  {
        animation: 1200ms ease 0s normal none 1 running shake;
        animation-iteration-count: infinite;
        -webkit-animation: 1200ms ease 0s normal none 1 running shake;
        -webkit-animation-iteration-count: infinite;
    }

    .email-bt {
        background:#F95C18;
        border:2px solid #F95C18;
        border-radius:50%;
        box-shadow:0 8px 10px rgba(249,92,24,0.3);
        cursor:pointer;
        height:68px;
        text-align:center;
        width:68px;
        position: fixed;
        left: 8%;
        bottom: 18%;
        z-index:999;
        transition:.3s;
        -webkit-animation:email-an linear 1s infinite;
        animation:email-an linear 1s infinite;
    }

    .email-bt .text-call{
        height:68px;
        width:68px;
        border-radius:50%;
        position:relative;
        overflow:hidden;
    }

    .email-bt .text-call span {
        text-align: center;
        color:#F95C18;
        opacity: 0;
        font-size: 0;
        position:absolute;
        right: 4px;
        top: 22px;
        line-height: 14px;
        font-weight: 600;
        text-transform: uppercase;
        transition: opacity .3s linear;
        font-family: 'montserrat', Arial, Helvetica, sans-serif;
    }

    .email-bt .text-call:hover span {
        opacity: 1;
        font-size: 11px;
    }
    .email-bt:hover i {
        display:none;
    }

    .email-bt:hover {
        z-index:1;
        background:#fff;
        transition:.3s;
    }
    .email-bt:hover i {
        color:#38a3fd;
        font-size:40px;
        transition:.3s;
    }
    .email-bt i {
        color:#fff;
        font-size:29px;
        transition:.3s;
        line-height: 66px;
    }

    .email-bt i  {
        -webkit-animation: opsimple 3s infinite;
        animation: opsimple 3s infinite;
    }

    .callback-bt {display: none;} @media only screen and (max-width: 400px) {.callback-bt {display: block;}}

    @media only screen and (max-width: 400px) {.email-bt {display: none;}}
</style>