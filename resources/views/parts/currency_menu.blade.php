@if(is_array($currencyItems) && !empty($currentCurrency) && (false !== array_search($currentCurrency, $currencyItems)))
<ul id="currency_menu" class="dropdown-menu"  aria-labelledby="currency_menu_btn">
@foreach($currencyItems as $key => $currency)
<li class="{!! ($currentCurrency == $currency) ? 'selected':'' !!} ">
    <a href="{!! route('currency.change', $key) !!}">{!! $currency !!}</a>
</li>
@endforeach
</ul>
@endif
