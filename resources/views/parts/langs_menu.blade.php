<ul id="lang_menu" class="dropdown-menu"  aria-labelledby="lang_menu_btn">
    @foreach(langswitch() as $localeCode => $properties)
        <li class="{!! (App::getLocale() == $properties['lang']) ? 'selected' : ''!!}">
            <a rel="alternate" hreflang="{{( ($localeCode == 'ua') ? 'uk' : $localeCode)}}" href="{{$properties['url'] }}">
                {!! strtoupper($properties['lang']) !!}
            </a>
        </li>
    @endforeach
</ul>
