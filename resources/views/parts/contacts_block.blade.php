<div id="c_contacts">
    <div class="header">{!! l('Залишились питання?') !!}</div>
    <div class="header">{!! l('Напишіть нам листа :)') !!}</div>
    <form action="{!! route('send') !!}" method="post">
        {!! csrf_field() !!}
        <input type="text" name="name" placeholder="{!! l('Ім`я') !!}" required maxlength="255">
        <input type="text" name="phone" placeholder="{!! l('Телефон') !!}" required maxlength="255">
        <input type="email" name="email" placeholder="{!! l('E-mail') !!}" required maxlength="255">
        <input type="text" name="message" placeholder="{!! l('Коментар') !!}" required maxlength="500">
        <button type="submit" class="vdz_btn">{!! l('Відправити') !!}</button>
    </form>
</div>