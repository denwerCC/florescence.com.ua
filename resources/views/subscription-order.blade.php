@extends('layouts.florescence')
@section('title',  $page->meta_title)
@section('page_class',  'subscribe_page')
@section('description', $page->meta_description)
@section('style')
    <style>
        .f-form form .form-group input[class='form-control']{
            border-radius: 0;
            border:none;
            background-color: #F5F5F5;
        }
        .f-form form .form-group input[name='phone']{
            border-radius: 0;
            border:none;
            background-color: #F5F5F5;
        }
        .f-form form .form-group input[name='phone']:focus{
            border: none;
            box-shadow: none;
            outline: none;
        }
        .f-form form .form-group input[class='form-control']:focus{
            border: none;
            box-shadow: none;
            outline: none;
        }
        .f-form form .form-group select{
            border-radius: 0;
            border:none;
            background-color: #F5F5F5;
        }
        .f-form form .form-group select:focus{
            border: none;
            box-shadow: none;
            outline: none;
        }
        .f-form form .form-group textarea{
            border-radius: 0;
            border:none;
            background-color: #F5F5F5;
        }
        .f-form form .form-group textarea:focus{
            border: none;
            box-shadow: none;
            outline: none;
        }
        .f-form form button[type='submit']{
            border-radius: 0;
            border:none;
            background-color: darkgray;
        }
    </style>
@endsection
@section('content')
    <div id="wedding_design_line"></div>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <div id="container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div id="breadcrumb">
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
                        </div>
                        @if($rubric)
                            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <a href="{{route('flowers-sub')}}" itemprop="url">
                                    <span itemprop="title">{{$rubric->title}}</span> </a> »
                            </div>
                        @endif
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('flower.subscribe',$page->slug)}}" itemprop="url">
                                <span itemprop="title">{{$page->title}}</span> </a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row subscribe">
                <div class="col-lg-4 col-md-5 col-sm-5 col-xs-12 image-left">
                    <img src="{{$page->image}}" alt="">
                    {{--<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="true" data-share="false" style="margin-top: 5px; display: block;float: left;"></div>--}}
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 desc-right">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1> {{$page->title}} </h1>
                        {{--<ul class="desc-right-subsc-opt">--}}
                            {{--{!!nl2br($page->description)!!}--}}
                        {{--</ul>--}}

                        <p>{!! $page->content !!}</p>

                    </div>
                </div>
            </div>
            <div class="hidden hide d-none">

                <div class="col-lg-5 col-lg-offset-4 f-form ">
                <form action="{{route('subs.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="hidden" name="size" class="form-control" value="{{$page->title}}">
                    </div>
                    <div class="form-group">
                        <input name="name" type="text" class="form-control" placeholder="{{l('Ім’я')}}">
                    </div>
                    <div class="form-group">
                        <input name="phone" type="text" class="form-control phone-mask" placeholder="{{l('Телефон')}}">
                    </div>
                    <div class="form-group">
                        <input name="email" type="text" class="form-control" placeholder="email">
                    </div>
                    <div class="form-group">
                        <input name="address" type="text" class="form-control" placeholder="{{l('Адреса')}}">
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="select" name="abonement">
                            <option>{{l('Місяць')}}</option>
                            <option>{{l('Квартал')}}</option>
                            <option>{{l('Рік')}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input name="time" type="text" class="form-control" placeholder="{{l('Зручний час для доставки')}}">
                    </div>
                    <div class="form-group">
                        <textarea name="information" class="form-control" rows="3" placeholder="{{l('Додаткова інформація')}}"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default">{{l('Замовити')}}</button>
                </form>
            </div>
            </div>

        </div>
    </div>

    @include('parts.contacts_block')

    @endsection
@section('footer_scripts')
<script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>
<script type="text/javascript">
    VK.init({
        apiId:6035611,
        onlyWidgets: true
    });
    $('.phone-mask').mask("+3(8099)999-99-99");
</script>
@endsection