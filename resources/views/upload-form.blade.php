<style>
    form div.form-group input{
        border-radius: 0;
    }
    form input[name="name"]{
        width: 95%;
        border: 1px solid gainsboro!important;
        padding: 10px;
        margin:0 auto;
    }
    form input[name="email"]{
        width: 95%;
        border: 1px solid gainsboro!important;
        padding: 10px;
        margin:0 auto;
    }
    form input[type="file"]{
        width: 60%;
        display: block;
    }
    form input[name="phone"]{
        width: 95%;
        border: 1px solid gainsboro!important;
        padding: 10px;
        margin:0 auto;
    }
    form input[name="file"]{
        opacity: 0;
        position: absolute;
        z-index: -1;
    }
    form label[for='file']{
        cursor: pointer;
        display: block;
        margin: 0 auto;
        padding-left: 12px;
        color: #959595;
        line-height: 32px;
    }
    div.h3{
        color: gray;
        padding: 15px;
        border-bottom: 1px solid gainsboro;
    }
    form button[type='submit']{
        width: 60%;
        display: block;
        margin: 0 auto;
        border: 1px solid!important;
        border-radius: 0;
    }
    #falseinput{
        padding: 5px;
        margin-left: 10px;
        background: transparent;
        border: 1px solid gainsboro;
        color: #959595;
        display: block;
        float: left;
        margin-right: 5px;
    }
</style>

<form action="{{route('photo.upload')}}" method="post"
      enctype="multipart/form-data" id="upload-photo">
    {{csrf_field()}}
    <div class="h3 text-center">{{l('Завантажити фото')}}</div>
    <div class="form-group">
        <button id="falseinput">{{l('Оберіть фото..')}}</button>
        <label for="file" id="label-file">{{l('Не вибрано')}}</label>
        <input type="file" id="file" class="upload-button form-control" name="file" required multiple accept="image/*,image/jpeg">
    </div>
    <div class="form-group">
        <input type="text" name="name" placeholder="{{l('Ваше ім’я')}}" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="email" name="email" placeholder="email" class="form-control" required>
    </div>
    <div class="form-group">
        <input type="text" name="phone" placeholder="{{l('Ваш контактний номер')}}" class="form-control phone-mask" required>
    </div>
    <button type="submit" class="input-sumbit form-control">{{l('Підтвердити')}}</button>
</form>
<script src="{{asset('/js/script.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#falseinput').click(function () {
            $('.upload-button').click();
        });
        $('.upload-button').change(function() {
            $('#label-file').text($('.upload-button')[0].files[0].name);
        });
    })
</script>