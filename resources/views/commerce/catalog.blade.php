@extends('layouts.florescence')
@section('page_class',  'shop')
@section('title',  isset($page->meta_title) ? $page->meta_title : $category->meta_title)
@section('description', isset($page->meta_description) ? $page->meta_description : $category->meta_description)
@section('content')

    <div id="container">
        <div class="catalog-list container">

            <div class="row">
                <div class="col-sm-12">
                    <div id="breadcrumb">

                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('home')}}" itemprop="url">
                                <span itemprop="title">{{l('Головна')}}</span>
                            </a> »
                        </div>
                        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{route('catalog')}}" itemprop="url">
                                <span itemprop="title">{{l('Каталог')}}</span>
                            </a> »
                        </div>

                        @if(isset($category))
                            @if(isset($category->parent->parent))
                                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a href="{{route('catalog.category',$category->parent->parent->slug)}}"
                                       itemprop="url">
                                        <span itemprop="title">{{$category->parent->parent->title}}</span>
                                    </a> »
                                </div>
                            @endif
                            @if(isset($category->parent))
                                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a href="{{route('catalog.category',$category->parent->slug)}}" itemprop="url">
                                        <span itemprop="title">{{$category->parent->title}}</span>
                                    </a> »
                                </div>
                            @endif
                            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <a href="{{url()->current()}}" class="last" itemprop="url">
                                    <span itemprop="title">{{$category->title}}</span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-3 col-md-4 col-sm-12 menu-categories-holder">
                    <aside id="categories" class="">
                        <h3>{!! trans('new_app.Категорії') !!}</h3>
                        <ul>
                            {!! menu('catalog') !!}
                        </ul>
                        <img id="aside_u" src="/public/img/blog_section.png" alt="" class="d-none d-md-block d-lg-block">
                    </aside>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 catalog-item-holder">

                    <section id="catalog">
                        <div class="catalog_header">
                        <h1>{{ isset($category->h1) ? $category->h1 : (isset($page->title) ? $page->title : $category->title) }}</h1>
                            <div id="sort">
                                {!! trans('new_app.Сортувати за:') !!}
                                <a href="#" class="dropdown-toggle" href="#" role="button" id="sort_items"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    --- <i class="fas fa-angle-down"></i>
                                </a>

                                <ul class="dropdown-menu" aria-labelledby="sort_items">
                                    <li><a href="{!! url()->current() !!}?sort=name&order=asc">{!! trans('new_app.sort_name_ASC') !!}</a></li>
                                    <li><a href="{!! url()->current() !!}?sort=name&order=desc">{!! trans('new_app.sort_name_DESC') !!}</a></li>
                                    <li><a href="{!! url()->current() !!}?sort=price&order=asc">{!! trans('new_app.sort_price_ASC') !!}</a></li>
                                    <li><a href="{!! url()->current() !!}?sort=price&order=desc">{!! trans('new_app.sort_price_DESC') !!}</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="row">
                            @foreach($items as $item)
                                @include('commerce.items')
                            @endforeach
                            {{--<div class="pagination-holder">--}}
                            {{--{{ $items->links() }}--}}
                            {{--</div>--}}
                        </div>
                        <div class="row">
                            @if($paginate)
                                {!! $paginate->links() !!}
                            @endif
                        </div>

                        <div id="main_content" class="row">
                            <div class="col-md-12">
                                <div class="content page-content"  data-read_more="content" data-more_btn_text="{!! trans('new_app.Читати далі') !!}">
                                    {!! isset($page->content) ? $page->content : $category->content !!}
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </div>




@endsection