@extends('layouts.app')
@section('title',  isset($category->title) ? $category->title : l('Категория'))
@section('description', isset($category->description) ? $category->description : '')
@section('content')

    <div id="breadcrumb">
        <a href="/">Главная</a> »

        @if(isset($category->parent->parent))
            <a href="{{route('catalog.category',$category->parent->parent->slug)}}">{{$category->parent->parent->title}}</a> »
        @endif

        @if($category->parent)
            <a href="{{route('catalog.category',$category->parent->slug)}}">{{$category->parent->title}}</a> »
        @endif

        <a href="{{url()->current()}}" class="last">{{$category->title}}</a>
    </div>

    <div class="category-info">
        <h1 class="text-center">{{$category->title}}</h1>
        {{$category->description}}
    </div>

    <div id="category-list" class="catalog-list">

        <div class="row">

            @foreach($category->childrens as $category)

                <div class="col-sm-3">

                    <a class="catalog-item" href="{{route('catalog.category',$category->slug)}}">
                        <img class="img-responsive" src="{{$category->image}}" alt="{{$category->title}}">

                    </a>
                </div>

            @endforeach
        </div>
    </div>

@endsection