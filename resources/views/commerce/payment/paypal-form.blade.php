<form style='text-align:center;margin: 30px' action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="khris01.01.12@gmail.com">
    <input type="hidden" name="lc" value="UA">
    <input type="hidden" name="item_name" value="Flowers. Order id {{$orderid}}">
    <input type="hidden" name="item_number" value="{{$orderid}}">
    <input type="hidden" name="amount" value="{{$amount}}">
    <input type="hidden" name="currency_code" value="EUR">
    <input type="hidden" name="button_subtype" value="services">
    <input type="hidden" name="no_note" value="1">
    <input type="hidden" name="no_shipping" value="2">
    <input type="hidden" name="rm" value="1">
    <input type="hidden" name="notify_url" value="{{route('paypal.server',$uniqid)}}">
    <input type="hidden" name="return" value="{{route('paypal.user',$uniqid)}}">
    <input type="hidden" name="cancel_return" value="{{route('paypal.error',$uniqid)}}">
    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>