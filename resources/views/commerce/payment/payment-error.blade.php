@extends('layouts.app')
@section('title', 'Payment Error')
@section('description', 'Payment Error')
@section('content')
    <div style="text-align: center">
        <h1>Помилка оплати</h1>
        Ідентифікатор покупки: {{$orderid}}
    </div>
@endsection