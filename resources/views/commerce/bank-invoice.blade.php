<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>

    <meta http-equiv="CONTENT-TYPE" content="text/html; charset=UTF-8">
    <title>Форма оплаты для печати</title>
    <style>
        <!--
        BODY, DIV, TABLE, THEAD, TBODY, TFOOT, TR, TH, TD, P {
            font-family: "Arial Cyr";
            font-size: x-small
        }

        -->
    </style>

</head>

<body text="#000000">
<table frame="VOID" cols="8" rules="NONE" cellspacing="0" border="0">
    <colgroup>
        <col width="148">
        <col width="137">
        <col width="80">
        <col width="88">
        <col width="39">
        <col width="137">
        <col width="46">
        <col width="77">
    </colgroup>
    <tbody>
    <tr>
        <td style="border-top: 3px solid #323232; border-left: 3px solid #323232" width="148" align="LEFT" height="18">
            <font face="Arial">ПОВІДОМЛЕННЯ</font></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            colspan="3" width="305" align="LEFT"><font face="Arial">{{$config['reseiver']}}</font></td>
        <td style="border-top: 3px solid #323232" width="39" align="LEFT"><br></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            sdval="2600430418032" sdnum="1049;0;0" width="137" align="CENTER"><font
                    face="Arial">{{$config['checking_account']}}</font></td>
        <td style="border-top: 3px solid #323232" width="46" align="LEFT"><br></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            sdval="{{$config['Code']}}" sdnum="1049;" width="77" align="CENTER"><font
                    face="Arial">{{$config['Code']}}</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br></td>
        <td style="border-left: 1px solid #323232" colspan="3" align="CENTER"><font face="Arial">отримувач
                платежу</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td align="LEFT"><font face="Arial">Р/рахунок отримувача</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-right: 3px solid #323232" align="CENTER"><font face="Arial">Код отримувача</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232" align="LEFT" height="19"><br></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            colspan="3" align="CENTER"><font face="Arial">{{$config['bank']}}</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            sdval="300012" sdnum="1049;" align="CENTER"><font face="Arial">{{$config['mfo']}}</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            align="LEFT"><font face="Arial"><br></font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br></td>
        <td style="border-left: 1px solid #323232" colspan="3" align="CENTER"><font face="Arial">назва установи
                банку</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td align="CENTER"><font face="Arial">МФО банку</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-right: 3px solid #323232" align="CENTER"><font face="Arial">Код банку</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br></td>
        <td style="border-bottom: 3px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="7" align="CENTER">{{$order->name}}<br></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br></td>
        <td style="border-top: 3px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="7" align="CENTER"><font size="1" face="Arial">{{l('прізвище, ім’я та по-батькові платника')}}</font></td>
    </tr>
    @if($order->address)
        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br>
            </td>
            <td style="border-bottom: 3px solid #323232; border-left: 1px solid #323232;border-top: 1px solid #323232; border-right: 3px solid #323232"
                colspan="7" align="CENTER">
                {{$order->address}}
            </td>
        </tr>
        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br>
            </td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
                colspan="7" align="CENTER"><font size="1" face="Arial">адреса платника</font></td>
        </tr>
    @endif
    <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br></td>
    <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
        colspan="3" align="LEFT"><font size="1" face="Arial">Призначення платежу</font></td>
    <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
        colspan="2" align="CENTER"><font face="Times New Roman">кількість примірників</font></td>
    <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
        colspan="2" align="CENTER"><font face="Arial">Сума</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232"
            align="LEFT"><b><i><font size="1" face="Arial">{{$config['payment']}} №</font></i></b></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232" align="LEFT"><b><i><font
                            size="1">{{$order->id}}</font></i></b></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-right: 1px solid #323232"
            align="LEFT"><b><i><font size="1" face="Arial">за</font></i></b>&nbsp;{{$order->created_at}}</td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="2" align="CENTER"><i><font face="Arial"> <br></font></i></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="2" align="LEFT"><font face="Arial"><br></font></td>
    </tr>
    <?$total = 0?>
    @foreach($order->items as $item)
        <?$total += $item->price * $item->count?>
        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br>
            </td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
                colspan="3" align="LEFT"><b><i><font face="Arial">&nbsp;{{$item->title}}<br></font></i></b></td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
                colspan="2" align="LEFT"><i><font face="Arial">{{$item->count}}&nbsp;<br></font></i></td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
                colspan="2" align="LEFT"><font face="Arial">{{$item->price * $item->count}}<br></font></td>
        </tr>

    @endforeach

    @if($total!=$order->amount)

        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br>
            </td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
                colspan="5" align="LEFT"><b><i><font face="Arial">Доставка</font></i></b></td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
                colspan="2" align="LEFT"><font face="Arial">{{$order->amount - $total}}</font></td>
        </tr>

    @endif
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><font
                    face="Arial">Касир</font></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="5" align="LEFT"><i><font size="1" face="Arial">без ПДВ, згідно ст.195.1.25 Податкового кодексу
                    України</font></i></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="2" align="LEFT"><font face="Arial"><br></font></td>
    </tr>
    <tr>
        <td style="border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 1px solid #323232"
            align="LEFT" height="18"><br></td>
        <td style="border-top: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="3" align="LEFT"><i><font size="1" face="Arial">Платник</font></i></td>
        <td style="border-top: 1px solid #323232; border-bottom: 3px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="2" align="CENTER"><font face="Arial">Всього</font></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="2" align="LEFT"><font face="Arial">{{$order->amount}}<br></font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232" align="LEFT" height="18"><font face="Arial">Квитанція </font></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            colspan="3" align="LEFT"><font face="Arial">{{$config['reseiver']}}</font></td>
        <td align="LEFT"><br></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            sdval="2600430418032" sdnum="1049;0;0" align="CENTER"><font
                    face="Arial">{{$config['checking_account']}}</font></td>
        <td align="LEFT"><br></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            sdval="{{$config['Code']}}" sdnum="1049;" align="CENTER"><font face="Arial">{{$config['Code']}}</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="19"><br></td>
        <td style="border-left: 1px solid #323232" colspan="3" align="CENTER"><font face="Arial">отримувач
                платежу</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td align="LEFT"><font face="Arial">Р/рахунок отримувача</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-right: 3px solid #323232" align="CENTER"><font face="Arial">Код отримувача</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232" align="LEFT" height="18"><br></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            colspan="3" align="CENTER"><font face="Arial">{{$config['bank']}}</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            sdval="300012" sdnum="1049;" align="CENTER"><font face="Arial">{{$config['mfo']}}</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-top: 3px solid #323232; border-bottom: 3px solid #323232; border-left: 3px solid #323232; border-right: 3px solid #323232"
            align="LEFT"><font face="Arial"><br></font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br></td>
        <td style="border-left: 1px solid #323232" colspan="3" align="CENTER"><font face="Arial">назва установи
                банку</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td align="CENTER"><font face="Arial">МФО банку</font></td>
        <td align="LEFT"><font face="Arial"><br></font></td>
        <td style="border-right: 3px solid #323232" align="CENTER"><font face="Arial">Код банку</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br></td>
        <td style="border-bottom: 3px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="7" align="CENTER">{{$order->name}}<br></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br></td>
        <td style="border-top: 3px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="7" align="CENTER"><font size="1" face="Arial">{{l('прізвище, ім’я та по-батькові платника')}}</font></td>
    </tr>

    @if($order->address)
        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br>
            </td>
            <td style="border-bottom: 3px solid #323232;border-top: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
                colspan="7" align="CENTER">
                {{$order->address}}
            </td>
        </tr>
        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="18"><br>
            </td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
                colspan="7" align="CENTER"><font size="1" face="Arial">адреса платника</font></td>
        </tr>
    @endif
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="3" align="LEFT"><font size="1" face="Arial">Призначення платежу</font></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="2" align="CENTER"><font face="Times New Roman">кількість примірників</font></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="2" align="CENTER"><font face="Arial">Сума</font></td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232"
            align="LEFT"><b><i><font size="1" face="Arial">{{$config['payment']}} №</font></i></b></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232" align="LEFT"><b><i><font
                            size="1">{{$order->id}}</font></i></b></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-right: 1px solid #323232"
            align="LEFT"><b><i><font size="1" face="Arial">за</font></i></b>&nbsp;{{$order->created_at}}</td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="2" align="CENTER"><i><font face="Arial"><br></font></i></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="2" align="LEFT"><font face="Arial"><br></font></td>
    </tr>
    <?$total = 0?>
    @foreach($order->items as $item)
        <?$total += $item->price * $item->count?>
        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br>
            </td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
                colspan="3" align="LEFT"><b><i><font face="Arial">&nbsp;{{$item->title}}<br></font></i></b></td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
                colspan="2" align="LEFT"><i><font face="Arial">{{$item->count}}&nbsp;<br></font></i></td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
                colspan="2" align="LEFT"><font face="Arial">{{$item->price * $item->count}}<br></font></td>
        </tr>

    @endforeach

    @if($total!=$order->amount)

        <tr>
            <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><br>
            </td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
                colspan="5" align="LEFT"><b><i><font face="Arial">Доставка</font></i></b></td>
            <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
                colspan="2" align="LEFT"><font face="Arial">{{$order->amount - $total}}</font></td>
        </tr>

    @endif

    <tr>
        <td style="border-left: 3px solid #323232; border-right: 1px solid #323232" align="LEFT" height="17"><font
                    face="Arial">Касир</font></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="5" align="LEFT"><i><font size="1" face="Arial">без ПДВ, згідно ст.195.1.25 Податкового кодексу
                    України</font></i></td>
        <td style="border-top: 1px solid #323232; border-bottom: 1px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="2" align="LEFT"><font face="Arial"><br></font></td>
    </tr>
    <tr>
        <td style="border-bottom: 3px solid #323232; border-left: 3px solid #323232" align="LEFT" height="18"><br></td>
        <td style="border-top: 1px solid #323232; border-bottom: 3px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="3" align="LEFT"><i><font size="1" face="Arial">Платник</font></i></td>
        <td style="border-top: 1px solid #323232; border-bottom: 3px solid #323232; border-left: 1px solid #323232; border-right: 1px solid #323232"
            colspan="2" align="CENTER"><font face="Arial">Всього</font></td>
        <td style="border-top: 1px solid #323232; border-bottom: 3px solid #323232; border-left: 1px solid #323232; border-right: 3px solid #323232"
            colspan="2" align="LEFT"><font face="Arial">{{$order->amount}}<br></font></td>
    </tr>
    <tr>
        <td colspan="9" align="CENTER"><span style="color:red; font-weight:bold; font-size: 17px;">Квитанция действительна в течение 3х дней!</span>
        </td>
        <td align="RIGHT">
            <a href="#" onclick="window.print(); return false;"><img
                        src="/img/print.gif"
                        alt="Печать документа" border="0"></a>
        </td>
    </tr>
    <tr>
        <td align="LEFT" height="21"><br></td>
        <td colspan="9" align="LEFT"><br></td>
    </tr>
    </tbody>
</table>

<script type="text/javascript" language="javascript">
    window.onload = function()
    {
        if (typeof(window.print) != 'undefined')
        {
            window.print();
        }
    }
</script>
</body>
</html>