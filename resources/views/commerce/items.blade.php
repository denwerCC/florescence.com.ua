<div class="col-lg-4 col-md-6 col-xs-12">
    <div class="item_product lazy">
        <div class="item_img_wrap position-relative">
            <a href="{{$item->link}}">
                <img src="{{$item->image(0,315)}}" alt="{{$item->title}}" title="{{$item->category->title}} - {{$item->title}}">
            </a>
            <div class="hover product_links d-none">
                    <a href="#" class="buy_one_click_modal_btn"  data-toggle="modal" data-target="#buy_one_click_modal" data-product_id="{{$item->id}}" data-product_name="{{$item->title}}" data-product_price="{{$item->priceFormatted}} {!! trans('new_app.'.$item->currency) !!}" data-product_img_src="{{$item->image(0,315)}}" ><i class="fas fa-phone"></i></a>
                    <a href="{{(route('cart.add',$item->id))}}"><i class="fas fa-shopping-cart"></i></a>
                    <a href="{{$item->link}}"><i class="fas fa-bars"></i></a>
            </div>
        </div>
        {{--@if(isset($item->filter(19)->value))--}}
        {{--<div class="sale">Акція!</div>--}}
        {{--@endif--}}
        <div class="product_description">
            <div class="price"><span>{{$item->priceFormatted}} {!! trans('new_app.'.$item->currency) !!}</span></div>
            <div class="name"><span>{{$item->title}}</span></div>
        </div>
    </div>
</div>
