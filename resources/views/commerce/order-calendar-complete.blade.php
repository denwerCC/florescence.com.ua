@extends('layouts.florescence')
@section('title',  isset($title) ? $title : l('Итоги заказа'))
@section('description', isset($description) ? $description : '')
@section('content')

    <div id="cart-page">
        <div id="container">
            <div class="container">
                <h1 class="text-center">{{l('Замовлення успішно оформлено')}}</h1>
                <div id="checkout-step">

                    <div id="step3" class="step row">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="block form1">
                                            <h2>{{l('Замовлення')}} #{{$order_id}}</h2>

                                            <ul class="fields dotted">
                                                <li><span class="first" style="text-transform: uppercase">{{trans('new_app.Розмір')}}</span>
                                                    <span class="second">{{Session::get('order.size')}}</span>
                                                </li>
                                                <li><span class="first">{{l('Кількість покупок')}}</span>
                                                    <span class="second">{{$count}}</span>
                                                </li>
                                                <li><span class="first">{{l('Всього')}}</span>
                                                    <strong><span class="second">{{$total}} UAH</span></strong>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="block form2">
                                            <h2 >{{l('Адреса доставки')}}</h2>
                                            <ul class="fields pad">
                                                <li>{{$form['name']}}</li>
                                                <li>{{$form['address']}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                @if(isset($payment))
                                    {!! $payment !!}
                                @endif

                                @if($payment_method=='MoneyGram')
                                    <h3>{{l('Оплата через MoneyGram')}}</h3>
                                    {{config('bank.moneygram')}}
                                @elseif($payment_method=='Privat24')
                                    <h3>{{l('Оплата на картку ПриватБанк')}}</h3>
                                    {!! config('bank.bank') !!}
                                    <p>{{l('В комментариях к оплате укажите номер заказа')}} </p>
                                @elseif($payment_method=='WesternUnion')
                                    <h3>{{l('Оплата через Western Union')}}</h3>
                                    {{config('bank.westernunion')}}
                                @endif
                            </div>
                        </div>




                        <div id="payment_status">
                            {{l('Статус замовлення')}}: {{$payment_status}}
                        </div>


                    </div>
                </div>
                <div>
                    <br>
                    <br>
                    <a href="/calendar" class="vdz_btn vdz_btn_dark">Зробити ще одне замовлення</a>
                </div>
            </div>

        </div>

    </div>
@endsection