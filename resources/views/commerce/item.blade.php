@extends('layouts.florescence')
@section('title',  $item->meta_title)
@section('description', $item->meta_description)
@section('page_class',  'product')
@section('content')
    <div class="header_bg_line"></div>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ua_UA/sdk.js#xfbml=1&version=v2.9";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <div id="product" class="container" itemscope itemtype="http://schema.org/Product">

        <div id="breadcrumb">
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="{{route('home')}}" itemprop="url"> <span itemprop="title">{{l('Головна')}}</span> </a> »
            </div>

            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="{{route('catalog')}}" itemprop="url"> <span itemprop="title">{{l('Каталог')}}</span> </a> »
            </div>

            @if($item->category)
                <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="{{route('catalog.category',$item->category->slug)}}" itemprop="url"><span
                                itemprop="title">{{$item->category->title}}</span></a> »
                </div>
            @endif

            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="{{$item->link}}" itemprop="url"><span itemprop="title">{{$item->title}}</span></a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 text-center left-side">
                <div id="gallery">

                    <div class="gallery_main">
                        <a class="gallery" href="{{$item->image}}" data-src="{!! $item->image !!}">
                            <img itemprop='image' src="{{$item->image(350,350)}}" alt="{{$item->title}}"
                                 title="{{$item->category->title}} - {{$item->title}}"/></a>
                    </div>
                    <div id="thumbs">
                        @if($item->images)
                            @foreach($item->images->splice(1) as $image)
                                <div>
                                    <a class="gallery" href="{{$item->image(0,0,$image)}}"
                                       data-src="{{$item->image(0,0,$image)}}">
                                        {{--<img itemprop='image' src="{{$item->image(100,100,$item->dir . "big/" . $image)}}" alt="{{$item->title}}" title="{{$item->category->title}} - {{$item->title}}"></a>--}}
                                        <img itemprop='image' src="{{$item->image(100,100,$image)}}"
                                             alt="{{$item->title}}"
                                             title="{{$item->category->title}} - {{$item->title}}"></a>
                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
                <div id="share" class="text-left">
                    <div class="ssk-group ssk-count ssk-sm">
                        <div class="fb-like" data-href="https://kvitochka.net" data-layout="button_count"
                             data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 right-side">
                <h1 itemprop="name">{{$item->title}}</h1>
                <div id="product_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <span itemprop="price" content="{{$item->price}}" class="price">{{$item->priceFormatted}}</span>
                    <span itemprop="priceCurrency" content="{{$item->currency}}">{{$item->currency}}</span>
                </div>
                <div id="description">
                    <div class="item_description" itemprop="description">
                        {!! $item->content !!}
                    </div>
                </div>
                <div id="payment">
                    <noindex>
                        <button type="button" id="buy_one_click" data-toggle="modal" data-target="#buy_one_click_modal" data-product_id="{{$item->id}}" data-product_name="{{$item->title}}" data-product_price="{{$item->priceFormatted}} {!! trans('new_app.'.$item->currency) !!}" data-product_img_src="{{$item->image(0,315)}}" >{{trans('new_app.Купити в 1 клік')}}</button>
                        <a href="{{route('cart.add', $item->id)}}" rel="nofollow" class="d-none hidden" >
                            {{trans('new_app.Додати в кошик')}}
                        </a>
                        <form action="{{route('cart.add', $item->id)}}" method="post" class="d-inline">
                            {!! csrf_field() !!}
                            <button id="buy_btn" type="submit">
                                {{trans('new_app.Додати в кошик')}}
                            </button>
                            <div id="count_box">
                                <span id="count_down">-</span>
                                <input id="count" name="count" type="number" value="1" min="1" step="1">
                                <span id="count_up">+</span>
                            </div>
                        </form>
                    </noindex>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="1a">
                <ul id="attr">
                    @if($item->sku)
                        <li id="sku">{{l('Артикул')}}: <span>{{$item->sku}}</span></li>
                    @endif

                    @foreach($item->filters as $filter)
                        @if($filter->attr->parent_id==8)
                            <li>   {{$filter->attr->title}}: <span>{{$filter->value}}</span></li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="tab-pane" id="2a">
                <div class="pane">
                    {!! $item->content !!}
                </div>
            </div>
            <div class="tab-pane" id="3a">
                @include('comments')
            </div>
        </div>
    </div>

    <section id="more_products">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <img id="more_products_u" src="/public/img/more_proructs.png" alt="" class="d-none d-md-block d-lg-block d-xl-block">

                    <h3>{{trans('new_app.Схожі пропозиції')}}</h3>

                    <section id="catalog" class="row">
                        @foreach($similarItems as $item)
                            @include('commerce.items_more')
                        @endforeach
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('head_styles')
    <link rel="stylesheet" href="/vendor/lightgallery/css/lightgallery.min.css">
    <link rel="stylesheet" href="/vendor/lightgallery/css/lg-transitions.min.css">
    <link rel="stylesheet" href="/vendor/slick/slick-theme.css">
    <link rel="stylesheet" href="/vendor/slick/slick.css">
@endsection
@section('footer_scripts')
    <script src="/vendor/lightgallery/js/lightgallery-all.min.js"></script>
    <script src="/vendor/slick/slick.min.js"></script>
    <script>
        (function ($) {
            $(document).ready(function () {

                $('#gallery').lightGallery({
                    pager: true,
                    selector: '.gallery',
                    download: false
                });

                $('#thumbs').slick({
                    dots: false,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    variableWidth: true,
                    speed: 500,
                });

                $('#addtocart2').change(function () {
                    $(this).submit();
                });

                var $count = $("#count");
                var $buy_btn = $("#buy_btn");
                $("#count_down").on("click", function () {
                    var count = (parseInt($count.val(), 10) > 1) ? parseInt($count.val(), 10) - 1 : 1;
                    $count.val(count);
                    $count.trigger("input");
                    console.log($count.val());
                });
                $("#count_up").on("click", function () {
                    var count = parseInt($count.val(), 10) + 1;
                    $count.val(count);
                    $count.trigger("input");
                    console.log($count.val());
                });
                $count.on("input", function () {
                    $buy_btn.data("count", $(this).val());
                });
                $buy_btn.off("click");
                $buy_btn.on("click", function (e) {
                    // e.preventDefault;
                    // console.log($(this).data());
                    // $btn = $(this);
                    // console.log($(this).data());
                    // $.ajax({
                    //     url: "/ajax/cart/add",
                    //     type: "POST",
                    //     data: $btn.data(),
                    //     success: function (data, textStatus, jqXHR) {
                    //         if (data.success !== undefined) {
                    //             if ($btn.data("url") !== undefined) {
                    //                 window.location = $btn.data("url");
                    //                 return;
                    //             }
                    //             $("#cart_popup").trigger("reload_cart");
                    //             swal(data.success);
                    //         } else {
                    //             if (data.error !== undefined) {
                    //                 swal(data.error);
                    //             } else {
                    //                 swal("Add Product: Error#1");
                    //             }
                    //         }
                    //     },
                    //     error: function () {
                    //         swal("Add Product: Error#2");
                    //     }
                    // });
                    // return false;
                });
            });
        })(jQuery);

    </script>
@endsection