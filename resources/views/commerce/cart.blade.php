@extends('layouts.app')
@section('title',  isset($title) ? $title : l('Корзина'))
@section('description', isset($description) ? $description : '')
@section('content')


    <div id="container">
        <div id="cart">

            @if(!$items)
                <h1 class="text-center">{{l('Кошик пустий')}}</h1>
                <div class="text-center">{{l('Бажаємо приємних покупок!')}}</div>
            @else
                <div id="checkout-step" class="container"></div>
                {!! Form::open(array('route' => 'order.complete','id'=>'checkout-form', 'onsubmit'=>"ga('send', 'event', 'zamoviti','buket'); yaCounter44630368.reachGoal('zamoviti-buket');", 'style'=>'padding-top:30px;')) !!}
                {{csrf_field()}}

                <div id="step2" class="step">
                    <div class="container cart-bg">
                        <h2 class="text-center cart-header">{{l('Оформлення замовлення')}}</h2>

                        <div class="col-sm-5 col-sm-offset-2 cart-left-side">
                            <div class="userform form1">
                                <label for="datetimepicker4" style="font-family:Trebuchet MS;color:#5E5971;">{{l('Дата доставки')}}</label>
                                <i class="fa fa-bars" aria-hidden="true"></i>
                                {{Form::text("order[delivery_date]",'',['required'=>true, 'class'=>'form-control', 'id' => 'datetimepicker4'])}}
                                <p class="userform-subheader" style="font-family:Trebuchet MS;color:#5E5971;">{{l('Замовник')}}</p>
                                <div class="form-group">
                                    {{Form::text("order[name]",Auth::user()['name'],['required'=>true, 'class'=>'form-control', 'placeholder' => 'П.І.Б'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::text("order[phone]",Auth::user()['phone'],['required'=>true, 'class'=>'form-control phone-mask', 'placeholder' => 'Телефон'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::text("order[email]",Auth::user()['email'],['required'=>true, 'class'=>'form-control', 'placeholder' => 'E-mail'])}}
                                </div>
                                <div class="form-group col-sm-10 second radio-group" id="cart-radio-grp" style="padding: 0;padding-top: 15px;">

                                    <div class="control-label">{{l('Оплата')}}</div>
                                    <br>

                                    <div>
                                        {{ Form::radio('order[payment_method]', 'Liqpay','',['id'=>'m5']) }}
                                        {{ Form::label('m5', l('Онлайн-платіж (Privat24, LiqPay, Visa, MasterCard)')) }}
                                    </div><br>

                                    <div>
                                        {{ Form::radio('order[payment_method]', 'Готівка','',['id'=>'m1'])}}
                                        {{ Form::label('m1', l('Оплата готівкою при отриманні'))}}
                                    </div><br>

                                    <div>
                                        {{ Form::radio('order[payment_method]', 'Privat24','',['id'=>'m2']) }}
                                        {{ Form::label('m2', l('Переказ на картку ПриватБанку')) }}
                                    </div><br>

                                    <div>
                                        {{ Form::radio('order[payment_method]', 'MoneyGram','',['id'=>'m3']) }}
                                        {{ Form::label('m3', l('Переказ через MoneyGram')) }}
                                    </div><br>

                                    <div>
                                        {{ Form::radio('order[payment_method]', 'WesternUnion','',['id'=>'m4']) }}
                                        {{ Form::label('m4', l('Переказ через Western Union')) }}
                                    </div><br>

                                    <div>
                                        {{ Form::radio('order[payment_method]', 'PayPal','',['id'=>'m6']) }}
                                        {{Form::label('m6', l('Онлайн-платіж PayPal'))}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-5 cart-right-side">

                            <div class="userform form2">

                                <input type="hidden" name="delivery_date"> <input type="hidden" name="delivery-time">

                                <div id="date-time" class="form-group" style="padding-top: 25px;">
                                    <div class="clockpicker">
                                        <label for="time">{{l('Час доставки')}}</label>
                                        <input type="text" name="order[time]" required class="form-control" value="Від">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="clockpicker" style="float: right!important;">
                                        <input type="text" required name="order[delivery_time]" class="form-control" value="До">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    </div>
                                </div>

                                <p class="userform-subheader" style="font-family:Trebuchet MS;color:#5E5971; padding-top: 34px;">{{l('Отримувач')}}</p>
                                <div class="form-group delivery">
                                    {{--<div>--}}
                                        {{--<p>Подарункова доставка</p>--}}
                                        {{--<input type="radio" name="order[text]" value="Подарункова доставка" class="form-control"><br/>--}}
                                    {{--</div>--}}
                                    <div>
                                        <p>{{l('Я отримувач')}}</p> <input type="checkbox" name="order[text]" id="receiver" value="Я отримувач" class="form-control">
                                    </div>
                                </div>


                                <div class="form-group" style="margin-top: 26px;">
                                    {{Form::text("order[delivery_name]",'',['required'=>true, 'class'=>'form-control', 'id' => 'PIB', 'placeholder'=>"П.І.Б"])}}
                                </div>

                                <div class="form-group">
                                    {{Form::text("order[delivery_phone]",'',['required'=>true, 'class'=>'form-control phone-mask', 'placeholder'=>"Телефон"])}}
                                </div>

                                <div class="form-group">
                                    {{Form::textarea("order[address]",Auth::user()['address'],['placeholder'=>'Адреса','required'=>true, 'class'=>'form-control'])}}
                                </div>

                                <div style="text-align: center;">
                                    <button class="btn" type="submit" id="confirm-btn">{{l('Підтвердити замовлення')}}</button>
                                </div>
                            </div>
                        </div>
                        {{--@endif--}}
                    </div>
                </div>
                {{Form::close()}}
            @endif
        </div>
    </div>

@endsection
@push('scripts')
<script>
	$(function () {

		form = $('#cart-form');
		FallCost = $('#allCost', form);
		FallCount = $('#count', form);
		totalSpan = $("#cart-total", form);
		discount = $("#discount", form);

		//deleteItem(form);

		$('.spinner', form).unbind('click').click(function () {

			input = $(this).parent().find('input');
			val = input.val();

			if ($(this).hasClass('minus')) {
				val--;
			} else {
				val++;
			}

			if (val < 1) val = 1;

			input.val(val).change();

		});

		form.change(function () {
			if (form.length == 0) return false;

			$.ajax({
				dataType: 'json',
				type: 'post',
				url: form.attr('action'),
				data: form.serialize(),
				success: function (result) {
					$('.cart-data').html(result.count + ' покупок - ' + result.total + ' грн');
					totalSpan.html(result.total);
					FallCount.html(result.count);
					//cartCount(result.count);
					if (result.discount_text) {
						discount.html(result.discount_text);
					} else {
						discount.html('');
					}
				},
				error: function (result) {
					alert(result.responseText);
				}
			});

		});

        $('#receiver').click(function () {
            if($(this).is(':checked')){
	            $('input[name="order[delivery_name]"]').val($('input[name="order[name]"]').val());

	            $('input[name="order[delivery_phone]"]').val($('input[name="order[phone]"]').val())
            }
        })

	})
</script>
@endpush