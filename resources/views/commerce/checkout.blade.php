{!! Form::open(array('route' => 'order.complete','id'=>'checkout-form')) !!}

<div class="form-group">
    {{Form::label("order[name]",'Имя Фамилия', ['class'=>'control-label'])}}
    {{Form::text("order[name]",Auth::user()['name'],['required'=>true, 'class'=>'form-control'])}}
</div>
<div class="form-group col-sm-6 first">
    {{Form::label("order[phone]",'Телефон', ['class'=>'control-label'])}}
    {{Form::text("order[phone]",Auth::user()['phone'],['required'=>true, 'class'=>'form-control'])}}
</div>
<div class="form-group col-sm-6 second">
    {{Form::label("order[email]",'Email', ['class'=>'control-label'])}}
    {{Form::email("order[email]",Auth::user()['email'],['required'=>true, 'class'=>'form-control'])}}
</div>

<div class="form-group col-sm-6 first radio-group">
    @if($delivery = config('delivery.delivery'))

        {{Form::label("order[delivery]",'Доставка', ['class'=>'control-label'])}}
        {{Form::select("order[delivery]",[''=>'Выберите способ доставки']+ array_combine($delivery,$delivery),'',['class'=>'form-control'])}}

    @endif
</div>

<div class="form-group col-sm-6 second radio-group">
    {{Form::label("order[payment_method]",'Способ оплаты', ['class'=>'control-label'])}}
    {{Form::select("order[payment_method]",['Cash'=>'Наличные','Bank'=>'Банковский платеж','LiqPay'=>'Online-оплата LiqPay / Privat24 / Visa / MasterCard (+ комиссия)'],'',['class'=>'form-control'])}}

    <p class="help-block">
        Подробнее можно узнать в разделе <a href="{{route('page','oplata-i-dostavka')}}" target="_blank">«Доставка и
            оплата»</a>.
    </p>
</div>
<div class="form-group">
    {{Form::label("order[address]",'Адрес доставки', ['class'=>'control-label'])}}
    {{Form::textarea("order[address]",Auth::user()['address'],['placeholder'=>'Город, адрес или номер отделения. При оформлении подписки указывайте полный почтовый адрес: индекс, улица, номер (дома, квартиры)город, поселок, село','required'=>true, 'class'=>'form-control'])}}
</div>

<div class="form-group">
    {{Form::label("order[text]",'Комментарий к заказу', ['class'=>'control-label'])}}
    {{Form::textarea("order[text]",old('note'),['placeholder'=>'Комментарий к заказу','class'=>'form-control'])}}
</div>

@if(!Auth::check())
    <p>
        На Ваш E-mail будет отправлено письмо с логином и паролем для авторизации на сайте
    </p>
@endif

<div class="form-group">
    <button type="submit" class="btn btn-primary">
        Подтверждаю покупку
    </button>
</div>
{{Form::close()}}