@extends('layouts.app')
@section('title',  $page->meta_title)
@section('description', $page->meta_description)
@section('content')
    <div id="container">
        <div class="container">
            <div class="h3 our-works-header text-center">{{l('Наші роботи')}}</div>
            <div class="col-lg-12 text-center" id="our-works-block">
                @foreach($page->images as $image)
                        <img src="{{$image}}" alt="" style="width: 310px;height: 310px;">
                @endforeach
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="{{asset('/js/jquery.shapeshift.min.js')}}"></script>
{{--<script>--}}
    {{--$(document).ready(function () {--}}
        {{--console.log('123');--}}
        {{--$('#our-works-block').shapeshift();--}}
    {{--});--}}
{{--</script>--}}
@endpush