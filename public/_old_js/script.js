(function ($) {

    $.fn.submitter = function (options) {

        var form = $(this);

        var defaults = {
            form: form,
            send: function () {

                var m_method = this.form.attr('method');
                var m_action = this.form.attr('action');
                var m_data = this.form.serialize();

                $.ajax({
                    type: m_method,
                    url: m_action,
                    data: m_data,
                    success: function (response) {
                        if (options.onSuccess) {
                            options.onSuccess(response);
                        }
                    },
                    error: function (response) {
                        options.onError(response)
                    }
                });
            },
            onSubmit: function () {
                this.send();
            },
            onSuccess: function (response) {
            },
            onError: function (response) {
                alert(response.responseText);
            }
        };

        var options = $.extend({}, defaults, options);

        options.form.submit(function (e) {
            options.onSubmit();
            e.preventDefault();
        });

        return this;

    };

}(jQuery));

$(function(){

    $(document).bind('cbox_complete', function(){
        $.colorbox.resize();
        $('.date-input').datepicker({
           minDate: dateToday
        });
        $('.phone-input').mask("+3(8099)999-99-99");
        $('.callbackform').submitter({
            onError: function (response) {
                formError($('.callbackform'),response.responseJSON);
            },
            onSuccess: function (response) {
                swal({
                    title: "Успішно!",
                    text: response.message,
                    type: "success",
                    html: true
                });
                $.colorbox.close();
            }
        });
    });


    function labelanimate() {
        var inpts = $(':input');
        inpts.each(function () {

            if ($(this).val()) {
                $(this).addClass('filled');
            }

        }).change(function () {

            if (!$(this).val()) {
                $(this).removeClass('filled');
            } else {
                $(this).addClass('filled');
            }
        })
    }
    labelanimate();
    $(".download-i").colorbox({href:'upload-form', title:'Букет по фото'});
    $("#callback-btn").colorbox({href:'callback', title:"Зворотній зв’язок"});
    $(".callback-btn2").colorbox({href:'callback', title:"Зворотній зв’язок"});
    $('#callback-btn-certificate').colorbox({href:'certificate-callback/1', title:"Подарунковий сертифікат", width:500, height:350});
    $('#callback-btn-certificate2').colorbox({href:'certificate-callback/2', title:"Подарунковий сертифікат", width:500, height:350});
    $('#callback-btn-certificate3').colorbox({href:'certificate-callback/3', title:"Подарунковий сертифікат", width:500, height:350});
    $('#callback-btn-master-show,.callback-btn-master-show1').colorbox({href:'callback-master-show'});
    $('#adress-time').clockpicker();
    $('.phone-mask').mask("+3(8099)999-99-99");
    $('.clockpicker').clockpicker();
    var dateToday = new Date();
    $('#datetimepicker4, .datepicker').datepicker({
        minDate: dateToday
    });

    $('#header-phone-block, .footer-phone-block').click(function () {
        $('#callback-btn2').click();
    });


    $('#datetimepicker-2').datetimepicker({
        inline: false
    });
    $('.portfolio').colorbox({rel:'.gallery', title:"Наше портфоліо", height:"95%" });
    $('.top-slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000
    });
    $('.wedding_slider').slick({
        infinite: true,
        slidesToShow: 5,
        arrows: true,
        centerMode: true,
        // centerPadding:'150px',
	    // variableWidth: true,
        responsive: [
            {
                brekpoint:1119,
                settings:{
                    slidesToShow:1
                }
            },
            {
                breakpoint: 569,
                settings:{
                    slidesToShow:3
                }
            },
            {
                breakpoint: 423,
                settings:{
                    slidesToShow:3
                }
            }
        ]
    });
    $('.wedding_slide').colorbox({
        rel:'.wedding-slider',
        width: "auto",
        maxHeight:'95%',
    });

});

$(function () {

    $('#content-1').show();
    $('#content-2').hide();
    $('#content-3').hide();

    $('#img-content-1').show();
    $('#img-content-2').hide();
    $('#img-content-3').hide();


    $(function () {
        $('#o-btn-1').click(function () {
            $('#content-1').show(500);
            $('#content-2').hide();
            $('#content-3').hide();
        })

    });

    $(function () {
        $('#o-btn-2').click(function () {
            $('#content-1').hide();
            $('#content-2').show(500);
            $('#content-3').hide();
        })
    });

    $(function () {
        $('#o-btn-3').click(function () {
            $('#content-1').hide();
            $('#content-2').hide();
            $('#content-3').show(500);
        })
    });

    $(function () {
        $('#img-btn-1').click(function () {
            $('#img-content-1').show();
            $('#img-content-2').hide();
            $('#img-content-3').hide();
        })
    });

    $(function () {
        $('#img-btn-2').click(function () {
            $('#img-content-1').hide();
            $('#img-content-2').show();
            $('#img-content-3').hide();
        })
    });

    $(function () {
        $('#img-btn-3').click(function () {
            $('#img-content-1').hide();
            $('#img-content-2').hide();
            $('#img-content-3').show();
        })
    });
});

function formError(form, data) {
	for(key in data){
		form.find('input[name='+key+']').after($('<div>',{class:'input-error',text:data[key]}));
	}
}


var path = window.location.href;
$('.main-menu-holder a[href="'+path+'"]').addClass('active');
