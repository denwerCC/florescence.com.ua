/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {

	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	//config.extraPlugins = 'youtube,codemirror,stat';
	config.skin = 'moono';
	config.extraPlugins = 'youtube,elementspath,codemirror,resize';
	config.removePlugins = 'basicstyles,dialogui,dialog,clipboard,button,toolbar,entities,floatingspace,wysiwygarea,indent,indentlist,fakeobjects,link,list,undo,table,panel,floatpanel,menu,contextmenu,tabletools,tableresize,stylesheetparser,specialchar,save,removeformat,pastefromword,pastetext,justify,smiley,imagepaste,imagebrowser,image,listblock,richcombo,font,flash,find,popup,filebrowser,panelbutton,colorbutton,colordialog,divarea,format,horizontalrule,liststyle,magicline,maximize,lineutils,widget,oembed,showblocks,showborders,wordcount,div,image2,indentblock,menubutton,sourcedialog,youtube,internpage';

	config.removePlugins = 'ajaxsave,flash,sourcedialog,imagepaste,imagebrowser,image2,divarea,oembed,modules,modgall';
	config.allowedContent = false;
	config.htmlEncodeOutput = false;
	config.height = '450px';
	config.codemirror = {showSearchButton: false,showFormatButton: false,showCommentButton: false,showUncommentButton: false,showAutoCompleteButton: false};
	// The toolbar groups arrangement, optimized for a single toolbar row.

	config.toolbarGroups = [
		{ name: 'document',	   groups: ['document','mode',  'doctools' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },{ name: 'links' },
		{ name: 'forms' },{ name: 'insert' },{ name: 'colors' },{ name: 'tools' },{ name: 'modules'},
		{ name: 'styles' },
		{ name: 'paragraph',   groups: [ 'align','basicstyles','list', 'indent', 'blocks',  'bidi' ] },
		{ name: 'basicstyles', groups: [  'cleanup' ] },
		{ name: 'others' },
	];

	// The default plugins included in the basic setup define some buttons that
	// we don't want too have in a basic editor. We remove them here.
	config.removeButtons = 'Cut,Copy,Paste,Anchor';
	// Let's have it basic on dialogs as well.
	//config.removeDialogTabs = 'link:advanced';
    config.contentsCss = '/css/fonts.css';
    config.font_names = 'RobotoCondensed-Bold;' + 'RobotoCondensed-Light;' + 'RobotoCondensed-Regular;' + 'PlayfairDisplay-Regular;' + 'PlayfairDisplay-Bold;' + 'PlayfairDisplay-Black;' + 'Lato-Black;' + 'Lato-Bold;' + 'Lato-Heavy;' + 'Lato-HeavyItalic;' + 'Lato-Light;' + 'Lato-Medium;' + 'Lato-Regular/Lato-Regular;' + 'Lato-Semibold;' + 'Lato-Thin;' +  + config.font_names;
};
