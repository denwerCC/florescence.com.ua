CKEDITOR.dialog.add('modgall', function(editor) {

var targetChanged = function(){

var dialog = this.getDialog(),
targetName = dialog.getContentElement( 'cut', 'cuttext' ),
value = this.getValue();

	switch (value)
 		{
			case 'gallery' :
				targetName.getElement().hide();
				break;
			case 'scroll_gallery' :
				break;
 		}
	
};

	
  return {
    title : 'Вставити галерею',
    minWidth : 400,
    minHeight : 200,
	onShow: function(){
		$("#cuttext3").hide();
	},
    onOk: function() {
		var cuttext = this.getContentElement( 'cut', 'modulename').getInputElement().getValue();
		cuttext +=","+ this.getContentElement( 'cut', 'cuttext3').getInputElement().getValue();
		this._.editor.insertHtml('{module:' + cuttext + '}');
    },
    contents : [
	{
      id : 'cut',
      label : 'Галерея',  // тайтл кнопки
      title : 'Галерея',  // 
      elements : [{
        id : 'modulename',
        type : 'select',
        label : 'Выбрати галерею',
		onChange : targetChanged,
		items:Galleries
      },{
        id : 'cuttext3',
        type : 'select',
        label : 'Галерея',
		items:Gallery,
      }]	  
    }]
  };
});