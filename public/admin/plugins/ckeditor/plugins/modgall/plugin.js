CKEDITOR.plugins.add('modgall', {
  init : function(editor) {
    var command = editor.addCommand('modgall', new CKEDITOR.dialogCommand('modgall'));
    command.modes = {wysiwyg:1, source:1};
    command.canUndo = true;

    editor.ui.addButton('Modgall', {
      label : 'Вставить галерею',
      command : 'modgall',
      toolbar: 'modules',
      icon: this.path + 'modgall.png'//Путь к иконке
    });

    CKEDITOR.dialog.add('modgall', this.path + 'dialogs/modgall.js');
  }
});
// подключаем файл, который хранит массив для вывода в диалоге плагина
CKEDITOR.scriptLoader.load(CKEDITOR.plugins.getPath('modgall')+'modgall.php');
