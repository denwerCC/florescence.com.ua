CKEDITOR.plugins.add('modules', {
  init : function(editor) {
    var command = editor.addCommand('modules', new CKEDITOR.dialogCommand('modules'));
    command.modes = {wysiwyg:1, source:1};
    command.canUndo = true;

    editor.ui.addButton('Modules', {
      label : 'Вставить модуль',
      command : 'modules',
      icon: this.path + 'modules.png'//Путь к иконке
    });

    CKEDITOR.dialog.add('modules', this.path + 'dialogs/modules.js');
  }
});
// подключаем файл, который хранит массив для вывода в диалоге плагина
CKEDITOR.scriptLoader.load(CKEDITOR.plugins.getPath('modules')+'modules.php');