CKEDITOR.dialog.add('modules', function(editor) {
	
var targetChanged = function(){
var dialog = this.getDialog(),
targetName = dialog.getContentElement( 'cut', 'cuttext' ),
value = this.getValue();

console.log(dialog)

//	if (!targetName) return;

//	targetName.setValue('');

	switch (value)
 		{
			case 'like' :
				targetName.setLabel("Необовязковий параметр");
				break;
			case 'comments' :
				targetName.setLabel("Назва кнопки");
				break;
			case 'contacts' :
				targetName.setLabel("Необовязковий параметр");
				break;
			case 'news' :
				targetName.setLabel("Назва категорії");
				break;
			case 'print' :
				targetName.setLabel("Необовязковий параметр");
				break;
 		}
	
	
};

	
  return {
    title : 'Вставить модуль',
    minWidth : 400,
    minHeight : 200,
    onOk: function() {
		var cuttext = this.getContentElement( 'cut', 'modulename').getInputElement().getValue();
		cuttext +=","+ this.getContentElement( 'cut', 'cuttext').getInputElement().getValue();
		this._.editor.insertHtml('{module:' + cuttext + '}');
    },
    contents : [
	{
      id : 'cut',
      label : 'Модулі',  // тайтл кнопки
      title : 'Модулі',  // 
      elements : [
	  	{
		type : 'html',
		html : 'Виберіть модуль, який бажаєте підключити.'
		},
	  {
        id : 'modulename',
        type : 'select',
        label : 'Выбрать модуль',
		onChange : targetChanged,
		items:ModulesSelectBox,
      },{
        id : 'cuttext',
        type : 'text',
        label : 'Дополнительный параметр'
      }]	  
    }]
  };
});