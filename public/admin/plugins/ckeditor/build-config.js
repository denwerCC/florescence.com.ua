/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) http://ckeditor.com/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) http://ckeditor.com/builder/81f9798f89ca9106b03d51b3f00b1a33
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) http://ckeditor.com/builder/download/81f9798f89ca9106b03d51b3f00b1a33
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'moonocolor',
	preset: 'basic',
	ignore: [
		'dev',
		'.gitignore',
		'.gitattributes',
		'README.md',
		'.mailmap'
	],
	plugins : {
		'basicstyles' : 1,
		'clipboard' : 1,
		'colorbutton' : 1,
		'colordialog' : 1,
		'div' : 1,
		'divarea' : 1,
		'entities' : 1,
		'filebrowser' : 1,
		'find' : 1,
		'flash' : 1,
		'floatingspace' : 1,
		'font' : 1,
		'format' : 1,
		'horizontalrule' : 1,
		'image' : 1,
		'image2' : 1,
		'imagebrowser' : 1,
		'imagepaste' : 1,
		'indentblock' : 1,
		'indentlist' : 1,
		'justify' : 1,
		'link' : 1,
		'list' : 1,
		'liststyle' : 1,
		'magicline' : 1,
		'maximize' : 1,
		'menubutton' : 1,
		'oembed' : 1,
		'pastefromword' : 1,
		'pastetext' : 1,
		'removeformat' : 1,
		'save' : 1,
		'showblocks' : 1,
		'showborders' : 1,
		'smiley' : 1,
		'sourcedialog' : 1,
		'specialchar' : 1,
		'stylesheetparser' : 1,
		'tableresize' : 1,
		'toolbar' : 1,
		'undo' : 1,
		'wordcount' : 1,
		'wysiwygarea' : 1
	},
	languages : {
		'en' : 1,
		'ru' : 1,
		'uk' : 1
	}
};