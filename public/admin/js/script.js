toastr.options = {
    "closeButton": true
}

$(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', ".confirm", function () {
        if (!confirm("Вы уверены?")) {
            return false;
        }
    });

    $(document).on('click', ".btn-ajax", function () {

        $.get(this.href,function(data){
            toastr.success(data);
        });
        return false;

    });

});

$.fn.submitter = function (callback,error) { // callback(result,form)

    form = $(this);

    form.submit(function (e) {
        e.preventDefault();

        var m_method = form.attr('method');
        //получаем адрес скрипта на сервере, куда нужно отправить форму
        var m_action = form.attr('action');
        //получаем данные, введенные пользователем в формате input1=value1&input2=value2...,
        //то есть в стандартном формате передачи данных формы
        var m_data = form.serialize();

        $.ajax({
            type: m_method,
            url: m_action,
            data: m_data,
            success: function (result) {
                if (callback) {
                    callback(result, form);
                }
            },
            error: function (result) {
                if (error) {
                    error(result, form);
                } else {
                    alert(result.responseText);
                }

            }
        });
    });

    return this;
};