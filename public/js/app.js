(function ($) {
    $(document).ready(function () {
        console.log('app');
        //site_menu add dropdown-menu
        $('#site_menu li > ul').each(function (index, value) {
            $sub_menu = $(this);
            var id = 'sub_menu'+index;
            $sub_menu.attr('aria-labelledby',id).addClass('dropdown-menu');
            $sub_menu.prev('a').attr('href', '#').attr({
                role: 'button',
                id: id,
                class: 'dropdown-toggle',
                'data-toggle': 'dropdown',
            }).append('<i class="fas fa-angle-down"></i>');
            $sub_menu.removeClass('sub_menu');
        });
        //main_page_slider height
        if($('#home_slider').length){
            var $home_slider_items = $('#home_slider .carousel-item');
            // $home_slider_items.height($(window).height() - $('header').height() - $('footer').height());
            $home_slider_items.height($(window).height()
                //- $('footer').height()
                + 10);
            $(window).resize(function () {
                // $home_slider_items.height($(window).height() - $('header').height() - $('footer').height());
                // $home_slider_items.height($(window).height()  - $('footer').height());
                $home_slider_items.height($(window).height()
                    //- $('footer').height()
                    + 10);
            });
        }
        if($('#video_bg').length){
            var $video_bg = $('#video_bg');
            // $home_slider_items.height($(window).height() - $('header').height() - $('footer').height());
            $video_bg.height($(window).height()
                //- $('footer').height()
                + 10);
            $(window).resize(function () {
                $video_bg.height($(window).height()
                    //- $('footer').height()
                    + 10);
            });
        }

        if($('.home').length){
            var $scroll_line = $('#scroll_line');
            $(window).on('scroll', function () {
                // console.log($scroll_line.offset().top);
                if($scroll_line.offset().top > 18){
                    $('header').addClass('scroll');
                    $('footer').addClass('scroll');
                }else{
                    $('header').removeClass('scroll');
                    $('footer').removeClass('scroll');
                }
            });
        }

        //Скролл к элементам
        $('a[href^="#"]').on('click', function(event) {
            var target = $(this.getAttribute('href'));
            if( target.length ) {
                event.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        });
        if($('#video_bg video').length){
            var $video = $('#video_bg video');
            // $video.prop('muted', true);
            $('#video_p').on('click', function () {
                var $btn = $(this);
                var video = $video.get(0);
                if(video.paused){
                    video.play();
                }else{
                    video.pause();
                }
                $btn.toggleClass('fa-play fa-pause');
                // video.paused ? video.play() : video.pause();
            });
            $('#video_s').on('click', function () {
                var $btn = $(this);
                if( $video.prop('muted') ) {
                    $video.prop('muted', false);
                } else {
                    $video.prop('muted', true);
                }
                $btn.toggleClass('fa-volume-up fa-volume-off');
            });
        }

        //Catalog
        $sort = $('#sort');
        if($sort.length){
            if(window.location.search.length){
                $('#sort_items').text($sort.find('ul a[href$="'+window.location.search+'"]').text() || '---');
            }
        }

        //Home uploadPhoto
        if($('#upload_photo_form').length){
            $('#falseinput').click(function () {
                $('.upload-button').click();
            });
            $('.upload-button').change(function() {
                $('#label-file').text($('.upload-button')[0].files[0].name);
            });
        }


        //Fix breadcrumb
        $('#breadcrumb div:last').replaceWith($('#breadcrumb div:last a'))

        var $home_content = $('.content.page-content[data-read_more]');
        if($home_content.length){
            var $home_p = $('.content.page-content[data-read_more] p:first');
            $home_content.after($('<span class="read_more_content">'+$home_content.data('more_btn_text')+'>>></span>'));
            $home_content.height($home_p.height()-5);
            $('.read_more_content').on('click', function () {
                $home_content.toggleClass('open');
                $(this).hide()
            })
        }
        $("#video_p").click();
    });
})(jQuery);