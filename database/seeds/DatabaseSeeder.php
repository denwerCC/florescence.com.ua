<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// $this->call(ItemSeeder::class);
        // $this->call(UsersTableSeeder::class);
	    DB::table('users')->insert([
		    'name'=>'Admin',
		    'email'=>'admin',
		    'password'=>bcrypt('admin'),
	    ]);
    }
}
