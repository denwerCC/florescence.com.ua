<?php

use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = include base_path(). '/database/data/items.php';
        foreach ($items as $item){
			\App\Models\Item::create($item);
        }
    }
}
