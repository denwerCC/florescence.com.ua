<?php
/**
 * Created by PhpStorm.
 * User: andry
 * Date: 06.03.2017
 * Time: 14:49
 */


return[
	[
		'title'=> 'Білий водоспад',
		'price' => '500',
		'img' => 'biliy-vodospad-44.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Щаслива коробка',
		'price' => '500',
		'img' => 'happy-box.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Престиж',
		'price' => '500',
		'img' => 'prestij-147.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Флоренція',
		'price' => '500',
		'img' => 'florentsiia.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Ніжні почуття',
		'price' => '500',
		'img' => 'nijni-pochuttia-177.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Серце з троянд',
		'price' => '500',
		'img' => 'sertse-z-troiand-175.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Чарівна прикраса',
		'price' => '500',
		'img' => '/common-flowers/charivna-prikrasa-179.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Солодкий момент',
		'price' => '500',
		'img' => '/common-flowers/solodkiy-moment-102.jpg',
		'category_id' => 2,
	],
	[
		'title' => 'Милозвучна ніжність',
		'price' => '500',
		'img' => '/common-flowers/milozvuchna-nijnist-29.jpg',
		'category_id' => 2,
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/1.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/2.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/3.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/4.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/5.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/6.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/7.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/8.jpg',
		'category_id' => 1
	],
	[
		'title' => '',
		'price' => '500',
		'img' => '/wedding-flowers/9.jpg',
		'category_id' => 1
	],
	[
		'title' => "М'яка іграшка висота 40см",
		'price' => '500',
		'img' => '/presents/m039iaka-igrashka-visota-40-sm-258.jpg',
		'category_id' => '3'
	],
	[
		'title' => "М'яка іграшка висота 50см",
		'price' => '500',
		'img' => '/presents/m039iaka-igrashka-visota-50-sm-257.jpg',
		'category_id' => '3'
	],
	[
		'title' => "М'яка іграшка висота 30см",
		'price' => '500',
		'img' => '/presents/m039iaka-igrashka-visota-30-sm-254.jpg',
		'category_id' => '3'
	],
	[
		'title' => "Кольорові кульки",
		'price' => '500',
		'img' => '/presents/kolorovi.jpg',
		'category_id' => '3'
	],
	[
		'title' => "Екзотичні фрукти",
		'price' => '500',
		'img' => '/presents/korobka-z-ekzotichnimi-fruktami-252.jpg',
		'category_id' => '3'
	],
	[
		'title' => "Екзотичні фрукти #2",
		'price' => '500',
		'img' => '/presents/velika-korobka-z-ekzotichnimi-fruktami-253.jpg',
		'category_id' => '3'
	],
	[
		'title' => "Кульки з гелієм",
		'price' => '500',
		'img' => '/presents/kulku.jpg',
		'category_id' => '3'
	],
	[
		'title' => "Шоколадка RitterSport",
		'price' => '500',
		'img' => '/presents/rittersport.jpg',
		'category_id' => '3'
	],
	[
		'title' => "Екзотика",
		'price' => '500',
		'img' => 'candy-boxes/frui1.jpg',
		'category_id' => '4'
	],
	[
		'title' => "Арабеск",
		'price' => '500',
		'img' => 'candy-boxes/fruit-arabesk.jpg',
		'category_id' => '4'
	],
	[
		'title' => "Коробка з кіндерами",
		'price' => '500',
		'img' => 'candy-boxes/kinder.jpg',
		'category_id' => '4'
	],
	[
		'title' => "Сюрприз",
		'price' => '500',
		'img' => 'candy-boxes/surprise.jpg',
		'category_id' => '4'
	],
	[
		'title' => "Чиста любов",
		'price' => '500',
		'img' => 'box-flowers/chista-lubov-74.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Щаслива коробка",
		'price' => '500',
		'img' => 'box-flowers/happy-box.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Коробка з квітами",
		'price' => '500',
		'img' => 'box-flowers/korobka-z-kvitami-2-171.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Кругла коробочка",
		'price' => '500',
		'img' => 'box-flowers/korobka-z-kvitami-170.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Рожева хмарка",
		'price' => '500',
		'img' => 'box-flowers/rojeva-khmarka-189.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Маленька коробочка",
		'price' => '500',
		'img' => 'box-flowers/malenka-korobochka-172.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Червоні троянди",
		'price' => '500',
		'img' => 'box-flowers/korobka-z-chervonimi-troiandami-184.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Троянди та орхідеї",
		'price' => '500',
		'img' => 'box-flowers/korobochka-z-troiandami-ta-orkhideiami-182.jpg',
		'category_id' => '5'
	],
	[
		'title' => "Романтика життя",
		'price' => '500',
		'img' => 'box-flowers/romantyka.jpg',
		'category_id' => '5'
	]
];