<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
	        $table->text('img')->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('level')->nullable();
	        $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');
        });

        Schema::create('categories_translations', function (Blueprint $table) {
            $table->string('locale');
            $table->integer('category_id')->unsigned();
            $table->string('slug', 255)->nullable();;
            $table->string('title')->nullable();;
            $table->string('description')->nullable();
            $table->timestamps();
            $table->primary(['category_id','locale']);
            //$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
        Schema::drop('categories_translations');
    }
}