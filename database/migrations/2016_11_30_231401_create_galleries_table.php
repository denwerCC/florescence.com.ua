<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('galleries', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id')->nullable();
			$table->text('img')->nullable();
			$table->integer('level')->nullable();
			$table->boolean('public')->default(1);
			$table->timestamps();
		});

		Schema::create('galleries_translations', function (Blueprint $table) {
			$table->string('locale');
			$table->integer('gallery_id')->unsigned();
			$table->string('title')->nullable();
			$table->string('description')->nullable();
			$table->string('link', 255)->nullable();
			$table->primary(['gallery_id', 'locale']);
		});

		Schema::create('galleries_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->text('img')->nullable();
			$table->integer('parent_id')->unsigned()->nullable();
			$table->integer('level')->nullable();
			$table->integer('img_width')->nullable();
			$table->integer('img_heigth')->nullable();
			$table->integer('thumb_width')->nullable();
			$table->integer('thumb_heigth')->nullable();
			$table->boolean('public')->default(1);
			$table->timestamps();
			$table->foreign('parent_id')->references('id')->on('galleries_categories')->onDelete('cascade');
		});

		Schema::create('galleries_categories_translations', function (Blueprint $table) {
			$table->string('locale');
			$table->integer('category_id')->unsigned();
			$table->string('title')->nullable();
			$table->string('description')->nullable();
			$table->string('slug', 255)->nullable();
			$table->primary(['category_id', 'locale']);
			//$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('galleries');
		Schema::dropIfExists('galleries_translations');
		Schema::dropIfExists('galleries_categories');
		Schema::dropIfExists('galleries_categories_translations');
	}
}