<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('user_id')->nullable();
	        $table->string('name');
	        $table->string('email')->nullable();
	        $table->string('phone')->nullable();
	        $table->ipAddress('ip');
	        $table->string('commentable_type');
	        $table->integer('commentable_id');
	        $table->text('text');
	        $table->boolean('public')->default(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
