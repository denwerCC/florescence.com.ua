<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb-users', function (Blueprint $table){
        	$table->increments('id');
        	$table->string('name');
        	$table->string('email')->unique();
        	$table->string('facebook_id')->unique();
        	$table->string('avatar');
        	$table->rememberToken();
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fb-users');
    }
}
