<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItems extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function (Blueprint $table) {
			$table->increments('id');
			$table->text('img')->nullable();
			$table->string('sku')->nullable();
			$table->decimal('price')->nullable();
			$table->integer('level')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('filter_id')->nullable();
			$table->dateTime('date')->nullable();
//			$table->boolean('stock')->default(1);
			$table->boolean('public')->default(1);
			$table->timestamps();
		});


		Schema::create('items_translations', function (Blueprint $table) {
			//$table->increments('id');
			$table->string('locale');
			$table->integer('item_id')->unsigned();
			$table->string('slug', 255)->nullable();
			$table->string('title')->nullable();
			$table->string('meta_title')->nullable();
			$table->string('description')->nullable();
			$table->string('meta_description')->nullable();
			$table->string('keywords')->nullable();
			$table->text('content')->nullable();
			//$table->timestamps();
			$table->primary(['item_id', 'locale']);
			//$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}
}