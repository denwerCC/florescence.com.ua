<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRubricsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rubrics', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('parent_id')->unsigned()->nullable();
			$table->integer('level')->nullable();
			$table->foreign('parent_id')->references('id')->on('rubrics')->onDelete('cascade');
		});

		Schema::create('rubrics_translations', function (Blueprint $table) {
			//$table->increments('id');
			$table->string('locale');
			$table->integer('rubric_id')->unsigned()->nullable();
			$table->string('slug', 255);
			$table->string('title')->nullable();
			$table->string('description')->nullable();
			$table->timestamps();
			$table->primary(['rubric_id','locale']);
			//$table->foreign('rubric_id')->references('id')->on('rubrics')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rubrics');
		Schema::drop('rubrics_translations');
	}
}
