<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrders extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('phone');
			$table->string('address');
			$table->integer('user_id');
			$table->string('delivery');
			$table->string('delivery-date');
			$table->string('delivery-time');
			$table->string('lang')->nullable();
			$table->text('text')->nullable();
			$table->text('note')->nullable();
			$table->decimal('amount');
			$table->string('payment_method')->nullable();
			$table->string('payment_status')->nullable();
			$table->string('tracking_number', 255)->nullable();
			$table->tinyInteger('status')->default(0);
			$table->ipAddress('ip')->nullable();
			$table->string('code',13);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}
}