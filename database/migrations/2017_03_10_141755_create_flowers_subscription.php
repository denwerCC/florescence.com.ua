<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlowersSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('f_subscribers', function (Blueprint $table){
        	$table->increments('id');
        	$table->string('name');
        	$table->integer('phone');
        	$table->string('email');
        	$table->string('address');
        	$table->string('abonement');
        	$table->string('time');
	        $table->string('size');
        	$table->text('additional_info');
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('f_subscribers');
    }
}
