<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

	    Schema::create('filters', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('parent_id')->unsigned()->nullable();
		    $table->integer('level')->nullable();
		    $table->timestamps();
		    $table->foreign('parent_id')->references('id')->on('filters')->onDelete('cascade');
	    });

	    Schema::create('filters_translations', function (Blueprint $table) {
		    //$table->increments('id');
		    $table->string('locale');
		    $table->integer('filter_id')->unsigned();
		    $table->string('slug', 255)->nullable();;
		    $table->string('title')->nullable();;
		    $table->string('description')->nullable();
		    //$table->timestamps();
		    $table->primary(['filter_id','locale']);
		    $table->foreign('filter_id')->references('id')->on('filters')->onDelete('cascade');
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('filters');
        Schema::drop('filters_translations');
    }
}
