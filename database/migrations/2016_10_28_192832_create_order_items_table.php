<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('order_id');
	        $table->integer('item_id')->unsigned();
	        $table->string('title');
	        $table->string('sku')->nullable();;
	        $table->decimal('price');
	        $table->text('parameters')->nullable();;
	        $table->integer('count');
            $table->timestamps();
	        $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_items');
    }
}
