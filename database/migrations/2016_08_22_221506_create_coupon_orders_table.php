<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('coupon_orders', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('coupon_id')->unsigned();
		    $table->integer('order_id')->unsigned();
		    $table->string('code');
		    $table->string('discount');
		    $table->timestamps();
	    });

	    Schema::table('coupon_orders', function ($table) {
		    $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
