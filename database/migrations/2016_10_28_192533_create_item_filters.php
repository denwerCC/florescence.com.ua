<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemFilters extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('item_filters', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('item_id')->unsigned();
			$table->integer('filter_id')->unsigned();
			$table->timestamps();
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
			$table->foreign('filter_id')->references('id')->on('filters')->onDelete('cascade');
		});

		Schema::create('item_filters_translations', function (Blueprint $table) {
			//$table->increments('id');
			$table->string('locale');
			$table->integer('item_filter_id')->unsigned();
			$table->text('value');
			$table->primary(['item_filter_id', 'locale']);
			//$table->foreign('item_filter_id')->references('id')->on('item_filters')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_filters');
		Schema::drop('item_filters_translations');
	}
}
