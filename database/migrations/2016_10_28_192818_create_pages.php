<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePages extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('img')->nullable();
			$table->integer('category_id');
			$table->integer('level')->nullable();
			$table->boolean('public');
			$table->dateTime('date');
			$table->timestamps();
		});

		Schema::create('pages_translations', function (Blueprint $table) {
			//$table->increments('id');
			$table->string('locale');
			$table->integer('page_id')->unsigned();
			$table->string('slug', 255)->nullable();
			$table->string('title')->nullable();
			$table->string('meta_title')->nullable();
			$table->string('description')->nullable();
			$table->string('meta_description')->nullable();
			$table->string('keywords')->nullable();
			$table->text('content')->nullable();
			//$table->timestamps();
			$table->primary(['page_id','locale']);
			//$table->foreign('page_id')->references('id')->on('pages');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
		Schema::drop('pages_translations');
	}
}

